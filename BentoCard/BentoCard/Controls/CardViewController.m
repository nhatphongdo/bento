//
//  CardViewViewController.m
//  BentoCard
//
//  Created by Do Nhat Phong on 12/10/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import "CardViewController.h"
#import "Constrain.h"
#import "ItemTopicMagView.h"
#import "ItemArticleMagView.h"
#import "ItemArticleListView.h"
#import "ArticleModel.h"
#import "ItemSubTopicMagViewController.h"
#import "SubTopicViewController.h"
#import "ArticleTopicViewController.h"
#import "UIImage+DeviceSpecificMedia.h"
#import "ServiceClient.h"
#import "ClientUtils.h"
#import "TempData.h"

@interface CardViewController ()
{
    int totalPage,totalItem,currentPage;
    int lastPosY;
    NSArray *jsonData;
    BOOL isLoading;
}

@end

static const int GAP = 10;
static const int itemPerPage = 2;
static const float duration = 0.5f;

@implementation CardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //init screen width
    [ClientUtils initializeWidthScreen:self.view];
    currentPage = 1;
    isLoading = FALSE;
    // init Swipe Gesture
    UISwipeGestureRecognizer *swipeleft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer *swiperight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];

    [self loadData:self.listItems];
    if(self.interest.Total <= 0 || self.interest.Total == nil)
    {
        self.countLabel.hidden = YES;
    }
    
}

- (void)loadData:(NSMutableArray *)arrayItems
{
    //TODO: LoadData
    //      Add subView from array object
    //      Add paging
    
    //Remove old View
    NSArray *oldSubViews = [self.containerView subviews];
    for (UIView *item in oldSubViews) {
        [item removeFromSuperview];
    }
    //remove old ViewController
    NSArray *oldChildViewController = [self childViewControllers];
    for(UIViewController *viewcontroller in oldChildViewController)
    {
        [viewcontroller willMoveToParentViewController:nil];
        [viewcontroller removeFromParentViewController];
    }
    
    [self.titleButton setTitle:self.interest.Name forState:UIControlStateNormal];
    self.countLabel.text = [NSString stringWithFormat:@"%d",self.interest.Total];
    self.listItems = arrayItems;

    totalItem = (int)self.interest.Total;
    totalPage = totalItem/itemPerPage;
    if (totalItem % itemPerPage > 0)
    {
        totalPage++;
    }
    
    int y = 0;
    int childheight = 35; //init Height of children view controller
    if ([self.Type isEqualToString:TOPIC_CARD_TYPE_KEY])
    {
        childheight = 35;
        //Topic Mag View Type
        for (InterestModel *item in self.listItems) {
            ItemTopicMagView *itemMagView = [[ItemTopicMagView alloc] initWithNibName:@"ItemTopicMagView" bundle:nil];
            itemMagView.topic = item.Name;
            itemMagView.topicID = item.ID;
            itemMagView.number = item.Total;
            CGRect frame = itemMagView.view.frame;
            frame.origin.x = 0;
            frame.origin.y = y;
            frame.size.width = self.containerView.frame.size.width;
            frame.size.height = childheight; //fix height of item Topic Mag
            itemMagView.view.frame = frame;
            [self.containerView addSubview:itemMagView.view];
            [self addChildViewController:itemMagView];
            [itemMagView didMoveToParentViewController:self];
            y += childheight + GAP;
        }
    }
    else if([self.Type isEqualToString:ARTICLE_CARD_TYPE_KEY])
    {
        if (self.isCollapsed == YES)
        {
            childheight = 25;
            // Topic Article list View Type
            for(ArticleModel *item in self.listItems)
            {
                ItemArticleListView *itemListView = [[ItemArticleListView alloc] initWithNibName:@"ItemArticleListView" bundle:nil];
                itemListView.article = item;
                CGRect frame = itemListView.view.frame;
                frame.origin.x = 0; // Fix Position X of item Article Mag
                frame.origin.y = y;
                frame.size.width = self.containerView.frame.size.width;
                frame.size.height = childheight; //fix height of item Topic Mag
                itemListView.view.frame = frame;
                [self.containerView addSubview:itemListView.view];
                [self addChildViewController:itemListView];
                [itemListView didMoveToParentViewController:self];
                y += frame.size.height + GAP;
            }
        }
        else if (self.isCollapsed == NO)
        {
            childheight = 92;
            //Topic Article Mag View Type
            for(ArticleModel *item in self.listItems)
            {
                ItemArticleMagView *itemMagView = [[ItemArticleMagView alloc] initWithNibName:@"ItemArticleMagView" bundle:nil];
                CGRect frame = itemMagView.view.frame;
                frame.origin.x = 0; // Fix Position X of item Article Mag
                frame.origin.y = y;
                frame.size.width = self.containerView.frame.size.width;
                frame.size.height = childheight; //fix height of item Topic Mag
                itemMagView.view.frame = frame;
                [self.containerView addSubview:itemMagView.view];
                [self addChildViewController:itemMagView];
                [itemMagView didMoveToParentViewController:self];
                [itemMagView setData:item];
                y += frame.size.height + GAP;
            }
        }
    }
    else if([self.Type isEqualToString:SUBTOPIC_CARD_TYPE_KEY])
    {
        //todo: load subtopic card
        //      create UI for ItemSubTopicMagViewController
        if(totalPage > 1) {
            UIImage *bg1 = [UIImage imageForDeviceWithName:@"bgGroupCardView.png"];
            [self.imgBg1 setImage:bg1];
        }
        childheight = 118;
        int itemremaining = ((currentPage*itemPerPage) > totalItem) ? totalItem : (currentPage*itemPerPage);
        for (int i = (currentPage - 1)*itemPerPage; i < itemremaining; i++) {
            ArticleModel *item = [self.listItems objectAtIndex:i];
            ItemSubTopicMagViewController *itemMagView = [[ItemSubTopicMagViewController alloc] initWithNibName:@"ItemSubTopicMagViewController" bundle:nil];
            itemMagView.article = item;
            CGRect frame = itemMagView.view.frame;
            frame.origin.x = 0;
            frame.origin.y = y;
            frame.size.width = self.containerView.frame.size.width;
            frame.size.height = childheight;
            itemMagView.view.frame = frame;
            [self.containerView addSubview:itemMagView.view];
            [self addChildViewController:itemMagView];
            [itemMagView didMoveToParentViewController:self];
            [itemMagView setContent:item];
            y+=childheight + GAP;
        }
    }
    
    if(([self.Type isEqualToString:TOPIC_CARD_TYPE_KEY] && self.isCollapsed == YES) || ([self.Type isEqualToString:SUBTOPIC_CARD_TYPE_KEY] && self.isCollapsed == YES))
    {
        self.imgBg1.image = [UIImage imageForDeviceWithName:@"bgCollaspeTopics.png"];
        [self.collapseButton setTitle:@"+" forState:UIControlStateNormal];
    }
    else if([self.Type isEqualToString:SUBTOPIC_CARD_TYPE_KEY] && self.interest.Total > 0)
    {
        //update Height for order element
        UILabel *paging = [[UILabel alloc] initWithFrame:CGRectMake(0, y, self.containerView.frame.size.width, 15)];
        paging.textAlignment = NSTextAlignmentCenter;
        [paging setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
        paging.textColor = [UIColor colorWithRed:152/255.0f green:152/255.0f blue:152/255.0f alpha:1];
        paging.text = [NSString stringWithFormat:@"%d of %d",currentPage,totalPage]; //Todo: Add text for page here
        [self.containerView addSubview:paging];
        if(totalPage > 1) {
            y += paging.frame.size.height + GAP*2;
        } else {
            y += paging.frame.size.height + GAP;
        }
    }
    CGRect smallFrame = self.containerView.frame;
    smallFrame.size.height = y;
    self.containerView.frame = smallFrame;
    
    CGRect bigFrame = self.view.frame;
    bigFrame.size.height = self.containerView.frame.origin.y + y + GAP;
    self.view.frame = bigFrame;
    if(self.pageviewcontroller) {
        [self.pageviewcontroller refreshView];
    }
    [self collapsed:self.isCollapsed type:self.Type];
}

- (void)collapsed:(BOOL)value type:(NSString *)type {
    if(value == YES)
    {
        [self.collapseButton setTitle:@"+" forState:UIControlStateNormal];
        if([type isEqualToString:TOPIC_CARD_TYPE_KEY])
        {
            self.imgBg1.image = [UIImage imageForDeviceWithName:@"bgCollaspeTopics.png"];
            float height = [ClientUtils getSizeImage:@"bgCollaspeTopics.png"].height;
            CGRect frame = self.view.frame;
            frame.size.height = height + GAP;
            self.view.frame = frame;
            self.containerView.hidden = YES;
            [self.pageviewcontroller refreshView];
        }
        else if([type isEqualToString:ARTICLE_CARD_TYPE_KEY])
        {
            [self clearsubviewcontainer];
            int childheight = 25;
            int y = 0;
            // Topic Article Mag View Type
            for(ArticleModel *item in self.listItems)
            {
                ItemArticleListView *itemListView = [[ItemArticleListView alloc] initWithNibName:@"ItemArticleListView" bundle:nil]
                ;
                itemListView.article = item;
                CGRect frame = itemListView.view.frame;
                frame.origin.x = 0; // Fix Position X of item Article Mag
                frame.origin.y = y;
                frame.size.width = self.containerView.frame.size.width;
                frame.size.height = childheight; //fix height of item Topic Mag
                itemListView.view.frame = frame;
                [self.containerView addSubview:itemListView.view];
                [self addChildViewController:itemListView];
                [itemListView didMoveToParentViewController:self];
                y += frame.size.height + GAP;
            }
            CGRect smallFrame = self.containerView.frame;
            smallFrame.size.height = y;
            self.containerView.frame = smallFrame;
            
            CGRect bigFrame = self.view.frame;
            bigFrame.size.height = self.containerView.frame.origin.y + y + GAP;
            self.view.frame = bigFrame;
            [self.pageviewcontroller refreshView];
            
        }
        else if([type isEqualToString:SUBTOPIC_CARD_TYPE_KEY])
        {
            self.imgBg1.image = [UIImage imageForDeviceWithName:@"bgCollaspeTopics.png"];
            float height = [ClientUtils getSizeImage:@"bgCollaspeTopics.png"].height;
            CGRect frame = self.view.frame;
            frame.size.height = height + GAP;
            self.view.frame = frame;
            self.containerView.hidden = YES;
            [self.pageviewcontroller refreshView];
        }
        self.isCollapsed = YES;
    } else if(value == NO)
    {
        [self.collapseButton setTitle:@"-" forState:UIControlStateNormal];
        if([type isEqualToString:TOPIC_CARD_TYPE_KEY])
        {
            self.imgBg1.image = [UIImage imageForDeviceWithName:@"bgSingleCardView.png"];
            self.containerView.hidden = NO;
            CGRect frame = self.view.frame;
            frame.size.height = self.containerView.frame.size.height + self.containerView.frame.origin.y + GAP;
            self.view.frame = frame;
            [self.pageviewcontroller refreshView];
        }
        else if([type isEqualToString:ARTICLE_CARD_TYPE_KEY])
        {
            [self clearsubviewcontainer];
            int childheight = 92;
            int y = 0;
            //Topic Article Mag View Type
            for(ArticleModel *item in self.listItems)
            {
                ItemArticleMagView *itemMagView = [[ItemArticleMagView alloc] initWithNibName:@"ItemArticleMagView" bundle:nil];
                CGRect frame = itemMagView.view.frame;
                frame.origin.x = 0; // Fix Position X of item Article Mag
                frame.origin.y = y;
                frame.size.width = self.containerView.frame.size.width;
                frame.size.height = childheight; //fix height of item Topic Mag
                itemMagView.view.frame = frame;
                [self.containerView addSubview:itemMagView.view];
                [self addChildViewController:itemMagView];
                [itemMagView didMoveToParentViewController:self];
                [itemMagView setData:item];
                y += frame.size.height + GAP;
            }
            CGRect smallFrame = self.containerView.frame;
            smallFrame.size.height = y;
            self.containerView.frame = smallFrame;
            
            CGRect bigFrame = self.view.frame;
            bigFrame.size.height = self.containerView.frame.origin.y + y + GAP;
            self.view.frame = bigFrame;
            [self.pageviewcontroller refreshView];
        }
        else if([type isEqualToString:SUBTOPIC_CARD_TYPE_KEY])
        {
            if(totalPage > 1)
            {
                self.imgBg1.image = [UIImage imageForDeviceWithName:@"bgGroupCardView.png"];
            } else {
                self.imgBg1.image = [UIImage imageForDeviceWithName:@"bgSingleCardView.png"];
            }
            self.containerView.hidden = NO;
            CGRect frame = self.view.frame;
            frame.size.height = self.containerView.frame.size.height + self.containerView.frame.origin.y + GAP;
            self.view.frame = frame;
            [self.pageviewcontroller refreshView];
        }
        self.isCollapsed = NO;
    }
}

- (IBAction)titleTouched:(id)sender {
    
    if(self.interest.Level == 1)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SubTopicViewController *topicview = [storyboard instantiateViewControllerWithIdentifier:@"SubTopicView"];
        topicview.topicID = self.interest.ID;
        [self.navigationController pushViewController:topicview animated:YES];
    }
    else if(self.interest.Level == 2)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ArticleTopicViewController *articleview = [storyboard instantiateViewControllerWithIdentifier:@"ArticleTopicView"];
        articleview.topicID = self.interest.ID;
        articleview.topicName = self.interest.Name;
        [self.navigationController pushViewController:articleview animated:YES];
    }
}

- (IBAction)collapseTouched:(id)sender {
    [self collapsed:!self.isCollapsed type:self.Type];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (float)getHeight {
    return self.view.frame.size.height;
}

- (void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer {
    //todo : set animation swipe
    NSLog(@"Swipe left");
    if([self.Type isEqualToString:SUBTOPIC_CARD_TYPE_KEY] && isLoading == NO) {
        if(currentPage < totalPage)
        {
            isLoading = YES;
            currentPage++;
            int itemremaining = ((currentPage*itemPerPage) > totalItem) ? totalItem : (currentPage*itemPerPage);
            if(self.listItems.count >= itemremaining)
            {
                [self animationSlide];
            } else {
                //check State Global Variable for Load Data
                TempData *data = [TempData getInstance];
                if([data.State isEqualToString:NAV_ARTICLE]) {
                    [self loadArticleTopic];
                } else if([data.State isEqualToString:NAV_MANAGE]) {
                    [self loadManageArticleTopic];
                }
            }
        }
    }
}

- (void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer {
    //todo : set animation swipe
    NSLog(@"Swipe right");
    if([self.Type isEqualToString:SUBTOPIC_CARD_TYPE_KEY] && isLoading == NO) {
        if(currentPage > 1 )
        {
            isLoading = YES;
            currentPage--;
            [self animationSlide];
        }
    }
}

- (void)clearsubviewcontainer {
    NSArray *uiviewcontrollers = [self childViewControllers];
    NSArray *subviews = [self.containerView subviews];
    for (UIView *view in subviews)
    {
        [view removeFromSuperview];
    }
    
    for(UIViewController *viewcontroller in uiviewcontrollers)
    {
        [viewcontroller willMoveToParentViewController:nil];
        [viewcontroller removeFromParentViewController];
    }
}

- (void)loadArticleTopic {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    [ServiceClient getArticlesOfInterest:self.view username:username md5Password:password interestID:self.interest.ID page:currentPage itemsPerPage:itemPerPage successBlock:^(id responseObject) {
        NSArray *articleItems = responseObject;
        for (ArticleModel *item in articleItems) {
            [self.listItems addObject:item];
        }
        [self animationSlide];
    } failureBlock:^(NSError *error) {
        isLoading = NO;
    }];
}

- (void)loadManageArticleTopic {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    [ServiceClient getUserArticlesOfInterest:self.view username:username md5Password:password interestID:self.interest.ID page:currentPage itemsPerPage:itemPerPage successBlock:^(id responseObject) {
        NSMutableArray *arrayArticles = responseObject;
        for (ArticleModel *item in arrayArticles) {
            [self.listItems addObject:item];
        }
        [self animationSlide];
    } failureBlock:^(NSError *error) {
        isLoading = NO;
    }];
}

- (void)animationSlide {
    [UIView animateWithDuration:duration delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self clearsubviewcontainer];
        [self loadData:self.listItems];
    } completion:^(BOOL finished) {
        isLoading = NO;
    }];
}

@end
