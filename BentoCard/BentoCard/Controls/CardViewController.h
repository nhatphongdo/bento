//
//  CardViewViewController.h
//  BentoCard
//
//  Created by Do Nhat Phong on 12/10/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InterestModel.h"
#import "PageViewController.h"

@interface CardViewController : UIViewController

@property (nonatomic, strong) PageViewController *pageviewcontroller;
@property (nonatomic) BOOL isCollapsed;
@property (nonatomic, strong) NSString *Type;
@property (nonatomic, strong) NSMutableArray *listItems;
@property (nonatomic, strong) InterestModel *interest;
//@property (nonatomic, strong) NSString *titleCard;
//@property (nonatomic, strong) NSString *topicId;
//@property (nonatomic) int totalItem;

@property (nonatomic, strong) IBOutlet UILabel  *countLabel;
@property (nonatomic, strong) IBOutlet UIButton *titleButton;
@property (nonatomic, strong) IBOutlet UIButton *collapseButton;
@property (nonatomic, strong) IBOutlet UIImageView  *imgBg1;
@property (nonatomic, strong) IBOutlet UIView *containerView;

@property (nonatomic, strong) id actionButtonHandler;

- (IBAction)titleTouched:(id)sender;
- (IBAction)collapseTouched:(id)sender;
- (void)collapsed:(BOOL)value type:(NSString *)type;
- (void)loadData:(NSMutableArray *)arrayItems;
- (float)getHeight;

@end
