//
//  PageViewViewController.m
//  BentoCard
//
//  Created by Do Nhat Phong on 12/10/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//Important Note:
// + Scroller view have -5px of top (y) for show effect shadow of image. ExtentGAP = 5


#import "PageViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImage+DeviceSpecificMedia.h"
#import <PKRevealController/PKRevealController.h>
#import "Constrain.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import "CardViewController.h"
#import "InterestModel.h"
#import "ArticleModel.h"
#import "TempData.h"
#import "ArticleSideBarViewController.h"
#import "NavigateViewController.h"

@interface PageViewController ()
{
    BOOL isSlideUp,atmaxitem;
    int currentPage;
    UIButton *activeButton;
    BOOL isArticleTopicLoad,isLoading;
    BOOL isManage;
}

@end

@implementation PageViewController

static const float GAP = 10.0f;
static const float ExtentGAP = 5;
static const float duration = 0.5f;
static const float menuHoverWidth = 35.0f;
static const int collapseHeight = 126;
static const int itemPerPage = 10;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.containerScrollView.delegate = self;
    isSlideUp = NO;
    currentPage = 1;
    atmaxitem = NO;
    isArticleTopicLoad = NO;
    isLoading = NO;
    UIImage *bgImage = [UIImage imageForDeviceWithName:@"landingthumb.png"];
    [self.backgroundImage setImage:bgImage];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSArray *navArray = [self.navigationController viewControllers];
    if(navArray.count > 1) {
        [self.btBackRight setHidden:NO];
    }
    else
    {
        [self.btBackRight setHidden:YES];
    }
    [self.menuScrollView setContentOffset:CGPointMake(self.oldMenuPostX, self.menuScrollView.contentOffset.y) animated:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self addNavigation:NO];
    if(self.isLandingPage == YES && self.isFirstTime == YES) {
        [self showNavigateView];
        self.isFirstTime = NO;
    }
}

- (void)addNavigation:(BOOL)isSameView {
    if(self.isLandingPage == YES)
    {
        [ClientUtils SaveNavigation:self Name:self.title level:NAV_LANDINGPAGE_LEVEL ParentId:NAV_PARENTID_NIL isSameView:isSameView];
    }
    else
    {
        int getTopicLevel = [ClientUtils getLevelByTopicId:self.topicId];
        NSString *topicName = [ClientUtils getTopicNameById:self.topicId];
        if (getTopicLevel == 1) {
            [ClientUtils SaveNavigation:self Name:topicName level:NAV_ORDERPAGE_LEVEL_1 ParentId:self.topicId isSameView:isSameView];
        }
        else if (getTopicLevel == 2) {
            InterestModel *parentTopic = [ClientUtils getParentInterest:self.topicId];
            [ClientUtils SaveNavigation:self Name:topicName level:NAV_ORDERPAGE_LEVEL_2 ParentId:parentTopic.ID isSameView:isSameView];
        }
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView == self.containerScrollView)
    {
        if(scrollView.contentOffset.y > 50 && isSlideUp == NO)
        {
            isSlideUp = YES;
            [UIView animateWithDuration:duration animations:^{
                self.wrapthumbView.frame = CGRectMake(0, self.statusViews.frame.origin.y, self.view.frame.size.width, self.wrapthumbView.frame.size.height - collapseHeight);
                self.letterLabel.hidden = YES;
                self.cricleImage.hidden = YES;
                [self.view bringSubviewToFront:self.statusViews];
                [self.statusViews setBackgroundColor:[UIColor clearColor]];
                //todo: set scrollview move up
                CGRect frame = scrollView.frame;
                frame.origin.y = self.wrapthumbView.frame.origin.y + self.wrapthumbView.frame.size.height - ExtentGAP;
                frame.size.height += (collapseHeight + self.statusViews.frame.size.height);
                scrollView.frame = frame;
                CGRect menuFrame = self.menuScrollView.frame;
                menuFrame.size.width = self.wrapthumbView.frame.size.width;
                menuFrame.origin.x = self.wrapthumbView.frame.size.width - menuFrame.size.width;
                menuFrame.origin.y = self.statusViews.frame.size.height - GAP;
                self.menuScrollView.frame = menuFrame;
                
                if(!self.btBackRight.isHidden)
                {
                    //todo: set button move
                    CGRect btrightFrame = self.btStatusRight.frame;
                    CGRect btbackFrame = self.btBackRight.frame;
                    btrightFrame.origin.x -= btbackFrame.size.width;
                    self.btStatusRight.frame = btrightFrame;
                    
                    btbackFrame.origin.y -= (self.statusViews.frame.size.height - btrightFrame.origin.y);
                    self.btBackRight.frame = btbackFrame;
                    [self.btBackRight setTitle:@"X" forState:UIControlStateNormal];
                    [self.btBackRight setImage:nil forState:UIControlStateNormal];
                    [self.view bringSubviewToFront:self.btBackRight];
                }
                
                [self activeButtonMenu:activeButton effect:NO];
            } completion:^(BOOL finished) {
                if(finished)
                {
                }
            }];
        }
        if(scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
//            NSLog(@"BOTTOM REACHED");
            if(isArticleTopicLoad == YES && atmaxitem == NO && isLoading == NO && isManage == NO)
            {
                isLoading = YES;
                currentPage++;
                [self loadArticleTopic:self.topicId topicName:self.topicName Continues:YES];
            }
            if(isArticleTopicLoad == YES && atmaxitem == NO && isLoading == NO && isManage == YES)
            {
                isLoading = YES;
                currentPage++;
                [self loadManageArticleTopic:self.topicId topicName:self.topicName Continues:YES];
            }
        }
        if(scrollView.contentOffset.y < -50.0 && isSlideUp == YES) {
            isSlideUp = NO;
            [UIView animateWithDuration:duration animations:^{
                //todo: set webview move up
                self.wrapthumbView.frame = CGRectMake(0, self.statusViews.frame.size.height + self.statusViews.frame.origin.y, self.wrapthumbView.frame.size.width, self.wrapthumbView.frame.size.height + collapseHeight);
                [self.statusViews setBackgroundColor:[UIColor colorWithRed:29/255.0f green:24/255.0f blue:71/255.0f alpha:1]];
                self.letterLabel.hidden = NO;
                self.cricleImage.hidden = NO;
                CGRect frame = scrollView.frame;
                frame.origin.y = self.wrapthumbView.frame.origin.y + self.wrapthumbView.frame.size.height - ExtentGAP;
                frame.size.height -= (collapseHeight + self.statusViews.frame.size.height);
                scrollView.frame=frame;
                
                CGRect menuFrame = self.menuScrollView.frame;
                menuFrame.size.width = self.wrapthumbView.frame.size.width;
                menuFrame.origin.x = 0;
                menuFrame.origin.y = self.wrapthumbView.frame.size.height - menuFrame.size.height - GAP;
                self.menuScrollView.frame = menuFrame;
                
                if(!self.btBackRight.isHidden)
                {
                    //todo: set button move
                    CGRect btrightFrame = self.btStatusRight.frame;
                    CGRect btbackFrame = self.btBackRight.frame;
                    btrightFrame.origin.x += btbackFrame.size.width;
                    self.btStatusRight.frame = btrightFrame;
                    
                    btbackFrame.origin.y += (self.statusViews.frame.size.height - btrightFrame.origin.y);
                    self.btBackRight.frame = btbackFrame;
                    
                    [self.btBackRight setTitle:@"" forState:UIControlStateNormal];
                    UIImage *btimg = [UIImage imageForDeviceWithName:@"closeRightCorner.png"];
                    [self.btBackRight setImage:btimg forState:UIControlStateNormal];
                }
                
                [self activeButtonMenu:activeButton effect:NO];
            } completion:^(BOOL finished) {
                if(finished)
                {
                }
            }];
        }
    }
}

- (void)loadMenuList:(NSMutableArray *)menuLists activeTopic:(NSString *)activeTopic {
    //remove old button menu
    NSArray *subMenus = [self.menuScrollView subviews];
    for (UIView *item in subMenus) {
        if([item isMemberOfClass:[UIButton class]])
        {
            [item removeFromSuperview];
        }
    }

    self.menuArray = menuLists;
    if(self.isLandingPage == YES)
    {
        int padding = 25; //padding for menu
        int x = padding;
        for (int i = 0; i < self.menuArray.count; i++) {
            
            NSDictionary *item = [self.menuArray objectAtIndex:i];
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [button addTarget:self
                       action:@selector(landingButtonTouched:)
             forControlEvents:UIControlEventTouchUpInside];
            [button setTitle:[item valueForKey:@"name"] forState:UIControlStateNormal];
            UIFont *font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
            button.titleLabel.font = font;
            [button setTitleColor:[UIColor whiteColor] forState:normal];
            
            CGSize size = [[item valueForKey:@"name"] sizeWithAttributes:@{NSFontAttributeName:font}];
            CGSize adjustedSize = CGSizeMake(ceilf(size.width), ceilf(size.height));
            
            button.frame = CGRectMake(x, 0, adjustedSize.width, 40.0);
            button.tag = i;
            if([[item valueForKey:@"name"] isEqualToString:activeTopic])
            {
                activeButton = button;
            }
            x += adjustedSize.width + padding;
            [self.menuScrollView addSubview:button];
        }
        self.menuScrollView.contentSize = CGSizeMake(x + padding, self.menuScrollView.contentSize.height);
        if(activeButton)
        {
            [self activeButtonMenu:activeButton effect:NO];
        }
    }
    else {
        int padding = 20;
        int x = padding;
        for (int i = 0; i < self.menuArray.count; i++) {
            InterestModel *model = [self.menuArray objectAtIndex:i];
            UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [button addTarget:self
                       action:@selector(menuButtonTouched:)
             forControlEvents:UIControlEventTouchUpInside];
            [button setTitle:model.Name forState:UIControlStateNormal];
            UIFont *font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
            button.titleLabel.font = font;
            [button setTitleColor:[UIColor whiteColor] forState:normal];
            
            CGSize size = [model.Name sizeWithAttributes:@{NSFontAttributeName:font}];
            CGSize adjustedSize = CGSizeMake(ceilf(size.width), ceilf(size.height));
            
            button.frame = CGRectMake(x, 0, adjustedSize.width, 40.0);
            button.tag = i;
            x += adjustedSize.width + padding;
            [self.menuScrollView addSubview:button];
            if([activeTopic isEqualToString:model.Name] || [activeTopic isEqualToString:model.ID]) // for active All menu & normal menu
            {
                activeButton = button;
            }
        }
        self.menuScrollView.contentSize = CGSizeMake(x + padding, self.menuScrollView.contentSize.height);
        if(activeButton)
        {
            [self activeButtonMenu:activeButton effect:NO];
        }
    }
}

- (void)activeButtonMenu:(UIButton *)btn effect:(BOOL)effect {
    UIView *removeView = [self.menuScrollView viewWithTag:6666699999];
    if(removeView != nil)
    {
        [removeView removeFromSuperview];
    }
    
    NSString *firstLetter = [btn.titleLabel.text substringToIndex:1];
    self.letterLabel.text = firstLetter;
    
//    UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, btn.frame.size.height - 1.0f, btn.frame.size.width, 1)];
    UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(btn.frame.size.width/2 - menuHoverWidth/2, btn.frame.size.height/2 + 15, menuHoverWidth, 1.5f)];
    bottomBorder.backgroundColor = [UIColor whiteColor];
    bottomBorder.tag = 6666699999;
    [btn addSubview:bottomBorder];
    
    float extent = (self.wrapthumbView.frame.size.width - self.menuScrollView.frame.size.width) / 2; // not collapse extent = 0
    self.oldMenuPostX = btn.frame.origin.x - (self.menuScrollView.frame.size.width / 2) + (btn.frame.size.width / 2) + extent;
    if(effect) {
        [UIView animateWithDuration:0.6f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             [self.menuScrollView setContentOffset:CGPointMake(self.oldMenuPostX, self.menuScrollView.contentOffset.y)];
                         }
                         completion:^(BOOL finished){
                             if (finished) {
                                 [self addNavigation:YES];
                             }
                         }];
    } else {
        [self.menuScrollView setContentOffset:CGPointMake(self.oldMenuPostX, self.menuScrollView.contentOffset.y)];
    }
}

- (void)menuButtonTouched:(id)sender {
    UIButton *btn = (UIButton *)sender;
    activeButton = btn;
    [self activeButtonMenu:activeButton effect:YES];
    TempData *data = [TempData getInstance];
    //todo: Load Data and create cardlist
    int index = btn.tag;
    InterestModel *menuItem = [self.menuArray objectAtIndex:index];
    
    if([menuItem.Name isEqualToString:@"All"])
    {
        if([data.State isEqualToString:NAV_ARTICLE]) {
            [self loadTopics];
        } else if([data.State isEqualToString:NAV_MANAGE]) {
            [self loadManageTopics];
        }
    }
    else
    {
        //todo: Load list Article of Topic
        if(menuItem.Level == 1)
        {
            //todo : navigate to subtopic include article viewcontroller
            if([data.State isEqualToString:NAV_ARTICLE]) {
                [self loadSubTopic:menuItem.ID];
            } else if([data.State isEqualToString:NAV_MANAGE]) {
                [self loadManageSubTopic:menuItem.ID];
            }
            
        }
        else if(menuItem.Level == 2)
        {
            //todo : navigate to article only viewcontroller
            [self removeCardList];
            if([data.State isEqualToString:NAV_ARTICLE]) {
                [self loadArticleTopic:menuItem.ID topicName:menuItem.Name Continues:NO];
            } else if([data.State isEqualToString:NAV_MANAGE]) {
                [self loadManageArticleTopic:menuItem.ID topicName:menuItem.Name Continues:NO];
            }
        }
    }
}

- (void)landingButtonTouched:(id)sender {
    //todo: action for landing button touch
    UIButton *btn = (UIButton *)sender;
    NSDictionary *curItem = [self.menuArray objectAtIndex:btn.tag];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *myview = [storyboard instantiateViewControllerWithIdentifier:[curItem objectForKey:@"nib"]];
    [self.parentViewController.navigationController setViewControllers:[NSArray arrayWithObjects:myview, nil] animated:YES]; //reset rootviewcontroller
}

- (void)removeCardList {
    NSArray *childviewcontrollers = [self childViewControllers];
    if(childviewcontrollers.count > 0)
    {
        for (UIViewController *item in childviewcontrollers)
        {
            if([item isMemberOfClass:[CardViewController class]])
            {
                [item willMoveToParentViewController:nil];
                [item.view removeFromSuperview];
                [item removeFromParentViewController];
            }
        }
        self.containerScrollView.contentSize = CGSizeMake(0,0);
    }
}

- (void)createCardList:(NSArray *)cards {
    [self removeCardList];
    
    self.cardList = cards;

    // Add to scroll view
    float y = GAP; // TODO: Use contanst temporarily, change to real value later
    for (int i = 0; i < self.cardList.count; i++) {
        CGRect frame = ((UIViewController *)[self.cardList objectAtIndex:i]).view.frame;
        frame.origin.y = y;
        frame.origin.x = 0;
        frame.size.width = self.containerScrollView.frame.size.width;
        ((UIViewController *)[self.cardList objectAtIndex:i]).view.frame = frame;
        [self.containerScrollView addSubview:((UIViewController *)[self.cardList objectAtIndex:i]).view];
        [self addChildViewController:[self.cardList objectAtIndex:i]];
        [[self.cardList objectAtIndex:i] didMoveToParentViewController:self];
        y += frame.size.height + GAP;
    }
    self.containerScrollView.contentSize = CGSizeMake(self.containerScrollView.frame.size.width, y + GAP);
}

- (void)createCardListContinues:(NSArray *)cards {
    self.cardList = cards;
    
    // Add to scroll view
    float y = self.containerScrollView.contentSize.height - GAP; // TODO: Use contanst temporarily, change to real value later
    if(y < 0)
    {
        y = GAP;
    }
    
    for (int i = 0; i < self.cardList.count; i++) {
        CGRect frame = ((UIViewController *)[self.cardList objectAtIndex:i]).view.frame;
        frame.origin.y = y;
        frame.origin.x = 0;
        frame.size.width = self.containerScrollView.frame.size.width;
        ((UIViewController *)[self.cardList objectAtIndex:i]).view.frame = frame;
        [self.containerScrollView addSubview:((UIViewController *)[self.cardList objectAtIndex:i]).view];
        [self addChildViewController:[self.cardList objectAtIndex:i]];
        [[self.cardList objectAtIndex:i] didMoveToParentViewController:self];
        y += frame.size.height + GAP;
    }
    self.containerScrollView.contentSize = CGSizeMake(self.containerScrollView.frame.size.width, y + GAP);
}

#pragma mark - Load Data article
- (void)loadMenuLv1:(NSString *)topicId {
    
    TempData *data = [TempData getInstance];
    if(data.interests.count > 0)
    {
        NSMutableArray *menulists = [[NSMutableArray alloc] init];
        InterestModel *firstModel = [[InterestModel alloc] init];
        firstModel.Name = @"All";
        firstModel.Level = 1;
        [menulists addObject:firstModel];
        for (InterestModel *item in data.interests)
        {
            if(item.Level == 1)
            {
                [menulists addObject:item];
            }
        }
        if(menulists.count > 0)
        {
            [self loadMenuList:menulists activeTopic:topicId];
        }
    }
}

- (void)loadMenuLv2:(NSString *)topicId {
    
    TempData *data = [TempData getInstance];
    if(data.interests.count > 0)
    {
        NSMutableArray *menulists = [[NSMutableArray alloc] init];
        NSString *excTopicId = @"";
        NSMutableArray *interests = data.interests;
        for (InterestModel *item in interests)
        {
            if([item.ID isEqualToString:topicId])
            {
                if([item.ParentID isEqualToString:@""] || item.ParentID == nil)
                {
                    excTopicId = item.ID;
                }
                else
                {
                    excTopicId = item.ParentID;
                }
            }
        }
        if(![excTopicId isEqualToString:@""])
        {
            for (InterestModel *item in interests) {
                if([item.ParentID isEqualToString:excTopicId])
                {
                    [menulists addObject:item];
                }
            }
        }
        if(menulists.count > 0)
        {
            [self loadMenuList:menulists activeTopic:topicId];
        }
    }
}

- (void)loadTopics {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    //todo: Load list topics
    TempData *data = [TempData getInstance];
    if(data.interests.count > 0)
    {
        NSMutableArray *interests = data.interests;
        NSMutableArray *parentInterest = [[NSMutableArray alloc] init];
        NSMutableArray *childInterest = [[NSMutableArray alloc] init];
        NSMutableArray *cards = [[NSMutableArray alloc] init];
        for(InterestModel *item in interests)
        {
            if(item.Level == 1)
            {
                [parentInterest addObject:item];
            }
            else if(item.Level == 2)
            {
                [childInterest addObject:item];
            }
        }
        for(InterestModel *parent in parentInterest)
        {
            NSMutableArray *childsofParent = [[NSMutableArray alloc] init];
            for (InterestModel *subitem in childInterest) {
                if(subitem.ParentID == parent.ID)
                {
                    [childsofParent addObject:subitem];
                }
            }
            CardViewController *cardView = [[CardViewController alloc] initWithNibName:@"CardViewController" bundle:nil];
            cardView.Type = TOPIC_CARD_TYPE_KEY;
            cardView.isCollapsed = YES;
            cardView.listItems = childsofParent;
            cardView.interest = parent;
            cardView.pageviewcontroller = self;
            [cards addObject:cardView];
        }
        if(cards.count > 0)
        {
            [self createCardList:cards];
        }
    }
}

- (void)loadSubTopic:(NSString *)topicID {
    //todo: Load Subtopic Data & Create CardView here
    //      API load data from topicID include subtopic and article of subtopic
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    NSMutableArray *cards = [[NSMutableArray alloc] init];
    [ServiceClient getArticlesOfSubTopicView:self.view username:username md5Password:password interestID:topicID successBlock:^(id responseObject) {
        NSMutableArray *subtopicarticles = responseObject;
        [ServiceClient getInterestsByParentID:self.view username:username md5Password:password parentID:topicID successBlock:^(id responseObject) {
            NSMutableArray *subtopics = responseObject;
            for (InterestModel *topic in subtopics)
            {
                //todo: create cardview
                topic.Level = 2; //init topic level for title button click in cardview
                CardViewController *cardview = [[CardViewController alloc] initWithNibName:@"CardViewController" bundle:nil];
                cardview.isCollapsed = YES;
                cardview.Type = SUBTOPIC_CARD_TYPE_KEY;
                cardview.interest = topic;
                cardview.pageviewcontroller = self;
                NSMutableArray *articlecardview = [[NSMutableArray alloc] init];
                for (ArticleModel *arc in subtopicarticles)
                {
                    if([arc.Category isEqualToString:topic.ID])
                    {
                        [articlecardview addObject:arc];
                    }
                }
                cardview.listItems = articlecardview;
                [cards addObject:cardview];
            }
            if(cards.count > 0)
            {
                [self createCardList:cards];
            }
        } failureBlock:^(NSError *error) {
            
        }];
        
    } failureBlock:^(NSError *error) {
        
    }];
}

- (void)loadArticleTopic:(NSString *)topicID topicName:(NSString *)topicName Continues:(BOOL)continues {
    //todo: Load Subtopic Data & Create CardView here
    //      API load data from topicID include subtopic and article of subtopic
    isManage = NO;
    self.topicId = topicID;
    self.topicName = topicName;
    isArticleTopicLoad = YES;
    if(!continues)
    {
        currentPage = 1;
    }
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    NSMutableArray *cards = [[NSMutableArray alloc] init];
    [ServiceClient getArticlesOfInterest:self.view username:username md5Password:password interestID:topicID page:currentPage itemsPerPage:itemPerPage successBlock:^(id responseObject) {
        
        NSMutableArray *arrayArticles = responseObject;
        if(arrayArticles.count > 0)
        {
            InterestModel *topicModel = [[InterestModel alloc] init];
            topicModel.Name = topicName;
            for (ArticleModel *item in arrayArticles) {
                //todo: load data
                NSMutableArray *listitem = [[NSMutableArray alloc] init];
                CardViewController *cardview = [[CardViewController alloc] initWithNibName:@"CardViewController" bundle:nil];
                cardview.Type = ARTICLE_CARD_TYPE_KEY;
                cardview.isCollapsed = YES;
                cardview.interest = topicModel;
                cardview.pageviewcontroller = self;
                [listitem addObject:item];
                cardview.listItems = listitem;
                [cards addObject:cardview];
            }
            [self createCardListContinues:cards];
        }
        if(arrayArticles.count < itemPerPage)
        {
            atmaxitem = YES;
        }
        isLoading = NO;
    } failureBlock:^(NSError *error) {
        isLoading = NO;
    }];
}

#pragma mark - Load data Manage
- (void)loadManageMenuLv1:(NSString *)topicId {
    
    TempData *data = [TempData getInstance];
    if(data.userinterests.count > 0)
    {
        NSMutableArray *menulists = [[NSMutableArray alloc] init];
        InterestModel *firstModel = [[InterestModel alloc] init];
        firstModel.Name = @"All";
        firstModel.Level = 1;
        [menulists addObject:firstModel];
        for (InterestModel *item in data.userinterests)
        {
            if(item.Level == 1)
            {
                [menulists addObject:item];
            }
        }
        if(menulists.count > 0)
        {
            [self loadMenuList:menulists activeTopic:topicId];
        }
    }
}

- (void)loadManageMenuLv2:(NSString *)topicId {
    
    TempData *data = [TempData getInstance];
    if(data.userinterests.count > 0)
    {
        NSMutableArray *menulists = [[NSMutableArray alloc] init];
        NSString *excTopicId = @"";
        NSMutableArray *interests = data.userinterests;
        for (InterestModel *item in interests)
        {
            if([item.ID isEqualToString:topicId])
            {
                if([item.ParentID isEqualToString:@""] || item.ParentID == nil)
                {
                    excTopicId = item.ID;
                }
                else
                {
                    excTopicId = item.ParentID;
                }
            }
        }
        if(![excTopicId isEqualToString:@""])
        {
            for (InterestModel *item in interests) {
                if([item.ParentID isEqualToString:excTopicId])
                {
                    [menulists addObject:item];
                }
            }
        }
        if(menulists.count > 0)
        {
            [self loadMenuList:menulists activeTopic:topicId];
        }
    }
}

- (void)loadManageTopics {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    [ServiceClient getStructureUserFavorites:self.view username:username md5Password:password successBlock:^(id responseObject) {
        NSArray *interests;
        NSMutableArray *appearInterests;
        interests = responseObject;
        NSMutableArray *parentInterest = [[NSMutableArray alloc] init];
        NSMutableArray *childInterest = [[NSMutableArray alloc] init];
        NSMutableArray *cards = [[NSMutableArray alloc] init];
        for(InterestModel *item in interests)
        {
            if(item.Level == 1)
            {
                [parentInterest addObject:item];
            }
            else if(item.Level == 2)
            {
                [childInterest addObject:item];
            }
        }
        for(InterestModel *parent in parentInterest)
        {
            NSMutableArray *childsofParent = [[NSMutableArray alloc] init];
            for (InterestModel *subitem in childInterest) {
                if(subitem.ParentID == parent.ID)
                {
                    [childsofParent addObject:subitem];
                }
            }
            CardViewController *cardView = [[CardViewController alloc] initWithNibName:@"CardViewController" bundle:nil];
            cardView.Type = TOPIC_CARD_TYPE_KEY;
            cardView.isCollapsed = YES;
            cardView.listItems = childsofParent;
            cardView.interest = parent;
            cardView.pageviewcontroller = self;
            [cards addObject:cardView];
        }
        if(cards.count > 0)
        {
            [self createCardList:cards];
        }
    } failureBlock:^(NSError *error) {
    }];
}

- (void)loadManageSubTopic:(NSString *)topicID {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    NSMutableArray *cards = [[NSMutableArray alloc] init];
    TempData *data = [TempData getInstance];
    [ServiceClient getMyArticlesOfSubTopicView:self.view username:username md5Password:password interestID:topicID successBlock:^(id responseObject) {
        NSMutableArray *subtopicarticles = responseObject;
        NSMutableArray *subtopics = [[NSMutableArray alloc] init];
        for (InterestModel *item in data.userinterests) {
            if([item.ParentID isEqualToString:topicID]) {
                [subtopics addObject:item];
            }
        }
        
        for (InterestModel *topic in subtopics)
        {
            //todo: create cardview
            topic.Level = 2; //init topic level for title button click in cardview
            CardViewController *cardview = [[CardViewController alloc] initWithNibName:@"CardViewController" bundle:nil];
            cardview.isCollapsed = YES;
            cardview.Type = SUBTOPIC_CARD_TYPE_KEY;
            cardview.interest = topic;
            cardview.pageviewcontroller = self;
            NSMutableArray *articlecardview = [[NSMutableArray alloc] init];
            for (ArticleModel *arc in subtopicarticles)
            {
                if([arc.Category isEqualToString:topic.ID])
                {
                    [articlecardview addObject:arc];
                }
            }
            cardview.listItems = articlecardview;
            [cards addObject:cardview];
        }
        if(cards.count > 0)
        {
            [self createCardList:cards];
        }
        
    } failureBlock:^(NSError *error) {
        
    }];

}

- (void)loadManageArticleTopic:(NSString *)topicID topicName:(NSString *)topicName Continues:(BOOL)continues {
    isManage = YES;
    self.topicId = topicID;
    self.topicName = topicName;
    isArticleTopicLoad = YES;
    if(!continues)
    {
        currentPage = 1;
    }
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    NSMutableArray *cards = [[NSMutableArray alloc] init];
    [ServiceClient getUserArticlesOfInterest:self.view username:username md5Password:password interestID:topicID page:currentPage itemsPerPage:itemPerPage successBlock:^(id responseObject) {
        
        NSMutableArray *arrayArticles = responseObject;
        if(arrayArticles.count > 0)
        {
            InterestModel *topicModel = [[InterestModel alloc] init];
            topicModel.Name = topicName;
            for (ArticleModel *item in arrayArticles) {
                //todo: load data
                NSMutableArray *listitem = [[NSMutableArray alloc] init];
                CardViewController *cardview = [[CardViewController alloc] initWithNibName:@"CardViewController" bundle:nil];
                cardview.Type = ARTICLE_CARD_TYPE_KEY;
                cardview.isCollapsed = YES;
                cardview.interest = topicModel;
                cardview.pageviewcontroller = self;
                [listitem addObject:item];
                cardview.listItems = listitem;
                [cards addObject:cardview];
            }
            [self createCardListContinues:cards];
        }
        if(arrayArticles.count < itemPerPage)
        {
            atmaxitem = YES;
        }
        isLoading = NO;
    } failureBlock:^(NSError *error) {
        isLoading = NO;
    }];
}

- (void)refreshView {

    NSArray *subViews = [self.containerScrollView subviews];
    float y = GAP;
    for(UIView *view in subViews)
    {
        if(view.frame.origin.x == 0) //remove height UIimage of scroller
        {
            CGRect frame = view.frame;
            frame.origin.y = y;
            view.frame = frame;
            y+= view.frame.size.height + GAP;
        }
    }
    self.containerScrollView.contentSize = CGSizeMake(self.containerScrollView.frame.size.width, y);
}

- (IBAction)statusLeftTouched:(id)sender {
    [self.revealController showViewController:self.revealController.leftViewController];
}

- (IBAction)statusRightTouched:(id)sender {
    [self showNavigateView];
}

- (void)showNavigateView {
    [ClientUtils showBlur:self.view];
    NavigateViewController *nav = [[NavigateViewController alloc] initWithNibName:@"NavigateViewController" bundle:nil];
    CGRect frame = nav.view.frame;
    frame.size.width = self.view.frame.size.width;
    frame.size.height = self.view.frame.size.height;
    nav.view.frame = frame;
    [self.view addSubview:nav.view];
    [self addChildViewController:nav];
    [nav didMoveToParentViewController:self];
}

- (IBAction)BackRightTouched:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (float)getHeight {
    return self.containerScrollView.contentSize.height;
}

@end
