//
//  PageViewViewController.h
//  BentoCard
//
//  Created by Do Nhat Phong on 12/10/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageViewController : UIViewController

@property (strong, nonatomic) NSArray *cardList;
@property (strong, nonatomic) NSMutableArray *menuArray;
@property (nonatomic) BOOL isLandingPage;
@property (nonatomic) BOOL isFirstTime;

@property (nonatomic) float oldMenuPostX;
@property (strong, nonatomic) NSString *topicId;
@property (strong, nonatomic) NSString *topicName;

@property (strong, nonatomic) IBOutlet UIView *statusViews;
@property (strong, nonatomic) IBOutlet UIView *wrapthumbView;
@property (strong, nonatomic) IBOutlet UIImageView *cricleImage;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (strong, nonatomic) IBOutlet UIScrollView *containerScrollView;
@property (strong, nonatomic) IBOutlet UIScrollView *menuScrollView;
@property (strong, nonatomic) IBOutlet UILabel *letterLabel;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *btStatusLeft;
@property (strong, nonatomic) IBOutlet UIButton *btStatusRight;
@property (strong, nonatomic) IBOutlet UIButton *btBackRight;

- (void)loadMenuLv1:(NSString *)topicId;
- (void)loadMenuLv2:(NSString *)topicId;
- (void)loadTopics;
- (void)loadSubTopic:(NSString *)topicID;
- (void)loadArticleTopic:(NSString *)topicID topicName:(NSString *)topicName Continues:(BOOL)continues;

- (void)loadManageMenuLv1:(NSString *)topicId;
- (void)loadManageMenuLv2:(NSString *)topicId;
- (void)loadManageTopics;
- (void)loadManageSubTopic:(NSString *)topicID;
- (void)loadManageArticleTopic:(NSString *)topicID topicName:(NSString *)topicName Continues:(BOOL)continues;

- (void)loadMenuList:(NSMutableArray *)menuLists activeTopic:(NSString *)activeTopic;
- (void)createCardList:(NSArray *)cards;
- (void)refreshView;
- (float)getHeight;
- (IBAction)statusLeftTouched:(id)sender;
- (IBAction)statusRightTouched:(id)sender;
- (IBAction)BackRightTouched:(id)sender;

@end
