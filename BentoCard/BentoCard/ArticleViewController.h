//
//  ArticleViewController.h
//  BentoCard
//
//  Created by VO HONG HAI on 12/16/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleViewController : UIViewController<UIWebViewDelegate>


@property (nonatomic,strong) NSString *postID;
@property (nonatomic,strong) NSString *topicID;
@property (nonatomic,strong) NSString *topicName;
@property (nonatomic) BOOL liked;
@property (nonatomic) int likes;

@property (nonatomic,strong) IBOutlet UIView *statusViews;
@property (nonatomic,strong) IBOutlet UIView *wrapthumbView;
@property (nonatomic,strong) IBOutlet UIView *toolBoxs;
@property (nonatomic,strong) IBOutlet UIImageView *thumbnailImage;
@property (nonatomic,strong) IBOutlet UIImageView *previewImageView;
@property (nonatomic,strong) IBOutlet UIImageView *toolsboxbg;
@property (nonatomic,strong) IBOutlet UIButton *exclButton;
@property (nonatomic,strong) IBOutlet UIButton *commentButton;
@property (nonatomic,strong) IBOutlet UIButton *noteButton;
@property (nonatomic,strong) IBOutlet UIButton *shareButton;
@property (nonatomic,strong) IBOutlet UIButton *saveButton;
@property (nonatomic,strong) IBOutlet UIButton *likeButton;
@property (nonatomic,strong) IBOutlet UIButton *leftButton;
@property (nonatomic,strong) IBOutlet UIButton *rightButton;
@property (nonatomic,strong) IBOutlet UIButton *backButton;
@property (strong,nonatomic) IBOutlet UIButton *hidePreviewButton;
@property (strong,nonatomic) IBOutlet UIScrollView *previewView;
@property (strong,nonatomic) IBOutlet UIScrollView *bodyScrollView;
@property (strong,nonatomic) IBOutlet UILabel *titleLabel;

- (IBAction)hidePreviewTouched:(id)sender;
- (IBAction)exclTouched:(id)sender;
- (IBAction)commentTouched:(id)sender;
- (IBAction)noteTouched:(id)sender;
- (IBAction)shareTouched:(id)sender;
- (IBAction)saveTouched:(id)sender;
- (IBAction)likeTouched:(id)sender;
- (IBAction)leftButtonTouched:(id)sender;
- (IBAction)rightButtonTouched:(id)sender;
- (IBAction)backButtonTouched:(id)sender;

- (void)loadContent;
- (void)loadNoteView;
- (void)loadShareView;
- (void)loadCommentView;
- (void)refreshView;
@end
