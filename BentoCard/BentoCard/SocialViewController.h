//
//  SocialViewController.h
//  BentoCard
//
//  Created by VO HONG HAI on 2/27/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PKRevealController/PKRevealController.h>

@interface SocialViewController : PKRevealController<PKRevealing>


@end
