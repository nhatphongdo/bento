//
//  ItemArticleListView.h
//  BentoCard
//
//  Created by VO HONG HAI on 12/15/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleModel.h"

@interface ItemArticleListView : UIViewController

@property (nonatomic,strong) ArticleModel *article;

@property (nonatomic,strong) IBOutlet UILabel *titleLabel;
@property (nonatomic,strong) IBOutlet UIButton *alvoverlayButton;

- (IBAction)alvoverlayTouched:(id)sender;

@end
