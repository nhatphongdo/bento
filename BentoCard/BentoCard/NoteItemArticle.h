//
//  NoteItemArticle.h
//  BentoCard
//
//  Created by VO HONG HAI on 12/18/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteItemArticle : UIViewController

@property (nonatomic, strong) IBOutlet NSString *postID;

@property (nonatomic, strong) IBOutlet UIView *listitemView;
@property (nonatomic, strong) IBOutlet UIButton *collapseButton;
@property (nonatomic, strong) IBOutlet UIButton *addNoteButton;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel *blankLabel;


- (float)loadNotes:(NSMutableArray *)listitem;
- (int)getHeight;
- (IBAction)addNoteTouched:(id)sender;

@end
