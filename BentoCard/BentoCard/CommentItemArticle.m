//
//  CommentItemArticle.m
//  BentoCard
//
//  Created by VO HONG HAI on 12/18/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import "CommentItemArticle.h"
#import "Constrain.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "CommentModel.h"
#import "ListItemComment.h"
#import "NewComment.h"

@interface CommentItemArticle ()

@end

static int const GAP = 10;

@implementation CommentItemArticle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (float)loadComment:(NSMutableArray *)listitem {
    if(listitem.count > 0)
    {
        float y = 0.0f;
        int count = 0;
        for(int i = 0; i < listitem.count; i++)
        {
            CommentModel *item = [listitem objectAtIndex:i];
            ListItemComment *itemview = [[ListItemComment alloc] initWithNibName:@"ListItemComment" bundle:nil];
            itemview.containerViewController = self;
            itemview.name = item.Username;
            itemview.body = item.Message;
            itemview.avatar = item.Avatar;
            itemview.likecount = item.Likes;
            itemview.Liked = item.Liked;
            itemview.commentID = item.ID;
            itemview.index = i;
            itemview.total = listitem.count;
            CGRect frame = itemview.view.frame;
            frame.origin.y = y;
            frame.size.width = self.listitemView.frame.size.width;
            itemview.view.frame = frame;
            [self addChildViewController:itemview];
            [self.listitemView addSubview:itemview.view];
            [itemview didMoveToParentViewController:self];
            y+=itemview.view.frame.size.height + GAP;
            if(count < listitem.count - 1)
            {
                UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, itemview.view.frame.size.height - 1.0f, itemview.view.frame.size.width, 1)];
                bottomBorder.backgroundColor = [UIColor colorWithRed:190/255.0f green:190/255.0f blue:190/255.0f alpha:1];
                [itemview.view addSubview:bottomBorder];
            }
            count++;
        }
        CGRect listitemFrame = self.listitemView.frame;
        listitemFrame.size.height = y;
        self.listitemView.frame = listitemFrame;

        CGRect mainFrame = self.view.frame;
        mainFrame.size.height = self.listitemView.frame.origin.y + y;
        self.view.frame = mainFrame;
        return self.listitemView.frame.origin.y + y;
    }
    return self.view.frame.size.height;
}

- (void)refreshComment:(float)extendHeight {
    CGRect listitemFrame = self.listitemView.frame;
    listitemFrame.size.height += extendHeight;
    self.listitemView.frame = listitemFrame;
    
    CGRect mainFrame = self.view.frame;
    mainFrame.size.height +=extendHeight;
    self.view.frame = mainFrame;
    
    if(self.articleContainer)
    {
        [self.articleContainer refreshView];
    }
}

- (IBAction)replyTouched:(id)sender {
    
    NewComment *comment = [[NewComment alloc] initWithNibName:@"NewComment" bundle:nil];
    comment.postID = self.postID;
    comment.type = COMMENT_ARTICLE;
    comment.articleContainer = self.parentViewController;
    CGRect frame = comment.view.frame;
    frame.origin.y = Y_POPUP_ITEMARTICLE;
    frame.origin.x = (self.parentViewController.view.frame.size.width - frame.size.width)/2;
    comment.view.frame = frame;
    [ClientUtils showBlur:self.parentViewController.view];
    [self.parentViewController.view addSubview:comment.view];
    [self.parentViewController addChildViewController:comment];
    [comment didMoveToParentViewController:self.parentViewController];
    
}

@end
