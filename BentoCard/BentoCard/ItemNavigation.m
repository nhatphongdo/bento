//
//  ItemNavigation.m
//  BentoCard
//
//  Created by VO HONG HAI on 1/29/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import "ItemNavigation.h"
#import "Constrain.h"
#import "TempData.h"
#import "ClientUtils.h"
#import "Navigation.h"
#import "UIImage+DeviceSpecificMedia.h"
#import "LandingViewController.h"
#import "SubTopicViewController.h"
#import "ArticleTopicViewController.h"

@interface ItemNavigation ()
{
    TempData *data;
    NSArray *navigates;
    BOOL isLastItem;
}

@end

@implementation ItemNavigation

- (void)viewDidLoad {
    [super viewDidLoad];
    data = [TempData getInstance];
    [self.folderImage setHidden:YES];
    // Do any additional setup after loading the view from its nib.
    [self.imageView.layer setCornerRadius:5.0f];
    self.letterLabel.text = [self.navitem.Name substringToIndex:1];
    UIImage *image = [UIImage imageWithData:self.navitem.ThumbImage];
    [self.imagebg setImage:image];
    //init when selected or non-seleted navigate card
    Navigation *lastItem = [data.navigations objectAtIndex:data.navigations.count - 1];
    if(self.navitem.Index == lastItem.Index)
    {
        UIImage *bgImage = [UIImage imageForDeviceWithName:@"round_rightclose.png"];
        [self.btClose setImage:bgImage forState:UIControlStateNormal];
        self.btBackground.enabled = NO;
        UIImage *arrowimghover = [UIImage imageForDeviceWithName:@"tab_nav_arrow_hover.png"];
        [self.arrowImage setImage:arrowimghover];
        isLastItem = YES;
        UIImage *folderwhite = [UIImage imageForDeviceWithName:@"icon_nav_folder_white.png"];
        [self.folderImage setImage:folderwhite];
        self.NameLabel.textColor = [UIColor whiteColor];
        [self.NameLabel setHidden:NO];
        [self.letterLabel setHidden:YES];
    }
    else
    {
        UIImage *bgImage = [UIImage imageForDeviceWithName:@"round_grayrightclose.png"];
        [self.btClose setImage:bgImage forState:UIControlStateNormal];
        self.btBackground.enabled = YES;
        isLastItem = NO;
        self.NameLabel.textColor = [UIColor colorWithRed:129.0f/255.0f green:129.0f/255.0f blue:129.0f/255.0f alpha:1];
        UIImage *foldergray = [UIImage imageForDeviceWithName:@"icon_nav_folder.png"];
        [self.folderImage setImage:foldergray];
        [self.NameLabel setHidden:YES];
        [self.letterLabel setHidden:NO];
    }
    
    
    if([self.navitem.Level isEqualToString:NAV_ARTICLE_LEVEL])
    {
        CGRect frame = self.NameLabel.frame;
        frame.size.width = self.arrowView.frame.size.height - (self.folderImage.frame.size.height * 2);
        frame.origin.y = self.arrowView.frame.size.height / 2 - frame.size.height / 2;
        frame.origin.x = (frame.size.width / 2 + self.arrowView.frame.size.width / 2) - frame.size.width;
        self.NameLabel.frame = frame;

    }
    else
    {
        float posX = (self.arrowView.frame.size.width / 2 + self.NameLabel.frame.size.width / 2) - self.NameLabel.frame.size.width;
        CGRect frame = self.NameLabel.frame;
        frame.origin.x = posX;
        self.NameLabel.frame = frame;
//        [self.folderImage setHidden:NO];
    }
    [self.NameLabel setTransform:CGAffineTransformMakeRotation(-M_PI_2)];
    [self.letterLabel setTransform:CGAffineTransformMakeRotation(-M_PI_2)];
    self.NameLabel.text = self.navitem.Name;
    navigates = [[NSArray alloc] init];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    navigates = [self.navigationController viewControllers];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeTouched:(id)sender {
    //todo: remove viewcontroller and update index.
    [ClientUtils removeNavigate:self.navitem.Index];
    UIViewController *currentView = [navigates objectAtIndex:self.navitem.Index];
    [self.navigationController popToViewController:currentView animated:YES];
}

- (IBAction)backgroundTouched:(id)sender {
    if(self.navitem.isNavigation == YES) {
        UIViewController *currentView = [navigates objectAtIndex:self.navitem.Index];
        if(currentView != nil)
        {
            [ClientUtils removeNavigate:self.navitem.Index];
            [self.navigationController popToViewController:currentView animated:YES];
        }
        
    } else {
        
        if([self.navitem.Level isEqualToString:NAV_LANDINGPAGE_LEVEL]) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            LandingViewController *myController = [storyboard instantiateViewControllerWithIdentifier:@"landingview"];
            [self.navigationController pushViewController:myController animated:YES];
        }
        if([self.navitem.Level isEqualToString:NAV_ORDERPAGE_LEVEL_1]) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SubTopicViewController *myController = [storyboard instantiateViewControllerWithIdentifier:@"SubTopicView"];
            myController.topicID = self.navitem.Id;
            myController.viewTitle = @"Article";
            [self.navigationController pushViewController:myController animated:YES];
        }
        if([self.navitem.Level isEqualToString:NAV_ORDERPAGE_LEVEL_2]) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            ArticleTopicViewController *myController = [storyboard instantiateViewControllerWithIdentifier:@"ArticleTopicView"];
            myController.topicID = self.navitem.Id;
            myController.topicName = self.navitem.Name;
            [self.navigationController pushViewController:myController animated:YES];
        }
    }
}

- (void)updateState {
    if([self.state isEqualToString:NAV_NEXT]) {
        [self.NameLabel setHidden:NO];
        [self.letterLabel setHidden:YES];
    }
    else {
        [self.NameLabel setHidden:YES];
        [self.letterLabel setHidden:NO];
    }
//    if (![self.navitem.Level isEqualToString:NAV_ARTICLE_LEVEL] && [self.state isEqualToString:NAV_NEXT]) {
//        [self.folderImage setHidden:NO];
//    } else {
//        [self.folderImage setHidden:YES];
//    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
