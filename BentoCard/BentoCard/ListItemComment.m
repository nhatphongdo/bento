//
//  ListItemComment.m
//  BentoCard
//
//  Created by VO HONG HAI on 12/19/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import "ListItemComment.h"
#import "Constrain.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <QuartzCore/QuartzCore.h>
#import "ReplyItem.h"
#import "NewComment.h"

@interface ListItemComment ()
{
}

@end

static int const GAP = 10;

@implementation ListItemComment

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.commentAvatarImage.layer.cornerRadius = IMAGE_ROUND_CORNER_RADIUS;
    self.commentAvatarImage.clipsToBounds = YES;
    [self.commentAvatarImage setImageWithURL:[NSURL URLWithString:self.avatar]];
    self.commentNameLabel.text = self.name;
    self.commentBodyLabel.text = self.body;
    [self.commentBodyLabel sizeToFit];
    [self.likeButton setTitle:[NSString stringWithFormat:@"%d",self.likecount] forState:normal];
    if(self.liked == YES)
    {
        [self.likeButton setImage:[UIImage imageNamed:@"iconcommentliked"] forState:UIControlStateNormal];
    }
    else
    {
        [self.likeButton setImage:[UIImage imageNamed:@"iconcommentlike"] forState:UIControlStateNormal];
    }
    
    float posy = self.commentBodyLabel.frame.origin.y + self.commentBodyLabel.frame.size.height;
    CGRect controlFrame = self.controlView.frame;
    controlFrame.origin.y = posy;
    self.controlView.frame = controlFrame;
    
    CGRect replyFrame = self.repliesView.frame;
    replyFrame.origin.y = posy + controlFrame.size.height;
    self.repliesView.frame = replyFrame;
    
    CGRect mainFrame = self.view.frame;
    mainFrame.size.height = replyFrame.size.height + replyFrame.origin.y + GAP;
    self.view.frame = mainFrame;
    
    self.showreplyButton.tag = self.index;
    self.replyButton.tag = self.index;
}

- (void)clearReply {
    //clear old listitem
    NSArray *oldViews = [self.repliesView subviews];
    for (UIView *item in oldViews)
    {
        [item removeFromSuperview];
    }
    CGRect frame = self.repliesView.frame;
    frame.size.height = 1;
    self.repliesView.frame = frame;
}

- (float)loadReply:(NSMutableArray *)listitem {
    if(listitem.count > 0)
    {
        [self clearReply];
        float y = GAP;
        for (CommentModel *reply in listitem) {
            ReplyItem *replyView = [[ReplyItem alloc] initWithNibName:@"ReplyItem" bundle:nil];
            replyView.replyID = reply.ID;
            replyView.name = reply.Username;
            replyView.body = reply.Message;
            replyView.avatar = reply.Avatar;
            replyView.liked = reply.Liked;
            replyView.likecount = reply.Likes;
            CGRect frame = replyView.view.frame;
            frame.origin.y = y;
            frame.size.width = self.repliesView.frame.size.width;
            replyView.view.frame = frame;
            [self addChildViewController:replyView];
            [self.repliesView addSubview:replyView.view];
            [replyView didMoveToParentViewController:self];
            y+= replyView.view.frame.size.height;
        }
        y+=GAP;
        CGRect frame = self.repliesView.frame;
        frame.size.height = y;
        self.repliesView.frame = frame;
        CGRect mainFrame = self.view.frame;
        mainFrame.size.height = frame.origin.y + y;
        self.view.frame = mainFrame;
        
        if(self.index < self.total - 1)
        {
            [self addBottomLine];
        }
        return y;
    }
    return 0.0f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showReplyButton:(id)sender {
    UIButton *button = (UIButton *)sender;
    NSString *title = button.currentTitle;
    int initIndex = (int)button.tag + 1;
    CommentModel *comment = [self.containerViewController.comments objectAtIndex:button.tag];
    if([title isEqualToString:@"Show Replies"])
    {
        NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
        NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
        [ServiceClient getRepliesOfComment:self.containerViewController.parentViewController.view username:username  md5Password:password postID:comment.ID page:1 itemsPerPage:9999 successBlock:^(id responseObject) {
            NSMutableArray *listreplies = responseObject;
            float heightofreply = [self loadReply:listreplies];
            [self updatePositionItemComment:heightofreply index:initIndex];

            if(self.containerViewController)
            {
                [self.containerViewController refreshComment:heightofreply];
            }
            
            [button setTitle:@"Hidden Replies" forState:UIControlStateNormal];

        } failureBlock:^(NSError *error) {
        }];
    }
    else if([title isEqualToString:@"Hidden Replies"])
    {
        NSArray *subViews = [self.repliesView subviews];
        float totalHeight = self.repliesView.frame.size.height;
        float extendHeight = 0;
        if(totalHeight < 10)
        {
            extendHeight = totalHeight = 0;
        }
        else
        {
            extendHeight = totalHeight - GAP;
        }
        for(UIView *view in subViews)
        {
            [view removeFromSuperview];
        }
        CGRect frame = self.repliesView.frame;
        frame.size.height = 1;
        self.repliesView.frame = frame;
        
        CGRect mainFrame = self.view.frame;
        mainFrame.size.height -= (extendHeight);
        self.view.frame = mainFrame;
        if(self.index < self.total - 1)
        {
            [self addBottomLine];
        }
        [self updatePositionItemComment:(-totalHeight) index:initIndex];
        if(self.containerViewController)
        {
            [self.containerViewController refreshComment:(-totalHeight)];
        }
        [button setTitle:@"Show Replies" forState:UIControlStateNormal];
    }
    
}

- (void)reloadReplyShow:(int)index {
    
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    int initIndex = index + 1;
    //get old height reply
    float oldreplyHeight = self.repliesView.frame.size.height;
    
    CommentModel *comment = [self.containerViewController.comments objectAtIndex:index];
    [ServiceClient getRepliesOfComment:self.containerViewController.parentViewController.view username:username  md5Password:password postID:comment.ID page:1 itemsPerPage:9999 successBlock:^(id responseObject) {
        NSMutableArray *listreplies = responseObject;
        float heightofreply = [self loadReply:listreplies];
        float extentHeight = heightofreply - oldreplyHeight;
        [self updatePositionItemComment:extentHeight index:initIndex];
        if(self.containerViewController)
        {
            [self.containerViewController refreshComment:extentHeight];
        }
        
        [self.showreplyButton setTitle:@"Hidden Replies" forState:UIControlStateNormal];
        
    } failureBlock:^(NSError *error) {
    }];
}

- (void)updatePositionItemComment:(float)extend index:(int)index {
    NSArray *viewArray = [self.containerViewController.listitemView subviews];
    for (int i = index; i < viewArray.count; i++) {
        UIView *parentComment = [viewArray objectAtIndex:i];
        CGRect frame = parentComment.frame;
        frame.origin.y += extend;
        parentComment.frame = frame;
    }
}

- (void)addBottomLine {
    //remove line bottom
    NSArray *subView = [self.view subviews];
    UIView *linebottom = [subView objectAtIndex:subView.count-1];
    [linebottom removeFromSuperview];
    //add line bottom
    UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 1.0f, self.view.frame.size.width, 1)];
    bottomBorder.backgroundColor = [UIColor colorWithRed:190/255.0f green:190/255.0f blue:190/255.0f alpha:1];
    [self.view addSubview:bottomBorder];
}

- (IBAction)likeButtonTouched:(id)sender {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    UIViewController *parentView = self.containerViewController.parentViewController;
    if (self.liked == YES) {
        // Unlike
        self.liked = NO;
        [ServiceClient unlikePost:parentView.view username:username md5Password:password postID:self.commentID successBlock:^(id responseObject) {
            // UnLike article succeeded
            [self.likeButton setTitle:[NSString stringWithFormat:@"%@", responseObject] forState:UIControlStateNormal];
            [self.likeButton setImage:[UIImage imageNamed:@"iconcommentlike"] forState:UIControlStateNormal];
        } failureBlock:^(NSError *error) {
        }];
    }
    else {
        // Like
        self.liked = YES;
        [ServiceClient likePost:parentView.view username:username md5Password:password postID:self.commentID successBlock:^(id responseObject) {
            // Like article succeeded
            [self.likeButton setTitle:[NSString stringWithFormat:@"%@", responseObject] forState:UIControlStateNormal];
            [self.likeButton setImage:[UIImage imageNamed:@"iconcommentliked"] forState:UIControlStateNormal];
        } failureBlock:^(NSError *error) {
        }];
    }
}

- (IBAction)replyButtonTouched:(id)sender {
    UIButton *button = (UIButton *)sender;
    UIViewController *mainViewController = self.parentViewController.parentViewController;
    NewComment *reply = [[NewComment alloc] initWithNibName:@"NewComment" bundle:nil];
    reply.type = REPLY_COMMENT;
    reply.postID = self.commentID;
    reply.index = button.tag;
    reply.commentContainer = self;
    reply.articleContainer = mainViewController;
    CGRect frame = reply.view.frame;
    frame.origin.y = Y_POPUP_ITEMARTICLE;
    frame.origin.x = (mainViewController.view.frame.size.width - frame.size.width)/2;
    reply.view.frame = frame;
    [ClientUtils showBlur:mainViewController.view];
    [mainViewController.view addSubview:reply.view];
    [mainViewController addChildViewController:reply];
    [reply didMoveToParentViewController:mainViewController];
}


@end
