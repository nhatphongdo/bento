//
//  CommentItemArticle.h
//  BentoCard
//
//  Created by VO HONG HAI on 12/18/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleViewController.h"

@interface CommentItemArticle : UIViewController
{
}

@property (nonatomic, strong) ArticleViewController *articleContainer;
@property (nonatomic, strong) NSMutableArray *comments;
@property (nonatomic, strong) NSString *postID;

@property (nonatomic, strong) IBOutlet UIButton *collapseButton;
@property (nonatomic, strong) IBOutlet UIButton *addCommentButton;
@property (nonatomic, strong) IBOutlet UIButton *replyButton;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UIView *listitemView;

- (float)loadComment:(NSMutableArray *)listitem;
- (void)refreshComment:(float)extendHeight;
- (IBAction)replyTouched:(id)sender;

@end
