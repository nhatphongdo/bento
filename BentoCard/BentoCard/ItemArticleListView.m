//
//  ItemArticleListView.m
//  BentoCard
//
//  Created by VO HONG HAI on 12/15/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import "ItemArticleListView.h"
#import "PKRArticleViewController.h"
#import "ClientUtils.h"
#import "InterestModel.h"

@interface ItemArticleListView ()

@end

@implementation ItemArticleListView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.titleLabel.text = self.article.Title;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)alvoverlayTouched:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PKRArticleViewController *myview = [storyboard instantiateViewControllerWithIdentifier:@"ArticleView"];
    myview.postID = self.article.ID;
    myview.likes = self.article.Likes;
    myview.liked = self.article.Liked;
    myview.topicName = self.article.Category;
    InterestModel *topic = [ClientUtils getInterestByName:self.article.Category];
    if(topic != nil) {
        myview.topicID = topic.ID;
    }
    [self.navigationController pushViewController:myview animated:YES];
}

@end
