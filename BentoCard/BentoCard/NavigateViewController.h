//
//  NavigateViewController.h
//  BentoCard
//
//  Created by VO HONG HAI on 1/29/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigateViewController : UIViewController

@property (nonatomic,strong) NSString *pageName;

@property (nonatomic,strong) IBOutlet UIButton *btDone;
@property (nonatomic,strong) IBOutlet UIButton *btNav;
@property (nonatomic,strong) IBOutlet UIButton *btManage;
@property (nonatomic,strong) IBOutlet UIButton *btArticle;
@property (nonatomic,strong) IBOutlet UIButton *btSocial;
@property (nonatomic,strong) IBOutlet UIButton *btSponsor;
@property (nonatomic,strong) IBOutlet UIScrollView *containerScrollview;

- (void)loadCard;
- (void)navigateselected:(NSString *)seletedPage;
- (IBAction)doneTouched:(id)sender;
- (IBAction)navTouched:(id)sender;
- (IBAction)manageTouched:(id)sender;
- (IBAction)articleTouched:(id)sender;
- (IBAction)socialTouched:(id)sender;
- (IBAction)sponsorTouched:(id)sender;

@end
