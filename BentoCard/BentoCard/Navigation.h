//
//  Navigation.h
//  BentoCard
//
//  Created by VO HONG HAI on 1/28/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Navigation : NSObject

@property (nonatomic,strong) NSString *Id;
@property (nonatomic,strong) NSData *ThumbImage;
@property (nonatomic,strong) NSString *Name;
@property (nonatomic,strong) NSString *Level;
@property (nonatomic,strong) NSString *ParentId; //parent topic id
@property (nonatomic) int Index;
@property (nonatomic) BOOL isNavigation;

@end
