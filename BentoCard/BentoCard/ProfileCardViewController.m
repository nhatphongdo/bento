//
//  ProfileCardViewController.m
//  BentoCard
//
//  Created by VO HONG HAI on 3/5/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import "ProfileCardViewController.h"

@interface ProfileCardViewController ()

@end

@implementation ProfileCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.viewmoreButton.layer setBorderWidth:1.0f];
    [self.viewmoreButton.layer setBorderColor:[UIColor colorWithRed:174.0f/255.0f green:174.0f/255.0f blue:174.0f/255.0f alpha:1.0f].CGColor];
    [self.viewmoreButton.layer setCornerRadius:2.0f];
    [self loadData:self.profile title:self.titleCard];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadData:(ProfileModel *)profile title:(NSString *)title {
    self.profile = profile;
    self.titleCard = title;
    [self.titleCardButton setTitle:self.titleCard forState:UIControlStateNormal];
    self.titleLabel.text = self.profile.Fullname;
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
    [dateformatter setDateStyle:NSDateFormatterMediumStyle];
    [dateformatter setTimeStyle:NSDateFormatterNoStyle];
    NSString *dateStr = [dateformatter stringFromDate:self.profile.BirthDay];
    [self.datetimeButton setTitle:dateStr forState:UIControlStateNormal];
    
    self.descTextView.text = self.profile.Caption;
    [self.addressButton setTitle:self.profile.Address forState:UIControlStateNormal];
    self.descTextView.textAlignment = NSTextAlignmentJustified;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
