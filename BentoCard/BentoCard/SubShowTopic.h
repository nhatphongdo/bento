//
//  SubShowTopic.h
//  BentoCard
//
//  Created by VO HONG HAI on 1/23/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InterestModel.h"

@interface SubShowTopic : UIViewController

@property (nonatomic,strong) InterestModel *interest;

@property (nonatomic,strong) IBOutlet UILabel *nameLabel;
@property (nonatomic,strong) IBOutlet UILabel *countLabel;
@property (nonatomic,strong) IBOutlet UIButton *ssv_overlayButton;

- (IBAction)ssv_overlayTouched:(id)sender;

@end
