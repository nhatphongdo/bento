//
//  ItemArticleMagView.m
//  BentoCard
//
//  Created by VO HONG HAI on 12/15/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import "ItemArticleMagView.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "PKRArticleViewController.h"
#import "ClientUtils.h"
#import "InterestModel.h"

@interface ItemArticleMagView ()

@end

@implementation ItemArticleMagView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setData:(ArticleModel *)item {
    self.article = item;
    self.sponsorLabel.text = self.article.Author;
    self.titleLabel.text = self.article.Title;
    self.descLabel.text = self.article.ShortDescription;
    [self.thumbnailImage setImageWithURL:[NSURL URLWithString:self.article.Thumbnail]];
}

- (IBAction)overlayTouched:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PKRArticleViewController *myview = [storyboard instantiateViewControllerWithIdentifier:@"ArticleView"];
    myview.postID = self.article.ID;
    myview.likes = self.article.Likes;
    myview.liked = self.article.Liked;
    myview.topicName = self.article.Category;
    InterestModel *topic = [ClientUtils getInterestByName:self.article.Category];
    if(topic != nil) {
        myview.topicID = topic.ID;
    }
    [self.navigationController pushViewController:myview animated:YES];
}

@end
