//
//  NewNote.h
//  BentoCard
//
//  Created by VO HONG HAI on 1/5/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleViewController.h"

@interface NewNote : UIViewController

@property (nonatomic,strong) ArticleViewController *articleContainer;

@property (nonatomic,strong) NSString *postID;

@property (nonatomic,strong) IBOutlet UITextField *txtNote;
@property (nonatomic,strong) IBOutlet UIButton *btNote;
@property (nonatomic,strong) IBOutlet UIButton *btClose;

-(IBAction)SubmitNote:(id)sender;
-(IBAction)closeNote:(id)sender;

@end
