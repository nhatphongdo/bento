//
//  Constrain.h
//  BentoCard
//
//  Created by VO HONG HAI on 12/11/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#ifndef BentoCard_Constrain_h
#define BentoCard_Constrain_h

#define TOPIC_CARD_TYPE_KEY @"TOPICCARDTYPE"
#define ARTICLE_CARD_TYPE_KEY @"ARTICLECARDTYPE"
#define SUBTOPIC_CARD_TYPE_KEY @"SUBTOPICCARDTYPE"

#define APPLICATION_TITLE @"BENTO"
#define GOOGLE_PLUS_ID @"402410395378-t71h8ue4pjdmusmuogihgocpl7rm1jmu.apps.googleusercontent.com"

#define USERNAME_KEY @"UserNameKey"
#define MD5_PASSWORD_KEY @"Md5PasswordKey"
#define TEMPGG_EMAIL_KEY @"TempGGEmailKey"
#define TEMPGG_PASSWORD_KEY @"TempGGPasswordKey"
#define TEMPFB_EMAIL_KEY @"TempFBEmailKey"
#define TEMPFB_PASSWORD_KEY @"TempFBPasswordKey"
#define FULLNAME_KEY @"FullNameKey"
#define AVATAR_KEY @"AvatarKey"
#define GENDER_KEY @"GenderKey"
#define ACCOUNT_TYPE_KEY @"AccountTypeKey"
#define DATE_OF_BIRTH_KEY @"DateOfBirthKey"
#define EMAIL_KEY @"EmailKey"
#define CAPTION_KEY @"CaptionKey"

#define ROOT_API_URL @"http://bentoportal.com/portal/api/"
#define SIGN_IN_URL [ROOT_API_URL stringByAppendingString:@"accounts/signin"]
#define SIGN_OUT_URL [ROOT_API_URL stringByAppendingString:@"accounts/signout"]
#define SIGN_UP_URL [ROOT_API_URL stringByAppendingString:@"accounts/signup"]
#define FORGOT_PASSWORD_URL [ROOT_API_URL stringByAppendingString:@"accounts/forgot"]
#define GET_PROFILE_URL [ROOT_API_URL stringByAppendingString:@"accounts/profile"]
#define UPDATE_PROFILE_URL [ROOT_API_URL stringByAppendingString:@"accounts/update"]
#define GET_USER_INTERESTS_URL [ROOT_API_URL stringByAppendingString:@"accounts/interests"]
#define SET_USER_INTERESTS_URL [ROOT_API_URL stringByAppendingString:@"accounts/interests/update"]
#define UPLOAD_AVATAR_URL [ROOT_API_URL stringByAppendingString:@"accounts/update/avatar"] //add binary file to this

#define GET_INTERESTS_URL [ROOT_API_URL stringByAppendingString:@"interests"]
#define GET_INTERESTS_BY_PARENTID_URL [ROOT_API_URL stringByAppendingString:@"interests/%@/children"]
#define GET_ARTICLES_OF_SUBTOPICVIEW_URL [ROOT_API_URL stringByAppendingString:@"interests/%@/articlesubtopics"]
#define GET_STRUCTURE_INTERESTS_URL [ROOT_API_URL stringByAppendingString:@"interests/structure"]
#define GET_STRUCTURE_FAVORITE_URL [ROOT_API_URL stringByAppendingString:@"interests/structure/favorite"]
#define GET_STRUCTURE_USER_INTERESTS_URL [ROOT_API_URL stringByAppendingString:@"interests/structure/user"]
#define GET_ARTICLES_OF_INTEREST_URL [ROOT_API_URL stringByAppendingString:@"interests/%@/articles"]
#define GET_MYARTICLES_OF_SUBTOPICVIEW_URL [ROOT_API_URL stringByAppendingString:@"interests/%@/myarticlesubtopics"]
#define GET_USER_ARTICLES_OF_INTEREST_URL [ROOT_API_URL stringByAppendingString:@"interests/%@/myarticles"]
#define GET_FAVORITE_ARTICLE_URL [ROOT_API_URL stringByAppendingString:@"interests/%@/myfavorites"]

#define GET_LATEST_ARTICLES_URL [ROOT_API_URL stringByAppendingString:@"articles/latest"]
#define GET_DETAIL_ARTICLE_URL [ROOT_API_URL stringByAppendingString:@"articles/%@"]
#define GET_COMMENTS_OF_ARTICLE_URL [ROOT_API_URL stringByAppendingString:@"articles/%@/comments"]
#define GET_REPLIES_OF_COMMENT_URL [ROOT_API_URL stringByAppendingString:@"comments/%@/replies"]
#define POST_COMMENT_URL [ROOT_API_URL stringByAppendingString:@"articles/%@/comments/post"]
#define POST_REPLY_URL [ROOT_API_URL stringByAppendingString:@"comments/%@/reply"]
#define GET_NOTES_OF_ARTICLE_URL [ROOT_API_URL stringByAppendingString:@"articles/%@/notes"]
#define POST_NOTE_FOR_ARTICLE_URL [ROOT_API_URL stringByAppendingString:@"articles/%@/notes/post"]
#define DELETE_NOTE_URL [ROOT_API_URL stringByAppendingString:@"notes/%@/delete"]
#define GET_SHARES_OF_ARTICLE_URL [ROOT_API_URL stringByAppendingString:@"articles/%@/shares"]
#define GET_LIKES_OF_ARTICLE_URL [ROOT_API_URL stringByAppendingString:@"articles/%@/likes"]
#define LIKE_POST_URL [ROOT_API_URL stringByAppendingString:@"action/%@/like"]
#define UNLIKE_POST_URL [ROOT_API_URL stringByAppendingString:@"action/%@/unlike"]
#define SHARE_POST_URL [ROOT_API_URL stringByAppendingString:@"action/%@/share"]
#define FAVORITE_POST_URL [ROOT_API_URL stringByAppendingString:@"action/%@/favorite"]
#define UNFAVORITE_POST_URL [ROOT_API_URL stringByAppendingString:@"action/%@/unfavorite"]
#define REPORT_POST_URL [ROOT_API_URL stringByAppendingString:@"articles/%@/reports/post"]

#define GET_SPONSORS_URL [ROOT_API_URL stringByAppendingString:@"sponsors"]
#define GET_SPONSOR_DETAIL_URL [ROOT_API_URL stringByAppendingString:@"sponsors/%@"]

#define SEARCH_ARTICLES_URL [ROOT_API_URL stringByAppendingString:@"articles/search"]
#define SEARCH_MY_ARTICLES_URL [ROOT_API_URL stringByAppendingString:@"articles/mysearch"]

#define MARGIN_TOP_PAGEVIEW 20
#define IMAGE_ROUND_CORNER_RADIUS 22
#define Y_POPUP_ITEMARTICLE 92
#define COMMENT_ARTICLE @"COMMENT"
#define REPLY_COMMENT @"REPLY"
#define CONTENT_REPORT @"1"
#define TECHNICAL_REPORT @"2"
#define OTHER_REPORT @"3"
#define SIZE_SIDEBAR 280.0f

#define TOPICS_DATA_CACHE @"alltopics"
#define NAV_PARENTID_NIL @"parentid_nil"
#define NAV_ARTICLE @"nav_article"
#define NAV_MANAGE @"nav_manage"
#define NAV_SOCIAL @"nav_social"
#define NAV_SPONSOR @"nav_sponsor"
#define NAV_LANDINGPAGE_LEVEL @"1"
#define NAV_ORDERPAGE_LEVEL_1 @"2"
#define NAV_ORDERPAGE_LEVEL_2 @"3"
#define NAV_ARTICLE_LEVEL @"4"
#define NAV_PREV @"prev"
#define NAV_NEXT @"next"

#endif
