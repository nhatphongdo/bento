//
//  PKRArticleViewController.m
//  BentoCard
//
//  Created by VO HONG HAI on 3/10/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import "PKRArticleViewController.h"
#import "Constrain.h"
#import "ClientUtils.h"
#import "ArticleViewController.h"
#import "ArticleSideBarViewController.h"

@interface PKRArticleViewController ()

@end

@implementation PKRArticleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ArticleViewController *page = [storyboard instantiateViewControllerWithIdentifier:@"articleitemview"];
    page.postID = self.postID;
    page.topicID = self.topicID;
    page.topicName = self.topicName;
    page.liked = self.liked;
    page.likes = self.likes;
    CGRect frame = page.view.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    frame.size.width = self.view.frame.size.width;
    frame.size.height = self.view.frame.size.height;
    page.view.frame = frame;
    
    ArticleSideBarViewController *leftview_article  = [[ArticleSideBarViewController alloc] initWithNibName:@"ArticleSideBarViewController" bundle:nil];
    [self setFrontViewController:page];
    [self setLeftViewController:leftview_article];
    
    self.recognizesPanningOnFrontView = NO;
    self.recognizesResetTapOnFrontView = NO;
    self.delegate = self;
    [page loadContent];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)revealController:(PKRevealController *)revealController didChangeToState:(PKRevealControllerState)state {
    [revealController setMinimumWidth:SIZE_SIDEBAR maximumWidth:SIZE_SIDEBAR forViewController:revealController.focusedController];
    if (revealController.focusedController == revealController.leftViewController) {
        [revealController.focusedController.view setFrame:CGRectMake(0, 0, SIZE_SIDEBAR, self.view.frame.size.height)];
    }
}


@end
