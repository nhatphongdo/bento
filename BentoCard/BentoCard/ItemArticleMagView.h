//
//  ItemArticleMagView.h
//  BentoCard
//
//  Created by VO HONG HAI on 12/15/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleModel.h"

@interface ItemArticleMagView : UIViewController

@property (nonatomic,strong) ArticleModel *article;

@property (nonatomic,strong) IBOutlet UIImageView *thumbnailImage;
@property (nonatomic,strong) IBOutlet UILabel *titleLabel;
@property (nonatomic,strong) IBOutlet UILabel *sponsorLabel;
@property (nonatomic,strong) IBOutlet UILabel *descLabel;
@property (nonatomic,strong) IBOutlet UIButton *overlayButton;

- (void)setData:(ArticleModel *)item;
- (IBAction)overlayTouched:(id)sender;

@end
