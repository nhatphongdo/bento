//
//  NewReport.m
//  BentoCard
//
//  Created by VO HONG HAI on 1/5/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import "NewReport.h"
#import "Constrain.h"
#import "ClientUtils.h"
#import "ServiceClient.h"

@interface NewReport ()
{
    NSString *type;
}
@end

@implementation NewReport

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.cbContent.text = @"Content";
    self.cbOther.text = @"Other";
    self.cbTechnical.text = @"Technical";
    self.txtMessage.layer.borderWidth = 1.0f;
    self.txtMessage.layer.borderColor = [[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor];
    self.txtMessage.layer.cornerRadius = 5;
    self.txtMessage.clipsToBounds = YES;
    self.txtMessage.editable = YES;
    self.txtMessage.delegate = self;
    type = CONTENT_REPORT;
    self.cbContent.checked = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeTouched:(id)sender {
    [self.view removeFromSuperview];
    [ClientUtils removeBlur:self.parentViewController.view];
    if(self.articleContainer)
    {
        [self.articleContainer.exclButton setImage:[UIImage imageNamed:@"iconexclam"] forState:UIControlStateNormal];
    }
    [self willMoveToParentViewController:nil];
    [self removeFromParentViewController];
}

- (IBAction)reportTouched:(id)sender {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *md5Password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    if([self.txtMessage.text isEqualToString:@""] || self.txtMessage.text == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"error" message:@"Please input infomation" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        alert = nil;
        [self.txtMessage  becomeFirstResponder];
        return;
    }
    
    if(self.cbContent.checked == NO && self.cbTechnical.checked == NO && self.cbOther.checked == NO)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"error" message:@"Please choose type of problem" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        alert = nil;
        return;
    }
    
    [ServiceClient reportsArticle:self.view username:username md5Password:md5Password postID:self.postID message:self.txtMessage.text type:type successBlock:^(id responseObject) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APPLICATION_TITLE message:@"Your report has been send." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        alert = nil;
    
        [self.view removeFromSuperview];
        [ClientUtils removeBlur:self.parentViewController.view];
        if(self.articleContainer)
        {
            [self.articleContainer.exclButton setImage:[UIImage imageNamed:@"iconexclam"] forState:UIControlStateNormal];
        }
        [self willMoveToParentViewController:nil];
        [self removeFromParentViewController];
        
    } failureBlock:^(NSError *error) {
    }];
}

- (IBAction)selectcbContent:(id)sender {
    UICheckbox *cb = (UICheckbox *)sender;
    if(cb.checked == YES)
    {
        type=CONTENT_REPORT;
        self.cbTechnical.checked = NO;
        self.cbOther.checked = NO;
    }
}

- (IBAction)selectcbTechnical:(id)sender {
    UICheckbox *cb = (UICheckbox *)sender;
    if(cb.checked == YES)
    {
        type=TECHNICAL_REPORT;
        self.cbContent.checked = NO;
        self.cbOther.checked = NO;
    }
}

- (IBAction)selectcbOther:(id)sender {
    UICheckbox *cb = (UICheckbox *)sender;
    if(cb.checked == YES)
    {
        type=OTHER_REPORT;
        self.cbTechnical.checked = NO;
        self.cbContent.checked = NO;
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text
{
    
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        
        return FALSE;
    }
    return TRUE;
}

@end
