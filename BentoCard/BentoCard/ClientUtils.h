//
//  ClientUtils.h
//  BentoCard
//
//  Created by VO HONG HAI on 12/17/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "InterestModel.h"
#import "Navigation.h"

@interface ClientUtils : NSObject

+ (void) saveSetting:(NSString *)name value:(id)value;
+ (id) loadSetting:(NSString *)name;
+ (void) deleteSetting:(NSString *)name;

+ (NSString *) encryptMd5:(NSString *)input;

+ (BOOL)validateEmail:(NSString *)emailStr;
+ (NSString *)urlEncode:(NSString *)input;
+ (NSString *)getGender:(NSInteger *)index;
+ (UIImage *)captureView:(UIView *)view;
+ (void)showBlur:(UIView *)currentview;
+ (void)removeBlur:(UIView *)view;
+ (void)saveUsernamePwd;
+ (void)initializeWidthScreen:(UIView *)view;
+ (InterestModel *)getParentInterest:(NSString *)topicId;
+ (InterestModel *)getInterestByName:(NSString *)name;
+ (NSString *)getTopicNameById:(NSString *)topicId;
+ (int)getLevelByTopicId:(NSString *)topicId;
+ (CGSize)getSizeImage:(NSString *)imgName;
+ (NSArray *)getImages:(NSString *)htmlContent baseUrl:(NSString *)baseUrl;
+ (void)SaveNavigation:(UIViewController *)controller Name:(NSString *)Name level:(NSString *)level ParentId:(NSString *)ParentId isSameView:(BOOL)isSameView;
+ (Navigation *)LoadNavigation:(int)index;
+ (void)removeNavigate:(int)index;

@end

@interface UIImageView (NetworkImage)

- (void) setUrl:(NSString *)url;

@end
