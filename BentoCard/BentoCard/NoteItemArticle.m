//
//  NoteItemArticle.m
//  BentoCard
//
//  Created by VO HONG HAI on 12/18/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import "NoteItemArticle.h"
#import "ListItemNote.h"
#import "Constrain.h"
#import "NoteModel.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import "NewNote.h"

@interface NoteItemArticle ()
{
    float initScreenWidth;
}
@end

//static float const navbackground = 41.0f + 4.0f; //41 of nav, 4 of padding botton of image --> view

@implementation NoteItemArticle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    initScreenWidth = self.view.frame.size.width;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (float)loadNotes:(NSMutableArray *)listitem {
    if (listitem.count > 0) {
        [self.blankLabel setHidden:YES];
        int y = 0;
        int count= 0;
        [[self.listitemView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        for(NoteModel *item in listitem)
        {
            ListItemNote *viewItem = [[ListItemNote alloc] initWithNibName:@"ListItemNote" bundle:nil];
            viewItem.note = item.Message;
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
            [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
            NSString *datetimeformatted = [dateFormatter stringFromDate:item.DateCreated];
            viewItem.datetime = datetimeformatted;
            CGRect frame = viewItem.view.frame;
            frame.origin.x = 0;
            frame.origin.y = y;
            frame.size.width = self.listitemView.frame.size.width;
            viewItem.view.frame = frame;
            if(count < listitem.count - 1)
            {
                UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, viewItem.view.frame.size.height - 1.0f, viewItem.view.frame.size.width, 1)];
                bottomBorder.backgroundColor = [UIColor colorWithRed:190/255.0f green:190/255.0f blue:190/255.0f alpha:1];
                [viewItem.view addSubview:bottomBorder];
            }
            [self.listitemView addSubview:viewItem.view];
            count++;
            y+=viewItem.view.frame.size.height;
        }
        self.listitemView.frame = CGRectMake(self.listitemView.frame.origin.x, self.listitemView.frame.origin.y, self.listitemView.frame.size.width, y);
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.listitemView.frame.origin.y + y);
        return self.view.frame.size.height;
    }
    else {
        [self.blankLabel setHidden:NO];
    }
    return self.view.frame.size.height;
}

- (int)getHeight {
    return self.view.frame.size.height;
}

- (IBAction)addNoteTouched:(id)sender {
    NewNote *newNote = [[NewNote alloc] initWithNibName:@"NewNote" bundle:nil];
    newNote.postID = self.postID;
    newNote.articleContainer = self.parentViewController;
    
    [ClientUtils showBlur:self.parentViewController.view];
    
    CGRect frame = newNote.view.frame;
    frame.origin.y = Y_POPUP_ITEMARTICLE;
    frame.origin.x = (self.view.frame.size.width - frame.size.width)/2;
    newNote.view.frame = frame;
    [self.parentViewController addChildViewController:newNote];
    [self.parentViewController.view addSubview:newNote.view];
    [newNote didMoveToParentViewController:self.parentViewController];
}

@end
