//
//  TempData.h
//  BentoCard
//
//  Created by VO HONG HAI on 1/16/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProfileModel.h"

@interface TempData : NSObject {
    NSMutableArray *interests;
    NSMutableArray *userinterests;
    NSMutableArray *navigations;
    NSString *State;
}

@property (nonatomic,retain) NSMutableArray *interests;
@property (nonatomic,retain) NSMutableArray *userinterests;
@property (nonatomic,retain) NSMutableArray *navigations;
@property (nonatomic,retain) NSString *State;
+ (TempData *)getInstance;

@end
