//
//  PKRArticleViewController.h
//  BentoCard
//
//  Created by VO HONG HAI on 3/10/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PKRevealController/PKRevealController.h>

@interface PKRArticleViewController : PKRevealController<PKRevealing>

@property (nonatomic,strong) NSString *postID;
@property (nonatomic,strong) NSString *topicID;
@property (nonatomic,strong) NSString *topicName;
@property (nonatomic) BOOL liked;
@property (nonatomic) int likes;

@end
