//
//  CommentModel.h
//  Bento
//
//  Created by Do Nhat Phong on 9/29/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommentModel : NSObject

@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) NSString *Message;
@property (strong, nonatomic) NSString *ProfileID;
@property (strong, nonatomic) NSString *Username;
@property (strong, nonatomic) NSString *Fullname;
@property (strong, nonatomic) NSString *Avatar;
@property (strong, nonatomic) NSDate *DateCreated;
@property (nonatomic) int Likes;
@property (nonatomic) BOOL Liked;

@end
