//
//  SocialPageViewController.m
//  BentoCard
//
//  Created by VO HONG HAI on 3/5/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import "SocialPageViewController.h"
#import "Constrain.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import "UIImage+DeviceSpecificMedia.h"
#import "TempData.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

@interface SocialPageViewController () {
    BOOL isSlideUp,atmaxitem,isLoading;
    int currentPage;
    UIButton *activeButton;
}

@end

static const float GAP = 10.0f;
static const float ExtentGAP = 5;
static const float duration = 0.5f;
static const float menuHoverWidth = 35.0f;
static const int collapseHeight = 126;
static const int itemPerPage = 10;

@implementation SocialPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.containerScrollView.delegate = self;
    isSlideUp = NO;
    currentPage = 1;
    atmaxitem = NO;
    isLoading = NO;
    
    self.avatarImage.layer.masksToBounds = YES;
    self.avatarImage.layer.cornerRadius = 40.0f;
    self.avatarImage.clipsToBounds = YES;
    NSString *avatarUrl = [ClientUtils loadSetting:AVATAR_KEY];
    [self.avatarImage setImageWithURL:[NSURL URLWithString:avatarUrl]];
    [self.avatarImage setHidden:NO];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView == self.containerScrollView)
    {
        if(scrollView.contentOffset.y > 50 && isSlideUp == NO)
        {
            isSlideUp = YES;
            [UIView animateWithDuration:duration animations:^{
                self.wrapthumbView.frame = CGRectMake(0, self.statusViews.frame.origin.y, self.view.frame.size.width, self.wrapthumbView.frame.size.height - collapseHeight);
                self.cricleImage.hidden = YES;
                self.avatarImage.hidden = YES;
                [self.view bringSubviewToFront:self.statusViews];
                [self.statusViews setBackgroundColor:[UIColor clearColor]];
                //todo: set scrollview move up
                CGRect frame = scrollView.frame;
                frame.origin.y = self.wrapthumbView.frame.origin.y + self.wrapthumbView.frame.size.height - ExtentGAP;
                frame.size.height += (collapseHeight + self.statusViews.frame.size.height);
                scrollView.frame = frame;
                CGRect menuFrame = self.menuScrollView.frame;
                menuFrame.size.width = self.wrapthumbView.frame.size.width;
                menuFrame.origin.x = self.wrapthumbView.frame.size.width - menuFrame.size.width;
                menuFrame.origin.y = self.statusViews.frame.size.height - GAP;
                self.menuScrollView.frame = menuFrame;
                
                if(!self.btBackRight.isHidden)
                {
                    //todo: set button move
                    CGRect btrightFrame = self.btStatusRight.frame;
                    CGRect btbackFrame = self.btBackRight.frame;
                    btrightFrame.origin.x -= btbackFrame.size.width;
                    self.btStatusRight.frame = btrightFrame;
                    
                    btbackFrame.origin.y -= (self.statusViews.frame.size.height - btrightFrame.origin.y);
                    self.btBackRight.frame = btbackFrame;
                    [self.btBackRight setTitle:@"X" forState:UIControlStateNormal];
                    [self.btBackRight setImage:nil forState:UIControlStateNormal];
                    [self.view bringSubviewToFront:self.btBackRight];
                }
                
                [self activeButtonMenu:activeButton effect:NO];
            } completion:^(BOOL finished) {
                if(finished)
                {
                }
            }];
        }
        if(scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            //            NSLog(@"BOTTOM REACHED");
        }
        if(scrollView.contentOffset.y < -50.0 && isSlideUp == YES) {
            isSlideUp = NO;
            [UIView animateWithDuration:duration animations:^{
                //todo: set webview move up
                self.wrapthumbView.frame = CGRectMake(0, self.statusViews.frame.size.height + self.statusViews.frame.origin.y, self.wrapthumbView.frame.size.width, self.wrapthumbView.frame.size.height + collapseHeight);
                [self.statusViews setBackgroundColor:[UIColor colorWithRed:29/255.0f green:24/255.0f blue:71/255.0f alpha:1]];
                self.cricleImage.hidden = NO;
                self.avatarImage.hidden = NO;
                CGRect frame = scrollView.frame;
                frame.origin.y = self.wrapthumbView.frame.origin.y + self.wrapthumbView.frame.size.height - ExtentGAP;
                frame.size.height -= (collapseHeight + self.statusViews.frame.size.height);
                scrollView.frame=frame;
                
                CGRect menuFrame = self.menuScrollView.frame;
                menuFrame.size.width = self.wrapthumbView.frame.size.width;
                menuFrame.origin.x = 0;
                menuFrame.origin.y = self.wrapthumbView.frame.size.height - menuFrame.size.height - GAP;
                self.menuScrollView.frame = menuFrame;
                
                if(!self.btBackRight.isHidden)
                {
                    //todo: set button move
                    CGRect btrightFrame = self.btStatusRight.frame;
                    CGRect btbackFrame = self.btBackRight.frame;
                    btrightFrame.origin.x += btbackFrame.size.width;
                    self.btStatusRight.frame = btrightFrame;
                    
                    btbackFrame.origin.y += (self.statusViews.frame.size.height - btrightFrame.origin.y);
                    self.btBackRight.frame = btbackFrame;
                    
                    [self.btBackRight setTitle:@"" forState:UIControlStateNormal];
                    UIImage *btimg = [UIImage imageForDeviceWithName:@"closeRightCorner.png"];
                    [self.btBackRight setImage:btimg forState:UIControlStateNormal];
                }
                
                [self activeButtonMenu:activeButton effect:NO];
            } completion:^(BOOL finished) {
                if(finished)
                {
                }
            }];
        }
    }
}

- (void)loadMenuList:(NSMutableArray *)menuLists activeTopic:(NSString *)activeTopic {
    //remove old button menu
    NSArray *subMenus = [self.menuScrollView subviews];
    for (UIView *item in subMenus) {
        if([item isMemberOfClass:[UIButton class]])
        {
            [item removeFromSuperview];
        }
    }
    
    self.menuArray = menuLists;
    int padding = 20;
    int x = padding;
    for (int i = 0; i < self.menuArray.count; i++) {
        InterestModel *model = [self.menuArray objectAtIndex:i];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button addTarget:self
                   action:@selector(menuButtonTouched:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:model.Name forState:UIControlStateNormal];
        UIFont *font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
        button.titleLabel.font = font;
        [button setTitleColor:[UIColor whiteColor] forState:normal];
        
        CGSize size = [model.Name sizeWithAttributes:@{NSFontAttributeName:font}];
        CGSize adjustedSize = CGSizeMake(ceilf(size.width), ceilf(size.height));
        
        button.frame = CGRectMake(x, 0, adjustedSize.width, 40.0);
        button.tag = i;
        x += adjustedSize.width + padding;
        [self.menuScrollView addSubview:button];
        if([activeTopic isEqualToString:model.Name] || [activeTopic isEqualToString:model.ID]) // for active All menu & normal menu
        {
            activeButton = button;
        }
    }
    self.menuScrollView.contentSize = CGSizeMake(x + padding, self.menuScrollView.contentSize.height);
    if(activeButton)
    {
        [self activeButtonMenu:activeButton effect:NO];
    }
    
}

- (void)activeButtonMenu:(UIButton *)btn effect:(BOOL)effect {
    UIView *removeView = [self.menuScrollView viewWithTag:6666699999];
    if(removeView != nil)
    {
        [removeView removeFromSuperview];
    }
    
    UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(btn.frame.size.width/2 - menuHoverWidth/2, btn.frame.size.height/2 + 15, menuHoverWidth, 1.5f)];
    bottomBorder.backgroundColor = [UIColor whiteColor];
    bottomBorder.tag = 6666699999;
    [btn addSubview:bottomBorder];
    
    float extent = (self.wrapthumbView.frame.size.width - self.menuScrollView.frame.size.width) / 2; // not collapse extent = 0
    self.oldMenuPostX = btn.frame.origin.x - (self.menuScrollView.frame.size.width / 2) + (btn.frame.size.width / 2) + extent;
    if(effect) {
        [UIView animateWithDuration:0.6f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             [self.menuScrollView setContentOffset:CGPointMake(self.oldMenuPostX, self.menuScrollView.contentOffset.y)];
                         }
                         completion:^(BOOL finished){
                             if (finished) {
                                 //todo: something when done active menu
                             }
                         }];
    } else {
        [self.menuScrollView setContentOffset:CGPointMake(self.oldMenuPostX, self.menuScrollView.contentOffset.y)];
    }
}

- (void)menuButtonTouched:(id)sender {
    UIButton *btn = (UIButton *)sender;
    activeButton = btn;
    [self activeButtonMenu:activeButton effect:YES];
    TempData *data = [TempData getInstance];
    //todo: Load Data and create cardlist
    int index = btn.tag;
    
//    InterestModel *menuItem = [self.menuArray objectAtIndex:index];
//    
//    NSString *firstLetter = [menuItem.Name substringToIndex:1];
//    self.letterLabel.text = firstLetter;
//    
//    if([menuItem.Name isEqualToString:@"All"])
//    {
//        if([data.State isEqualToString:NAV_ARTICLE]) {
//            [self loadTopics];
//        } else if([data.State isEqualToString:NAV_MANAGE]) {
//            [self loadManageTopics];
//        }
//    }
//    else
//    {
//        //todo: Load list Article of Topic
//        if(menuItem.Level == 1)
//        {
//            //todo : navigate to subtopic include article viewcontroller
//            if([data.State isEqualToString:NAV_ARTICLE]) {
//                [self loadSubTopic:menuItem.ID];
//            } else if([data.State isEqualToString:NAV_MANAGE]) {
//                [self loadManageSubTopic:menuItem.ID];
//            }
//            
//        }
//        else if(menuItem.Level == 2)
//        {
//            //todo : navigate to article only viewcontroller
//            [self removeCardList];
//            if([data.State isEqualToString:NAV_ARTICLE]) {
//                [self loadArticleTopic:menuItem.ID topicName:menuItem.Name Continues:NO];
//            } else if([data.State isEqualToString:NAV_MANAGE]) {
//                [self loadManageArticleTopic:menuItem.ID topicName:menuItem.Name Continues:NO];
//            }
//        }
//    }
}

- (void)removeCardList {
    NSArray *childviewcontrollers = [self childViewControllers];
    if(childviewcontrollers.count > 0)
    {
        for (UIViewController *item in childviewcontrollers)
        {
            if(![item isMemberOfClass:[UIImageView class]])
            {
                [item willMoveToParentViewController:nil];
                [item.view removeFromSuperview];
                [item removeFromParentViewController];
            }
        }
        self.containerScrollView.contentSize = CGSizeMake(0,0);
    }
}

- (void)createCardList:(NSArray *)cards {
    [self removeCardList];
    
    self.cardList = cards;
    
    // Add to scroll view
    float y = GAP; // TODO: Use contanst temporarily, change to real value later
    for (int i = 0; i < self.cardList.count; i++) {
        CGRect frame = ((UIViewController *)[self.cardList objectAtIndex:i]).view.frame;
        frame.origin.y = y;
        frame.origin.x = 0;
        frame.size.width = self.containerScrollView.frame.size.width;
        ((UIViewController *)[self.cardList objectAtIndex:i]).view.frame = frame;
        [self.containerScrollView addSubview:((UIViewController *)[self.cardList objectAtIndex:i]).view];
        [self addChildViewController:[self.cardList objectAtIndex:i]];
        [[self.cardList objectAtIndex:i] didMoveToParentViewController:self];
        y += frame.size.height + GAP;
    }
    self.containerScrollView.contentSize = CGSizeMake(self.containerScrollView.frame.size.width, y + GAP);
}

- (void)createCardListContinues:(NSArray *)cards {
    self.cardList = cards;
    
    // Add to scroll view
    float y = self.containerScrollView.contentSize.height - GAP; // TODO: Use contanst temporarily, change to real value later
    if(y < 0)
    {
        y = GAP;
    }
    
    for (int i = 0; i < self.cardList.count; i++) {
        CGRect frame = ((UIViewController *)[self.cardList objectAtIndex:i]).view.frame;
        frame.origin.y = y;
        frame.origin.x = 0;
        frame.size.width = self.containerScrollView.frame.size.width;
        ((UIViewController *)[self.cardList objectAtIndex:i]).view.frame = frame;
        [self.containerScrollView addSubview:((UIViewController *)[self.cardList objectAtIndex:i]).view];
        [self addChildViewController:[self.cardList objectAtIndex:i]];
        [[self.cardList objectAtIndex:i] didMoveToParentViewController:self];
        y += frame.size.height + GAP;
    }
    self.containerScrollView.contentSize = CGSizeMake(self.containerScrollView.frame.size.width, y + GAP);
}

- (void)refreshView {
    
    NSArray *subViews = [self.containerScrollView subviews];
    float y = GAP;
    for(UIView *view in subViews)
    {
        if(view.frame.origin.x == 0) //remove height UIimage of scroller
        {
            CGRect frame = view.frame;
            frame.origin.y = y;
            view.frame = frame;
            y+= view.frame.size.height + GAP;
        }
    }
    self.containerScrollView.contentSize = CGSizeMake(self.containerScrollView.frame.size.width, y);
}

- (IBAction)btBackTouched:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *myview = [storyboard instantiateViewControllerWithIdentifier:@"landingview"];
    [self.parentViewController.navigationController setViewControllers:[NSArray arrayWithObjects:myview, nil] animated:YES]; //reset rootviewcontroller
}

@end
