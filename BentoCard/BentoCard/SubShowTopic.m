//
//  SubShowTopic.m
//  BentoCard
//
//  Created by VO HONG HAI on 1/23/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import "SubShowTopic.h"
#import "ArticleTopicViewController.h"

@interface SubShowTopic ()

@end

@implementation SubShowTopic

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.nameLabel.text = self.interest.Name;
    self.countLabel.text = [NSString stringWithFormat:@"%d",self.interest.Total];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ssv_overlayTouched:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ArticleTopicViewController *myview = [storyboard instantiateViewControllerWithIdentifier:@"ArticleTopicView"];
    myview.topicID = self.interest.ID;
    myview.topicName = self.interest.Name;
    [self.navigationController pushViewController:myview animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
