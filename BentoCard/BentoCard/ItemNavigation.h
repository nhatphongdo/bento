//
//  ItemNavigation.h
//  BentoCard
//
//  Created by VO HONG HAI on 1/29/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Navigation.h"

@interface ItemNavigation : UIViewController

@property (nonatomic,strong) Navigation *navitem;
@property (nonatomic,strong) NSString *state;

@property (nonatomic,strong) IBOutlet UIImageView *arrowImage;
@property (nonatomic,strong) IBOutlet UIImageView *folderImage;
@property (nonatomic,strong) IBOutlet UILabel *NameLabel;
@property (nonatomic,strong) IBOutlet UILabel *letterLabel;
@property (nonatomic,strong) IBOutlet UIButton *btBackground;
@property (nonatomic,strong) IBOutlet UIButton *btClose;
@property (nonatomic,strong) IBOutlet UIView *arrowView;
@property (nonatomic,strong) IBOutlet UIView *imageView;
@property (nonatomic,strong) IBOutlet UIImageView *imagebg;

- (IBAction)closeTouched:(id)sender;
- (IBAction)backgroundTouched:(id)sender;
- (void)updateState;
@end
