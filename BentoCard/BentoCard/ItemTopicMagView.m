//
//  ItemTopicMagView.m
//  BentoCard
//
//  Created by VO HONG HAI on 12/11/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import "ItemTopicMagView.h"
#import "ArticleTopicViewController.h"
#import "InterestModel.h"
#import "ClientUtils.h"

@interface ItemTopicMagView ()

@end

@implementation ItemTopicMagView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.topicLabel setText:self.topic];
    [self.countLabel setText:[NSString stringWithFormat:@"%d",self.number]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (float)getHeight {
    return self.view.frame.size.height;
}

- (IBAction)overlayTouched:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ArticleTopicViewController *myController = [storyboard instantiateViewControllerWithIdentifier:@"ArticleTopicView"];
    myController.topicID = self.topicID;
    myController.topicName = self.topic;
    [self.navigationController pushViewController:myController animated:YES];
    
}

//#pragma mark - Navigation
//
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}


@end
