//
//  PwdViewController.h
//  Bento
//
//  Created by VO HONG HAI on 10/27/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PwdViewController : UIViewController

@property (strong,nonatomic) IBOutlet UILabel *emailLabel;
@property (strong,nonatomic) IBOutlet UITextField *pwdTextField;
@property (strong,nonatomic) IBOutlet UITextField *confirmpwdTextField;
@property (strong,nonatomic) IBOutlet UIButton *confirmButton;
@property (strong,nonatomic) IBOutlet UIButton *closeButton;
@property (strong,nonatomic) NSString *email;
@property (strong,nonatomic) NSString *gender;
@property (strong,nonatomic) NSString *fullname;
@property (strong,nonatomic) NSString *birthday;
@property (strong,nonatomic) NSString *social;
@property (strong,nonatomic) NSString *accountType;
@property (strong,nonatomic) NSString *accountID;
@property (strong,nonatomic) NSData *profilePic;

- (IBAction)confirmButton:(id)sender;
- (IBAction)closeTouched:(id)sender;

@end
