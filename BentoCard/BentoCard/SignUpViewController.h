//
//  SignUpViewController.h
//  BentoCard
//
//  Created by VO HONG HAI on 3/4/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"

@interface SignUpViewController : UIViewController<NIDropDownDelegate> {
        NIDropDown *dropDown;
}
@property (nonatomic,strong) IBOutlet UIButton *GenderButton;
@property (nonatomic,strong) IBOutlet UITextField *name;
@property (nonatomic,strong) IBOutlet UITextField *email;
@property (nonatomic,strong) IBOutlet UITextField *birthday;
@property (nonatomic,strong) IBOutlet UITextField *pwd;
@property (nonatomic,strong) IBOutlet UITextField *confirmpwd;
@property (nonatomic,strong) IBOutlet UIButton    *btSignUp;
@property (nonatomic,strong) IBOutlet UIButton    *btBack;

-(IBAction)SignUpTouch:(id)sender;

-(IBAction)backTouch:(id)sender;
-(IBAction)selectGender:(id)sender;

@end
