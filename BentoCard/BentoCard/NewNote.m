//
//  NewNote.m
//  BentoCard
//
//  Created by VO HONG HAI on 1/5/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import "NewNote.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import "Constrain.h"

@interface NewNote ()

@end

@implementation NewNote

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)SubmitNote:(id)sender {
    if ([self.txtNote.text isEqualToString:@""]) {
        return;
    }

    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    [ServiceClient postNoteForArticle:self.view username:username md5Password:password postID:self.postID message:self.txtNote.text successBlock:^(id responseObject) {
        // Post note succeeded
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Your note is saved successfully"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        if(self.articleContainer)
        {
            [self.articleContainer loadNoteView];
        }
        [self.view removeFromSuperview];
        [ClientUtils removeBlur:self.parentViewController.view];
        [self willMoveToParentViewController:nil];
        [self removeFromParentViewController];
    } failureBlock:^(NSError *error) {
    }];
}

- (IBAction)closeNote:(id)sender {
    [self.view removeFromSuperview];
    [ClientUtils removeBlur:self.parentViewController.view];
    [self willMoveToParentViewController:nil];
    [self removeFromParentViewController];
}

@end
