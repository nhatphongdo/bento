//
//  LandingViewController.h
//  BentoCard
//
//  Created by VO HONG HAI on 12/10/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PKRevealController/PKRevealController.h>

@interface LandingViewController : PKRevealController<PKRevealing>

@property (nonatomic) BOOL isFirstTime;

@end
