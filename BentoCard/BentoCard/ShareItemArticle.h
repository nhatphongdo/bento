//
//  ShareItemArticle.h
//  BentoCard
//
//  Created by VO HONG HAI on 12/18/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "AppDelegate.h"
#define APP ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface ShareItemArticle : UIViewController<MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>

@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) IBOutlet UIButton *collapseButton;
@property (nonatomic, strong) IBOutlet UIButton *editButton;
@property (nonatomic, strong) IBOutlet UIButton *facebookButton;
@property (nonatomic, strong) IBOutlet UIButton *twitterButton;
@property (nonatomic, strong) IBOutlet UIButton *messageButton;
@property (nonatomic, strong) IBOutlet UIButton *emailButton;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

- (IBAction)facebookTouched:(id)sender;
- (IBAction)twitterTouched:(id)sender;
- (IBAction)smsTouched:(id)sender;
- (IBAction)emailTouched:(id)sender;


@end
