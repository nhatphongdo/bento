//
//  ListItemComment.h
//  BentoCard
//
//  Created by VO HONG HAI on 12/19/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentModel.h"
#import "CommentItemArticle.h"

@interface ListItemComment : UIViewController

@property (strong, nonatomic) CommentItemArticle *containerViewController;

@property (strong, nonatomic) NSString *commentID;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *body;
@property (strong, nonatomic) NSString *avatar;
@property (nonatomic) int index;
@property (nonatomic) int total; //check last index comment for remove bottom line
@property (nonatomic) int likecount;
@property (nonatomic) BOOL liked;

@property (strong, nonatomic) IBOutlet UIImageView *commentAvatarImage;
@property (strong, nonatomic) IBOutlet UILabel *commentNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *commentBodyLabel;
@property (strong, nonatomic) IBOutlet UIButton *replyButton;
@property (strong, nonatomic) IBOutlet UIButton *likeButton;
@property (strong, nonatomic) IBOutlet UIButton *showreplyButton;
@property (strong, nonatomic) IBOutlet UIView *repliesView;
@property (strong, nonatomic) IBOutlet UIView *controlView;

- (void)reloadReplyShow:(int)index;

- (IBAction)replyButtonTouched:(id)sender;
- (IBAction)likeButtonTouched:(id)sender;
- (IBAction)showReplyButton:(id)sender;


@end
