//
//  ArticleModel.h
//  Bento
//
//  Created by Do Nhat Phong on 9/28/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ArticleModel : NSObject

@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) NSString *Title;
@property (strong, nonatomic) NSString *Thumbnail;
@property (strong, nonatomic) NSString *ShortDescription;
@property (strong, nonatomic) NSString *Author;
@property (strong, nonatomic) NSString *Body;
@property (strong, nonatomic) NSString *Category;
@property (strong, nonatomic) NSDate *DateCreated;
@property (nonatomic) int Likes;
@property (nonatomic) BOOL Liked;
@property (nonatomic) BOOL isFavorite;

@end
