//
//  ClientUtils.m
//  BentoCard
//
//  Created by VO HONG HAI on 12/17/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import "ClientUtils.h"
#import <CommonCrypto/CommonDigest.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "UIImage+DeviceSpecificMedia.h"
#import <TFHpple.h>
#import "UIImage+StackBlur.h"
#import "Constrain.h"
#import "TempData.h"

@implementation ClientUtils

+ (void) saveSetting:(NSString *)name value:(id)value {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:name];
    [defaults synchronize];
}

+ (id) loadSetting:(NSString *)name {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:name];
}

+ (void) deleteSetting:(NSString *)name {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:name];
    [defaults synchronize];
}

+ (NSString *) encryptMd5:(NSString *)input {
    const char *cStr = [input UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, strlen(cStr), result);
    return [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1],
            result[2], result[3],
            result[4], result[5],
            result[6], result[7],
            result[8], result[9],
            result[10], result[11],
            result[12], result[13],
            result[14], result[15]
            ];
}

+ (BOOL)validateEmail:(NSString *)emailStr {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}

+ (NSString *)urlEncode:(NSString *)input {
    if (input == nil || [input isEqualToString:@""]) {
        return @"";
    }
    
    input = [input stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    NSURL *url = [NSURL URLWithString:input];
    
    NSMutableString *output = [[NSMutableString alloc] init];
    [output appendString:[url scheme]];
    [output appendString:@"://"];
    [output appendString:[url host]];
    for (int i = 0; i < [[url pathComponents] count]; i++) {
        if ([[[url pathComponents] objectAtIndex:i] isEqualToString:@"/"]) {
            continue;
        }
        
        [output appendString:@"/"];
        [output appendString:((NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                                    NULL,
                                                                                                    (CFStringRef)[[url pathComponents] objectAtIndex:i],
                                                                                                    NULL,
                                                                                                    (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                    kCFStringEncodingUTF8 )))];
    }
    
    return output;
}

+ (NSString *)getGender:(NSInteger *)index {
    NSArray *genderArr = [NSArray arrayWithObjects:@"Female",@"Male", nil];
    return [genderArr objectAtIndex:index];
}

+ (NSArray *)getImages:(NSString *)htmlContent baseUrl:(NSString *)baseUrl {
    NSData *htmlData = [htmlContent dataUsingEncoding:NSUTF8StringEncoding];
    TFHpple *document = [[TFHpple alloc] initWithHTMLData:htmlData];
    
    NSArray *imageElements = [document searchWithXPathQuery:@"//img"];
    NSMutableArray *images = [[NSMutableArray alloc] init];
    for (TFHppleElement *element in imageElements) {
        [images addObject:[element objectForKey:@"src"]];
    }
    
    return images;
}

+ (UIImage *)captureView:(UIView *)view {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    UIGraphicsBeginImageContext(screenRect.size);
    
    [[UIColor blackColor] set];
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextFillRect(ctx, screenRect);
    
    [view.layer renderInContext:ctx];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (void)showBlur:(UIView *)view {
    UIImage *screenimg = [self captureView:view];
    UIImage *blurimg = [screenimg stackBlur:5];
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    imageview.tag = 8888;
    [imageview setImage:blurimg];
    [view addSubview:imageview];
    
    UIView *overlay = [[UIView alloc] initWithFrame:[imageview frame]];
    [overlay setBackgroundColor:[UIColor blackColor]];
    [overlay setAlpha:0.8f];
    overlay.tag = 8888;
    [view addSubview:overlay];
}

+ (void)removeBlur:(UIView *)view {
    NSArray *subviews = [view subviews];
    for(UIView *item in subviews) {
        if(item.tag == 8888)
        {
            [item removeFromSuperview];
        }
    }
}

+ (void)saveUsernamePwd { //todo: deleted after done
    NSString  *username = @"haivo@gmail.com";
    NSString *password = @"e10adc3949ba59abbe56e057f20f883e";
    
    [self saveSetting:USERNAME_KEY value:username];
    [self saveSetting:MD5_PASSWORD_KEY value:password];
    
}

+ (void)initializeWidthScreen:(UIView *)view
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGRect frame = view.frame;
    frame.size.width = screenRect.size.width;
    view.frame = frame;
}

+ (InterestModel *)getParentInterest:(NSString *)topicId {
    TempData *data = [TempData getInstance];
    if(data.interests.count > 0)
    {
        for (InterestModel *item in data.interests)
        {
            if([item.ID isEqualToString:topicId])
            {
                for (InterestModel *parent in data.interests)
                {
                    if([item.ParentID isEqualToString:parent.ID])
                    {
                        return parent;
                    }
                }
            }
        }
    }
    return nil;
}

+ (NSString *)getTopicNameById:(NSString *)topicId {
    TempData *data = [TempData getInstance];
    if(data.interests.count > 0)
    {
        for (InterestModel *item in data.interests)
        {
            if([item.ID isEqualToString:topicId])
            {
                return  item.Name;
            }
        }
    }
    return @"";
}

+ (int)getLevelByTopicId:(NSString *)topicId {
    TempData *data = [TempData getInstance];
    if(data.interests.count > 0)
    {
        for (InterestModel *item in data.interests)
        {
            if([item.ID isEqualToString:topicId])
            {
                return  item.Level;
            }
        }
    }
    return 0;
}

+ (CGSize)getSizeImage:(NSString *)imgName {

    UIImage *img = [UIImage imageForDeviceWithName:imgName];
    CGSize size = img.size;
    return size;
}

+ (void)SaveNavigation:(UIViewController *)controller Name:(NSString *)Name level:(NSString *)level ParentId:(NSString *)ParentId isSameView:(BOOL)isSameView {
    TempData *data = [TempData getInstance];
    
    int index = 0;
    
    NSArray *navigation = [controller.navigationController viewControllers];
    if(navigation.count > 0)
    {
        index = navigation.count - 1;
    }
    
    //old indexing navigation
//    if(data.navigations.count > 0)
//    {
//        Navigation *lastIndex = [data.navigations objectAtIndex:data.navigations.count - 1];
//        if(isSameView == NO) {
//            index = lastIndex.Index + 1;
//        }
//        else {
//            index = lastIndex.Index;
//        }
//    }
    //remove all item at this level
    [self removeNavigateByLevel:level];
    //check if parent of this item not exists
    int intlevel = [level intValue];
    for (int i = 1; i < intlevel; i++) {
        BOOL haveValue = NO;
        InterestModel *topic = [self GetInterestByIdLevel:ParentId Level:(i - 1)]; //if topic level 1 ParentId = self topic id.
        for (Navigation *item in data.navigations) {
            int intItemLevel = [item.Level intValue];
            NSLog(@"Item Name: %@ - Item Level: %d - Current Name: %@ - Current Level: %d", item.Name, intItemLevel, topic.Name, i);
            if(intItemLevel == i && [item.Name isEqualToString:topic.Name])
            {
                haveValue = YES;
                break;
            }
        }
        if(haveValue == NO) {
            //todo: Create View add to navigations and navigation controller attr: level - thumbImage - Name - Index - ParentId
            if(topic != nil) {
                //add object to navigations
                NSString *parentLevel = [NSString stringWithFormat:@"%d",i];
                [self removeNavigateByLevel:parentLevel];
                UIImage *bgImage = [UIImage imageForDeviceWithName:@"landingthumb.png"];
                Navigation *newNav = [[Navigation alloc] init];
                newNav.ThumbImage = UIImagePNGRepresentation(bgImage);
                newNav.Name = topic.Name;
                newNav.ParentId = topic.ParentID;
                newNav.Id = topic.ID;
                newNav.Level = parentLevel;
                newNav.isNavigation = NO;
                [data.navigations addObject:newNav];
            }
        }
    }
    
    
    NSArray *subviews = [controller.view subviews];
    if(subviews.count > 0)
    {
        UIView *tagisthree69 = [controller.view viewWithTag:696969];
        UIImage *bgImage = [self captureView:controller.view];
        CGRect croprect = CGRectMake(0, tagisthree69.frame.origin.y, bgImage.size.width, tagisthree69.frame.size.height);
        UIImage *cropImage = [bgImage crop:croprect];
        Navigation *nav = [[Navigation alloc] init];
        nav.ThumbImage = UIImagePNGRepresentation(cropImage);
        nav.Name = Name;
        nav.Level = level;
        nav.Index = index;
        nav.isNavigation = YES;
        [data.navigations addObject:nav];
    }
}

+ (InterestModel *)GetInterestByIdLevel:(NSString *)topicId Level:(int)Level {
    TempData *data = [TempData getInstance];
    if(Level == 0) {
        InterestModel *landingPageModel = [[InterestModel alloc] init];
        landingPageModel.Name = APPLICATION_TITLE;
        landingPageModel.ParentID = NAV_PARENTID_NIL;
        return landingPageModel;
    }
    for (InterestModel *item in data.interests) {
        if([item.ID isEqualToString:topicId] && item.Level == Level) {
            return item;
        }
    }
    return nil;
}

+ (InterestModel *)getInterestByName:(NSString *)name {
    TempData *data = [TempData getInstance];
    if(data.interests.count > 0) {
        for (InterestModel *item in data.interests) {
            if([name isEqualToString:item.Name]) {
                return item;
            }
        }
    }
    return nil;
}

+ (Navigation *)LoadNavigation:(int)index {
    TempData *data = [TempData getInstance];
    for (Navigation *item in data.navigations) {
        if(item.Index == index)
        {
            return item;
        }
    }
    return nil;
}

+ (void)removeNavigateByLevel:(NSString *)level {
    TempData *data = [TempData getInstance];
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    for (Navigation *item in data.navigations)
    {
        if([item.Level intValue] < [level intValue])
        {
            [dataArray addObject:item];
        }
    }
    data.navigations = dataArray;
}

+ (void)removeNavigate:(int)index {
    TempData *data = [TempData getInstance];
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    for (Navigation *item in data.navigations)
    {
        if(item.Index < index)
        {
            [dataArray addObject:item];
        }
    }
    data.navigations = dataArray;
}


@end



@implementation UIImageView (NetworkImage)

- (void) setUrl:(NSString *)url {
    if (url == nil || [url isEqualToString:@""]) {
        self.image = [UIImage imageNamed:@"no_image"];
        return;
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    [self setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"no_image"] success:nil failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        self.image = [UIImage imageNamed:@"no_image"];
    }];
}

@end


