//
//  SplashViewController.m
//  BentoCard
//
//  Created by VO HONG HAI on 12/9/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import "SplashViewController.h"
#import <pop/POP.h>
#import "Constrain.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import "LandingViewController.h"
#import "TempData.h"
#import "LoginViewController.h"

@interface SplashViewController ()
{
    TempData *data;
    BOOL syncInterests, syncUserInterests, animationLoaded;
    NSTimer *mytimer;
}
@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //init variable
    
    data = [TempData getInstance];
//    data.navigations = [[NSMutableArray alloc] init];
//    data.interests = [[NSMutableArray alloc] init];
//    data.userinterests = [[NSMutableArray alloc] init];
//    syncUserInterests = NO;
//    syncInterests = NO;
//    animationLoaded = NO;
//    
//    mytimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self
//                                   selector:@selector(doFireTimer) userInfo:nil repeats:YES];
//    [self SyncData];
}

- (void)doFireTimer {
    if (syncInterests == YES && syncUserInterests == YES && animationLoaded == YES) {
        [mytimer invalidate];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LandingViewController *landingView = [storyboard instantiateViewControllerWithIdentifier:@"landingview"];
        [self.navigationController setViewControllers:[NSArray arrayWithObjects:landingView, nil] animated:YES]; //reset rootviewcontroller
    }
    else if (animationLoaded == YES)
    {
        POPBasicAnimation *opacityAnimation11 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
        opacityAnimation11.fromValue = @(0);
        opacityAnimation11.toValue = @(1);
        opacityAnimation11.duration = 0.25f;
        opacityAnimation11.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        [opacityAnimation11 setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
            //todo: nothing...
        }];

        POPBasicAnimation *opacityAnimation10 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
        opacityAnimation10.fromValue = @(1);
        opacityAnimation10.toValue = @(0);
        opacityAnimation10.duration = 0.25f;
        opacityAnimation10.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        [opacityAnimation10 setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
            [self.textOImage.layer pop_addAnimation:opacityAnimation11 forKey:@"opacityAnimation_11"];
            [self.textBentImage.layer pop_addAnimation:opacityAnimation11 forKey:@"opacityAnimation_11"];
        }];
        [self.textOImage.layer pop_addAnimation:opacityAnimation10 forKey:@"opacityAnimation_10"];
        [self.textBentImage.layer pop_addAnimation:opacityAnimation10 forKey:@"opacityAnimation_10"];

    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self animationLandingPage];
}

- (void)animationLandingPage {
    
    // Animate loading logo
    POPBasicAnimation *opacityAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    opacityAnimation.fromValue = @(0);
    opacityAnimation.toValue = @(1);
    opacityAnimation.duration = 0.5;
    opacityAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [opacityAnimation setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        //todo: after animation finished
//        animationLoaded = YES;
        LoginViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
        [self.navigationController pushViewController:vc animated:YES];
    }];
    
    POPBasicAnimation *opacityAnimation1 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    opacityAnimation1.fromValue = @(0);
    opacityAnimation1.toValue = @(1);
    opacityAnimation1.duration = 0.5;
    opacityAnimation1.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [opacityAnimation1 setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        self.textBentImage.hidden = NO;
        self.textOImage.hidden = NO;
        self.textBusiness.hidden = YES;
        self.textExpert.hidden = YES;
        self.textNetworks.hidden = YES;
        self.textTraining.hidden = YES;
        self.textOperation.hidden = YES;
        [self.textOImage.layer pop_addAnimation:opacityAnimation forKey:@"opacityAnimation"];
        [self.textBentImage.layer pop_addAnimation:opacityAnimation forKey:@"opacityAnimation"];
    }];
    
    POPBasicAnimation *opacityAnimation2 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    opacityAnimation2.fromValue = @(0);
    opacityAnimation2.toValue = @(1);
    opacityAnimation2.duration = 0.5;
    opacityAnimation2.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [opacityAnimation2 setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        self.textOperation.hidden = NO;
        [self.textOperation.layer pop_addAnimation:opacityAnimation1 forKey:@"opacityAnimation"];
    }];
    
    POPBasicAnimation *opacityAnimation3 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    opacityAnimation3.fromValue = @(0);
    opacityAnimation3.toValue = @(1);
    opacityAnimation3.duration = 0.5;
    opacityAnimation3.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [opacityAnimation3 setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        self.textTraining.hidden = NO;
        [self.textTraining.layer pop_addAnimation:opacityAnimation2 forKey:@"opacityAnimation"];
    }];
    
    POPBasicAnimation *opacityAnimation4 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    opacityAnimation4.fromValue = @(0);
    opacityAnimation4.toValue = @(1);
    opacityAnimation4.duration = 0.5;
    opacityAnimation4.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [opacityAnimation4 setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        self.textNetworks.hidden = NO;
        [self.textNetworks.layer pop_addAnimation:opacityAnimation3 forKey:@"opacityAnimation"];
    }];
    
    POPBasicAnimation *opacityAnimation5 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    opacityAnimation5.fromValue = @(0);
    opacityAnimation5.toValue = @(1);
    opacityAnimation5.duration = 0.5;
    opacityAnimation5.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [opacityAnimation5 setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        self.textExpert.hidden = NO;
        [self.textExpert.layer pop_addAnimation:opacityAnimation4 forKey:@"opacityAnimation"];
    }];
    [self.textBusiness.layer pop_addAnimation:opacityAnimation5 forKey:@"opacityAnimation"];
}

- (void)SyncData {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    [ServiceClient getStructureInterests:nil username:username md5Password:password successBlock:^(id responseObject) {
        data.interests = responseObject;
        syncInterests = YES;
    } failureBlock:^(NSError *error) {
        syncInterests = YES;
        NSLog(@"Sync get interests Error: %@",error);
    }];
    
    [ServiceClient getStructureUserFavorites:nil username:username md5Password:password successBlock:^(id responseObject) {
        data.userinterests = responseObject;
        syncUserInterests = YES;
    } failureBlock:^(NSError *error) {
        syncUserInterests = YES;
        NSLog(@"Sync get user interests Error: %@",error);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
