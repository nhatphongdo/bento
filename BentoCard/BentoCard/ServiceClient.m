//
//  ServiceClient.m
//  BentoCard
//
//  Created by VO HONG HAI on 12/17/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import "ServiceClient.h"
#import "Constrain.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "InterestModel.h"
#import "ArticleModel.h"
#import "ProfileModel.h"
#import "CommentModel.h"
#import "ClientUtils.h"
#import "NoteModel.h"
#import "SponsorModel.h"

@implementation ServiceClient

+ (void)loadService:(UIView *)view
                url:(NSString *)url
             params:(NSDictionary *)params
       successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
       failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure {
    
    if (view != nil) {
        // Show loading HUD
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading";
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id JSON) {
        
        NSLog(@"\nService client log\nUrl: %@\nResult: %@\n\n", url, JSON);
        
        if ([[JSON objectForKey:@"Error"] intValue] == 0) {
            if (success) {
                success(operation, [JSON objectForKey:@"Data"]);
            }
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                           message:[JSON objectForKey:@"ErrorMessage"]
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;
            
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"ServiceClient" code:[[JSON objectForKey:@"Error"] intValue] userInfo:@{@"ErrorMessage": [JSON objectForKey:@"ErrorMessage"]}];
                failure(operation, error);
            }
        }
        
        if (view != nil)
        {
            [MBProgressHUD hideAllHUDsForView:view animated:YES];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"\nService client log\nUrl: %@\nError: %@\n\n", url, error);
        
        // Network problem
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Network connection problem"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        if (failure) {
            failure(operation, error);
        }
    
        if (view != nil)
        {
            [MBProgressHUD hideHUDForView:view animated:YES];
        }
    }];
}

+ (void)postFile:(UIView *)view
             url:(NSString *)url
          params:(NSDictionary *)params
        fileData:(NSData *)fileData
            name:(NSString *)name
        fileName:(NSString *)fileName
        fileType:(NSString *)fileType
    successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
    failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure {
    
    // Show loading HUD
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading";
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    
    //    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        NSMutableDictionary *headers = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                        @"multipart/form-data", @"Content-Type",
                                        //                                        @"form-data; name=\"registration\"", @"Content-Disposition",
                                        nil];
        [formData appendPartWithHeaders:headers body:jsonData];
        [formData appendPartWithFileData:fileData name:name fileName:fileName mimeType:fileType];
        
    } success:^(AFHTTPRequestOperation *operation, id JSON) {
        
        NSLog(@"\nService client log\nUrl: %@\nResult: %@\n\n", url, JSON);
        
        if ([[JSON objectForKey:@"Error"] intValue] == 0) {
            if (success) {
                success(operation, [JSON objectForKey:@"Data"]);
            }
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                           message:[JSON objectForKey:@"ErrorMessage"]
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;
            
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"ServiceClient" code:[[JSON objectForKey:@"Error"] intValue] userInfo:@{@"ErrorMessage": [JSON objectForKey:@"ErrorMessage"]}];
                failure(operation, error);
            }
        }
        
        [MBProgressHUD hideAllHUDsForView:view animated:YES];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"\nService client log\nUrl: %@\nError: %@\n\n", url, error);
        
        // Network problem
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Network connection problem"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        if (failure) {
            failure(operation, error);
        }
        
        [MBProgressHUD hideHUDForView:view animated:YES];
    }];
}


+ (void) signIn:(UIView *)view
       username:(NSString *)username
    md5Password:(NSString *)md5Password
   successBlock:(void (^)(id responseObject))success
   failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password
                             };
    [self loadService:view url:SIGN_IN_URL params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) signOut:(UIView *)view
        username:(NSString *)username
     md5Password:(NSString *)md5Password
    successBlock:(void (^)(id responseObject))success
    failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password
                             };
    [self loadService:view url:SIGN_OUT_URL params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) signUp:(UIView *)view
       username:(NSString *)username
       password:(NSString *)password
confirmpassword:(NSString *)confirmpassword
       fullname:(NSString *)fullname
       birthday:(NSString *)birthday
    accountType:(NSString *)accountType
      accountID:(NSString *)accountID
   successBlock:(void (^)(id responseObject))success
   failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": password,
                             @"confirmpassword": confirmpassword,
                             @"FullName":fullname,
                             @"Birthday":birthday,
                             @"AccountType":accountType,
                             @"AccountID":accountID
                             };
    [self loadService:view url:SIGN_UP_URL params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

//+ (void) forgotPassword:(UIView *)view
//               username:(NSString *)username
//            md5Password:(NSString *)md5Password
//           successBlock:(void (^)(id responseObject))success
//           failureBlock:(void (^)(NSError *error))failure;

+ (void) getProfile:(UIView *)view
           username:(NSString *)username
        md5Password:(NSString *)md5Password
       successBlock:(void (^)(id responseObject))success
       failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password
                             };
    [self loadService:view url:GET_PROFILE_URL params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject){
        if (success)
        {
            ProfileModel *profile = [[ProfileModel alloc] init];
            profile.Username = [responseObject objectForKey:@"Username"] == [NSNull null] ? @"" : [responseObject objectForKey:@"Username"];
            profile.Email = [responseObject objectForKey:@"Email"] == [NSNull null] ? @"" : [responseObject objectForKey:@"Email"];
            profile.AccountType = [responseObject objectForKey:@"AccountType"] == [NSNull null] ? @"" : [responseObject objectForKey:@"AccountType"];
            profile.Gender = [responseObject objectForKey:@"Gender"] == [NSNull null] ? 0 : [[responseObject objectForKey:@"Gender"] intValue];
            profile.Avatar = [responseObject objectForKey:@"Avatar"] == [NSNull null] ? @"" : [responseObject objectForKey:@"Avatar"];
            profile.Avatar = [ClientUtils urlEncode:profile.Avatar];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
            profile.BirthDay = [responseObject objectForKey:@"BirthDay"] == [NSNull null] ? nil : [dateFormatter dateFromString:[responseObject objectForKey:@"BirthDay"]];
            profile.Fullname = [responseObject objectForKey:@"Fullname"] == [NSNull null] ? @"" : [responseObject objectForKey:@"Fullname"];
            profile.Caption = [responseObject objectForKey:@"Caption"] == [NSNull null] ? @"" : [responseObject objectForKey:@"Caption"];
            success(profile);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
    
}

+ (void) updateProfile:(UIView *)view
              username:(NSString *)username
           md5Password:(NSString *)md5Password
              fullname:(NSString *)fullname
                gender:(NSString *)gender
              birthday:(NSString *)birthday
                 email:(NSString *)email
          successBlock:(void (^)(id responseObject))success
          failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password,
                             @"FullName": fullname,
                             @"Gender"  : gender,
                             @"BirthDay": birthday,
                             @"Email": email
                             };
    [self loadService:view url:UPDATE_PROFILE_URL params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) getUserInterests:(UIView *)view
                 username:(NSString *)username
              md5Password:(NSString *)md5Password
             successBlock:(void (^)(id responseObject))success
             failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password
                             };
    
    [self loadService:view url:GET_USER_INTERESTS_URL params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            NSMutableArray *interests = [[NSMutableArray alloc] init];
            
            for (uint i = 0; i < [responseObject count]; i++) {
                InterestModel *item = [[InterestModel alloc] init];
                item.ID = [[responseObject objectAtIndex:i] objectForKey:@"ID"];
                item.Name = [[responseObject objectAtIndex:i] objectForKey:@"Name"];
                item.Thumbnail = [[responseObject objectAtIndex:i] objectForKey:@"Thumbnail"] == [NSNull null] ? nil : [[responseObject objectAtIndex:i] objectForKey:@"Thumbnail"];
                item.Thumbnail = [ClientUtils urlEncode:item.Thumbnail];
                [interests addObject:item];
            }
            
            success(interests);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) setUserInterests:(UIView *)view
                 username:(NSString *)username
              md5Password:(NSString *)md5Password
                interests:(NSArray *)interests
             successBlock:(void (^)(id responseObject))success
             failureBlock:(void (^)(NSError *error))failure {
    
    NSMutableString *interestsStr = [[NSMutableString alloc] init];
    for (int i = 0; i < [interests count]; i++) {
        InterestModel *item = [interests objectAtIndex:i];
        [interestsStr appendString:item.ID];
        if (i < [interests count] - 1) {
            [interestsStr appendString:@","];
        }
    }
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password,
                             @"interests": interestsStr
                             };
    
    [self loadService:view url:SET_USER_INTERESTS_URL params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) getInterests:(UIView *)view
             username:(NSString *)username
          md5Password:(NSString *)md5Password
         successBlock:(void (^)(id responseObject))success
         failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password
                             };
    
    [self loadService:view url:GET_INTERESTS_URL params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            NSMutableArray *interests = [[NSMutableArray alloc] init];
            
            for (uint i = 0; i < [responseObject count]; i++) {
                InterestModel *item = [[InterestModel alloc] init];
                item.ID = [[responseObject objectAtIndex:i] objectForKey:@"ID"];
                item.Name = [[responseObject objectAtIndex:i] objectForKey:@"Name"];
                item.Thumbnail = [[responseObject objectAtIndex:i] objectForKey:@"Thumbnail"] == [NSNull null] ? nil : [[responseObject objectAtIndex:i] objectForKey:@"Thumbnail"];
                item.Thumbnail = [ClientUtils urlEncode:item.Thumbnail];
                [interests addObject:item];
            }
            
            success(interests);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) getInterestsByParentID:(UIView *)view
                       username:(NSString *)username
                    md5Password:(NSString *)md5Password
                       parentID:(NSString *)parentID
                   successBlock:(void (^)(id responseObject))success
                   failureBlock:(void (^)(NSError *error))failure {
    NSDictionary *params = @{
                             @"username":username,
                             @"password":md5Password
                             };
    [self loadService:view url:[NSString stringWithFormat:GET_INTERESTS_BY_PARENTID_URL, parentID] params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            NSMutableArray *interests = [[NSMutableArray alloc] init];
            
            for (uint i = 0; i < [responseObject count]; i++) {
                InterestModel *item = [[InterestModel alloc] init];
                item.ID = [[responseObject objectAtIndex:i] objectForKey:@"Id"];
                item.Name = [[responseObject objectAtIndex:i] objectForKey:@"Name"];
                item.Thumbnail = [[responseObject objectAtIndex:i] objectForKey:@"Thumbnail"] == [NSNull null] ? nil : [[responseObject objectAtIndex:i] objectForKey:@"Thumbnail"];
                item.Thumbnail = [ClientUtils urlEncode:item.Thumbnail];
                item.ParentID = [[responseObject objectAtIndex:i] objectForKey:@"ParentId"] == [NSNull null] ? @"" : [[responseObject objectAtIndex:i] objectForKey:@"ParentId"];
                item.Total = [[responseObject objectAtIndex:i] objectForKey:@"Total"] == [NSNull null] ? 0 : [[[responseObject objectAtIndex:i] objectForKey:@"Total"] integerValue];
                [interests addObject:item];
            }
            success(interests);
        }
        
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
            if(failure)
            {
                failure(error);
            }
    }];
}

+ (void) getStructureInterests:(UIView *)view
                      username:(NSString *)username
                   md5Password:(NSString *)md5Password
                  successBlock:(void (^)(id responseObject))success
                  failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password
                             };
    
    [self loadService:view url:GET_STRUCTURE_INTERESTS_URL params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            NSMutableArray *interests = [[NSMutableArray alloc] init];
            
            for (uint i = 0; i < [responseObject count]; i++) {
                InterestModel *item = [[InterestModel alloc] init];
                item.ID = [[responseObject objectAtIndex:i] objectForKey:@"Id"];
                item.Name = [[responseObject objectAtIndex:i] objectForKey:@"Name"];
                item.Thumbnail = [[responseObject objectAtIndex:i] objectForKey:@"Thumbnail"] == [NSNull null] ? nil : [[responseObject objectAtIndex:i] objectForKey:@"Thumbnail"];
                item.Thumbnail = [ClientUtils urlEncode:item.Thumbnail];
                item.Level = [[[responseObject objectAtIndex:i] objectForKey:@"Level"] integerValue];
                item.ParentID = [[responseObject objectAtIndex:i] objectForKey:@"ParentId"] == [NSNull null] ? @"" : [[responseObject objectAtIndex:i] objectForKey:@"ParentId"];
                item.Total = [[responseObject objectAtIndex:i] objectForKey:@"Total"] == [NSNull null] ? 0 : [[[responseObject objectAtIndex:i] objectForKey:@"Total"] integerValue];
                [interests addObject:item];
            }
            
            success(interests);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) getStructureUserInterests:(UIView *)view
                          username:(NSString *)username
                       md5Password:(NSString *)md5Password
                      successBlock:(void (^)(id responseObject))success
                      failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password
                             };
    
    [self loadService:view url:GET_STRUCTURE_USER_INTERESTS_URL params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            NSMutableArray *interests = [[NSMutableArray alloc] init];
            
            for (uint i = 0; i < [responseObject count]; i++) {
                InterestModel *item = [[InterestModel alloc] init];
                item.ID = [[responseObject objectAtIndex:i] objectForKey:@"Id"];
                item.Name = [[responseObject objectAtIndex:i] objectForKey:@"Name"];
                item.Thumbnail = [[responseObject objectAtIndex:i] objectForKey:@"Thumbnail"] == [NSNull null] ? nil : [[responseObject objectAtIndex:i] objectForKey:@"Thumbnail"];
                item.Thumbnail = [ClientUtils urlEncode:item.Thumbnail];
                item.Level = [[[responseObject objectAtIndex:i] objectForKey:@"Level"] integerValue];
                item.Total = [[[responseObject objectAtIndex:i] objectForKey:@"Total"] integerValue];
                item.ParentID = [[responseObject objectAtIndex:i] objectForKey:@"ParentId"] == [NSNull null] ? @"" : [[responseObject objectAtIndex:i] objectForKey:@"ParentId"];
                [interests addObject:item];
            }
            
            success(interests);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) getStructureUserFavorites:(UIView *)view
                          username:(NSString *)username
                       md5Password:(NSString *)md5Password
                      successBlock:(void (^)(id responseObject))success
                      failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password
                             };
    
    [self loadService:view url:GET_STRUCTURE_FAVORITE_URL params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            NSMutableArray *interests = [[NSMutableArray alloc] init];
            
            for (uint i = 0; i < [responseObject count]; i++) {
                InterestModel *item = [[InterestModel alloc] init];
                item.ID = [[responseObject objectAtIndex:i] objectForKey:@"Id"];
                item.Name = [[responseObject objectAtIndex:i] objectForKey:@"Name"];
                item.Thumbnail = [[responseObject objectAtIndex:i] objectForKey:@"Thumbnail"] == [NSNull null] ? nil : [[responseObject objectAtIndex:i] objectForKey:@"Thumbnail"];
                item.Thumbnail = [ClientUtils urlEncode:item.Thumbnail];
                item.Level = [[[responseObject objectAtIndex:i] objectForKey:@"Level"] integerValue];
                item.Total = [[[responseObject objectAtIndex:i] objectForKey:@"Total"] integerValue];
                item.ParentID = [[responseObject objectAtIndex:i] objectForKey:@"ParentId"] == [NSNull null] ? @"" : [[responseObject objectAtIndex:i] objectForKey:@"ParentId"];
                [interests addObject:item];
            }
            
            success(interests);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}


+ (void) getArticlesOfInterest:(UIView *)view
                      username:(NSString *)username
                   md5Password:(NSString *)md5Password
                    interestID:(NSString *)interestID
                          page:(int)page
                  itemsPerPage:(int)itemsPerPage
                  successBlock:(void (^)(id responseObject))success
                  failureBlock:(void (^)(NSError *error))failure {
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password,
                             @"page": [NSString stringWithFormat:@"%d", page],
                             @"itemsperpage": [NSString stringWithFormat:@"%d", itemsPerPage]
                             };
    [self loadService:view url:[NSString stringWithFormat:GET_ARTICLES_OF_INTEREST_URL, interestID]  params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(success)
        {
            NSMutableArray *articles = [[NSMutableArray alloc] init];
            
            for (uint i = 0; i < [responseObject count]; i++) {
                NSDictionary *item = [responseObject objectAtIndex:i];
                ArticleModel *article = [[ArticleModel alloc] init];
                article.ID = [item objectForKey:@"ID"];
                article.Title = [item objectForKey:@"Title"] == [NSNull null] ? @"" : [item objectForKey:@"Title"];
                article.Author = [item objectForKey:@"Author"] == [NSNull null] ? @"" : [item objectForKey:@"Author"];
                article.Body = [item objectForKey:@"Body"] == [NSNull null] ? @"" : [item objectForKey:@"Body"];
                article.Thumbnail = [item objectForKey:@"Thumbnail"] == [NSNull null] ? nil : [item objectForKey:@"Thumbnail"];
                article.Thumbnail = [ClientUtils urlEncode:article.Thumbnail];
                article.ShortDescription = [item objectForKey:@"ShortDescription"] == [NSNull null] ? @"" : [item objectForKey:@"ShortDescription"];
                article.Likes = [item objectForKey:@"Likes"] == [NSNull null] ? 0 : [[item objectForKey:@"Likes"] intValue];
                article.Liked = [item objectForKey:@"Liked"] == [NSNull null] ? NO : [[item objectForKey:@"Liked"] boolValue];
                article.Category = [item objectForKey:@"Categories"] == [NSNull null] ? @"" : [item objectForKey:@"Categories"];
                //                article.isFavorite = [responseObject objectForKey:@"Favorited"] == [NSNull null] ? NO : [[responseObject objectForKey:@"Favorited"] boolValue];
                [articles addObject:article];
            }
            
            success(articles);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(failure)
        {
            failure(error);
        }
    }];
}

+ (void) getArticlesOfSubTopicView:(UIView *)view
                          username:(NSString *)username
                       md5Password:(NSString *)md5Password
                        interestID:(NSString *)interestID
                      successBlock:(void (^)(id responseObject))success
                      failureBlock:(void (^)(NSError *error))failure {
    NSDictionary *params = @{
                             @"username":username,
                             @"password":md5Password
                             };
    [self loadService:view url:[NSString stringWithFormat:GET_ARTICLES_OF_SUBTOPICVIEW_URL,interestID] params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSMutableArray *articles = [[NSMutableArray alloc] init];
        for (uint i = 0; i < [responseObject count]; i++) {
            NSDictionary *item = [responseObject objectAtIndex:i];
            ArticleModel *article = [[ArticleModel alloc] init];
            article.ID = [item objectForKey:@"ID"];
            article.Title = [item objectForKey:@"Title"] == [NSNull null] ? @"" : [item objectForKey:@"Title"];
            article.Author = [item objectForKey:@"Author"] == [NSNull null] ? @"" : [item objectForKey:@"Author"];
            article.Thumbnail = [item objectForKey:@"Thumbnail"] == [NSNull null] ? nil : [item objectForKey:@"Thumbnail"];
            article.Thumbnail = [ClientUtils urlEncode:article.Thumbnail];
            article.ShortDescription = [item objectForKey:@"ShortDescription"] == [NSNull null] ? @"" : [item objectForKey:@"ShortDescription"];
            article.Likes = [item objectForKey:@"Likes"] == [NSNull null] ? 0 : [[item objectForKey:@"Likes"] intValue];
            article.Liked = [item objectForKey:@"Liked"] == [NSNull null] ? NO : [[item objectForKey:@"Liked"] boolValue];
            article.Category = [item objectForKey:@"Categories"] == [NSNull null] ? @"" : [item objectForKey:@"Categories"];
            [articles addObject:article];
        }
        
        success(articles);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(failure)
        {
            failure(error);
        }
    }];
    
}

+ (void) getMyArticlesOfSubTopicView:(UIView *)view
                            username:(NSString *)username
                         md5Password:(NSString *)md5Password
                          interestID:(NSString *)interestID
                        successBlock:(void (^)(id responseObject))success
                        failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username":username,
                             @"password":md5Password
                             };
    [self loadService:view url:[NSString stringWithFormat:GET_MYARTICLES_OF_SUBTOPICVIEW_URL,interestID] params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSMutableArray *articles = [[NSMutableArray alloc] init];
        NSLog(@"in result");
        for (uint i = 0; i < [responseObject count]; i++) {
            NSDictionary *item = [responseObject objectAtIndex:i];
            ArticleModel *article = [[ArticleModel alloc] init];
            article.ID = [item objectForKey:@"ID"];
            article.Title = [item objectForKey:@"Title"] == [NSNull null] ? @"" : [item objectForKey:@"Title"];
            article.Author = [item objectForKey:@"Author"] == [NSNull null] ? @"" : [item objectForKey:@"Author"];
            article.Thumbnail = [item objectForKey:@"Thumbnail"] == [NSNull null] ? nil : [item objectForKey:@"Thumbnail"];
            article.Thumbnail = [ClientUtils urlEncode:article.Thumbnail];
            article.ShortDescription = [item objectForKey:@"ShortDescription"] == [NSNull null] ? @"" : [item objectForKey:@"ShortDescription"];
            article.Likes = [item objectForKey:@"Likes"] == [NSNull null] ? 0 : [[item objectForKey:@"Likes"] intValue];
            article.Liked = [item objectForKey:@"Liked"] == [NSNull null] ? NO : [[item objectForKey:@"Liked"] boolValue];
            article.Category = [item objectForKey:@"Categories"] == [NSNull null] ? @"" : [item objectForKey:@"Categories"];
            [articles addObject:article];
        }
        
        success(articles);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(failure)
        {
            failure(error);
        }
    }];
}

+ (void) getUserArticlesOfInterest:(UIView *)view
                          username:(NSString *)username
                       md5Password:(NSString *)md5Password
                        interestID:(NSString *)interestID
                              page:(int)page
                      itemsPerPage:(int)itemsPerPage
                      successBlock:(void (^)(id responseObject))success
                      failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password,
                             @"page": [NSString stringWithFormat:@"%d", page],
                             @"itemsperpage": [NSString stringWithFormat:@"%d", itemsPerPage]
                             };
    
    [self loadService:view url:[NSString stringWithFormat:GET_FAVORITE_ARTICLE_URL, interestID] params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            NSMutableArray *articles = [[NSMutableArray alloc] init];
            
            for (uint i = 0; i < [responseObject count]; i++) {
                NSDictionary *item = [responseObject objectAtIndex:i];
                ArticleModel *article = [[ArticleModel alloc] init];
                article.ID = [item objectForKey:@"ID"];
                article.Title = [item objectForKey:@"Title"] == [NSNull null] ? @"" : [item objectForKey:@"Title"];
                article.Author = [item objectForKey:@"Author"] == [NSNull null] ? @"" : [item objectForKey:@"Author"];
                article.Body = [item objectForKey:@"Body"] == [NSNull null] ? @"" : [item objectForKey:@"Body"];
                article.Thumbnail = [item objectForKey:@"Thumbnail"] == [NSNull null] ? nil : [item objectForKey:@"Thumbnail"];
                article.Thumbnail = [ClientUtils urlEncode:article.Thumbnail];
                article.ShortDescription = [item objectForKey:@"ShortDescription"] == [NSNull null] ? @"" : [item objectForKey:@"ShortDescription"];
                article.Likes = [item objectForKey:@"Likes"] == [NSNull null] ? 0 : [[item objectForKey:@"Likes"] intValue];
                article.Liked = [item objectForKey:@"Liked"] == [NSNull null] ? NO : [[item objectForKey:@"Liked"] boolValue];
                article.Category = [item objectForKey:@"Categories"] == [NSNull null] ? @"" : [item objectForKey:@"Categories"];
                //                article.isFavorite = [responseObject objectForKey:@"Favorited"] == [NSNull null] ? NO : [[responseObject objectForKey:@"Favorited"] boolValue];
                [articles addObject:article];
            }
            
            success(articles);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) getLatestArticles:(UIView *)view
                  username:(NSString *)username
               md5Password:(NSString *)md5Password
                      page:(int)page
              itemsPerPage:(int)itemsPerPage
              successBlock:(void (^)(id responseObject))success
              failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password,
                             @"page": [NSString stringWithFormat:@"%d", page],
                             @"itemsperpage": [NSString stringWithFormat:@"%d", itemsPerPage]
                             };
    
    [self loadService:view url:GET_LATEST_ARTICLES_URL params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            NSMutableArray *articles = [[NSMutableArray alloc] init];
            
            for (uint i = 0; i < [responseObject count]; i++) {
                NSDictionary *item = [responseObject objectAtIndex:i];
                ArticleModel *article = [[ArticleModel alloc] init];
                article.ID = [item objectForKey:@"ID"];
                article.Title = [item objectForKey:@"Title"] == [NSNull null] ? @"" : [item objectForKey:@"Title"];
                article.Author = [item objectForKey:@"Author"] == [NSNull null] ? @"" : [item objectForKey:@"Author"];
                article.Body = [item objectForKey:@"Body"] == [NSNull null] ? @"" : [item objectForKey:@"Body"];
                article.Thumbnail = [item objectForKey:@"Thumbnail"] == [NSNull null] ? nil : [item objectForKey:@"Thumbnail"];
                article.Thumbnail = [ClientUtils urlEncode:article.Thumbnail];
                article.ShortDescription = [item objectForKey:@"ShortDescription"] == [NSNull null] ? @"" : [item objectForKey:@"ShortDescription"];
                article.Likes = [item objectForKey:@"Likes"] == [NSNull null] ? 0 : [[item objectForKey:@"Likes"] intValue];
                article.Liked = [item objectForKey:@"Liked"] == [NSNull null] ? NO : [[item objectForKey:@"Liked"] boolValue];
                article.Category = [item objectForKey:@"Categories"] == [NSNull null] ? @"" : [item objectForKey:@"Categories"];
                //                article.isFavorite = [responseObject objectForKey:@"Favorited"] == [NSNull null] ? NO : [[responseObject objectForKey:@"Favorited"] boolValue];
                [articles addObject:article];
            }
            
            success(articles);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) getDetailArticle:(UIView *)view
                 username:(NSString *)username
              md5Password:(NSString *)md5Password
                articleID:(NSString *)articleID
             successBlock:(void (^)(id responseObject))success
             failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password
                             };
    [self loadService:view url:[NSString stringWithFormat:GET_DETAIL_ARTICLE_URL, articleID] params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            ArticleModel *item = [[ArticleModel alloc] init];
            item.ID = [responseObject objectForKey:@"ID"];
            item.Title = [responseObject objectForKey:@"Title"] == [NSNull null] ? @"" : [responseObject objectForKey:@"Title"];
            item.Body = [responseObject objectForKey:@"Body"] == [NSNull null] ? @"" : [responseObject objectForKey:@"Body"];
            item.Thumbnail = [responseObject objectForKey:@"Thumbnail"] == [NSNull null] ? nil : [responseObject objectForKey:@"Thumbnail"];
            item.Thumbnail = [ClientUtils urlEncode:item.Thumbnail];
            item.isFavorite = [responseObject objectForKey:@"Favorited"] == [NSNull null] ? NO : [[responseObject objectForKey:@"Favorited"] boolValue];
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
            item.DateCreated = [responseObject objectForKey:@"DateCreated"] == [NSNull null] ? nil : [dateFormat dateFromString:[responseObject objectForKey:@"DateCreated"]];
            item.Author = [responseObject objectForKey:@"Author"] == [NSNull null] ? @"" : [responseObject objectForKey:@"Author"];
            success(item);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) getCommentsOfArticle:(UIView *)view
                     username:(NSString *)username
                  md5Password:(NSString *)md5Password
                       postID:(NSString *)postID
                         page:(int)page
                 itemsPerPage:(int)itemsPerPage
                 successBlock:(void (^)(id responseObject))success
                 failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password,
                             @"page": [NSString stringWithFormat:@"%d", page],
                             @"itemsperpage": [NSString stringWithFormat:@"%d", itemsPerPage]
                             };
    
    [self loadService:view url:[NSString stringWithFormat:GET_COMMENTS_OF_ARTICLE_URL, postID] params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            NSMutableArray *comments = [[NSMutableArray alloc] init];
            
            for (uint i = 0; i < [responseObject count]; i++) {
                NSDictionary *item = [responseObject objectAtIndex:i];
                CommentModel *comment = [[CommentModel alloc] init];
                comment.ID = [item objectForKey:@"ID"];
                comment.Message = [item objectForKey:@"Message"] == [NSNull null] ? @"" : [item objectForKey:@"Message"];
                comment.ProfileID = [item objectForKey:@"ProfileID"] == [NSNull null] ? @"" : [item objectForKey:@"ProfileID"];
                comment.Username = [item objectForKey:@"Username"] == [NSNull null] ? @"" : [item objectForKey:@"Username"];
                comment.Fullname = [item objectForKey:@"Fullname"] == [NSNull null] ? @"" : [item objectForKey:@"Fullname"];
                comment.Avatar = [item objectForKey:@"Avatar"] == [NSNull null] ? @"" : [item objectForKey:@"Avatar"];
                comment.Avatar = [ClientUtils urlEncode:comment.Avatar];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                dateFormatter.dateFormat = @"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS";
                comment.DateCreated = [item objectForKey:@"DateCreated"] == [NSNull null] ? nil : [dateFormatter dateFromString:[item objectForKey:@"DateCreated"]];
                comment.Likes = [item objectForKey:@"Likes"] == [NSNull null] ? 0 : [[item objectForKey:@"Likes"] intValue];
                comment.Liked = [item objectForKey:@"Liked"] == [NSNull null] ? NO : [[item objectForKey:@"Liked"] boolValue];
                [comments addObject:comment];
            }
            
            success(comments);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) getRepliesOfComment:(UIView *)view
                    username:(NSString *)username
                 md5Password:(NSString *)md5Password
                      postID:(NSString *)postID
                        page:(int)page
                itemsPerPage:(int)itemsPerPage
                successBlock:(void (^)(id responseObject))success
                failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password,
                             @"page": [NSString stringWithFormat:@"%d", page],
                             @"itemsperpage": [NSString stringWithFormat:@"%d", itemsPerPage]
                             };
    
    [self loadService:view url:[NSString stringWithFormat:GET_REPLIES_OF_COMMENT_URL, postID] params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            NSMutableArray *comments = [[NSMutableArray alloc] init];
            
            for (uint i = 0; i < [responseObject count]; i++) {
                NSDictionary *item = [responseObject objectAtIndex:i];
                CommentModel *comment = [[CommentModel alloc] init];
                comment.ID = [item objectForKey:@"ID"];
                comment.Message = [item objectForKey:@"Message"] == [NSNull null] ? @"" : [item objectForKey:@"Message"];
                comment.ProfileID = [item objectForKey:@"ProfileID"] == [NSNull null] ? @"" : [item objectForKey:@"ProfileID"];
                comment.Username = [item objectForKey:@"Username"] == [NSNull null] ? @"" : [item objectForKey:@"Username"];
                comment.Fullname = [item objectForKey:@"Fullname"] == [NSNull null] ? @"" : [item objectForKey:@"Fullname"];
                comment.Avatar = [item objectForKey:@"Avatar"] == [NSNull null] ? @"" : [item objectForKey:@"Avatar"];
                comment.Avatar = [ClientUtils urlEncode:comment.Avatar];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                dateFormatter.dateFormat = @"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS";
                comment.DateCreated = [item objectForKey:@"DateCreated"] == [NSNull null] ? nil : [dateFormatter dateFromString:[item objectForKey:@"DateCreated"]];
                comment.Likes = [item objectForKey:@"Likes"] == [NSNull null] ? 0 : [[item objectForKey:@"Likes"] intValue];
                comment.Liked = [item objectForKey:@"Liked"] == [NSNull null] ? NO : [[item objectForKey:@"Liked"] boolValue];
                [comments addObject:comment];
            }
            
            success(comments);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) postComment:(UIView *)view
            username:(NSString *)username
         md5Password:(NSString *)md5Password
              postID:(NSString *)postID
             message:(NSString*)message
        successBlock:(void (^)(id responseObject))success
        failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password,
                             @"message": message
                             };
    [self loadService:view url:[NSString stringWithFormat:POST_COMMENT_URL, postID] params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) postReply:(UIView *)view
          username:(NSString *)username
       md5Password:(NSString *)md5Password
            postID:(NSString *)postID
           message:(NSString*)message
      successBlock:(void (^)(id responseObject))success
      failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password,
                             @"message": message
                             };
    [self loadService:view url:[NSString stringWithFormat:POST_REPLY_URL, postID] params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) getNotesOfArticle:(UIView *)view
                  username:(NSString *)username
               md5Password:(NSString *)md5Password
                    postID:(NSString *)postID
                      page:(int)page
              itemsPerPage:(int)itemsPerPage
              successBlock:(void (^)(id responseObject))success
              failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password,
                             @"page": [NSString stringWithFormat:@"%d", page],
                             @"itemsperpage": [NSString stringWithFormat:@"%d", itemsPerPage]
                             };
    
    [self loadService:view url:[NSString stringWithFormat:GET_NOTES_OF_ARTICLE_URL, postID] params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            NSMutableArray *notes = [[NSMutableArray alloc] init];
            
            for (uint i = 0; i < [responseObject count]; i++) {
                NSDictionary *item = [responseObject objectAtIndex:i];
                NoteModel *note = [[NoteModel alloc] init];
                note.ID = [item objectForKey:@"ID"];
                note.Message = [item objectForKey:@"Message"] == [NSNull null] ? @"" : [item objectForKey:@"Message"];
                note.ProfileID = [item objectForKey:@"ProfileID"] == [NSNull null] ? @"" : [item objectForKey:@"ProfileID"];
                note.Username = [item objectForKey:@"Username"] == [NSNull null] ? @"" : [item objectForKey:@"Username"];
                note.Fullname = [item objectForKey:@"Fullname"] == [NSNull null] ? @"" : [item objectForKey:@"Fullname"];
                note.Avatar = [item objectForKey:@"Avatar"] == [NSNull null] ? @"" : [item objectForKey:@"Avatar"];
                note.Avatar = [ClientUtils urlEncode:note.Avatar];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                dateFormatter.dateFormat = @"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS";
                note.DateCreated = [item objectForKey:@"DateCreated"] == [NSNull null] ? nil : [dateFormatter dateFromString:[item objectForKey:@"DateCreated"]];
                [notes addObject:note];
            }
            
            success(notes);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) postNoteForArticle:(UIView *)view
                   username:(NSString *)username
                md5Password:(NSString *)md5Password
                     postID:(NSString *)postID
                    message:(NSString*)message
               successBlock:(void (^)(id responseObject))success
               failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password,
                             @"message": message
                             };
    [self loadService:view url:[NSString stringWithFormat:POST_NOTE_FOR_ARTICLE_URL, postID] params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) deleteNote:(UIView *)view
           username:(NSString *)username
        md5Password:(NSString *)md5Password
             noteID:(NSString *)noteID
       successBlock:(void (^)(id responseObject))success
       failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password
                             };
    [self loadService:view url:[NSString stringWithFormat:DELETE_NOTE_URL, noteID] params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

//+ (void) getSharesOfArticle:(UIView *)view
//                   username:(NSString *)username
//                md5Password:(NSString *)md5Password
//postID:(NSString *)postID
//               successBlock:(void (^)(id responseObject))success
//               failureBlock:(void (^)(NSError *error))failure;
//+ (void) getLikesOfArticle:(UIView *)view
//                  username:(NSString *)username
//               md5Password:(NSString *)md5Password
//postID:(NSString *)postID
//              successBlock:(void (^)(id responseObject))success
//              failureBlock:(void (^)(NSError *error))failure;

+ (void) likePost:(UIView *)view
         username:(NSString *)username
      md5Password:(NSString *)md5Password
           postID:(NSString *)postID
     successBlock:(void (^)(id responseObject))success
     failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password
                             };
    [self loadService:view url:[NSString stringWithFormat:LIKE_POST_URL, postID] params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            success([responseObject objectForKey:@"Count"]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) unlikePost:(UIView *)view
           username:(NSString *)username
        md5Password:(NSString *)md5Password
             postID:(NSString *)postID
       successBlock:(void (^)(id responseObject))success
       failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password
                             };
    [self loadService:view url:[NSString stringWithFormat:UNLIKE_POST_URL, postID] params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            success([responseObject objectForKey:@"Count"]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

//+ (void) sharePost:(UIView *)view
//          username:(NSString *)username
//       md5Password:(NSString *)md5Password
//postID:(NSString *)postID
//      successBlock:(void (^)(id responseObject))success
//      failureBlock:(void (^)(NSError *error))failure;

+ (void) getSponsors:(UIView *)view
            username:(NSString *)username
         md5Password:(NSString *)md5Password
        successBlock:(void (^)(id responseObject))success
        failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password
                             };
    
    [self loadService:view url:GET_SPONSORS_URL params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            NSMutableArray *sponsors = [[NSMutableArray alloc] init];
            
            for (uint i = 0; i < [responseObject count]; i++) {
                NSDictionary *item = [responseObject objectAtIndex:i];
                SponsorModel *sponsor = [[SponsorModel alloc] init];
                sponsor.ID = [item objectForKey:@"ID"];
                sponsor.Address = [item objectForKey:@"Address"] == [NSNull null] ? @"" : [item objectForKey:@"Address"];
                sponsor.Email = [item objectForKey:@"Email"] == [NSNull null] ? @"" : [item objectForKey:@"Email"];
                sponsor.Fax = [item objectForKey:@"Fax"] == [NSNull null] ? @"" : [item objectForKey:@"Fax"];
                sponsor.Name = [item objectForKey:@"Name"] == [NSNull null] ? @"" : [item objectForKey:@"Name"];
                sponsor.Phone = [item objectForKey:@"Phone"] == [NSNull null] ? @"" : [item objectForKey:@"Phone"];
                sponsor.WebAddress = [item objectForKey:@"WebAddress"] == [NSNull null] ? @"" : [item objectForKey:@"WebAddress"];
                sponsor.ShortDescription = [item objectForKey:@"ShortDescription"] == [NSNull null] ? @"" : [item objectForKey:@"ShortDescription"];
                sponsor.Logo = [item objectForKey:@"Logo"] == [NSNull null] ? @"" : [item objectForKey:@"Logo"];
                sponsor.Logo = [ClientUtils urlEncode:sponsor.Logo];
                [sponsors addObject:sponsor];
            }
            
            success(sponsors);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) getDetailSponsor:(UIView *)view
                 username:(NSString *)username
              md5Password:(NSString *)md5Password
                sponsorID:(NSString *)sponsorID
             successBlock:(void (^)(id responseObject))success
             failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password
                             };
    
    [self loadService:view url:[NSString stringWithFormat:GET_SPONSOR_DETAIL_URL, sponsorID] params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            SponsorModel *sponsor = [[SponsorModel alloc] init];
            sponsor.ID = [responseObject objectForKey:@"ID"];
            sponsor.Address = [responseObject objectForKey:@"Address"] == [NSNull null] ? @"" : [responseObject objectForKey:@"Address"];
            sponsor.Email = [responseObject objectForKey:@"Email"] == [NSNull null] ? @"" : [responseObject objectForKey:@"Email"];
            sponsor.Fax = [responseObject objectForKey:@"Fax"] == [NSNull null] ? @"" : [responseObject objectForKey:@"Fax"];
            sponsor.Name = [responseObject objectForKey:@"Name"] == [NSNull null] ? @"" : [responseObject objectForKey:@"Name"];
            sponsor.Phone = [responseObject objectForKey:@"Phone"] == [NSNull null] ? @"" : [responseObject objectForKey:@"Phone"];
            sponsor.WebAddress = [responseObject objectForKey:@"WebAddress"] == [NSNull null] ? @"" : [responseObject objectForKey:@"WebAddress"];
            sponsor.ShortDescription = [responseObject objectForKey:@"ShortDescription"] == [NSNull null] ? @"" : [responseObject objectForKey:@"ShortDescription"];
            sponsor.Logo = [responseObject objectForKey:@"Logo"] == [NSNull null] ? @"" : [responseObject objectForKey:@"Logo"];
            sponsor.Logo = [ClientUtils urlEncode:sponsor.Logo];
            sponsor.BodyContent = [responseObject objectForKey:@"BodyContent"] == [NSNull null] ? @"" : [responseObject objectForKey:@"BodyContent"];
            success(sponsor);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (NSString *)generateBoundaryString {
    // generate boundary string
    //
    // adapted from http://developer.apple.com/library/ios/#samplecode/SimpleURLConnections
    
    CFUUIDRef  uuid;
    NSString  *uuidStr;
    
    uuid = CFUUIDCreate(NULL);
    
    uuidStr = CFBridgingRelease(CFUUIDCreateString(NULL, uuid));
    
    CFRelease(uuid);
    
    return [NSString stringWithFormat:@"Boundary-%@", uuidStr];
}

+ (void) saveProfilePicture:(UIView *)view
                   username:(NSString *)username
                md5Password:(NSString *)md5Password
                       data:(NSData *)data
                       name:(NSString *)name
                   filename:(NSString *)filename
                   filetype:(NSString *)filetype
               successBlock:(void (^)(id responseObject))success
               failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password
                             };
    // Show loading HUD
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading";
    
    NSMutableData *httpBody = [NSMutableData data];
    NSString *boundary = [ServiceClient generateBoundaryString];
    
    // configure the request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:UPLOAD_AVATAR_URL]];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setHTTPMethod:@"POST"];
    
    // set content type
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // add params (all params are strings)
    [params enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    // add image data
    if (data) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", name, filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", filetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:data];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:httpBody];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        [MBProgressHUD hideAllHUDsForView:view animated:YES];
        
        if (connectionError)
        {
            if (failure) {
                failure(connectionError);
            }
            return;
        }
        
        NSLog(@"Server Respons: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        
        NSError *err;
        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
        
        if ([[jsonObject objectForKey:@"Error"] intValue] == 0) {
            if (success) {
                success([jsonObject objectForKey:@"Data"]);
            }
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                           message:[jsonObject objectForKey:@"ErrorMessage"]
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;
            
            if (failure) {
                NSError *error = [NSError errorWithDomain:@"ServiceClient" code:[[jsonObject objectForKey:@"Error"] intValue] userInfo:@{@"ErrorMessage": [jsonObject objectForKey:@"ErrorMessage"]}];
                failure(error);
            }
        }
    }];
}

+ (void) searchUserFavoriteArticles:(UIView *)view
                           username:(NSString *)username
                        md5Password:(NSString *)md5Password
                         searchTerm:(NSString *)searchTerm
                               Page:(int)Page
                       ItemsPerPage:(int)ItemsPerPage
                       successBlock:(void (^)(id responseObject))success
                       failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password,
                             @"searchTerm":searchTerm,
                             @"Page":[NSString stringWithFormat:@"%d", Page],
                             @"ItemsPerPage":[NSString stringWithFormat:@"%d", ItemsPerPage]
                             };
    
    [self loadService:view url:SEARCH_MY_ARTICLES_URL params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            NSLog(@"DATA: %@",responseObject);
            NSMutableArray *articles = [[NSMutableArray alloc] init];
            
            for (uint i = 0; i < [responseObject count]; i++) {
                NSDictionary *item = [responseObject objectAtIndex:i];
                ArticleModel *article = [[ArticleModel alloc] init];
                article.ID = [item objectForKey:@"ID"];
                article.Title = [item objectForKey:@"Title"] == [NSNull null] ? @"" : [item objectForKey:@"Title"];
                article.Author = [item objectForKey:@"Author"] == [NSNull null] ? @"" : [item objectForKey:@"Author"];
                article.Body = [item objectForKey:@"Body"] == [NSNull null] ? @"" : [item objectForKey:@"Body"];
                article.Thumbnail = [item objectForKey:@"Thumbnail"] == [NSNull null] ? nil : [item objectForKey:@"Thumbnail"];
                article.Thumbnail = [ClientUtils urlEncode:article.Thumbnail];
                article.ShortDescription = [item objectForKey:@"ShortDescription"] == [NSNull null] ? @"" : [item objectForKey:@"ShortDescription"];
                article.Likes = [item objectForKey:@"Likes"] == [NSNull null] ? 0 : [[item objectForKey:@"Likes"] intValue];
                article.Liked = [item objectForKey:@"Liked"] == [NSNull null] ? NO : [[item objectForKey:@"Liked"] boolValue];
                article.Category = [item objectForKey:@"Categories"] == [NSNull null] ? @"" : [item objectForKey:@"Categories"];
                //                article.isFavorite = [responseObject objectForKey:@"Favorited"] == [NSNull null] ? NO : [[responseObject objectForKey:@"Favorited"] boolValue];
                [articles addObject:article];
            }
            success(articles);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(failure)
        {
            failure(error);
        }
    }];
}

+ (void) favoritePost:(UIView *)view
             username:(NSString *)username
          md5Password:(NSString *)md5Password
               postID:(NSString *)postID
         successBlock:(void (^)(id responseObject))success
         failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password
                             };
    [self loadService:view url:[NSString stringWithFormat:FAVORITE_POST_URL, postID] params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) unfavoritePost:(UIView *)view
               username:(NSString *)username
            md5Password:(NSString *)md5Password
                 postID:(NSString *)postID
           successBlock:(void (^)(id responseObject))success
           failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username": username,
                             @"password": md5Password
                             };
    [self loadService:view url:[NSString stringWithFormat:UNFAVORITE_POST_URL, postID] params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void) reportsArticle:(UIView *)view
               username:(NSString *)username
            md5Password:(NSString *)md5Password
                 postID:(NSString *)postID
                message:(NSString *)message
                   type:(NSString *)type
           successBlock:(void (^)(id responseObject))success
           failureBlock:(void (^)(NSError *error))failure {
    
    NSDictionary *params = @{
                             @"username":username,
                             @"password":md5Password,
                             @"message":message,
                             @"type":type
                             };
    [self loadService:view url:[NSString stringWithFormat:REPORT_POST_URL, postID] params:params successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(success)
        {
            success(responseObject);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(failure)
        {
            failure(error);
        }
    }];
}


@end
