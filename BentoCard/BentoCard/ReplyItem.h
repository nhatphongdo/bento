//
//  ReplyItem.h
//  BentoCard
//
//  Created by VO HONG HAI on 12/19/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentItemArticle.h"
//#import "ArticleViewController.h"

@interface ReplyItem : UIViewController

@property (strong, nonatomic) CommentItemArticle *commentContainer;

@property (strong, nonatomic) NSString *replyID;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *body;
@property (strong, nonatomic) NSString *avatar;
@property (nonatomic) int likecount;
@property (nonatomic) BOOL liked;

@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *contentLabel;
@property (strong, nonatomic) IBOutlet UIButton *replylikeButton;

- (IBAction)likeButtonTouched:(id)sender;

@end
