//
//  ItemActionArticle.h
//  BentoCard
//
//  Created by VO HONG HAI on 12/16/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemActionArticle : UIViewController

@property (nonatomic) NSString *title;

@property (nonatomic,strong) IBOutlet UIButton *collapseButton;
@property (nonatomic,strong) IBOutlet UIButton *editButton;
@property (nonatomic,strong) IBOutlet UILabel *titleLabel;

@end
