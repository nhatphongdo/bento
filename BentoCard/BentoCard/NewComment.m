//
//  NewComment.m
//  BentoCard
//
//  Created by VO HONG HAI on 1/5/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import "NewComment.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import "Constrain.h"

@interface NewComment ()

@end

@implementation NewComment

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)replyTouched:(id)sender {
    
    if ([self.txtComment.text isEqualToString:@""]) {
        return;
    }
    
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    if([self.type isEqualToString:REPLY_COMMENT])
    {
        [ServiceClient postReply:self.view username:username md5Password:password postID:self.postID message:self.txtComment.text successBlock:^(id responseObject) {
            // Post comment succeeded
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                           message:@"Your comment is saved successfully"
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;
            if(self.commentContainer)
            {
                [self.commentContainer reloadReplyShow:self.index];
            }
            [self.view removeFromSuperview];
            [ClientUtils removeBlur:self.parentViewController.view];
            [self willMoveToParentViewController:nil];
            [self removeFromParentViewController];
            
        } failureBlock:^(NSError *error) {
        }];
    }
    else if([self.type isEqualToString:COMMENT_ARTICLE])
    {
        [ServiceClient postComment:self.view username:username md5Password:password postID:self.postID message:self.txtComment.text successBlock:^(id responseObject) {
            // Post comment succeeded
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                           message:@"Your comment is saved successfully"
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;
            if(self.articleContainer)
            {
                [self.articleContainer loadCommentView];
            }
            [self.view removeFromSuperview];
            [ClientUtils removeBlur:self.parentViewController.view];
            [self willMoveToParentViewController:nil];
            [self removeFromParentViewController];
            
        } failureBlock:^(NSError *error) {
        }];
    }
}

- (IBAction)closeTouched:(id)sender {
    [self.view removeFromSuperview];
    [ClientUtils removeBlur:self.parentViewController.view];
    [self willMoveToParentViewController:nil];
    [self removeFromParentViewController];
}

@end
