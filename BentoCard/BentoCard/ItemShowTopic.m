//
//  ItemShowTopic.m
//  BentoCard
//
//  Created by VO HONG HAI on 1/23/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import "ItemShowTopic.h"
#import "TempData.h"
#import "SubShowTopic.h"
#import "SubTopicViewController.h"
#import "Constrain.h"

@interface ItemShowTopic () {
}

@end

@implementation ItemShowTopic

static const float GAP = 10.0f;
static const float rotation_duration = 0.1f;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.nameLabel.text = self.interest.Name;
    self.countLabel.text = [NSString stringWithFormat:@"%d",self.interest.Total];
    self.isCollapse = YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSMutableArray *)getListInterests {
    TempData *data = [TempData getInstance];
    NSMutableArray *childInterests = [[NSMutableArray alloc] init];
    if([data.State isEqualToString:NAV_ARTICLE]) {
        if(data.interests.count > 0)
        {
            for (InterestModel *item in data.interests)
            {
                if([item.ParentID isEqualToString:self.interest.ID])
                {
                    [childInterests addObject:item];
                }
            }
        }
    }
    if([data.State isEqualToString:NAV_MANAGE]) {
        if(data.userinterests.count > 0)
        {
            for (InterestModel *item in data.userinterests)
            {
                if([item.ParentID isEqualToString:self.interest.ID])
                {
                    [childInterests addObject:item];
                }
            }
        }
    }
    return childInterests;
}

- (void)clearContainer {
    NSArray *subviews = [self.containerView subviews];
    NSArray *controllers = [self childViewControllers];
    for (UIView *item in subviews) {
        [item removeFromSuperview];
    }
    
    for (UIViewController *item in controllers)
    {
        [item willMoveToParentViewController:nil];
        [item removeFromParentViewController];
    }
}

- (IBAction)arrowButton_touched:(id)sender {
    [self clearContainer];
    if(self.isCollapse == YES)
    {
        CABasicAnimation* rotationAnimation;
        rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        rotationAnimation.toValue = [NSNumber numberWithFloat:M_PI_2];
        rotationAnimation.duration = rotation_duration;
        rotationAnimation.removedOnCompletion = NO;
        rotationAnimation.fillMode = kCAFillModeForwards;
        [self.arrowImage.layer addAnimation:rotationAnimation forKey:@"rotationAnimation1"];
        
        NSMutableArray *childinterests = [self getListInterests];
        if(childinterests.count > 0)
        {
            float y = 0.0f;
            for (InterestModel *item in childinterests)
            {
                SubShowTopic *subtopic = [[SubShowTopic alloc] initWithNibName:@"SubShowTopic" bundle:nil];
                subtopic.interest = item;
                
                CGRect frame = subtopic.view.frame;
                frame.size.width = self.view.frame.size.width;
                frame.origin.y = y;
                subtopic.view.frame = frame;
                UIView *topBorder = [[UIView alloc] initWithFrame:CGRectMake(subtopic.nameLabel.frame.origin.x, 0, subtopic.view.frame.size.width - subtopic.nameLabel.frame.origin.x, 1)];
                topBorder.backgroundColor = [UIColor colorWithRed:190/255.0f green:190/255.0f blue:190/255.0f alpha:1];
                [subtopic.view addSubview:topBorder];
                
                [self.containerView addSubview:subtopic.view];
                [self addChildViewController:subtopic];
                [subtopic didMoveToParentViewController:self];
                
                y+=frame.size.height;
            }
            CGRect childFrame = self.containerView.frame;
            childFrame.size.height = y;
            self.containerView.frame = childFrame;
            
            CGRect mainFrame = self.view.frame;
            mainFrame.size.height = childFrame.origin.y + y;
            self.view.frame = mainFrame;
        }
        self.isCollapse = NO;
    }
    else
    {
        CABasicAnimation* rotationAnimation;
        rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        rotationAnimation.toValue = [NSNumber numberWithFloat:0];
        rotationAnimation.duration = rotation_duration;
        rotationAnimation.removedOnCompletion = NO;
        rotationAnimation.fillMode = kCAFillModeForwards;
        [self.arrowImage.layer addAnimation:rotationAnimation forKey:@"rotationAnimation2"];
        
        CGRect childFrame = self.containerView.frame;
        childFrame.size.height = 1.0f;
        self.containerView.frame = childFrame;
        
        CGRect mainFrame = self.view.frame;
        mainFrame.size.height = childFrame.origin.y + 1.0f;
        self.view.frame = mainFrame;
        self.isCollapse = YES;
    }
    if(self.articlesidebar)
    {
        [self.articlesidebar refreshContainer];
    }
}

- (IBAction)titleButton_touched:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SubTopicViewController *myview = [storyboard instantiateViewControllerWithIdentifier:@"SubTopicView"];
    myview.topicID = self.interest.ID;
    [self.navigationController pushViewController:myview animated:YES];
}

@end
