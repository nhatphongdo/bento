//
//  ItemTopicMagView.h
//  BentoCard
//
//  Created by VO HONG HAI on 12/11/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemTopicMagView : UIViewController

@property (nonatomic) NSString *topicID;
@property (nonatomic) NSString *topic;
@property (nonatomic) int number;

@property (nonatomic, strong) IBOutlet UILabel *topicLabel;
@property (nonatomic, strong) IBOutlet UILabel *countLabel;
@property (nonatomic, strong) IBOutlet UIButton *overlayButton;

- (float)getHeight;
- (IBAction)overlayTouched:(id)sender;

@end
