//
//  SocialPageViewController.h
//  BentoCard
//
//  Created by VO HONG HAI on 3/5/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialPageViewController : UIViewController

@property (strong, nonatomic) NSArray *cardList;
@property (strong, nonatomic) NSMutableArray *menuArray;
@property (nonatomic) float oldMenuPostX;


@property (strong, nonatomic) IBOutlet UIView *statusViews;
@property (strong, nonatomic) IBOutlet UIView *wrapthumbView;
@property (strong, nonatomic) IBOutlet UIImageView *cricleImage;
@property (strong, nonatomic) IBOutlet UIImageView *avatarImage;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (strong, nonatomic) IBOutlet UIScrollView *containerScrollView;
@property (strong, nonatomic) IBOutlet UIScrollView *menuScrollView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *btStatusLeft;
@property (strong, nonatomic) IBOutlet UIButton *btStatusRight;
@property (strong, nonatomic) IBOutlet UIButton *btBackRight;

- (void)loadMenuList:(NSMutableArray *)menuLists activeTopic:(NSString *)activeTopic;
- (void)createCardList:(NSArray *)cards;
- (void)createCardListContinues:(NSArray *)cards;
- (void)refreshView;
- (IBAction)btBackTouched:(id)sender;

@end
