//
//  TempData.m
//  BentoCard
//
//  Created by VO HONG HAI on 1/16/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import "TempData.h"

@implementation TempData

@synthesize interests;
@synthesize userinterests;
@synthesize navigations;
@synthesize State;

static TempData *instance = nil;
+(TempData *)getInstance
{
    @synchronized(self)
    {
        if(instance==nil)
        {
            instance= [TempData new];
        }
    }
    return instance;
}

@end
