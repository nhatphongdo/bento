//
//  ListItemNote.h
//  BentoCard
//
//  Created by VO HONG HAI on 12/18/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListItemNote : UIViewController

@property (nonatomic,retain) NSString *datetime;
@property (nonatomic,retain) NSString *note;

@property (nonatomic,strong) IBOutlet UILabel *datetimeLabel;
@property (nonatomic,strong) IBOutlet UILabel *noteLabel;

@end
