//
//  ProfileCardViewController.h
//  BentoCard
//
//  Created by VO HONG HAI on 3/5/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileModel.h"

@interface ProfileCardViewController : UIViewController

@property (nonatomic) ProfileModel *profile;
@property (nonatomic) NSString *titleCard;

@property (nonatomic,strong) IBOutlet UILabel *titleLabel;
@property (nonatomic,strong) IBOutlet UIButton *titleCardButton;
@property (nonatomic,strong) IBOutlet UIButton *addressButton;
@property (nonatomic,strong) IBOutlet UIButton *datetimeButton;
@property (nonatomic,strong) IBOutlet UIButton *viewmoreButton;
@property (nonatomic,strong) IBOutlet UITextView *descTextView;

- (void)loadData:(ProfileModel *)profile title:(NSString *)title;
@end
