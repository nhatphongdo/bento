//
//  ArticleSideBarViewController.m
//  BentoCard
//
//  Created by VO HONG HAI on 1/23/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import "ArticleSideBarViewController.h"
#import "UIImage+DeviceSpecificMedia.h"
#import "Constrain.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import "InterestModel.h"
#import "TempData.h"
#import "ItemShowTopic.h"
#import "itemSettingSideBar.h"
#import <PKRevealController/PKRevealController.h>

@interface ArticleSideBarViewController ()
{
    TempData *data;
}
@end

@implementation ArticleSideBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    data = [TempData getInstance];
    // Do any additional setup after loading the view from its nib.
    // Default load data for article
    CGRect frame = self.view.frame;
    frame.size.width = SIZE_SIDEBAR;
    self.view.frame = frame;
    
    [self loadDataShowTopic];
    [self loadDataSetting];
    
    //init button hover
    UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, self.settingButton.frame.size.height - 1.0f, self.settingButton.frame.size.width, 1)];
    bottomBorder.backgroundColor = [UIColor colorWithRed:194.0f/255.0f green:194.0f/255.0f blue:194.0f/255.0f alpha:1];
    [self.settingButton addSubview:bottomBorder];
    
    UIView *leftBorder = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, self.settingButton.frame.size.height)];
    leftBorder.backgroundColor = [UIColor colorWithRed:194.0f/255.0f green:194.0f/255.0f blue:194.0f/255.0f alpha:1];
    [self.settingButton addSubview:leftBorder];
    
    [self.settingButton setBackgroundColor:[UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1]];
    [self clearSubViewButton:self.showtopicButton];
    [self.containerView setHidden:NO];
    [self.settingView setHidden:YES];
    
    //check State of globel variable
    if([data.State isEqualToString:NAV_MANAGE]) {
        self.showtopicButton.hidden = YES;
        self.settingButton.hidden = YES;
        UIImage *titleImage = [UIImage imageForDeviceWithName:@"icon_sidebar_folder.png"];
        [self.titleButton setImage:titleImage forState:UIControlStateNormal];
        [self.titleButton setTitle:@"MANAGE" forState:UIControlStateNormal];
        [self.showtopicTitleLabel setText:@"All Saved Articles"];
        float newposY = self.tabsView.frame.origin.y + self.tabsView.frame.size.height;
        CGRect frame = self.containerView.frame;
        frame.size.height += (frame.origin.y - newposY);
        frame.origin.y = newposY;
        self.containerView.frame = frame;
    }
    
    // init Swipe Gesture
    UISwipeGestureRecognizer *swipeleft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
}

-(void)swipeleft:(UISwipeGestureRecognizer *)gestureRecognizer {
    [self.revealController showViewController:self.revealController.frontViewController];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)clearContainer {
    //clear subview
    NSArray *views = [self.containerScrollView subviews];
    NSArray *controllers = [self childViewControllers];
    
    for (UIView *item in views)
    {
        if([item isMemberOfClass:[ItemShowTopic class]])
        {
            [item removeFromSuperview];
        }
    }
    for (UIViewController *item in controllers)
    {

        if([item isMemberOfClass:[ItemShowTopic class]])
        {
            [item willMoveToParentViewController:nil];
            [item removeFromParentViewController];
        }
    }
}

- (void)loadDataShowTopic {
    [self clearContainer];
    if([data.State isEqualToString:NAV_ARTICLE]) {
        if(data.interests.count > 0)
        {
            float y = 0.0f;
            int totalarticle = 0;
            for (InterestModel *item in data.interests)
            {
                if([item.ParentID isEqualToString:@""])
                {
                    ItemShowTopic *topic = [[ItemShowTopic alloc] initWithNibName:@"ItemShowTopic" bundle:nil];
                    topic.interest = item;
                    topic.articlesidebar = self;
                    
                    CGRect frame = topic.view.frame;
                    frame.origin.y = y;
                    frame.size.width = self.view.frame.size.width;
                    topic.view.frame = frame;
                    
                    UIView *topBorder = [[UIView alloc] initWithFrame:CGRectMake(topic.nameLabel.frame.origin.x, 0, topic.view.frame.size.width - (topic.nameLabel.frame.origin.x), 1)];
                    topBorder.backgroundColor = [UIColor colorWithRed:190/255.0f green:190/255.0f blue:190/255.0f alpha:1];
                    [topic.view addSubview:topBorder];
                    
                    [self.containerScrollView addSubview:topic.view];
                    [self addChildViewController:topic];
                    [topic didMoveToParentViewController:self];
                    
                    y+=frame.size.height;
                    totalarticle+=item.Total;
                }
            }
            self.allTotalLabel.text = [NSString stringWithFormat:@"%d",totalarticle];
            self.containerScrollView.contentSize = CGSizeMake(self.containerScrollView.frame.size.width, y);
        }
    }
    if([data.State isEqualToString:NAV_MANAGE]) {
        if(data.userinterests.count > 0)
        {
            float y = 0.0f;
            int totalarticle = 0;
            for (InterestModel *item in data.userinterests)
            {
                if([item.ParentID isEqualToString:@""])
                {
                    ItemShowTopic *topic = [[ItemShowTopic alloc] initWithNibName:@"ItemShowTopic" bundle:nil];
                    topic.interest = item;
                    topic.articlesidebar = self;
                    
                    CGRect frame = topic.view.frame;
                    frame.origin.y = y;
                    frame.size.width = self.view.frame.size.width;
                    topic.view.frame = frame;
                    
                    UIView *topBorder = [[UIView alloc] initWithFrame:CGRectMake(topic.nameLabel.frame.origin.x, 0, topic.view.frame.size.width - (topic.nameLabel.frame.origin.x), 1)];
                    topBorder.backgroundColor = [UIColor colorWithRed:190/255.0f green:190/255.0f blue:190/255.0f alpha:1];
                    [topic.view addSubview:topBorder];
                    
                    [self.containerScrollView addSubview:topic.view];
                    [self addChildViewController:topic];
                    [topic didMoveToParentViewController:self];
                    
                    y+=frame.size.height;
                    totalarticle+=item.Total;
                }
            }
            self.allTotalLabel.text = [NSString stringWithFormat:@"%d",totalarticle];
            self.containerScrollView.contentSize = CGSizeMake(self.containerScrollView.frame.size.width, y);
        }
    }
}

- (void)refreshContainer {
    NSArray *subviews = [self.containerScrollView subviews];
    float y = 0.0f;
    for (UIView *item in subviews)
    {
        if(![item isKindOfClass:[UIImageView class]])
        {
            CGRect frame = item.frame;
            frame.origin.y = y;
            item.frame = frame;
            y+=frame.size.height;
        }
    }
    self.containerScrollView.contentSize = CGSizeMake(self.containerScrollView.frame.size.width, y);
}

- (void)clearSubViewButton:(UIButton *)button {
    
    [button setBackgroundColor:[UIColor clearColor]];
    NSArray *subviews = [button subviews];
    for (UIView *item in subviews)
    {
        if(![item isKindOfClass:[UILabel class]] && ![item isKindOfClass:[UIImageView class]])
        {
            [item removeFromSuperview];
        }
    }
}

- (void)loadDataSetting {
    [self clearSettingView];
    
    if(data.interests.count > 0)
    {
        float y = 0.0f;
        for (InterestModel *item in data.interests)
        {
            if([item.ParentID isEqualToString:@""])
            {
                itemSettingSideBar *settingitem = [[itemSettingSideBar alloc] initWithNibName:@"itemSettingSideBar" bundle:nil];
                settingitem.interest = item;
                
                CGRect frame = settingitem.view.frame;
                frame.origin.y = y;
                frame.size.width = self.view.frame.size.width;
                settingitem.view.frame = frame;
                
                UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(settingitem.nameLabel.frame.origin.x - 8.0f, settingitem.view.frame.size.height - 1, settingitem.view.frame.size.width - (settingitem.nameLabel.frame.origin.x - 8.0f), 1)];
                bottomBorder.backgroundColor = [UIColor colorWithRed:205.0f/255.0f green:205.0f/255.0f blue:205.0f/255.0f alpha:1];
                [settingitem.view addSubview:bottomBorder];
                
                [self.settingScrollView addSubview:settingitem.view];
                [self addChildViewController:settingitem];
                [settingitem didMoveToParentViewController:self];
                
                y+=frame.size.height;
            }
        }
        
        //update bottom line
        NSArray *subviews = [self.settingScrollView subviews];
        UIView *settingview = [subviews objectAtIndex:subviews.count - 1];
        NSArray *childsubviews = [settingview subviews];
        UIView *line = [childsubviews objectAtIndex:childsubviews.count - 1];
        [line removeFromSuperview];
        UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, settingview.frame.size.height - 1, self.settingScrollView.frame.size.width, 1)];
        bottomBorder.backgroundColor = [UIColor colorWithRed:205.0f/255.0f green:205.0f/255.0f blue:205.0f/255.0f alpha:1];
        [settingview addSubview:bottomBorder];
        
        //add extent height to bottom
        UIView *extentView = [[UIView alloc] initWithFrame:CGRectMake(0, y, self.settingScrollView.frame.size.width, settingview.frame.size.height)];
        [self.settingScrollView addSubview:extentView];
        y+=settingview.frame.size.height;
        
        self.settingScrollView.contentSize = CGSizeMake(self.settingScrollView.frame.size.width, y);
    }
}

- (void)clearSettingView {
    //clear subview
    NSArray *views = [self.settingScrollView subviews];
    NSArray *controllers = [self childViewControllers];
    
    for (UIView *item in views)
    {
        if([item isMemberOfClass:[itemSettingSideBar class]])
        {
            [item removeFromSuperview];
        }
    }
    for (UIViewController *item in controllers)
    {
        
        if([item isMemberOfClass:[itemSettingSideBar class]])
        {
            [item willMoveToParentViewController:nil];
            [item removeFromParentViewController];
        }
    }
}

- (IBAction)showtopicTouched:(id)sender {
    
    UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, self.settingButton.frame.size.height - 1.0f, self.settingButton.frame.size.width, 1)];
    bottomBorder.backgroundColor = [UIColor colorWithRed:194.0f/255.0f green:194.0f/255.0f blue:194.0f/255.0f alpha:1];
    [self.settingButton addSubview:bottomBorder];
    
    UIView *leftBorder = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, self.settingButton.frame.size.height)];
    leftBorder.backgroundColor = [UIColor colorWithRed:194.0f/255.0f green:194.0f/255.0f blue:194.0f/255.0f alpha:1];
    [self.settingButton addSubview:leftBorder];
    
    [self.settingButton setBackgroundColor:[UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1]];
    [self clearSubViewButton:self.showtopicButton];
    [self.containerView setHidden:NO];
    [self.settingView setHidden:YES];
}

- (IBAction)setttingTouched:(id)sender {
    
    UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, self.settingButton.frame.size.height - 1.0f, self.settingButton.frame.size.width, 1)];
    bottomBorder.backgroundColor = [UIColor colorWithRed:194.0f/255.0f green:194.0f/255.0f blue:194.0f/255.0f alpha:1];
    [self.showtopicButton addSubview:bottomBorder];
    
    UIView *rightBorder = [[UIView alloc] initWithFrame:CGRectMake(self.showtopicButton.frame.size.width - 1, 0, 1, self.settingButton.frame.size.height)];
    rightBorder.backgroundColor = [UIColor colorWithRed:194.0f/255.0f green:194.0f/255.0f blue:194.0f/255.0f alpha:1];
    [self.showtopicButton addSubview:rightBorder];
    
    [self.showtopicButton setBackgroundColor:[UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1]];
    [self clearSubViewButton:self.settingButton];
    [self.containerView setHidden:YES];
    [self.settingView setHidden:NO];
}


@end
