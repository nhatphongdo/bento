//
//  ArticleViewController.m
//  BentoCard
//
//  Created by VO HONG HAI on 12/16/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import "ArticleViewController.h"
#import "Constrain.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import "ArticleModel.h"
#import <PKRevealController/PKRevealController.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "UIImage+DeviceSpecificMedia.h"
#import "SRRefreshView.h"
#import "ArticleModel.h"
#import <QuartzCore/QuartzCore.h>
#import "ShareItemArticle.h"
#import "CommentItemArticle.h"
#import "NoteItemArticle.h"
#import "NewReport.h"
#import "NavigateViewController.h"
#import "TempData.h"

@interface ArticleViewController ()
{
    ArticleModel *article;
    BOOL isSlideUp;
    UIWebView *contentWebView;
    float curFrameWidth;
    float posYofComment, posYofNote, posYofShare;
}
@end

static float const duration = 0.5f;
static int const collapseHeight = 213;
static int const GAP = 20;

@implementation ArticleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    // webview tag = 9, noteview tag = 99, shareview tag = 999, commentview = 9999
    //init webview
    contentWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.bodyScrollView.frame.size.width,self.bodyScrollView.frame.size.height)];
    contentWebView.scrollView.scrollEnabled = NO;
    [contentWebView setDelegate:self];
    contentWebView.tag = 9;
    [self.bodyScrollView addSubview:contentWebView];
    
    [self.bodyScrollView setDelegate:self];

    isSlideUp = NO;
    curFrameWidth = self.bodyScrollView.frame.size.width;
    UIImage *defaultimage = [UIImage imageForDeviceWithName:@"default_placeholder.png"];
    [self.thumbnailImage setImage:defaultimage];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadContent {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    [self.titleLabel setText:self.topicName];
    [ServiceClient getDetailArticle:self.view username:username md5Password:password articleID:self.postID successBlock:^(id responseObject) {
        article = responseObject;
        self.title = self.topicName;
        
        [self.thumbnailImage setImageWithURL:[NSURL URLWithString:article.Thumbnail]];
        if(self.liked == YES)
        {
            [self.likeButton setImage:[UIImage imageNamed:@"iconcommentliked"] forState:UIControlStateNormal];
        }
        else
        {
            [self.likeButton setImage:[UIImage imageNamed:@"iconcommentlike"] forState:UIControlStateNormal];
        }
        if(article.isFavorite == YES)
        {
            [self.saveButton setImage:[UIImage imageNamed:@"iconSave_pressed"] forState:UIControlStateNormal];
        }
        else
        {
            [self.saveButton setImage:[UIImage imageNamed:@"iconSave"] forState:UIControlStateNormal];
        }
        
        [self.likeButton setTitle:[NSString stringWithFormat:@"%d",self.likes] forState:UIControlStateNormal];
        [self setBody:article.Body];
    } failureBlock:^(NSError *error) {
        
    }];
}

- (IBAction)hidePreviewTouched:(id)sender {
    [self.previewView setHidden:YES];
    [self.hidePreviewButton setHidden:YES];
}

- (void)setBody:(NSString *)body {
    if (body == nil) {
        [contentWebView loadHTMLString:@"" baseURL:nil];
        return;
    }
    
    NSDateFormatter *formated = [[NSDateFormatter alloc] init];
    formated.dateStyle = NSDateFormatterLongStyle;
    formated.timeStyle = NSDateFormatterNoStyle;
    NSString *datecreated = [formated stringFromDate:article.DateCreated];
    if ([body rangeOfString:@"<html>" options:NSCaseInsensitiveSearch].location == NSNotFound) {
        body = [NSString stringWithFormat:@"\
                <html>\
                <head>\
                <style>\
                a {\
                cursor: pointer;\
                }\
                img {\
                max-width: 100%%;\
                height: auto;\
                }\
                </style>\
                <script>\
                window.onload = function() {\
                window.location.href = 'ready://' + document.body.offsetHeight;\
                }\
                </script>\
                </head>\
                <body style='color: #333; font-family: Helvetica; font-size: 12px;'>\
                <h2>%@</h2>\
                <em style='font-size:9px; color:#848283;'>%@&nbsp&nbsp&nbsp&nbsp%@</em></br>\
                %@</body>\
                </html>",article.Title,article.Author, datecreated,body];
    }
    
    [contentWebView loadHTMLString:body baseURL:nil];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSString *jsContent = @"\
    var images = document.getElementsByTagName('img');\
    for (var i = images.length - 1; i >= 0; i--) {\
    images[i].style.display = 'none';\
    var anchor = document.createElement('a');\
    anchor.href = 'nolink://#' + images[i].src;\
    var img = document.createElement('img');\
    img.src = images[i].src;\
    anchor.appendChild(img);\
    var parent = images[i].parentNode;\
    parent.insertBefore(anchor, images[i]);\
    }\
    ";
    
    [webView stringByEvaluatingJavaScriptFromString:jsContent];
}

- (float)getScrollContentHeight:(int)tag {
    float totalHeight = 0;
    for(UIView *zview in self.bodyScrollView.subviews)
    {
        if(zview.tag <= tag && zview.frame.origin.x == 0)
        {
            totalHeight +=zview.frame.size.height;
        }
    }
    return totalHeight;
}

- (void)removeChildView:(int)tag {
    //remove old noteview, noteviewcontroller
    NSArray *uiviewcontrollers = [self childViewControllers];
    NSArray *subviews = [self.bodyScrollView subviews];
    switch (tag) {
        case 99:
        {
            for (UIViewController *item in uiviewcontrollers)
            {
                if([item.nibName isEqualToString:@"NoteItemArticle"])
                {
                    [item willMoveToParentViewController:nil];
                    [item removeFromParentViewController];
                }
            }
            
            for (UIView *item in subviews) {
                if(item.tag == 99)
                {
                    [item removeFromSuperview];
                }
            }
            break;
        }
        case 999:
        {
            for (UIViewController *item in uiviewcontrollers)
            {
                if([item.nibName isEqualToString:@"ShareItemArticle"])
                {
                    [item willMoveToParentViewController:nil];
                    [item removeFromParentViewController];
                }
            }
            
            for (UIView *item in subviews) {
                if(item.tag == 999)
                {
                    [item removeFromSuperview];
                }
            }
            break;
        }
        case 9999:
        {
            for (UIViewController *item in uiviewcontrollers)
            {
                if([item.nibName isEqualToString:@"CommentItemArticle"])
                {
                    [item willMoveToParentViewController:nil];
                    [item removeFromParentViewController];
                }
            }
            
            for (UIView *item in subviews) {
                if(item.tag == 9999)
                {
                    [item removeFromSuperview];
                }
            }
            break;
        }
        default:
            break;
    }
}

- (void)loadNoteView {
    //remove old noteview
    [self removeChildView:99];
    
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    [ServiceClient getNotesOfArticle:self.view username:username md5Password:password postID:self.postID page:1 itemsPerPage:9999 successBlock:^(id responseObject) {
        NSMutableArray *listItem = responseObject;
        NoteItemArticle *noteItem = [[NoteItemArticle alloc] initWithNibName:@"NoteItemArticle" bundle:nil];
        noteItem.view.tag = 99;
        noteItem.postID = article.ID;
        CGRect frame = noteItem.view.frame;
        float totalHeight = [self getScrollContentHeight:9];
        posYofNote = totalHeight;
        frame.origin.y = totalHeight;
        frame.size.width = curFrameWidth;
        noteItem.view.frame = frame;
        [self addChildViewController:noteItem];
        [self.bodyScrollView addSubview:noteItem.view];
        [noteItem didMoveToParentViewController:self];
        float extentHeight = [noteItem loadNotes:listItem];
        self.bodyScrollView.contentSize = CGSizeMake(self.bodyScrollView.contentSize.width, totalHeight + extentHeight);
        
        //add Share
        [self loadShareView];
    } failureBlock:^(NSError *error) {
    }];
}

- (void)loadShareView {
    //remove old shareview
    [self removeChildView:999];
    
    ShareItemArticle *shareview = [[ShareItemArticle alloc] initWithNibName:@"ShareItemArticle" bundle:nil];
    shareview.view.tag = 999;
    shareview.message = @"";
    shareview.url = @"";
    shareview.title = @"";
    float totalHeight = [self getScrollContentHeight:99];
    
    CGRect frame = shareview.view.frame;
    posYofShare = totalHeight;
    frame.origin.y = totalHeight;
    frame.size.width = curFrameWidth;
    shareview.view.frame = frame;
    [self addChildViewController:shareview];
    [self.bodyScrollView addSubview:shareview.view];
    [shareview didMoveToParentViewController:self];
    self.bodyScrollView.contentSize = CGSizeMake(self.bodyScrollView.contentSize.width, totalHeight + shareview.view.frame.size.height);

    //add comment
    [self loadCommentView];

}

- (void)loadCommentView {
    //remove old commentview
    [self removeChildView:9999];
    
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    [ServiceClient getCommentsOfArticle:self.view username:username md5Password:password postID:self.postID page:1 itemsPerPage:9999 successBlock:^(id responseObject) {
        // Load articles succeeded
        NSMutableArray *listItem = responseObject;
        float totalHeight = [self getScrollContentHeight:999];
        CommentItemArticle *commentview = [[CommentItemArticle alloc] initWithNibName:@"CommentItemArticle" bundle:nil];
        commentview.view.tag = 9999;
        commentview.articleContainer = self;
        commentview.comments = listItem;
        commentview.postID = self.postID;
        CGRect frame = commentview.view.frame;
        posYofComment = totalHeight;
        frame.origin.y = totalHeight;
        frame.size.width = curFrameWidth;
        commentview.view.frame = frame;
        float commentHeight = [commentview loadComment:listItem];
        [self addChildViewController:commentview];
        [self.bodyScrollView addSubview:commentview.view];
        [commentview didMoveToParentViewController:self];
        self.bodyScrollView.contentSize = CGSizeMake(self.bodyScrollView.contentSize.width, totalHeight + commentHeight);
        //save view to navigate
        [ClientUtils SaveNavigation:self Name:self.title level:NAV_ARTICLE_LEVEL ParentId:self.topicID isSameView:NO];
    } failureBlock:^(NSError *error) {
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView.contentOffset.y > 50 && isSlideUp == NO)
    {
        isSlideUp = YES;
        [UIView animateWithDuration:duration animations:^{
            self.wrapthumbView.frame = CGRectMake(0, self.wrapthumbView.frame.origin.y - self.statusViews.frame.size.height, self.wrapthumbView.frame.size.width, self.wrapthumbView.frame.size.height - collapseHeight);
            [self.view bringSubviewToFront:self.statusViews];
            [self.statusViews setBackgroundColor:[UIColor clearColor]];
            //todo: set webview move up
            CGRect frame = scrollView.frame;
            frame.origin.y = self.wrapthumbView.frame.origin.y + self.wrapthumbView.frame.size.height;
            frame.size.height += (collapseHeight + self.statusViews.frame.size.height);
            scrollView.frame = frame;
            
            //change background tools box
            UIImage *bgtoolsbox = [UIImage imageForDeviceWithName:@"toolBoxsCollapse.png"];
            [self.toolsboxbg setImage:bgtoolsbox];
            
            //todo: set button move
            CGRect btrightFrame = self.rightButton.frame;
            CGRect btbackFrame = self.backButton.frame;
            btrightFrame.origin.x -= btbackFrame.size.width;
            self.rightButton.frame = btrightFrame;
            
            btbackFrame.origin.y -= (self.statusViews.frame.size.height - btrightFrame.origin.y);
            self.backButton.frame = btbackFrame;
            [self.backButton setTitle:@"X" forState:UIControlStateNormal];
            [self.backButton setImage:nil forState:UIControlStateNormal];
            [self.view bringSubviewToFront:self.backButton];
        
            
        } completion:^(BOOL finished) {
            if(finished)
            {
                float posY = self.wrapthumbView.frame.size.height - (self.toolBoxs.frame.size.height + 30);
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(GAP, posY, self.wrapthumbView.frame.size.width - GAP*2, 30)];
                titleLabel.textColor = [UIColor whiteColor];
                titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15.0f];
                titleLabel.textAlignment = NSTextAlignmentCenter;
                titleLabel.text = article.Title;
                [self.wrapthumbView addSubview:titleLabel];
            }
        }];
    }
    if(scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
//        NSLog(@"BOTTOM REACHED");
    }
    if(scrollView.contentOffset.y < -50.0 && isSlideUp == YES) {
        //remove TitleLabel
        NSArray *subViews = [self.wrapthumbView subviews];
        for(UIView *item in subViews)
        {
            if ([item isMemberOfClass:[UILabel class]]) {
                [item removeFromSuperview];
            }
        }
        isSlideUp = NO;
        [UIView animateWithDuration:duration animations:^{
            //todo: set webview move up
            self.wrapthumbView.frame = CGRectMake(0, self.statusViews.frame.size.height + self.statusViews.frame.origin.y, self.wrapthumbView.frame.size.width, self.wrapthumbView.frame.size.height + collapseHeight);
            [self.statusViews setBackgroundColor:[UIColor colorWithRed:29/255.0f green:24/255.0f blue:71/255.0f alpha:1]];
            
            CGRect frame = scrollView.frame;
            frame.origin.y = self.wrapthumbView.frame.origin.y + self.wrapthumbView.frame.size.height;
            frame.size.height -= (collapseHeight + self.statusViews.frame.size.height);
            scrollView.frame=frame;
            
            //change background tools box
            UIImage *bgtoolsbox = [UIImage imageForDeviceWithName:@"toolBoxs.png"];
            [self.toolsboxbg setImage:bgtoolsbox];
            
            //todo: set button move
            CGRect btrightFrame = self.rightButton.frame;
            CGRect btbackFrame = self.backButton.frame;
            btrightFrame.origin.x += btbackFrame.size.width;
            self.rightButton.frame = btrightFrame;
            
            btbackFrame.origin.y += (self.statusViews.frame.size.height - btrightFrame.origin.y);
            self.backButton.frame = btbackFrame;
            
            [self.backButton setTitle:@"" forState:UIControlStateNormal];
            UIImage *btimg = [UIImage imageForDeviceWithName:@"closeRightCorner.png"];
            [self.backButton setImage:btimg forState:UIControlStateNormal];
            
        } completion:^(BOOL finished) {
            if(finished)
            {
            }
        }];
    }
}

- (void)refreshView {
    NSArray *subViews = [self.bodyScrollView subviews];
    float y = 0;
    for(UIView *view in subViews)
    {
        if(view.frame.origin.x == 0) //remove height UIimage of scroller
        {
            y+= view.frame.size.height;
        }
    }
    self.bodyScrollView.contentSize = CGSizeMake(self.bodyScrollView.contentSize.width, y);
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSURL *url = [request URL];
    if(navigationType == UIWebViewNavigationTypeLinkClicked || navigationType == UIWebViewNavigationTypeFormSubmitted) {
        if ([[url scheme] isEqualToString:@"nolink"]) {
            NSString *urlstr = [url fragment];
            [self.previewImageView setUrl:urlstr];
            [self.previewView setHidden:NO];
            [self.hidePreviewButton setHidden:NO];
            [self.previewView setZoomScale:1.0 animated:YES];
            return NO;
        }
    }

    if (navigationType == UIWebViewNavigationTypeOther) {
        if ([[url scheme] isEqualToString:@"ready"]) {
            float contentHeight = [[url host] floatValue];
            contentHeight += GAP;
            CGRect fr = webView.frame;
            fr.size = CGSizeMake(contentWebView.frame.size.width, contentHeight);
            contentWebView.frame = fr;
            self.bodyScrollView.contentSize = CGSizeMake(self.bodyScrollView.frame.size.width, contentHeight + contentWebView.frame.origin.y);
            //add note
            [self loadNoteView];
            return NO;
        }
    }
    return YES;
}

- (void)SyncUserFavorite {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    TempData *data = [TempData getInstance];
    [ServiceClient getStructureUserFavorites:nil username:username md5Password:password successBlock:^(id responseObject) {
        data.userinterests = responseObject;
    } failureBlock:^(NSError *error) {
    }];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.previewImageView;
}

- (IBAction)shareTouched:(id)sender {
    [self.bodyScrollView setContentOffset:CGPointMake(0, posYofShare) animated:YES];
    NSLog(@"Share !!!");
}

- (IBAction)exclTouched:(id)sender  {
    [self.exclButton setImage:[UIImage imageNamed:@"iconexclam_pressed"] forState:UIControlStateNormal];
    NewReport *report = [[NewReport alloc] initWithNibName:@"NewReport" bundle:nil];
    report.postID = self.postID;
    report.articleContainer = self;
    CGRect frame = report.view.frame;
    frame.origin.y = Y_POPUP_ITEMARTICLE;
    frame.origin.x = (self.view.frame.size.width - frame.size.width)/2;
    report.view.frame = frame;
    [ClientUtils showBlur:self.view];
    [self.view addSubview:report.view];
    [self addChildViewController:report];
    [report didMoveToParentViewController:self];
    
    NSLog(@"Excal !!!");
}
          
- (IBAction)saveTouched:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    if (article.isFavorite == YES) {
        // Unlike
        article.isFavorite = NO;
        [ServiceClient unfavoritePost:self.view username:username md5Password:password postID:article.ID successBlock:^(id responseObject) {
            // UnLike article succeeded
            [self SyncUserFavorite];
            [button setImage:[UIImage imageNamed:@"iconSave"] forState:UIControlStateNormal];
        } failureBlock:^(NSError *error) {
        }];
    }
    else if(article.isFavorite == NO){
        // Like
        article.isFavorite = YES;
        [ServiceClient favoritePost:self.view username:username md5Password:password postID:article.ID successBlock:^(id responseObject) {
            // Like article succeeded
            [self SyncUserFavorite];
            [button setImage:[UIImage imageNamed:@"iconSave_pressed"] forState:UIControlStateNormal];
        } failureBlock:^(NSError *error) {
        }];
    }
    NSLog(@"Save !!!");
}
          
- (IBAction)commentTouched:(id)sender {
    [self.bodyScrollView setContentOffset:CGPointMake(0, posYofComment) animated:YES];
    NSLog(@"Comment !!!");
}

- (IBAction)noteTouched:(id)sender {
    [self.bodyScrollView setContentOffset:CGPointMake(0, posYofNote) animated:YES];
    NSLog(@"Note !!!");
}

- (IBAction)likeTouched:(id)sender {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    if (self.liked == YES) {
        // Unlike
        self.liked = NO;
        [ServiceClient unlikePost:self.view username:username md5Password:password postID:article.ID successBlock:^(id responseObject) {
            // UnLike article succeeded
            [self.likeButton setTitle:[NSString stringWithFormat:@"%@", responseObject] forState:UIControlStateNormal];
            [self.likeButton setImage:[UIImage imageNamed:@"iconcommentlike"] forState:UIControlStateNormal];
        } failureBlock:^(NSError *error) {
        }];
    }
    else {
        // Like
        self.liked = YES;
        [ServiceClient likePost:self.view username:username md5Password:password postID:article.ID successBlock:^(id responseObject) {
            // Like article succeeded
            [self.likeButton setTitle:[NSString stringWithFormat:@"%@", responseObject] forState:UIControlStateNormal];
            [self.likeButton setImage:[UIImage imageNamed:@"iconcommentliked"] forState:UIControlStateNormal];
        } failureBlock:^(NSError *error) {
        }];
    }
}

- (IBAction)leftButtonTouched:(id)sender {
    [self.revealController showViewController:self.revealController.leftViewController];
}

- (IBAction)rightButtonTouched:(id)sender {
    [ClientUtils showBlur:self.view];
    NavigateViewController *nav = [[NavigateViewController alloc] initWithNibName:@"NavigateViewController" bundle:nil];
    CGRect frame = nav.view.frame;
    frame.size.width = self.view.frame.size.width;
    frame.size.height = self.view.frame.size.height;
    nav.view.frame = frame;
    [self.view addSubview:nav.view];
    [self addChildViewController:nav];
    [nav didMoveToParentViewController:self];
}

- (IBAction)backButtonTouched:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
