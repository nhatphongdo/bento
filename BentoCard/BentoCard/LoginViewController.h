//
//  LoginViewController.h
//  BentoCard
//
//  Created by VO HONG HAI on 3/4/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GooglePlus/GooglePlus.h>
#import <FacebookSDK/FacebookSDK.h>

@class GPPSignInButton;

@interface LoginViewController : UIViewController<UIGestureRecognizerDelegate, GPPSignInDelegate, FBLoginViewDelegate>

@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) IBOutlet UIButton *signUpButton;
@property (strong, nonatomic) IBOutlet UIButton *loginFBButton;
@property (weak, nonatomic) IBOutlet GPPSignInButton *loginGGButton;

- (IBAction)loginButtonTouched:(id)sender;

@end
