//
//  NavigateViewController.m
//  BentoCard
//
//  Created by VO HONG HAI on 1/29/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import "NavigateViewController.h"
#import "Constrain.h"
#import "ItemNavigation.h"
#import "ClientUtils.h"
#import "TempData.h"
#import "UIImage+DeviceSpecificMedia.h"
#import "TempData.h"
#import "ManagerViewController.h"
#import "LandingViewController.h"
#import "SocialViewController.h"

@interface NavigateViewController ()
{
    BOOL isSlideUp;
    TempData *data;
}
@end

static const float GAP = 10.0f;
static const float extentCard = 4*GAP;
static const float duration = 0.6f;

@implementation NavigateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    data = [TempData getInstance];
    self.containerScrollview.delegate = self;
    [self loadCard];
    isSlideUp = NO;
    
    // init Swipe Gesture
    UISwipeGestureRecognizer *navswipeup = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeup)];
    navswipeup.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:navswipeup];
    
    UISwipeGestureRecognizer *navswipedown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedown)];
    navswipedown.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:navswipedown];
    
    [self navigateselected:data.State];
}

- (void)swipeup {
    NSLog(@"swipe up");
    [self animationSlideUp];
}

- (void)swipedown {
    NSLog(@"swipe down");
    [self animationSlideDown];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadCard {
    TempData *data = [TempData getInstance];
    if(data.navigations.count > 0)
    {
        //todo: check level of navigation controller
        
        float y= 0.0f;
        int count = 0;
        for (Navigation *item in data.navigations)
        {
            ItemNavigation *nav = [[ItemNavigation alloc] initWithNibName:@"ItemNavigation" bundle:nil];
            nav.navitem = item;
            if(count == data.navigations.count - 1)
            {
                nav.state = NAV_NEXT;
                [nav updateState];
            }
            CGRect frame = nav.view.frame;
            frame.origin.y = y;
            nav.view.frame = frame;
            [self.containerScrollview addSubview:nav.view];
            [self addChildViewController:nav];
            [nav didMoveToParentViewController:self];
            //y+=frame.size.height + GAP;
            y+= extentCard;
            count++;
        }
        self.containerScrollview.contentSize = CGSizeMake(self.containerScrollview.frame.size.width, y);
        if(y > self.containerScrollview.frame.size.height)
        {
            float topScroll = y - self.containerScrollview.frame.size.height;
            [self.containerScrollview setContentOffset:CGPointMake(0, topScroll) animated:YES];
        }
    }
}

- (void)animationSlideDown {
    isSlideUp = YES;
    NSArray *arrayView = [self childViewControllers];
    if(arrayView.count > 1)
    {
        //move out index 0
        float y = 0.0f;
        int count = 0;
        for (int i = 0; i < arrayView.count; i++) {
            if ([[arrayView objectAtIndex:i] isMemberOfClass:[ItemNavigation class]]) {
                ItemNavigation *current = [arrayView objectAtIndex:i];
                if([current.state isEqualToString:NAV_NEXT] && i > 0)
                {
                    if((i+1) < arrayView.count)
                    {
                        ItemNavigation *nextItem = [arrayView objectAtIndex:i+1];
                        nextItem.state = @"";
                    }
                    if((i-1)>=0)
                    {
                        ItemNavigation *prevItem = [arrayView objectAtIndex:i-1];
                        prevItem.state = NAV_NEXT;
                        [prevItem updateState];
                    }
                    current.state = NAV_PREV;
                    y = current.view.frame.origin.y + self.containerScrollview.frame.size.height;
                    [UIView animateWithDuration:duration delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
                        current.view.frame = CGRectMake(current.view.frame.origin.x, y, current.view.frame.size.width, current.view.frame.size.height);
                    } completion:^(BOOL finished) {
                        if(finished)
                        {
                        }
                    }];
                    break;
//                    y+=current.view.frame.size.height + GAP;
                }
            }
        }
//        self.containerScrollview.contentSize = CGSizeMake(self.containerScrollview.frame.size.width, y);
    }
}

- (void)animationSlideUp {
    isSlideUp = NO;
    NSArray *arrayView = [self childViewControllers];
    if(arrayView.count > 1)
    {
        //move out index 0
        float y = extentCard;
        for (int i = 0; i < arrayView.count; i++) {
            if ([[arrayView objectAtIndex:i] isMemberOfClass:[ItemNavigation class]]) {
                ItemNavigation *current = [arrayView objectAtIndex:i];
                if([current.state isEqualToString:NAV_PREV])
                {
                    if((i+1) < arrayView.count)
                    {
                        ItemNavigation *nextItem = [arrayView objectAtIndex:i+1];
                        nextItem.state = NAV_PREV;
                    }
                    if((i-1) >=0)
                    {
                        ItemNavigation *prevItem = [arrayView objectAtIndex:i-1];
                        prevItem.state = @"";
                        [prevItem updateState];
                    }
                    current.state = NAV_NEXT;
                    [current updateState];
                    y = current.view.frame.origin.y - self.containerScrollview.frame.size.height;
                    [UIView animateWithDuration:duration delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
                        current.view.frame = CGRectMake(current.view.frame.origin.x, y, current.view.frame.size.width, current.view.frame.size.height);
                    } completion:^(BOOL finished) {
                        if(finished)
                        {
                        }
                    }];
                    break;
                }
            }
        }
//        self.containerScrollview.contentSize = CGSizeMake(self.containerScrollview.frame.size.width, y);
    }
}

- (void)clearMenuButton {
    //clear menu hover
    UIImage *manageimg = [UIImage imageForDeviceWithName:@"icon_nav_manage.png"];
    [self.btManage setImage:manageimg forState:UIControlStateNormal];
    UIImage *articleimg = [UIImage imageForDeviceWithName:@"icon_nav_article.png"];
    [self.btArticle setImage:articleimg forState:UIControlStateNormal];
    UIImage *sponsorimg = [UIImage imageForDeviceWithName:@"icon_nav_sponsor.png"];
    [self.btSponsor setImage:sponsorimg forState:UIControlStateNormal];
    UIImage *socialimg = [UIImage imageForDeviceWithName:@"icon_nav_social.png"];
    [self.btSocial setImage:socialimg forState:UIControlStateNormal];
}

- (void)navigateselected:(NSString *)selectedPage {
    //clear menu hover
    [self clearMenuButton];
    self.pageName = selectedPage;
    if([self.pageName isEqualToString:NAV_ARTICLE])
    {
        UIImage *articleimg = [UIImage imageForDeviceWithName:@"icon_nav_article_hover.png"];
        [self.btArticle setImage:articleimg forState:UIControlStateNormal];
    }
    else if([self.pageName isEqualToString:NAV_MANAGE])
    {
        UIImage *manageimg = [UIImage imageForDeviceWithName:@"icon_nav_manage_hover.png"];
        [self.btManage setImage:manageimg forState:UIControlStateNormal];
    }
    else if([self.pageName isEqualToString:NAV_SOCIAL])
    {
        UIImage *socialimg = [UIImage imageForDeviceWithName:@"icon_nav_social_hover.png"];
        [self.btSocial setImage:socialimg forState:UIControlStateNormal];
    }
    else if([self.pageName isEqualToString:NAV_SPONSOR])
    {
        UIImage *sponsorimg = [UIImage imageForDeviceWithName:@"icon_nav_sponsor_hover.png"];
        [self.btSponsor setImage:sponsorimg forState:UIControlStateNormal];
    }
}

- (IBAction)doneTouched:(id)sender {
    [self closeview];
}

- (IBAction)navTouched:(id)sender {
    [self closeview];
//    if(isSlideUp == YES)
//    {
//        [self animationSlideUp];
//    }
//    else
//    {
//        [self animationSlideDown];
//    }
}

- (void)closeview {
    [ClientUtils removeBlur:self.parentViewController.view];
    
    NSArray *subviewcontrollers = [self.parentViewController childViewControllers];
    for (UIViewController *item in subviewcontrollers)
    {
        if([item isKindOfClass:[NavigateViewController class]])
        {
            [item.view removeFromSuperview];
            [item removeFromParentViewController];
        }
    }
}

- (IBAction)manageTouched:(id)sender {
    [self clearMenuButton];
    UIImage *manageimg = [UIImage imageForDeviceWithName:@"icon_nav_manage_hover.png"];
    [self.btManage setImage:manageimg forState:UIControlStateNormal];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ManagerViewController *myview = [storyboard instantiateViewControllerWithIdentifier:@"managerview"];
    [self.navigationController setViewControllers:[NSArray arrayWithObjects:myview, nil] animated:YES];

}

- (IBAction)articleTouched:(id)sender {
    [self clearMenuButton];
    UIImage *articleimg = [UIImage imageForDeviceWithName:@"icon_nav_article_hover.png"];
    [self.btArticle setImage:articleimg forState:UIControlStateNormal];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LandingViewController *myview = [storyboard instantiateViewControllerWithIdentifier:@"landingview"];
    [self.navigationController setViewControllers:[NSArray arrayWithObjects:myview, nil] animated:YES];
}

- (IBAction)socialTouched:(id)sender {
    [self clearMenuButton];
    UIImage *socialimg = [UIImage imageForDeviceWithName:@"icon_nav_social_hover.png"];
    [self.btSocial setImage:socialimg forState:UIControlStateNormal];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SocialViewController *myview = [storyboard instantiateViewControllerWithIdentifier:@"socialview"];
    [self.navigationController setViewControllers:[NSArray arrayWithObjects:myview, nil] animated:YES];
}

- (IBAction)sponsorTouched:(id)sender {
    [self clearMenuButton];
    UIImage *sponsorimg = [UIImage imageForDeviceWithName:@"icon_nav_sponsor_hover.png"];
    [self.btSponsor setImage:sponsorimg forState:UIControlStateNormal];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
