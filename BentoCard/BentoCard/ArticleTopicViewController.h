//
//  ArticleTopicViewController.h
//  BentoCard
//
//  Created by VO HONG HAI on 1/12/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PKRevealController/PKRevealController.h>

@interface ArticleTopicViewController : PKRevealController<PKRevealing>

@property (nonatomic,strong) NSString *topicID;
@property (nonatomic,strong) NSString *topicName;

@end
