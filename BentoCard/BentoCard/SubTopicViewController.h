//
//  SubTopicViewController.h
//  BentoCard
//
//  Created by VO HONG HAI on 1/12/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PKRevealController/PKRevealController.h>

@interface SubTopicViewController : PKRevealController<PKRevealing>

@property (nonatomic,strong) NSString *topicID;
@property (nonatomic,strong) NSString *viewTitle;

@end
