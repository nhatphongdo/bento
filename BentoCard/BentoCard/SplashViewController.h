//
//  SplashViewController.h
//  BentoCard
//
//  Created by VO HONG HAI on 12/9/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *textBentImage;
@property (strong, nonatomic) IBOutlet UIImageView *textOImage;
@property (strong, nonatomic) IBOutlet UIImageView *textBusiness;
@property (strong, nonatomic) IBOutlet UIImageView *textExpert;
@property (strong, nonatomic) IBOutlet UIImageView *textNetworks;
@property (strong, nonatomic) IBOutlet UIImageView *textTraining;
@property (strong, nonatomic) IBOutlet UIImageView *textOperation;

@end
