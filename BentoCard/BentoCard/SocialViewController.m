//
//  SocialViewController.m
//  BentoCard
//
//  Created by VO HONG HAI on 2/27/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import "SocialViewController.h"
#import "LandingViewController.h"
#import "SocialPageViewController.h"
#import "Constrain.h"
#import "TempData.h"
#import "ClientUtils.h"
#import "ProfileCardViewController.h"
#import "InterestModel.h"

@interface SocialViewController () {
    TempData *data;
}

@end

@implementation SocialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    data = [TempData getInstance];
    data.State = NAV_SOCIAL;
    
    SocialPageViewController *socialPage = [[SocialPageViewController alloc] initWithNibName:@"SocialPageViewController" bundle:nil];
    CGRect frame = socialPage.view.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    frame.size.width = self.view.frame.size.width;
    frame.size.height = self.view.frame.size.height;
    socialPage.view.frame = frame;
    InterestModel *model = [[InterestModel alloc] init];
    model.Name = @"Profile";
    model.ID = @"profile123123123";
    [socialPage loadMenuList:[NSArray arrayWithObjects:model, nil] activeTopic:model.ID];

    [self setFrontViewController:socialPage];
    [self setLeftViewController:nil];
    
    self.recognizesPanningOnFrontView = NO;
    self.recognizesResetTapOnFrontView = NO;
    self.delegate = self;
    
    ProfileCardViewController *card = [[ProfileCardViewController alloc] initWithNibName:@"ProfileCardViewController" bundle:nil];
    ProfileModel *userProfile = [[ProfileModel alloc] init];
    userProfile.Fullname = [ClientUtils loadSetting:FULLNAME_KEY];
    userProfile.Caption = [ClientUtils loadSetting:CAPTION_KEY];
    NSLog(@"Caption: ",userProfile.Caption);
    userProfile.BirthDay = [ClientUtils loadSetting:DATE_OF_BIRTH_KEY];
    userProfile.Address = @"355 Dien Bien Phu, Q.3, Ho Chi Minh, Viet Nam";
    card.profile = userProfile;
    card.titleCard = @"Info";
    
    ProfileCardViewController *card1 = [[ProfileCardViewController alloc] initWithNibName:@"ProfileCardViewController" bundle:nil];
    ProfileModel *education = [[ProfileModel alloc] init];
    education.Fullname = @"Education";
    education.Caption = @"Design at RMIT is about the journey from what might be possible to what can be done. It is characterised by people engaged in conceiving and producing objects, buildings, systems and environments.";
    education.BirthDay = [NSDate dateWithTimeIntervalSince1970:100000000];
    education.Address = @"134 Hoang hoa tham, Q.1, Ho Chi Minh, Viet Nam";
    card1.profile = education;
    card1.titleCard = @"Education";
    
    [socialPage createCardList:[NSArray arrayWithObjects:card,card1, nil]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)revealController:(PKRevealController *)revealController didChangeToState:(PKRevealControllerState)state {
    [revealController setMinimumWidth:SIZE_SIDEBAR maximumWidth:SIZE_SIDEBAR forViewController:revealController.focusedController];
    if (revealController.focusedController == revealController.leftViewController) {
        [revealController.focusedController.view setFrame:CGRectMake(0, 0, SIZE_SIDEBAR, self.view.frame.size.height)];
    }
}


@end
