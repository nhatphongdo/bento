//
//  ReplyItem.m
//  BentoCard
//
//  Created by VO HONG HAI on 12/19/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import "ReplyItem.h"
#import "Constrain.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

@interface ReplyItem ()

@end

@implementation ReplyItem

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.avatarImageView.layer.cornerRadius = 15;
    self.avatarImageView.clipsToBounds = YES;
    [self.avatarImageView setImageWithURL:[NSURL URLWithString:self.avatar]];
    self.nameLabel.text = self.name;
    self.contentLabel.text = self.body;
    [self.contentLabel sizeToFit];
    [self.replylikeButton setTitle:[NSString stringWithFormat:@"%d",self.likecount] forState:normal];
    if(self.liked == YES)
    {
        [self.replylikeButton setImage:[UIImage imageNamed:@"iconcommentliked"] forState:UIControlStateNormal];
    }
    else
    {
        [self.replylikeButton setImage:[UIImage imageNamed:@"iconcommentlike"] forState:UIControlStateNormal];
    }
    
    float posY = self.contentLabel.frame.origin.y + self.contentLabel.frame.size.height;
    CGRect likeFrame = self.replylikeButton.frame;
    likeFrame.origin.y = posY;
    self.replylikeButton.frame = likeFrame;

    CGRect frame = self.view.frame;
    frame.size.height = posY + self.replylikeButton.frame.size.height;
    self.view.frame = frame;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)likeButtonTouched:(id)sender {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    UIViewController *parentView = self.commentContainer.parentViewController;
    if (self.liked == YES) {
        // Unlike
        self.liked = NO;
        [ServiceClient unlikePost:parentView.view username:username md5Password:password postID:self.replyID successBlock:^(id responseObject) {
            // UnLike article succeeded
            [self.replylikeButton setTitle:[NSString stringWithFormat:@"%@", responseObject] forState:UIControlStateNormal];
            [self.replylikeButton setImage:[UIImage imageNamed:@"iconcommentlike"] forState:UIControlStateNormal];
        } failureBlock:^(NSError *error) {
        }];
    }
    else {
        // Like
        self.liked = YES;
        [ServiceClient likePost:parentView.view username:username md5Password:password postID:self.replyID successBlock:^(id responseObject) {
            // Like article succeeded
            [self.replylikeButton setTitle:[NSString stringWithFormat:@"%@", responseObject] forState:UIControlStateNormal];
            [self.replylikeButton setImage:[UIImage imageNamed:@"iconcommentliked"] forState:UIControlStateNormal];
        } failureBlock:^(NSError *error) {
        }];
    }
}

@end
