//
//  SubTopicViewController.m
//  BentoCard
//
//  Created by VO HONG HAI on 1/12/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import "SubTopicViewController.h"
#import "Constrain.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import "ArticleModel.h"
#import "InterestModel.h"
#import "PageViewController.h"
#import "CardViewController.h"
#import "ArticleSideBarViewController.h"
#import "TempData.h"

@interface SubTopicViewController ()
{
    NSMutableArray *menulists;
    TempData *data;
}
@end

@implementation SubTopicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    data = [TempData getInstance];
    PageViewController *page = [[PageViewController alloc] initWithNibName:@"PageViewController" bundle:nil];
    page.isLandingPage = NO;
    CGRect frame = page.view.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    frame.size.width = self.view.frame.size.width;
    frame.size.height = self.view.frame.size.height;
    page.view.frame = frame;
    
//    [self.view addSubview:page.view];
//    [self addChildViewController:page];
//    [page didMoveToParentViewController:self];
    ArticleSideBarViewController *leftview_article  = [[ArticleSideBarViewController alloc] initWithNibName:@"ArticleSideBarViewController" bundle:nil];
    [self setFrontViewController:page];
    [self setLeftViewController:leftview_article];
    
    self.recognizesPanningOnFrontView = NO;
    self.recognizesResetTapOnFrontView = NO;
    self.delegate = self;
    
    page.topicId = self.topicID;
    
    if([data.State isEqualToString:NAV_MANAGE]) {
        [page loadManageMenuLv1:self.topicID];
        [page loadManageSubTopic:self.topicID];
        [page.titleLabel setText:@"Manage"];
        page.title = @"Manage";
    } else {
        [page loadMenuLv1:self.topicID];
        [page loadSubTopic:self.topicID];
        [page.titleLabel setText:@"Article"];
        page.title = @"Article";
    }
}

- (void)viewDidAppear:(BOOL)animated {

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)revealController:(PKRevealController *)revealController didChangeToState:(PKRevealControllerState)state {
    [revealController setMinimumWidth:SIZE_SIDEBAR maximumWidth:SIZE_SIDEBAR forViewController:revealController.focusedController];
    if (revealController.focusedController == revealController.leftViewController) {
        [revealController.focusedController.view setFrame:CGRectMake(0, 0, SIZE_SIDEBAR, self.view.frame.size.height)];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
