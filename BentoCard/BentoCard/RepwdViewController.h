//
//  RepwdViewController.h
//  Bento
//
//  Created by VO HONG HAI on 10/30/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RepwdViewController : UIViewController
@property (strong,nonatomic) IBOutlet UILabel *emailLabel;
@property (strong,nonatomic) IBOutlet UITextField *pwdTextField;
@property (strong,nonatomic) IBOutlet UIButton *loginButton;
@property (strong,nonatomic) IBOutlet UIButton *closeButton;
@property (strong,nonatomic) NSString *email;

- (IBAction)closeTouched:(id)sender;
- (IBAction)loginTouched:(id)sender;

@end
