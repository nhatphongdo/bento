//
//  ListItemNote.m
//  BentoCard
//
//  Created by VO HONG HAI on 12/18/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import "ListItemNote.h"

@interface ListItemNote ()

@end

static int const GAP = 16;
@implementation ListItemNote

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.datetimeLabel.text = self.datetime;
    self.noteLabel.text = self.note;
    [self.noteLabel sizeToFit];
    CGRect frame = self.view.frame;
    frame.size.height = self.noteLabel.frame.origin.y + self.noteLabel.frame.size.height + GAP;
    self.view.frame = frame;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
