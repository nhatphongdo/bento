//
//  ItemShowTopic.h
//  BentoCard
//
//  Created by VO HONG HAI on 1/23/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleSideBarViewController.h"
#import "InterestModel.h"

@interface ItemShowTopic : UIViewController

@property (nonatomic,strong) ArticleSideBarViewController *articlesidebar;
@property (nonatomic,strong) InterestModel *interest;
@property (nonatomic) BOOL isCollapse;

@property (nonatomic,strong) IBOutlet UIImageView *arrowImage;
@property (nonatomic,strong) IBOutlet UILabel *nameLabel;
@property (nonatomic,strong) IBOutlet UILabel *countLabel;
@property (nonatomic,strong) IBOutlet UIButton *arrow_Button;
@property (nonatomic,strong) IBOutlet UIButton *title_Button;
@property (nonatomic,strong) IBOutlet UIView *containerView;

- (IBAction)arrowButton_touched:(id)sender;
- (IBAction)titleButton_touched:(id)sender;

@end
