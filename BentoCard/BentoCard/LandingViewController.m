//
//  LandingViewController.m
//  BentoCard
//
//  Created by VO HONG HAI on 12/10/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import "LandingViewController.h"
#import "Constrain.h"
#import "PageViewController.h"
#import "ClientUtils.h"
#import "ArticleSideBarViewController.h"
#import "TempData.h"
#import "ServiceClient.h"

@interface LandingViewController ()
{
    PageViewController *page;
    TempData *data;
}

@end

@implementation LandingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    data = [TempData getInstance];
    data.State = NAV_ARTICLE;
    page = [[PageViewController alloc] initWithNibName:@"PageViewController" bundle:nil];
    page.isLandingPage = YES;
    page.isFirstTime = NO;
    if(self.isFirstTime == YES) {
        page.isFirstTime = YES;
    }
    CGRect frame = page.view.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    frame.size.width = self.view.frame.size.width;
    frame.size.height = self.view.frame.size.height;
    page.view.frame = frame;
    
    ArticleSideBarViewController *leftview_article  = [[ArticleSideBarViewController alloc] initWithNibName:@"ArticleSideBarViewController" bundle:nil];
    [self setFrontViewController:page];
    [self setLeftViewController:leftview_article];
    
    self.recognizesPanningOnFrontView = NO;
    self.recognizesResetTapOnFrontView = NO;
    self.delegate = self;
    
    page.title = APPLICATION_TITLE;
    NSArray *menuItems = [NSArray arrayWithObjects:@{@"name":@"MANAGE",@"nib":@"managerview"}, @{@"name":@"ARTICLE",@"nib":@"landingview"}, @{@"name":@"SOCIAL",@"nib":@"socialview"}, nil];
    [page loadMenuList:menuItems activeTopic:@"ARTICLE"];
    
    //todo: Load list topics
    [page loadTopics];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
}

- (void)revealController:(PKRevealController *)revealController didChangeToState:(PKRevealControllerState)state {
    [revealController setMinimumWidth:SIZE_SIDEBAR maximumWidth:SIZE_SIDEBAR forViewController:revealController.focusedController];
    if (revealController.focusedController == revealController.leftViewController) {
        [revealController.focusedController.view setFrame:CGRectMake(0, 0, SIZE_SIDEBAR, self.view.frame.size.height)];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
