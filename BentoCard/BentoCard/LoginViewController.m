//
//  LoginViewController.m
//  BentoCard
//
//  Created by VO HONG HAI on 3/4/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import "LoginViewController.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import "ServiceClient.h"
#import "Constrain.h"
#import "ClientUtils.h"
#import "ProfileModel.h"
#import "PwdViewController.h"  
#import "RepwdViewController.h"
#import "LandingViewController.h"
#import "TempData.h"

@interface LoginViewController () {
    BOOL syncInterests, syncUserInterests;
    TempData *data;
    NSTimer *mytimer;
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    data = [TempData getInstance];
    data.navigations = [[NSMutableArray alloc] init];
    data.interests = [[NSMutableArray alloc] init];
    data.userinterests = [[NSMutableArray alloc] init];
    syncUserInterests = NO;
    syncInterests = NO;
    mytimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self
                                selector:@selector(doFireTimer) userInfo:nil repeats:YES];


    // Do any additional setup after loading the view.
    GPPSignIn *signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.shouldFetchGoogleUserEmail = YES;  // get the user's email
    
    // You previously set kClientId in the "Initialize the Google+ client" step
    signIn.clientID = GOOGLE_PLUS_ID;
    
    // Uncomment one of these two statements for the scope you chose in the previous step
    signIn.scopes = @[ kGTLAuthScopePlusLogin ];  // "https://www.googleapis.com/auth/plus.login" scope
    //signIn.scopes = @[ @"profile" ];            // "profile" scope
    
    // Optional: declare signIn.actions, see "app activities"
    signIn.delegate = self;
    
    // Facebook Login
    FBLoginView *loginView = [[FBLoginView alloc] initWithReadPermissions:@[@"public_profile", @"email"]];
    loginView.delegate = self;
    loginView.frame = CGRectOffset(loginView.frame,
                                   (self.view.center.x - (loginView.frame.size.width / 2)),
                                   110);
    [self.view addSubview:loginView];
    
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self
                                          action:@selector(handleViewTap:)];
    tapGesture.delegate = self;
    [self.view addGestureRecognizer:tapGesture];
    tapGesture = nil;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 0)];
    self.emailTextField.leftView = paddingView;
    self.emailTextField.leftViewMode = UITextFieldViewModeAlways;
    
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 0)];
    self.passwordTextField.leftView = paddingView;
    self.passwordTextField.leftViewMode = UITextFieldViewModeAlways;
}

- (void)doFireTimer {
    if (syncInterests == YES && syncUserInterests == YES) {
        [mytimer invalidate];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LandingViewController *landingView = [storyboard instantiateViewControllerWithIdentifier:@"landingview"];
        landingView.isFirstTime = YES;
        [self.navigationController setViewControllers:[NSArray arrayWithObjects:landingView, nil] animated:YES]; //reset rootviewcontroller
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    // Check signed in
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    if (username != nil && password != nil && ![username isEqualToString:@""] && ![password isEqualToString:@""]) {
        [self signIn:username md5Password:password];
    }
}

- (void) clearAccount {
    // test only
    [ClientUtils deleteSetting:USERNAME_KEY];
    [ClientUtils deleteSetting:MD5_PASSWORD_KEY];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)keyboardWillHide:(NSNotification *)aNotification
{
    // Get the size of the keyboard.
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];
}

- (void)keyboardWillShow:(NSNotification *)aNotification
{
    // Get the size of the keyboard.
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect frame = self.view.frame;
    if (self.view.frame.size.height - kbSize.height < 370) {
        frame.origin.y = -150;
    }
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];
}

- (void)handleViewTap:(UITapGestureRecognizer *)recognizer {
    // Insert your own code to handle singletap
    [self.view endEditing:YES];
}

- (IBAction)loginButtonTouched:(id)sender {
    if ([self.emailTextField.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Email address cannot be empty"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        [self.emailTextField becomeFirstResponder];
        
        return;
    }
    
    if ([self.passwordTextField.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Password cannot be empty"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        [self.passwordTextField becomeFirstResponder];
        
        return;
    }
    
    NSString *md5Password = [ClientUtils encryptMd5:self.passwordTextField.text];
    [ServiceClient signIn:self.view username:self.emailTextField.text md5Password:md5Password successBlock:^(id responseObject) {
        // Login succeeded
        [ClientUtils saveSetting:USERNAME_KEY value:self.emailTextField.text];
        [ClientUtils saveSetting:MD5_PASSWORD_KEY value:md5Password];
        
        [ServiceClient getProfile:self.view username:self.emailTextField.text md5Password:md5Password successBlock:^(id responseObject) {
            // Load Profile success
            ProfileModel *profile = responseObject;

            [ClientUtils saveSetting:FULLNAME_KEY value:profile.Fullname];
            [ClientUtils saveSetting:AVATAR_KEY value:profile.Avatar];
            [ClientUtils saveSetting:GENDER_KEY value:[NSNumber numberWithInt:profile.Gender]];
            [ClientUtils saveSetting:ACCOUNT_TYPE_KEY value:profile.AccountType];
            [ClientUtils saveSetting:DATE_OF_BIRTH_KEY value:profile.BirthDay];
            [ClientUtils saveSetting:CAPTION_KEY value:profile.Caption];
            [ClientUtils saveSetting:EMAIL_KEY value:profile.Email];
            
            [self SyncData];
            
            [self.loginButton setEnabled:YES];
            [self.signUpButton setEnabled:YES];
        } failureBlock:^(NSError *error) {
            [self.loginButton setEnabled:YES];
            [self.signUpButton setEnabled:YES];
        }];
    } failureBlock:^(NSError *error) {
        [self.loginButton setEnabled:YES];
        [self.signUpButton setEnabled:YES];
    }];
    [self.view endEditing:YES];
    [self.loginButton setEnabled:NO];
    [self.signUpButton setEnabled:NO];
    
}

- (void)signIn:(NSString *)username md5Password:(NSString *)md5Password {
    [ServiceClient signIn:self.view username:username md5Password:md5Password successBlock:^(id responseObject) {
        // Login succeeded
        [ClientUtils saveSetting:USERNAME_KEY value:username];
        [ClientUtils saveSetting:MD5_PASSWORD_KEY value:md5Password];
        
        [ServiceClient getProfile:self.view username:username md5Password:md5Password successBlock:^(id responseObject) {
            // Load Profile success
            ProfileModel *profile = responseObject;
            
            [ClientUtils saveSetting:FULLNAME_KEY value:profile.Fullname];
            [ClientUtils saveSetting:AVATAR_KEY value:profile.Avatar];
            [ClientUtils saveSetting:GENDER_KEY value:[NSNumber numberWithInt:profile.Gender]];
            [ClientUtils saveSetting:ACCOUNT_TYPE_KEY value:profile.AccountType];
            [ClientUtils saveSetting:DATE_OF_BIRTH_KEY value:profile.BirthDay];
            [ClientUtils saveSetting:CAPTION_KEY value:profile.Caption];
            [ClientUtils saveSetting:EMAIL_KEY value:profile.Email];
//            NSLog(@"profile: %@ - Save: %@",profile.Caption, [ClientUtils loadSetting:CAPTION_KEY]);
            [self SyncData];
            
        } failureBlock:^(NSError *error) {
        }];
    } failureBlock:^(NSError *error) {
        NSDictionary *userInfo = [error userInfo];
        NSString *errorMessage = [[userInfo objectForKey:NSUnderlyingErrorKey] localizedDescription];
        if([errorMessage isEqualToString:@"Wrong username or password"])
        {
            RepwdViewController *rePwdView = [[RepwdViewController alloc] init];
            rePwdView.email = username;
            [self presentViewController:rePwdView animated:YES completion:nil];
        }
    }];
}

- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
                   error: (NSError *) error {
    NSLog(@"Received error %@ and auth object %@",error, auth);
    if (error) {
        // Do some error handling here.
    } else {
        //[self refreshInterfaceBasedOnSignIn];
        NSString *email = [GPPSignIn sharedInstance].userEmail;
        NSString *name = @"";
        NSString *birthday = @"";
        // The googlePlusUser member will be populated only if the appropriate
        // scope is set when signing in.
        GTLPlusPerson *person = [GPPSignIn sharedInstance].googlePlusUser;
        if (person == nil) {
            return;
        }
        
        name = person.displayName;
        birthday = person.birthday == nil ? @"01-01-2000" : person.birthday;
        
        // Load avatar image asynchronously, in background
        //        dispatch_queue_t backgroundQueue =
        //        dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        //        __block NSData *dataImage = [[NSData alloc] init];
        //        dispatch_async(backgroundQueue, ^{
        //            NSData *avatarData = nil;
        //            NSString *imageURLString = person.image.url;
        //            if (imageURLString) {
        //                NSURL *imageURL = [NSURL URLWithString:imageURLString];
        //                avatarData = [NSData dataWithContentsOfURL:imageURL];
        //            }
        //
        //            if (avatarData) {
        //                // Update UI from the main thread when available
        //                dispatch_async(dispatch_get_main_queue(), ^{
        //                    //self.userAvatar.image = [UIImage imageWithData:avatarData];
        //                    dataImage = avatarData;
        //                });
        //            }
        //        });
        
        NSString *getTempPwd = [ClientUtils loadSetting:TEMPGG_PASSWORD_KEY];
        NSString *getTempEmail = [ClientUtils loadSetting:TEMPGG_EMAIL_KEY];
        if([getTempPwd isEqualToString:@""] || getTempPwd == nil)
        {
            PwdViewController *pwdView = [[PwdViewController alloc] init];
            pwdView.email = email;
            pwdView.fullname = name;
            pwdView.birthday = birthday;
            pwdView.social = TEMPGG_PASSWORD_KEY;
            pwdView.accountID = person.identifier;
            pwdView.accountType = @"Google+";
            [self presentViewController:pwdView animated:YES completion:nil];
        }
        else if([email isEqualToString:getTempEmail])
        {
            [self signIn:email md5Password:getTempPwd];
        }
    }
}


- (void)presentSignInViewController:(UIViewController *)viewController {
    // This is an example of how you can implement it if your app is navigation-based.
    [[self navigationController] pushViewController:viewController animated:YES];
    
}


// This method will be called when the user information has been fetched
- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
    
    
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    if([username isEqualToString:@""] || username == nil)
    {
        //
        //    NSString *urlString = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large", user.id];
        //    NSURL *url = [NSURL URLWithString:urlString];
        //    NSData *data = [NSData dataWithContentsOfURL:url];
        //    [ServiceClient saveProfilePicture:self.view username:username md5Password:md5Password data:data successBlock:^(id responseObject) {
        //    } failureBlock:^(NSError *error) {
        //    }];
        
        NSString *email = [user objectForKey:@"email"];
        NSString *getTempEmail = [ClientUtils loadSetting:TEMPFB_EMAIL_KEY];
        NSString *getTempPwd = [ClientUtils loadSetting:TEMPFB_PASSWORD_KEY];
        if([getTempPwd isEqualToString:@""] || getTempPwd == nil)
        {
            NSString *name = user.name;
            NSString *birthday = user.birthday == nil ? @"01-01-2000" : user.birthday;
            PwdViewController *pwdView = [[PwdViewController alloc] init];
            pwdView.email = email;
            pwdView.fullname = name;
            pwdView.birthday = birthday;
            pwdView.social = TEMPFB_PASSWORD_KEY;
            pwdView.accountID = [NSString stringWithFormat:@"%@",user.id];
            pwdView.accountType = @"Facebook";
            [self presentViewController:pwdView animated:YES completion:nil];
        }
        else if([email isEqualToString:getTempEmail])
        {
            [self signIn:email md5Password:getTempPwd];
        }
    }
}


// You need to override loginView:handleError in order to handle possible errors that can occur during login
- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error {
    NSString *alertMessage, *alertTitle;
    
    // If the user should perform an action outside of you app to recover,
    // the SDK will provide a message for the user, you just need to surface it.
    // This conveniently handles cases like Facebook password change or unverified Facebook accounts.
    if ([FBErrorUtility shouldNotifyUserForError:error]) {
        alertTitle = @"Facebook error";
        alertMessage = [FBErrorUtility userMessageForError:error];
        
        // This code will handle session closures since that happen outside of the app.
        // You can take a look at our error handling guide to know more about it
        // https://developers.facebook.com/docs/ios/errors
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession) {
        alertTitle = @"Session Error";
        alertMessage = @"Your current session is no longer valid. Please log in again.";
        
        // If the user has cancelled a login, we will do nothing.
        // You can also choose to show the user a message if cancelling login will result in
        // the user not being able to complete a task they had initiated in your app
        // (like accessing FB-stored information or posting to Facebook)
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
        NSLog(@"user cancelled login");
        
        // For simplicity, this sample handles other errors with a generic message
        // You can checkout our error handling guide for more detailed information
        // https://developers.facebook.com/docs/ios/errors
    } else {
        alertTitle  = @"Something went wrong";
        alertMessage = @"Please try again later.";
        NSLog(@"Unexpected error:%@", error);
    }
    
    if (alertMessage) {
        [[[UIAlertView alloc] initWithTitle:alertTitle
                                    message:alertMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}

- (void)SyncData {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    [ServiceClient getStructureInterests:nil username:username md5Password:password successBlock:^(id responseObject) {
        data.interests = responseObject;
        syncInterests = YES;
    } failureBlock:^(NSError *error) {
        syncInterests = YES;
        NSLog(@"Sync get interests Error: %@",error);
    }];
    
    [ServiceClient getStructureUserFavorites:nil username:username md5Password:password successBlock:^(id responseObject) {
        data.userinterests = responseObject;
        syncUserInterests = YES;
    } failureBlock:^(NSError *error) {
        syncUserInterests = YES;
        NSLog(@"Sync get user interests Error: %@",error);
    }];
}


@end
