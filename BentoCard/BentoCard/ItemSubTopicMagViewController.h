//
//  ItemSubTopicMagViewController.h
//  BentoCard
//
//  Created by VO HONG HAI on 1/12/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleModel.h"

@interface ItemSubTopicMagViewController : UIViewController

@property (nonatomic, strong) ArticleModel *article;

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel *descLabel;
@property (nonatomic, strong) IBOutlet UILabel *authorLabel;
@property (nonatomic, strong) IBOutlet UIImageView *thumbnailImage;
@property (nonatomic, strong) IBOutlet UIButton *overlayButton;

- (void)setContent:(ArticleModel *)model;
- (IBAction)btOverlayTouched:(id)sender;

@end
