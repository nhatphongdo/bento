//
//  ArticleSideBarViewController.h
//  BentoCard
//
//  Created by VO HONG HAI on 1/23/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleSideBarViewController : UIViewController

@property (nonatomic,strong) IBOutlet UILabel *allTotalLabel;
@property (nonatomic,strong) IBOutlet UILabel *showtopicTitleLabel;
@property (nonatomic,strong) IBOutlet UIButton *showtopicButton;
@property (nonatomic,strong) IBOutlet UIButton *settingButton;
@property (nonatomic,strong) IBOutlet UIButton *titleButton;
@property (nonatomic,strong) IBOutlet UIScrollView *containerScrollView;
@property (nonatomic,strong) IBOutlet UIScrollView *settingScrollView;
@property (nonatomic,strong) IBOutlet UIView *containerView;
@property (nonatomic,strong) IBOutlet UIView *settingView;
@property (nonatomic,strong) IBOutlet UIView *tabsView;

- (void)refreshContainer;
- (void)loadDataShowTopic;
- (void)loadDataSetting;
- (IBAction)showtopicTouched:(id)sender;
- (IBAction)setttingTouched:(id)sender;


@end
