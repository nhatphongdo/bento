//
//  ItemSubTopicMagViewController.m
//  BentoCard
//
//  Created by VO HONG HAI on 1/12/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import "ItemSubTopicMagViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "PKRArticleViewController.h"
#import "ClientUtils.h"

@interface ItemSubTopicMagViewController ()

@end

@implementation ItemSubTopicMagViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)setContent:(ArticleModel *)model {
    
    self.article = model;
    self.titleLabel.text = self.article.Title;
    self.descLabel.text = self.article.ShortDescription;
    self.authorLabel.text = [NSString stringWithFormat:@"Contributed by %@", self.article.Author];
    [self.thumbnailImage setImageWithURL:[NSURL URLWithString:self.article.Thumbnail]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btOverlayTouched:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PKRArticleViewController *myController = [storyboard instantiateViewControllerWithIdentifier:@"ArticleView"];
    myController.postID = self.article.ID;
    myController.liked = self.article.Liked;
    myController.likes = self.article.Likes;
    myController.topicID = self.article.Category;
    myController.topicName = [ClientUtils getTopicNameById:self.article.Category];
    [self.navigationController pushViewController:myController animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
