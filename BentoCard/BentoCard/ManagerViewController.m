//
//  ManagerViewController.m
//  BentoCard
//
//  Created by VO HONG HAI on 1/12/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import "ManagerViewController.h"
#import "Constrain.h"
#import "PageViewController.h"
#import "ClientUtils.h"
#import "ArticleSideBarViewController.h"
#import "TempData.h"

@interface ManagerViewController () {
    PageViewController *page;
}

@end

@implementation ManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    TempData *data = [TempData getInstance];
    data.State = NAV_MANAGE;
    page = [[PageViewController alloc] initWithNibName:@"PageViewController" bundle:nil];
    page.isLandingPage = YES;
    CGRect frame = page.view.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    frame.size.width = self.view.frame.size.width;
    frame.size.height = self.view.frame.size.height;
    page.view.frame = frame;
    
    ArticleSideBarViewController *leftview_article  = [[ArticleSideBarViewController alloc] initWithNibName:@"ArticleSideBarViewController" bundle:nil];
    [self setFrontViewController:page];
    [self setLeftViewController:leftview_article];
    
    self.recognizesPanningOnFrontView = NO;
    self.recognizesResetTapOnFrontView = NO;
    self.delegate = self;
    
    page.title = APPLICATION_TITLE;
    NSArray *menuItems = [NSArray arrayWithObjects:@{@"name":@"MANAGE",@"nib":@"managerview"}, @{@"name":@"ARTICLE",@"nib":@"landingview"}, @{@"name":@"SOCIAL",@"nib":@"socialview"}, nil];
    [page loadMenuList:menuItems activeTopic:@"MANAGE"];
    
    //todo: Load list topics
    [page loadManageTopics];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
}

- (void)revealController:(PKRevealController *)revealController didChangeToState:(PKRevealControllerState)state {
    [revealController setMinimumWidth:SIZE_SIDEBAR maximumWidth:SIZE_SIDEBAR forViewController:revealController.focusedController];
    if (revealController.focusedController == revealController.leftViewController) {
        [revealController.focusedController.view setFrame:CGRectMake(0, 0, SIZE_SIDEBAR, self.view.frame.size.height)];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
