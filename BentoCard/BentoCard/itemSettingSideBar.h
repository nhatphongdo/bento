//
//  itemSettingSideBar.h
//  BentoCard
//
//  Created by VO HONG HAI on 1/27/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InterestModel.h"

@interface itemSettingSideBar : UIViewController

@property (nonatomic,strong) InterestModel *interest;

@property (nonatomic,strong) IBOutlet UILabel *nameLabel;
@property (nonatomic,strong) IBOutlet UIImageView *radioImage;
@property (nonatomic,strong) IBOutlet UIButton *settingItemButton;

- (IBAction)settingItemTouched:(id)sender;

@end
