//
//  NewReport.h
//  BentoCard
//
//  Created by VO HONG HAI on 1/5/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICheckbox.h"
#import "ArticleViewController.h"
        
@interface NewReport : UIViewController

@property (nonatomic,strong) NSString *postID;
@property (nonatomic,strong) ArticleViewController *articleContainer;

@property (nonatomic,strong) IBOutlet UICheckbox *cbContent;
@property (nonatomic,strong) IBOutlet UICheckbox *cbTechnical;
@property (nonatomic,strong) IBOutlet UICheckbox *cbOther;
@property (nonatomic,strong) IBOutlet UIButton *btClose;
@property (nonatomic,strong) IBOutlet UIButton *btReport;
@property (nonatomic,strong) IBOutlet UITextView *txtMessage;

- (IBAction)closeTouched:(id)sender;
- (IBAction)reportTouched:(id)sender;
- (IBAction)selectcbContent:(id)sender;
- (IBAction)selectcbTechnical:(id)sender;
- (IBAction)selectcbOther:(id)sender;

@end
