//
//  itemSettingSideBar.m
//  BentoCard
//
//  Created by VO HONG HAI on 1/27/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import "itemSettingSideBar.h"
#import "TempData.h"
#import "UIImage+DeviceSpecificMedia.h"

@interface itemSettingSideBar ()
{
    NSArray *interests;
    TempData *data;
}
@end

@implementation itemSettingSideBar

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.nameLabel.text = self.interest.Name;
    
    data = [TempData getInstance];
    InterestModel *item = [self findByInterest:self.interest];
    if(item == nil)
    {
        UIImage *radimage = [UIImage imageForDeviceWithName:@"icon_radio_uncheck.png"];
        [self.radioImage setImage:radimage];
    }
    else
    {
        UIImage *radimage = [UIImage imageForDeviceWithName:@"icon_radio_checked.png"];
        [self.radioImage setImage:radimage];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (InterestModel *)findByInterest:(InterestModel *)interestitem {
    for (InterestModel *item in data.userinterests) {
        if([item.ID isEqualToString:interestitem.ID])
        {
            return item;
        }
    }
    return nil;
}

- (IBAction)settingItemTouched:(id)sender {
    InterestModel *item = [self findByInterest:self.interest];
    if(item != nil)
    {
//        [data.userinterests removeObject:item];
        UIImage *radimage = [UIImage imageForDeviceWithName:@"icon_radio_uncheck.png"];
        [self.radioImage setImage:radimage];
    }
    else
    {
//        [data.userinterests addObject:self.interest];
        UIImage *radimage = [UIImage imageForDeviceWithName:@"icon_radio_checked.png"];
        [self.radioImage setImage:radimage];
    }
}

@end
