//
//  ServiceClient.h
//  BentoCard
//
//  Created by VO HONG HAI on 12/17/14.
//  Copyright (c) 2014 VietDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@interface ServiceClient : NSObject

+ (void)loadService:(UIView *)view
                url:(NSString *)url
             params:(NSDictionary *)params
       successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
       failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;
+ (void)postFile:(UIView *)view
             url:(NSString *)url
          params:(NSDictionary *)params
        fileData:(NSData *)fileData
            name:(NSString *)name
        fileName:(NSString *)fileName
        fileType:(NSString *)fileType
    successBlock:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
    failureBlock:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

+ (void) signIn:(UIView *)view
       username:(NSString *)username
    md5Password:(NSString *)md5Password
   successBlock:(void (^)(id responseObject))success
   failureBlock:(void (^)(NSError *error))failure;
+ (void) signOut:(UIView *)view
        username:(NSString *)username
     md5Password:(NSString *)md5Password
    successBlock:(void (^)(id responseObject))success
    failureBlock:(void (^)(NSError *error))failure;
+ (void) signUp:(UIView *)view
       username:(NSString *)username
       password:(NSString *)password
confirmpassword:(NSString *)confirmpassword
       fullname:(NSString *)fullname
       birthday:(NSString *)birthday
    accountType:(NSString *)accountType
      accountID:(NSString *)accountID
   successBlock:(void (^)(id responseObject))success
   failureBlock:(void (^)(NSError *error))failure;
+ (void) forgotPassword:(UIView *)view
               username:(NSString *)username
            md5Password:(NSString *)md5Password
           successBlock:(void (^)(id responseObject))success
           failureBlock:(void (^)(NSError *error))failure;
+ (void) getProfile:(UIView *)view
           username:(NSString *)username
        md5Password:(NSString *)md5Password
       successBlock:(void (^)(id responseObject))success
       failureBlock:(void (^)(NSError *error))failure;
+ (void) updateProfile:(UIView *)view
              username:(NSString *)username
           md5Password:(NSString *)md5Password
              fullname:(NSString *)fullname
                gender:(NSString *)gender
              birthday:(NSString *)birthday
                 email:(NSString *)email
          successBlock:(void (^)(id responseObject))success
          failureBlock:(void (^)(NSError *error))failure;
+ (void) getUserInterests:(UIView *)view
                 username:(NSString *)username
              md5Password:(NSString *)md5Password
             successBlock:(void (^)(id responseObject))success
             failureBlock:(void (^)(NSError *error))failure;
+ (void) setUserInterests:(UIView *)view
                 username:(NSString *)username
              md5Password:(NSString *)md5Password
                interests:(NSArray *)interests
             successBlock:(void (^)(id responseObject))success
             failureBlock:(void (^)(NSError *error))failure;

+ (void) getInterests:(UIView *)view
             username:(NSString *)username
          md5Password:(NSString *)md5Password
         successBlock:(void (^)(id responseObject))success
         failureBlock:(void (^)(NSError *error))failure;

+ (void) getInterestsByParentID:(UIView *)view
             username:(NSString *)username
          md5Password:(NSString *)md5Password
          parentID:(NSString *)parentID
         successBlock:(void (^)(id responseObject))success
         failureBlock:(void (^)(NSError *error))failure;

+ (void) getStructureInterests:(UIView *)view
                      username:(NSString *)username
                   md5Password:(NSString *)md5Password
                  successBlock:(void (^)(id responseObject))success
                  failureBlock:(void (^)(NSError *error))failure;
+ (void) getStructureUserInterests:(UIView *)view
                          username:(NSString *)username
                       md5Password:(NSString *)md5Password
                      successBlock:(void (^)(id responseObject))success
                      failureBlock:(void (^)(NSError *error))failure;
+ (void) getStructureUserFavorites:(UIView *)view
                          username:(NSString *)username
                       md5Password:(NSString *)md5Password
                      successBlock:(void (^)(id responseObject))success
                      failureBlock:(void (^)(NSError *error))failure;
+ (void) getArticlesOfInterest:(UIView *)view
                      username:(NSString *)username
                   md5Password:(NSString *)md5Password
                    interestID:(NSString *)interestID
                          page:(int)page
                  itemsPerPage:(int)itemsPerPage
                  successBlock:(void (^)(id responseObject))success
                  failureBlock:(void (^)(NSError *error))failure;

+ (void) getArticlesOfSubTopicView:(UIView *)view
                      username:(NSString *)username
                   md5Password:(NSString *)md5Password
                    interestID:(NSString *)interestID
                  successBlock:(void (^)(id responseObject))success
                  failureBlock:(void (^)(NSError *error))failure;

+ (void) getMyArticlesOfSubTopicView:(UIView *)view
                          username:(NSString *)username
                       md5Password:(NSString *)md5Password
                        interestID:(NSString *)interestID
                      successBlock:(void (^)(id responseObject))success
                      failureBlock:(void (^)(NSError *error))failure;

+ (void) getUserArticlesOfInterest:(UIView *)view
                          username:(NSString *)username
                       md5Password:(NSString *)md5Password
                        interestID:(NSString *)interestID
                              page:(int)page
                      itemsPerPage:(int)itemsPerPage
                      successBlock:(void (^)(id responseObject))success
                      failureBlock:(void (^)(NSError *error))failure;

+ (void) getLatestArticles:(UIView *)view
                  username:(NSString *)username
               md5Password:(NSString *)md5Password
                      page:(int)page
              itemsPerPage:(int)itemsPerPage
              successBlock:(void (^)(id responseObject))success
              failureBlock:(void (^)(NSError *error))failure;
+ (void) getDetailArticle:(UIView *)view
                 username:(NSString *)username
              md5Password:(NSString *)md5Password
                articleID:(NSString *)articleID
             successBlock:(void (^)(id responseObject))success
             failureBlock:(void (^)(NSError *error))failure;
+ (void) getCommentsOfArticle:(UIView *)view
                     username:(NSString *)username
                  md5Password:(NSString *)md5Password
                       postID:(NSString *)postID
                         page:(int)page
                 itemsPerPage:(int)itemsPerPage
                 successBlock:(void (^)(id responseObject))success
                 failureBlock:(void (^)(NSError *error))failure;
+ (void) getRepliesOfComment:(UIView *)view
                    username:(NSString *)username
                 md5Password:(NSString *)md5Password
                      postID:(NSString *)postID
                        page:(int)page
                itemsPerPage:(int)itemsPerPage
                successBlock:(void (^)(id responseObject))success
                failureBlock:(void (^)(NSError *error))failure;
+ (void) postComment:(UIView *)view
            username:(NSString *)username
         md5Password:(NSString *)md5Password
              postID:(NSString *)postID
             message:(NSString*)message
        successBlock:(void (^)(id responseObject))success
        failureBlock:(void (^)(NSError *error))failure;
+ (void) postReply:(UIView *)view
          username:(NSString *)username
       md5Password:(NSString *)md5Password
            postID:(NSString *)postID
           message:(NSString*)message
      successBlock:(void (^)(id responseObject))success
      failureBlock:(void (^)(NSError *error))failure;
+ (void) getNotesOfArticle:(UIView *)view
                  username:(NSString *)username
               md5Password:(NSString *)md5Password
                    postID:(NSString *)postID
                      page:(int)page
              itemsPerPage:(int)itemsPerPage
              successBlock:(void (^)(id responseObject))success
              failureBlock:(void (^)(NSError *error))failure;
+ (void) postNoteForArticle:(UIView *)view
                   username:(NSString *)username
                md5Password:(NSString *)md5Password
                     postID:(NSString *)postID
                    message:(NSString*)message
               successBlock:(void (^)(id responseObject))success
               failureBlock:(void (^)(NSError *error))failure;
+ (void) deleteNote:(UIView *)view
           username:(NSString *)username
        md5Password:(NSString *)md5Password
             noteID:(NSString *)noteID
       successBlock:(void (^)(id responseObject))success
       failureBlock:(void (^)(NSError *error))failure;
+ (void) getSharesOfArticle:(UIView *)view
                   username:(NSString *)username
                md5Password:(NSString *)md5Password
                     postID:(NSString *)postID
               successBlock:(void (^)(id responseObject))success
               failureBlock:(void (^)(NSError *error))failure;
+ (void) getLikesOfArticle:(UIView *)view
                  username:(NSString *)username
               md5Password:(NSString *)md5Password
                    postID:(NSString *)postID
              successBlock:(void (^)(id responseObject))success
              failureBlock:(void (^)(NSError *error))failure;
+ (void) likePost:(UIView *)view
         username:(NSString *)username
      md5Password:(NSString *)md5Password
           postID:(NSString *)postID
     successBlock:(void (^)(id responseObject))success
     failureBlock:(void (^)(NSError *error))failure;
+ (void) unlikePost:(UIView *)view
           username:(NSString *)username
        md5Password:(NSString *)md5Password
             postID:(NSString *)postID
       successBlock:(void (^)(id responseObject))success
       failureBlock:(void (^)(NSError *error))failure;
+ (void) sharePost:(UIView *)view
          username:(NSString *)username
       md5Password:(NSString *)md5Password
            postID:(NSString *)postID
      successBlock:(void (^)(id responseObject))success
      failureBlock:(void (^)(NSError *error))failure;

+ (void) getSponsors:(UIView *)view
            username:(NSString *)username
         md5Password:(NSString *)md5Password
        successBlock:(void (^)(id responseObject))success
        failureBlock:(void (^)(NSError *error))failure;
+ (void) getDetailSponsor:(UIView *)view
                 username:(NSString *)username
              md5Password:(NSString *)md5Password
                sponsorID:(NSString *)sponsorID
             successBlock:(void (^)(id responseObject))success
             failureBlock:(void (^)(NSError *error))failure;

+ (void) saveProfilePicture:(UIView *)view
                   username:(NSString *)username
                md5Password:(NSString *)md5Password
                       data:(NSData *)data
                       name:(NSString *)name
                   filename:(NSString *)filename
                   filetype:(NSString *)filetype
               successBlock:(void (^)(id responseObject))success
               failureBlock:(void (^)(NSError *error))failure;

+ (void) searchUserFavoriteArticles:(UIView *)view
                           username:(NSString *)username
                        md5Password:(NSString *)md5Password
                         searchTerm:(NSString *)searchTerm
                               Page:(int)Page
                       ItemsPerPage:(int)ItemsPerPage
                       successBlock:(void (^)(id responseObject))success
                       failureBlock:(void (^)(NSError *error))failure;

+ (void) searchArticles:(UIView *)view
               username:(NSString *)username
            md5Password:(NSString *)md5Password
             searchTerm:(NSString *)searchTerm
                   Page:(int)Page
           ItemsPerPage:(int)ItemsPerPage
           successBlock:(void (^)(id responseObject))success
           failureBlock:(void (^)(NSError *error))failure;

+ (void) favoritePost:(UIView *)view
             username:(NSString *)username
          md5Password:(NSString *)md5Password
               postID:(NSString *)postID
         successBlock:(void (^)(id responseObject))success
         failureBlock:(void (^)(NSError *error))failure;

+ (void) unfavoritePost:(UIView *)view
               username:(NSString *)username
            md5Password:(NSString *)md5Password
                 postID:(NSString *)postID
           successBlock:(void (^)(id responseObject))success
           failureBlock:(void (^)(NSError *error))failure;

+ (void) reportsArticle:(UIView *)view
               username:(NSString *)username
            md5Password:(NSString *)md5Password
                 postID:(NSString *)postID
                message:(NSString *)message
                   type:(NSString *)type
           successBlock:(void (^)(id responseObject))success
           failureBlock:(void (^)(NSError *error))failure;

@end
