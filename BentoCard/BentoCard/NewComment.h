//
//  NewComment.h
//  BentoCard
//
//  Created by VO HONG HAI on 1/5/15.
//  Copyright (c) 2015 VietDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleViewController.h"
#import "ListItemComment.h"

@interface NewComment : UIViewController

@property (nonatomic,strong) ArticleViewController *articleContainer;
@property (nonatomic,strong) ListItemComment *commentContainer;

@property (nonatomic,strong) NSString *postID; //article or comment id
@property (nonatomic,strong) NSString *type;  //reply: reply comment, comment: comment on a article
@property (nonatomic) int index;

@property (nonatomic,strong) IBOutlet UITextField *txtComment;
@property (nonatomic,strong) IBOutlet UIButton *btReply;
@property (nonatomic,strong) IBOutlet UIButton *btClose;

- (IBAction)closeTouched:(id)sender;
- (IBAction)replyTouched:(id)sender;

@end
