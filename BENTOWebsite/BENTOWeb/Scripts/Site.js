﻿$(document).ready(function () {
    $('.setting-icon-card').hide();

    $('.change-view').click(function () {
        $('.setting-icon-card').toggle();
        $('.setting-icon-title').toggle();
        $('.list-view').toggleClass("title-view").toggleClass("card-view");
        $('.view-item div:nth-child(1)').toggleClass("col-xs-4").toggleClass("col-xs-2");
        $('.view-item div:nth-child(2)').toggleClass("col-xs-8").toggleClass("col-xs-10");
    });

    $('.article-tools > .btn-tool').click(function () {
        $('.article-tools > .btn-tool:nth-child(' + ($(this).index() + 1) + ')').toggleClass("selected");
        switch ($(this).index() + 1) {
            case 1:
                $('#Comment').focus();
                $('.comments-list').slideToggle("slow");
                $('.notes-list, .share-tools').slideUp("fast");
                $('.article-tools > .btn-tool:nth-child(2).selected, .article-tools > .btn-tool:nth-child(3).selected').toggleClass("selected");
                break;
            case 2:
                $('#Note').focus();
                $('.notes-list').slideToggle("slow");
                $('.comments-list, .share-tools').slideUp("fast");
                $('.article-tools > .btn-tool:nth-child(1).selected, .article-tools > .btn-tool:nth-child(3).selected').toggleClass("selected");
                break;
            case 3:
                $('.share-tools').slideToggle("slow");
                $('.notes-list, .comments-list').slideUp("fast");
                $('.article-tools > .btn-tool:nth-child(1).selected, .article-tools > .btn-tool:nth-child(2).selected').toggleClass("selected");
                break;
        }

        $('#scrollEnd').focus();

    });

    $('.reply-comment').click(function () {
        $("#Comment").focus();
    });

    $('.zoom-in').click(function () {
        var width = $('#zoom_img').find(".modal-dialog").width() + 100;
        $('#zoom_img').find(".modal-dialog").width(width >= screen.availWidth ? screen.width : width);
    });

    $('.zoom-out').click(function () {
        var width = $('#zoom_img').find(".modal-dialog").width() - 100;
        $('#zoom_img').find(".modal-dialog").width(width < 600 ? 600 : width);
    });

    //$('.profile-name').click(function () {
    //    $(this).hide();
    //    $('.profile-name-input').show().focus();
    //});

    //$('.profile-name-input').focusout(function () {
    //    $(this).hide();
    //    $('.profile-name').text($(this).val()).show();
    //});

    //$('.profile-addr').click(function () {
    //    $(this).hide();
    //    $('.profile-addr-input').show().focus();
    //});

    //$('.profile-addr-input').focusout(function () {
    //    $(this).hide();
    //    $('.profile-addr').text($(this).val()).show();
    //});

    //$('#dob').parent().click(function () {
    //    $("#Birthday, .birthday-icon").show();
    //});

    //$('#Birthday').change(function () {
    //    if ($('#dob').text() != $(this).val() && $(this).val() != null && $(this).val() != '') {
    //        $('#dob').text($(this).val());
    //    }
    //    $("#Birthday, .birthday-icon").hide();
    //});

    //$('#gender').click(function () {
    //    $(this).hide();
    //    $('#select_gender').show().click();
    //});

    //$('#select_gender').focusout(function () {
    //    $(this).hide();
    //    $('#gender').text($(this).find('option:selected').text()).show();
    //});

    $('#log-in .button.signup').click(function () {
        $('#log-in').hide();

        $('#name-signup').val('');
        $('#pwd-signup').val('');
        $('#email-signup').val('');
        $('#date-signup').val('');
        $('#sign-up').show();
    });

    $('.btn-address.btn-setting.btn-search > a').click(function () {
        $('#Search').toggle("slow").focus();
    })

    $('.nav.setting > .expect_collapse').click(function (index) {
        $('#choose-interest').removeClass('in');
        $('#collapse_choose_interest').addClass('collapsed');
    });

    $('#Note').keypress(function (e) {
        if (e.which == 13) {
            $('#send-note').click();
        }
    });

    $('#Comment').keypress(function (e) {
        if (e.which == 13) {
            $('#send-comment').click();
        }
    });

    $('.profile-avatar, #upavatar, input#Data').hover(function () {
        $('#upavatar > i').addClass("hover");
    }, function () {
        $('#upavatar > i').removeClass("hover");
    });

});

$(window).load(function () {
    $(window).scrollTop(0);
    $('.list-view').scrollTop(0);
    if ($.cookie("stage") == "url_req") {
        $.removeCookie("stage", { path: '/' });
    }
    // $(window).height();   // returns height of browser viewport
    // $(document).height(); // returns height of HTML document
    // $(window).width();   // returns width of browser viewport
    // $(document).width(); // returns width of HTML document

    if ($(window).width() < 768) {
        $('.setting-icon-card').hide();
        $('.setting-icon-title').show();
        $('.home-view.list-view').removeClass("card-view").addClass("title-view");
        $('.home-view .view-item div:nth-child(1)').removeClass("col-xs-4").addClass("col-xs-2");
        $('.home-view .view-item div:nth-child(2)').removeClass("col-xs-8").addClass("col-xs-10");
    }

});

$(window).resize(function () {
    if ($(window).width() < 768) {
        $('.setting-icon-card').hide();
        $('.setting-icon-title').show();
        $('.home-view.list-view').removeClass("card-view").addClass("title-view");
        $('.home-view .view-item div:nth-child(1)').removeClass("col-xs-4").addClass("col-xs-2");
        $('.home-view .view-item div:nth-child(2)').removeClass("col-xs-8").addClass("col-xs-10");
    }

});

$(window).scroll(function () {
    //var scrollHeight = $(document).height();
    //var scrollPosition = $(window).height() + $(window).scrollTop();
    //if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
    //    // when scroll to bottom of the page
    //    console.log("BING!");
    //}
});