var rootUrl = 'http://bentoportal.com/portal/api/';

var SIGN_IN_URL = rootUrl + "accounts/signin";
var SIGN_OUT_URL = rootUrl + "accounts/signout";
var SIGN_UP_URL = rootUrl + "accounts/signup";
var FORGOT_PASSWORD_URL = rootUrl + "accounts/forgot";
var GET_PROFILE_URL = rootUrl + "accounts/profile";
var UPDATE_PROFILE_URL = rootUrl + "accounts/update";
var UPLOAD_AVATAR_URL = rootUrl + "accounts/update/avatar";
var GET_USER_INTERESTS_URL = rootUrl + "accounts/interests";
var SET_USER_INTERESTS_URL = rootUrl + "accounts/interests/update";

var GET_INTERESTS_URL = rootUrl + "interests";
var GET_STRUCTURE_INTERESTS_URL = rootUrl + "interests/structure";
var GET_STRUCTURE_USER_INTERESTS_URL = rootUrl + "interests/structure/user";
var GET_STRUCTURE_FAVORITE_INTERESTS_URL = rootUrl + "interests/structure/favorite";
var GET_ARTICLES_OF_INTEREST_URL = rootUrl + "interests/{0}/articles";
var GET_ARTICLES_OF_INTEREST_SLUG_URL = rootUrl + "interests/{0}/slugarticles";
var GET_USER_ARTICLES_OF_INTEREST_URL = rootUrl + "interests/{0}/myarticles";
var GET_USER_ARTICLES_OF_INTEREST_SLUG_URL = rootUrl + "interests/{0}/slugmyarticles";
var GET_USER_FAVORITES_OF_INTEREST_URL = rootUrl + "interests/{0}/myfavorites";
var GET_USER_FAVORITES_OF_INTEREST_SLUG_URL = rootUrl + "interests/{0}/slugmyfavorites";

var GET_LATEST_ARTICLES_URL = rootUrl + "articles/latest";
var GET_LATEST_ARTICLES_SLUG_URL = rootUrl + "articles/latestincludeslug";
var GET_SEARCH_ARTICLES_URL = rootUrl + "articles/search";
var GET_SEARCH_ARTICLES_SLUG_URL = rootUrl + "articles/searchincludeslug";
var GET_MYSEARCH_ARTICLES_URL = rootUrl + "articles/mysearch";
var GET_MYSEARCH_ARTICLES_SLUG_URL = rootUrl + "articles/mysearchincludeslug";
var GET_DETAIL_ARTICLE_URL = rootUrl + "articles/{0}";
var GET_DETAIL_ARTICLE_SLUG_URL = rootUrl + "articles/{0}/slug";
var GET_COMMENTS_OF_ARTICLE_URL = rootUrl + "articles/{0}/comments";
var POST_COMMENT_URL = rootUrl + "articles/{0}/comments/post";
var GET_REPLIES_OF_COMMENT_URL = rootUrl + "comments/{0}/replies";
var POST_REPLY_URL = rootUrl + "comments/{0}/reply";
var LIKE_POST_URL = rootUrl + "action/{0}/like";
var UNLIKE_POST_URL = rootUrl + "action/{0}/unlike";
var SHARE_POST_URL = rootUrl + "action/{0}/share";
var GET_NOTES_OF_ARTICLE_URL = rootUrl + "articles/{0}/notes";
var POST_NOTE_FOR_ARTICLE_URL = rootUrl + "articles/{0}/notes/post";
var DELETE_NOTE_URL = rootUrl + "notes/{0}/delete";
var GET_SHARES_OF_ARTICLE_URL = rootUrl + "articles/{0}/shares";
var GET_LIKES_OF_ARTICLE_URL = rootUrl + "articles/{0}/likes";
var POST_REPORTS_OF_ARTICLE_URL = rootUrl + "articles/{0}/reports/post";
var ADD_ARTICLE_TO_FAVORITE_LIST_URL = rootUrl + "action/{0}/favorite";
var REMOVE_ARTICLE_FROM_FAVORITE_LIST_URL = rootUrl + "action/{0}/unfavorite";

var GET_SPONSORS_URL = rootUrl + "sponsors";
var GET_SPONSOR_DETAIL_URL = rootUrl + "sponsors/{0}";
var GET_SPONSOR_DETAIL_SLUG_URL = rootUrl + "sponsors/{0}/slug";

var page = 1;
var itemsPerPage = 10;

var avatar_default = '/images/default-avatar.png';
var fullname_default = 'Anonymous.';
var name_default = 'anonymous@bentoportal.com';
var email_default = 'anonymous@bentoportal.com';
var title_default = 'Quote.';
var caption_default = '';
var author_default = 'Unknown.';
var shortDescr_default = 'No Description Available.';
var web_default = 'http://bentoportal.com/';
var body_default = 'No Content Available.';
var published_default = 'Unspecified time';

//if (avatar == null || avatar == '') { avatar = avatar_default; }
//if (fullname == null || fullname == '') { fullname = fullname_default; }
//if (name == null || name == '') { name = name_default; }
//if (email == null || email == '') { email = email_default; }
//if (title == null || title == '') { title = title_default; }
//if (author == null || author == '') { author = author_default; }
//if (shortDescription == null || shortDescription == '') { shortDescription = shortDescr_default; }
//if (caption == null || caption == '') { caption = caption_default; }
//if (web == null || web == '') { web = web_default; }
//if (body == null || body == '') { body = body_default; }

var userInterest = new Array();


function genSlug(title, id) {
    return monoGenSlug(title) + '-' + id.substr(0, 8);
}

function monoGenSlug(title) {
    var slugArticle = '';
    slugArticle = title;
    slugArticle = slugArticle.replace(/[^\w\s-]/g, '');
    slugArticle = slugArticle.replace(/[_\s]/g, '-');
    slugArticle = slugArticle.toLowerCase();
    return slugArticle;
}

function createGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function redirect404() {
    var url = location.href;
    if (location.pathname != '/') {
        url = url.replace(location.pathname, '/error404')
    }
    else {
        url = url + '/error404';
    }
    location.replace(url)
}

//Layout
function getFeatured(username, pwd) {
    username = '' || username;
    pwd = '' || pwd;
    itemsPerPage = 3;

    //console.log('BEGIN');
    $('.feature-item').hide();

    $.ajax({
        type: 'POST',
        url: GET_LATEST_ARTICLES_SLUG_URL,
        dataType: 'json',
        data: { "Username": username, "Password": pwd, "Page": page, "ItemsPerPage": itemsPerPage },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 1 || result.Error == 2) {
                $.removeCookie('username', { path: '/' });
                $.removeCookie('password', { path: '/' });
                location.reload();
            }

            if (result.Error == 0) {
                $('#featured').html('');

                for (var i = 0; i < itemsPerPage; i++) {
                    var id = result.Data[i].ID;
                    var thumbnail = result.Data[i].Thumbnail;
                    var title = result.Data[i].Title;
                    if (title == null || title == '') { title = title_default; }
                    var likes = result.Data[i].Likes;
                    var author = result.Data[i].Author;
                    if (author == null || author == '') { author = author_default; }
                    var categ_str = html_text(result.Data[i].Categories);
                    var categories = categ_str.split(";")[0];

                    var shortDescription = result.Data[i].ShortDescription;
                    if (shortDescription == null || shortDescription == '') { shortDescription = shortDescr_default; }

                    var categSlug = categ_str.split(";")[1];
                    var articleSlug = result.Data[i].Slug;

                    var link = '/' + categSlug + '/' + articleSlug;

                    var item = '';
                    item += '<div class="feature-item ' + id + '">';
                    item += '<img src="' + thumbnail + '" class="img-responsive" />';
                    item += '<p style="margin: 0; font-weight: bold;">';
                    item += '<a href="' + link + '"><q>' + title + '</q></a>';
                    item += '<br />';
                    item += likes + ' likes - {comments} comments';
                    item += '</p>';
                    item += '<cite>' + author + '.</cite>';
                    item += '</div>'

                    $('#featured').append(item);
                    getCommentsOfArticle(username, pwd, id);
                }

            }

        },
        error: function () {
            $('#featured').html('');
        }
    });
}

function getCommentsOfArticle(username, pwd, id) {

    //console.log('BEGIN');
    $('.feature-item').hide();

    $.ajax({
        type: 'POST',
        url: GET_COMMENTS_OF_ARTICLE_URL.replace("{0}", id),
        dataType: 'json',
        data: { "Username": username, "Password": pwd },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                var text = $('.feature-item.' + id).find('p').html();
                $('.feature-item.' + id).find('p').html(text.replace('{comments}', result.Data.length));
            }

            //console.log('END.');
            $('.feature-item').show();

        },
        error: function () {
            var text = $('.feature-item.' + id).find('p').html();
            $('.feature-item.' + id).find('p').html(text.replace('{comments}', 0));
        }
    });
}

//Index
function loadSettingUserInterest(username, pwd) {

    //console.log('BEGIN');
    $('.nav.setting').hide();

    //all
    $.ajax({
        type: 'POST',
        url: GET_STRUCTURE_INTERESTS_URL,
        dataType: 'json',
        data: { "Username": username, "Password": pwd },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                $('#level1.multi-level').html('<li class="text-center"><a href="javascript:void(0)"><strong class="title">Show</strong></a></li><li class="divider"></li><li id="all"></li><li class="divider"></li>');
                var sumTotal = 0;
                for (var i = 0; i < result.Data.length; i++) {
                    if (result.Data[i].Level == 1) {
                        sumTotal += result.Data[i].Total;
                    }
                }
                $.cookie("sumTotal", sumTotal, { expires: 7, path: '/' });
                $('#all').html('<a href="javascript:void(0)">All<span class="count-number">' + sumTotal + '</span></a>');
            }
        },
        error: function () {
            //alert('error');
        }
    });

    //choose
    $.ajax({
        type: 'POST',
        url: GET_INTERESTS_URL,//GET_USER_INTERESTS_URL,
        dataType: 'json',
        data: { "Username": username, "Password": pwd },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                $('#choose-interest').html('<li class="text-center"><a href="javascript:void(0)"><strong class="title">Choose your Interest</strong></a></li><li class="divider"></li>');

                for (var i = 0; i < result.Data.length; i++) {
                    var id = result.Data[i].ID;
                    var name = result.Data[i].Name;
                    var interest = '';
                    interest += '<li id="' + id + '">';
                    interest += '<a href="javascript:void(0)" onclick="chooseInterest(\'' + id + '\')" >' + name;
                    interest += '<span class="check-interest"></span>';
                    interest += '</a>';
                    interest += '<input type="checkbox" value="' + name + '" name="interest" />';
                    interest += '</li>';
                    interest += '<li class="divider"></li>';
                    $('#choose-interest').append(interest);
                    userInterest[i] = { 'id': id, 'checked': false, 'choosed': false, 'changed': false };
                }

                var paras = '\'' + username + '\'';
                paras += ',';
                paras += '\'' + pwd + '\'';
                $('#choose-interest').append('<li><button class="btn btn-default" type="" onclick="setUserInterest(' + paras + ')" >Submit</button></li>');
            }
        },
        error: function () {
            //alert('error');
            $('#choose-interest').html('<li class="text-center"><a href="javascript:void(0)"><strong class="title">Choose your Interest</strong></a></li><li class="divider"></li>');
            $('#choose-interest').append('<li><button class="btn btn-default" type="" onclick="" >Submit</button></li>');
        }
    });

    //built user 's tree interest
    $.ajax({
        type: 'POST',
        url: GET_STRUCTURE_USER_INTERESTS_URL,
        dataType: 'json',
        data: { "Username": username, "Password": pwd },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                //console.log(result.Data.length);
                for (var i = 0; i < result.Data.length; i++) {
                    //console.log(i + '------' + result.Data[i].Level);
                    var id = result.Data[i].Id;
                    var total = result.Data[i].Total;
                    var name = result.Data[i].Name;

                    var categSlug = result.Data[i].Slug;

                    var link = '/' + categSlug;

                    var itemLvl = '';
                    var itemSubLvl = '';
                    var cont = { "Items": itemSubLvl, "Index": i };

                    //console.log(i);
                    if (result.Data[i].Level == 1 && result.Data[i].ParentId == null) {
                        var next = Math.max(i + 1, result.Data.length - 1);

                        itemLvl += '<li class="' + ((next > i && next < result.Data.length) ? 'dropdown-submenu' : '') + '" >';
                        itemLvl += '<a href="' + link + '">' + name;
                        itemLvl += '<span class="count-number">' + result.Data[i].Total + '&nbsp<i class="fa fa-angle-right"></i></span>';
                        itemLvl += '</a>';

                        //console.log(i);
                        if (next > i && next < result.Data.length) {
                            cont = buildSubInterest(result, i, page);
                            //console.log(cont);
                            itemLvl += cont.Items;
                        }

                        itemLvl += '</li>';
                        itemLvl += i == result.Data.length ? '' : '<li class="divider"></li>';

                        $('#level1.multi-level').append(itemLvl);

                        // choose user interest
                        $('#choose-interest').find(' > li#' + id).find('input[type="checkbox"]').attr("checked", "checked");
                        $('#choose-interest').find(' > li#' + id).find('.check-interest').addClass('checked');
                        //console.log('userInterest.length = ' + userInterest.length);
                        for (var x = 0; x < userInterest.length; x++) {
                            //console.log('x = '+ x);
                            if (userInterest[x].id == id) {
                                userInterest[x].checked = true;
                                userInterest[x].choosed = true;
                                x = userInterest.length;
                            }
                        }
                    }
                    i = cont.Index;
                }

                //console.log('END.');
                $('.nav.setting').show();

            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function buildSubInterest(result, curr, page) {
    var idxnextlvl = curr + 1;
    var itemLvl = '';
    var index = curr;
    if (idxnextlvl > curr && idxnextlvl < result.Data.length && result.Data[idxnextlvl].Level > result.Data[curr].Level) {
        //console.log('curr = ' + curr + '--- next = ' + idxnextlvl);
        itemLvl += '<ul class="dropdown-menu" >';
        var itemSubLvl = '';

        for (var i = idxnextlvl; i < result.Data.length; i++) {
            //console.log(i + '------' + result.Data[i].Level);
            var cont = { "Items": itemSubLvl, "Index": i };

            if (result.Data[i].Level == result.Data[curr].Level + 1
                && result.Data[i].ParentId == result.Data[curr].Id) {
                var next = Math.max(i + 1, result.Data.length - 1);

                var id = result.Data[i].Id;
                var name = result.Data[i].Name;
                var total = result.Data[i].Total;

                var categSlug = result.Data[i].Slug;

                var link = '/' + categSlug;

                itemSubLvl += '<li class="' + (next > curr ? 'dropdown-submenu' : '') + '" id="' + id + '" >';
                itemSubLvl += '<a href="' + link + '">' + name;
                itemSubLvl += '<span class="count-number">' + total + '&nbsp<i class="fa fa-angle-right"></i></span>';
                itemSubLvl += '</a>';

                //console.log(i);
                if (next > i) {// && next < result.Data.length) {
                    cont = buildSubInterest(result, i, page);
                    //console.log(cont);
                    itemLvl += cont.Items;
                }

                itemSubLvl += '</li>';
                itemSubLvl += '<li class="divider"></li>';
            }
            i = cont.Index;
        }

        itemLvl += itemSubLvl;
        itemLvl += '</ul>';
    }

    return { "Items": itemLvl, "Index": index };
}

function chooseInterest(id) {
    $('#choose-interest > #' + id + ' > a').find('.check-interest').toggleClass("checked");
    for (var x = 0; x < userInterest.length; x++) {
        if (userInterest[x].id == id) {
            var choosed = $('#choose-interest > #' + id + ' > a').find('.check-interest').hasClass('checked');
            userInterest[x].choosed = choosed;

            var checked = userInterest[x].checked;
            userInterest[x].changed = (checked || choosed) && !(checked && choosed);

            x = userInterest.length;
        }
    }
    //console.log(userInterest);
    $('#choose-interest > #' + id + ' > a').parent().find(' > input[type="checkbox"]').click();
}

function setUserInterest(username, pwd) {
    var interests = '';
    var changed = false;
    for (var x = 0; x < userInterest.length; x++) {
        changed = changed || userInterest[x].changed;
        if (userInterest[x].choosed) {
            interests += userInterest[x].id + ',';
        }
    }
    if (interests.length > 0)
        interests = interests.substr(0, interests.lastIndexOf(','));

    //console.log(userInterest);

    if (changed) {
        //console.log(interests);
        $.ajax({
            type: 'POST',
            url: SET_USER_INTERESTS_URL,
            dataType: 'json',
            data: { "Username": username, "Password": pwd, "Interests": interests },
            crossDomain: true,
            success: function (result) {
                if (result.Error == 0) {
                    $('#collapse_choose_interest').click();
                    location.reload();
                }
            },
            error: function () {
                $('#collapse_choose_interest').click();
                location.reload();
            }
        });
    }
}

//get List Articles Lasted
function getListArticlesLasted(username, pwd, pages, itemsPerPage) {
    var search_string = $.cookie('search_string') || '';
    $.removeCookie('search_string', { path: '/' });

    //console.log('BEGIN');
    $('.list-view').hide();

    if (search_string.length <= 0) {
        $('.setting-icon-card').show();
        $('.setting-icon-title').hide();
        $('.list-view').removeClass("title-view").addClass("card-view");
        $.ajax({
            type: 'POST',
            url: GET_LATEST_ARTICLES_SLUG_URL,
            dataType: 'json',
            data: { "Username": username, "Password": pwd, "Page": pages, "ItemsPerPage": itemsPerPage },
            crossDomain: true,
            success: function (result) {
                if (result.Error == 250 || result.Error == 251) {
                    redirect404();
                }

                if (result.Error == 0) {
                    $('.list-view').html('');

                    for (var i = 0; i < result.Data.length; i++) {
                        var id = result.Data[i].ID;
                        var categ_str = html_text(result.Data[i].Categories);
                        var categories = categ_str.split(";")[0];
                        var thumbnail = result.Data[i].Thumbnail;
                        var title = result.Data[i].Title;
                        if (title == null || title == '') { title = title_default; }
                        var shortDescription = result.Data[i].ShortDescription;
                        if (shortDescription == null || shortDescription == '') { shortDescription = shortDescr_default; }

                        var categSlug = categ_str.split(";")[1];
                        var articleSlug = result.Data[i].Slug;

                        var link = '/' + categSlug + '/' + articleSlug;

                        var likes = result.Data[i].Likes;
                        var author = result.Data[i].Author;
                        if (author == null || author == '') { author = author_default; }

                        var view = '';
                        view += '<div class="view-item" id="' + (i + 1) + '">';
                        if ($(window).width() < 768)
                            view += '<div class="col-xs-2">';
                        else
                            view += '<div class="col-xs-4">';
                        view += '<img src="' + thumbnail + '" class="img-responsive zoom_popup" />';
                        view += '</div>';
                        if ($(window).width() < 768)
                            view += '<div class="col-xs-10">';
                        else
                            view += '<div class="col-xs-8">';
                        view += '<h3 class="interest-name">' + categories + '</h3>';
                        view += '<a href="' + link + '"><h1 class="title">' + title + '</h1></a>';
                        view += '<a href="' + link + '"><h2 class="title">' + title + '</h2></a>';
                        view += '<cite>' + author + '</cite>';
                        view += '<p style="margin: 0; padding-top: 20px;">' + shortDescription + '<a class="more" href="' + link + '">more&nbsp<i class="fa fa-angle-right"></i></a></p>';
                        view += '</div>';
                        view += '<div class="clear"></div>';
                        view += '</div>';

                        $('.list-view').append(view);
                    }

                    //console.log('END.');
                    $('.list-view').show();

                }
            },
            error: function () {
                $('.list-view').html('');
            }
        });
    }
    else {
        searchArticle(username, pwd, search_string)
    }
}

function getMoreListArticlesLasted(username, pwd, pages, itemsPerPage) {
    var search_string = $.cookie('search_string') || '';
    $.removeCookie('search_string', { path: '/' });

    if (search_string.length <= 0) {
        $.ajax({
            type: 'POST',
            url: GET_LATEST_ARTICLES_SLUG_URL,
            dataType: 'json',
            data: { "Username": username, "Password": pwd, "Page": pages, "ItemsPerPage": itemsPerPage },
            crossDomain: true,
            success: function (result) {
                    for (var i = 0; i < result.Data.length; i++) {
                        var id = result.Data[i].ID;
                        var categ_str = html_text(result.Data[i].Categories);
                        var categories = categ_str.split(";")[0];
                        var thumbnail = result.Data[i].Thumbnail;
                        var title = result.Data[i].Title;
                        if (title == null || title == '') { title = title_default; }
                        var shortDescription = result.Data[i].ShortDescription;
                        if (shortDescription == null || shortDescription == '') { shortDescription = shortDescr_default; }

                        var categSlug = categ_str.split(";")[1];
                        var articleSlug = result.Data[i].Slug;

                        var link = '/' + categSlug + '/' + articleSlug;

                        var likes = result.Data[i].Likes;
                        var author = result.Data[i].Author;
                        if (author == null || author == '') { author = author_default; }

                        var view = '';
                        view += '<div class="view-item" id="' + (i + 1) + '">';
                        var div_small = '<div class="col-xs-4">';
                        if ($('.setting-icon-card').css('display') == 'none') {
                            div_small = '<div class="col-xs-2">';
                        }
                        view += div_small;
                        view += '<img src="' + thumbnail + '" class="img-responsive zoom_popup" />';
                        view += '</div>';
                        var div_large = '<div class="col-xs-8">';
                        if ($('.setting-icon-card').css('display') == 'none') {
                            div_large = '<div class="col-xs-10">';
                        }
                        view += div_large;
                        view += '<h3 class="interest-name">' + categories + '</h3>';
                        view += '<a href="' + link + '"><h1 class="title">' + title + '</h1></a>';
                        view += '<a href="' + link + '"><h2 class="title">' + title + '</h2></a>';
                        view += '<cite>' + author + '</cite>';
                        view += '<p style="margin: 0; padding-top: 20px;">' + shortDescription + '<a class="more" href="' + link + '">more&nbsp<i class="fa fa-angle-right"></i></a></p>';
                        view += '</div>';
                        view += '<div class="clear"></div>';
                        view += '</div>';

                        $('.list-view').append(view);
                    }
            },
            error: function () {
            }
        });
    }
    else {
        searchMoreArticle(username, pwd, search_string)
    }
}

//get List Articles of Interest
function getListArticlesOfInterest(username, pwd, manageSlug, pages, itemsPerPage) {
    $('.setting-icon-card').show();
    $('.setting-icon-title').hide();
    $('.list-view').removeClass("title-view").addClass("card-view");
    $.ajax({
        type: 'POST',
        url: GET_ARTICLES_OF_INTEREST_SLUG_URL.replace("{0}", manageSlug),
        dataType: 'json',
        data: { "Username": username, "Password": pwd, "Page": pages, "ItemsPerPage": itemsPerPage },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 250 || result.Error == 251) {
                redirect404();
            }

            if (result.Error == 0) {
                if (result.Data == null) {
                    redirect404();
                }
                else {
                    if (result.Data.length <= 0) {
                        redirect404();
                    }
                }

                $('.list-view').html('');
                for (var i = 0; i < result.Data.length; i++) {
                    var id = result.Data[i].ID;
                    var categ_str = html_text(result.Data[i].Categories);
                    var categories = categ_str.split(";")[0];

                    var thumbnail = result.Data[i].Thumbnail;
                    var title = result.Data[i].Title;
                    if (title == null || title == '') { title = title_default; }
                    var shortDescription = result.Data[i].ShortDescription;
                    if (shortDescription == null || shortDescription == '') { shortDescription = shortDescr_default; }

                    var categSlug = categ_str.split(";")[1];
                    var articleSlug = result.Data[i].Slug;

                    var link = '/' + categSlug + '/' + articleSlug;

                    var likes = result.Data[i].Likes;
                    var author = result.Data[i].Author;
                    if (author == null || author == '') { author = author_default; }

                    var view = '';
                    view += '<div class="view-item" id="' + (i + 1) + '">';
                    view += '<div class="col-xs-4">';
                    view += '<img src="' + thumbnail + '" class="img-responsive zoom_popup" />';
                    view += '</div>';
                    view += '<div class="col-xs-8">';
                    view += '<h3 class="interest-name">' + categories + '</h3>';
                    view += '<a href="' + link + '"><h1 class="title">' + title + '</h1></a>';
                    view += '<a href="' + link + '"><h2 class="title">' + title + '</h2></a>';
                    view += '<cite>' + author + '</cite>';
                    view += '<p style="margin: 0; padding-top: 20px;">' + shortDescription + '<a class="more" href="' + link + '">more&nbsp<i class="fa fa-angle-right"></i></a></p>';
                    view += '</div>';
                    view += '<div class="clear"></div>';
                    view += '</div>';

                    $('.list-view').append(view);
                }

            }
        },
        error: function () {
            $('.list-view').html('');
        }
    });
}

function getMoreListArticlesOfInterest(username, pwd, manageSlug, pages, itemsPerPage) {
    $.ajax({
        type: 'POST',
        url: GET_ARTICLES_OF_INTEREST_SLUG_URL.replace("{0}", manageSlug),
        dataType: 'json',
        data: { "Username": username, "Password": pwd, "Page": pages, "ItemsPerPage": itemsPerPage },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                if (result.Data != null) {
                    if (result.Data.length > 0) {
                        for (var i = 0; i < result.Data.length; i++) {
                            var id = result.Data[i].ID;
                            var categ_str = html_text(result.Data[i].Categories);
                            var categories = categ_str.split(";")[0];

                            var thumbnail = result.Data[i].Thumbnail;
                            var title = result.Data[i].Title;
                            if (title == null || title == '') { title = title_default; }
                            var shortDescription = result.Data[i].ShortDescription;
                            if (shortDescription == null || shortDescription == '') { shortDescription = shortDescr_default; }

                            var categSlug = categ_str.split(";")[1];
                            var articleSlug = result.Data[i].Slug;

                            var link = '/' + categSlug + '/' + articleSlug;

                            var likes = result.Data[i].Likes;
                            var author = result.Data[i].Author;
                            if (author == null || author == '') { author = author_default; }

                            var view = '';
                            view += '<div class="view-item" id="' + ((pages - 1) * itemsPerPage + i + 1) + '">';
                            var div_small = '<div class="col-xs-4">';
                            if ($('.setting-icon-card').css('display') == 'none') {
                                div_small = '<div class="col-xs-2">';
                            }
                            view += div_small;
                            view += '<img src="' + thumbnail + '" class="img-responsive zoom_popup" />';
                            view += '</div>';
                            var div_large = '<div class="col-xs-8">';
                            if ($('.setting-icon-card').css('display') == 'none') {
                                div_large = '<div class="col-xs-10">';
                            }
                            view += div_large;
                            view += '<h3 class="interest-name">' + categories + '</h3>';
                            view += '<a href="' + link + '"><h1 class="title">' + title + '</h1></a>';
                            view += '<a href="' + link + '"><h2 class="title">' + title + '</h2></a>';
                            view += '<cite>' + author + '</cite>';
                            view += '<p style="margin: 0; padding-top: 20px;">' + shortDescription + '<a class="more" href="' + link + '">more&nbsp<i class="fa fa-angle-right"></i></a></p>';
                            view += '</div>';
                            view += '<div class="clear"></div>';
                            view += '</div>';

                            $('.list-view').append(view);
                        }
                    }
                }
            }
        },
        error: function () {
            $('.list-view').html('');
        }
    });
}

// get Detail of Article
function getDetailOfArticle(username, pwd, slug, category, indexInList) {
    $('#choose-interest').removeClass('in');
    $('#collapse_choose_interest').addClass('collapsed');
    $('.article-tools > .btn-tool').removeClass('selected');
    $('.article-tools .like-tool > .btn-tool').removeClass('selected');
    $('#Note, #Comment').val('');
    $.ajax({
        type: 'POST',
        url: GET_DETAIL_ARTICLE_SLUG_URL.replace("{0}", slug),
        dataType: 'json',
        data: { "Username": username, "Password": pwd },
        crossDomain: true,
        beforeSend: function () {
            // Handle the beforeSend event
            if (location.pathname.replace(/\//g, '').indexOf('favorite') == 0) {
                getFavListArticlesOfInterest(username, pwd, category, 1, 20, category);
            }
            else {
            //var maxpage = 1;
            //$('#level1 a').each(function () {
            //    if (location.pathname.replace(/\//g, '').indexOf($(this).attr('href').replace(/\//g, '')) == 0) {
            //        maxpage = Math.round(($(this).find('.count-number').text().match(/\d+/g)[0]) / 10 + 0.5);
            //        return false;
            //    }
            //});

            getListArticlesOfInterest(username, pwd, category, 1, 20);

            //for (pg = 2; pg < maxpage ; pg++) {
            //    //console.log(pg);
            //    getMoreListArticlesOfInterest(username, pwd, manageSlug, pg, 10);
            //}
            }

        },
        success: function (result) {
            if (result.Error == 250 || result.Error == 251) {
                redirect404();
            }

            $('.list-view, .change-view, #search-breadcrumb, #result_search').hide();
            $('.share-tools, .notes-list, .comments-list').hide();
            $('.next-previous, .article-tools').hide();

            if (result.Error == 0) {
                if (result.Data == null) {
                    redirect404();
                }
                else {
                    if (result.Data.length <= 0) {
                        redirect404();
                    }
                }



                var id = result.Data.ID;
                $('.article-tools .like-tool').attr('id', id);


                var li_item = '';
                li_item += '<li class="btn-address btn-setting" id="toHome">';
                var link_home = '/';
                li_item += '<a href="' + link_home + '">';
                li_item += '<i class="menu-icon menu-icon-house"></i>';
                li_item += '</a>';
                li_item += '</li>';
                $('#breadcrumb_link .breadcrumb').html(li_item);

                $('.next-previous').html('');

                var title = result.Data.Title;
                if (title == null || title == '') { title = title_default; }
                //console.log(genSlug(title, id));
                var author = result.Data.Author;
                if (author == null || author == '') { author = author_default; }
                var time = result.Data.PublishedDate,
                    start = new Date(time),
                    end = new Date(),
                    diff = new Date(end - start);
                var days = diff / 1000 / 60 / 60 / 24;
                var day_text = Math.round(days) + ' days ago';
                if (days < 1) {
                    day_text = 'Today';
                }
                if (days > 30) {
                    day_text = Math.round(days / 30) + ' months ago';
                }
                if (days > 365) {
                    day_text = Math.round(days / 30 / 12) + ' years ago';
                }
                if (result.Data.PublishedDate == null || result.Data.PublishedDate == '')
                { day_text = published_default; }
                var thumbnail = result.Data.Thumbnail;
                var body = result.Data.Body;
                if (body == null || body == '') { body = body_default; }

                $('.article-view').find(".title").text(title);
                $('.article-view').find("#authorvstime").text('By ' + author + ' - ' + day_text + '.');
                $('.article-view').find(".article-content").html(body);

                var imgs = body.match(/<img.*?src=[\"'](.+?)[\"'].*?\/>/g);
                if (imgs != null) {
                    console.log(imgs.length);
                    for (var m = 0; m < imgs.length; m++) {
                        var img = imgs[m].match(/src=[\"'](.+?)[\"'].*?/);
                        var img_tag = imgs[m].replace(img[0], img[0] + ' onclick="showZoomPopup(\'' + img[1] + '\')"');
                        //console.log($('.article-view').find('.article-content').find('img:eq(' + m + ')').parent().html());
                        //console.log(img_tag);
                        $('.article-view').find('.article-content').find('img:eq(' + m + ')').parent().html(img_tag);
                    }
                }

                $('.list-view .view-item').each(function (index) {
                    if ($(this).find('a:first-child').attr('href') == location.pathname) {
                        indexInList = index;
                    }
                });
                //$.cookie("indexInList", indexInList, { path: '/' });
                //$.cookie("category", category, { path: '/' });
                var categName = $('.list-view > #' + (indexInList + 1) + '.view-item').find('.interest-name').text() || category;
                if (location.pathname.replace(/\//g, '').indexOf('favorite') == -1) {
                    if (categName == category) {
                        //redirect404();
                    }
                }

                findParentName(categName, username, pwd);
                $('#breadcrumb_link .breadcrumb').append('<li class="btn-address active" title="' + categName + '">' + categName.substr(0, 17) + '...</li>');
                $('#breadcrumb_link .breadcrumb > .btn-address.active').show();

                findAvatar(username, pwd);
                findLikes(username, pwd, id);
                getNotes(username, pwd, id, indexInList);
                getComments(username, pwd, id, indexInList);

                $('#send-note')
                    .attr('onclick', 'postNote("' + username + '", "' + pwd + '", "' + id + '", "' + (indexInList + 1) + '")');
                $('#send-comment')
                    .attr('onclick', 'postComment("' + username + '", "' + pwd + '", "' + id + '", "' + (indexInList + 1) + '")');

                removeMetaDataTag();
                //facebook meta
                $('head').append('<meta name="fbimage" property="og:image" content="' + thumbnail + '"/>');
                $('head').append('<meta name="fbtitle" property="og:title" content="' + title + '"/>');
                //$('head').append('<meta name="fburl" property="og:url" content="' + url + '"/>');
                $('head').append('<meta name="fbtype" property="og:type" content="journal"/>');

                var social = new Array();
                social[0] = 'Facebook';
                social[1] = 'Google Plus';
                social[2] = 'Pinterest';
                social[3] = 'Twitter';
                social[4] = 'Tumblr';
                $('.share-social > .facebook')
                    .attr('onclick', 'shareArticle("' + username + '", "' + pwd + '", "' + id + '", "' + social[0] + '")');
                $('.share-social > .google-plus')
                    .attr('onclick', 'shareArticle("' + username + '", "' + pwd + '", "' + id + '", "' + social[1] + '")');
                $('.share-social > .pinterest')
                    .attr('onclick', 'shareArticle("' + username + '", "' + pwd + '", "' + id + '", "' + social[2] + '")');
                $('.share-social > .twitter')
                    .attr('onclick', 'shareArticle("' + username + '", "' + pwd + '", "' + id + '", "' + social[3] + '")');
                $('.share-social > .tumblr')
                    .attr('onclick', 'shareArticle("' + username + '", "' + pwd + '", "' + id + '", "' + social[4] + '")');

                $('.nav.setting > .dropdown').show();
                $('.nav.setting > .collapse-menu').show();
                $('.article-view, #breadcrumb_link').show();
                $('.next-previous, .article-tools').show();
            }
        },
        error: function () {
            $('.list-view').html('');
        }
    });
}

function removeMetaDataTag() {
    //remove facebook meta
    $('meta[name=fbimage]').remove();
    $('meta[name=fbtitle]').remove();
    $('meta[name=fburl]').remove();
    $('meta[name=fbtype]').remove();
}

function showZoomPopup(thumbnail) {
    $('#zoom_img').find("img").attr("src", thumbnail);
    $('#zoom_img').modal('toggle');
    $('#zoom_img').find(".modal-dialog").removeAttr("style");
}

function toHome() {
    $('.list-view, .change-view').show();
    $('.article-view, #breadcrumb_link').hide();
}

function clickArticle(index) {
    var link = $('.list-view > #' + index + '.view-item a:first-child').attr('href') || '#';
    location.replace(link)
}

function findParentName(category, username, pwd) {
    var path = '';
    $.ajax({
        type: 'POST',
        url: GET_STRUCTURE_INTERESTS_URL,
        dataType: 'json',
        data: { "Username": username, "Password": pwd },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                for (var i = 0; i < result.Data.length; i++) {
                    if (result.Data[i].Name == category) {
                        for (var lvl = 0; lvl < i; lvl++) {
                            if (result.Data[lvl].Level != null
                                && result.Data[lvl].Level > 0
                                && result.Data[lvl].Level == result.Data[i].Level - 1
                                && result.Data[lvl].Id == result.Data[i].ParentId) {
                                if (result.Data[lvl].Name != '') {
                                    var id = result.Data[lvl].Id;
                                    var total = result.Data[lvl].Total;
                                    var name = result.Data[lvl].Name;

                                    var categSlug = result.Data[lvl].Slug;

                                    var link = '/' + categSlug;

                                    var link_a = '<a href="' + link + '">'
                                        + name.substr(0, 17) + '...</a>';
                                    path += '<li class="btn-address"' + ' title="' + name + '"' + '>'
                                        + link_a + '</li>';
                                    var had = 0;
                                    $('#breadcrumb_link .breadcrumb > li').each(function () {
                                        if ($(this).html() == link_a) {
                                            had++;
                                        }
                                    });
                                    if (had == 0) {
                                        $('#breadcrumb_link .breadcrumb #toHome').after(path);
                                    }
                                    path += findParentName(name, username, pwd)
                                }
                                break;
                            }
                        }
                        break;
                    }
                }
                $('#breadcrumb_link .breadcrumb > .btn-address.active').show();
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function findAvatar(username, pwd) {
    $.ajax({
        type: 'POST',
        url: GET_PROFILE_URL,
        dataType: 'json',
        data: { "Username": username, "Password": pwd },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                var avatar = result.Data.Avatar;
                if (avatar == null || avatar == '') { avatar = avatar_default; }
                $('.write-text').find('img').attr({ 'src': avatar, 'alt': result.Data.Fullname });
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function findLikes(username, pwd, id) {
    $.ajax({
        type: 'POST',
        url: GET_LIKES_OF_ARTICLE_URL.replace("{0}", id),
        dataType: 'json',
        data: { "Username": username, "Password": pwd, "Page": 1, "ItemsPerPage": 99999 },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                var likes = result.Data.length;
                var currLike = 0;

                var wholiked = '';
                for (var i = 0; i < likes; i++) {
                    if (result.Data[i].Action == 1) {
                        var fullname = result.Data[i].Fullname;
                        if (fullname == null || fullname == '') { fullname = fullname_default; }
                        var name = result.Data[i].Username;
                        if (name == null || name == '') { name = name_default; }
                        wholiked += fullname != null ? fullname : name.substr(0, name.indexOf('@'));
                        wholiked += '; ';
                        currLike++;
                    }
                }

                $('.like-tool#' + id + ' > .like-number').text(currLike);
                //$('#' + id + ' > .like-number').attr('title', wholiked.substr(0, wholiked.lastIndexOf(';')));

                $('#' + id + ' > .btn-tool').attr('onclick', 'clickLike("' + username + '", "' + pwd + '", "' + id + '")');
                for (var i = 0; i < likes; i++) {
                    var name = result.Data[i].Username;
                    if (name == null || name == '') { name = name_default; }

                    if (username == name && result.Data[i].Action == 1) {
                        //console.log(username + '===' + name);
                        $('#' + id + ' > .btn-tool').addClass('selected');
                        $('#' + id + ' > .btn-tool').attr('onclick', 'clickUnLike("' + username + '", "' + pwd + '", "' + id + '")');
                        return;
                    }
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function clickLike(username, pwd, id) {
    $.ajax({
        type: 'POST',
        url: LIKE_POST_URL.replace("{0}", id),
        dataType: 'json',
        data: { "Username": username, "Password": pwd },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                var likes = result.Data.Count;
                $('#' + id + ' > .like-number').text(likes);
                if ($('.feature-item').hasClass(id)) {
                    $('.feature-item.' + id + ' > p')
                        .html($('.feature-item.' + id + ' > p').html().replace(/\d likes/, likes + ' likes'));
                }
                $('#' + id + ' > .btn-tool').addClass('selected');
                $('#' + id + ' > .btn-tool').attr('onclick', 'clickUnLike("' + username + '", "' + pwd + '", "' + id + '")');
            }
        },
        error: function () {
            //alert('error');
        }
    });

}

function clickUnLike(username, pwd, id) {
    $.ajax({
        type: 'POST',
        url: UNLIKE_POST_URL.replace("{0}", id),
        dataType: 'json',
        data: { "Username": username, "Password": pwd },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                var likes = result.Data.Count;
                $('#' + id + ' > .like-number').text(likes);
                if ($('.feature-item').hasClass(id)) {
                    $('.feature-item.' + id + ' > p')
                        .html($('.feature-item.' + id + ' > p').html().replace(/\d likes/, likes + ' likes'));
                }
                $('#' + id + ' > .btn-tool').removeClass('selected');
                $('#' + id + ' > .btn-tool').attr('onclick', 'clickLike("' + username + '", "' + pwd + '", "' + id + '")');
            }
        },
        error: function () {
            //alert('error');
        }
    });

}

function getNotes(username, pwd, id, indexInList) {
    $.ajax({
        type: 'POST',
        url: GET_NOTES_OF_ARTICLE_URL.replace("{0}", id),
        dataType: 'json',
        data: { "Username": username, "Password": pwd, "Page": 1, "ItemsPerPage": 99999 },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                $('.notes-list > #noteofuser').html('');
                for (var i = 0; i < result.Data.length; i++) {
                    var month = new Array();
                    month[0] = "January";
                    month[1] = "February";
                    month[2] = "March";
                    month[3] = "April";
                    month[4] = "May";
                    month[5] = "June";
                    month[6] = "July";
                    month[7] = "August";
                    month[8] = "September";
                    month[9] = "October";
                    month[10] = "November";
                    month[11] = "December";

                    var date = new Date(result.Data[i].DateCreated);
                    date = date.getDate() + ' ' + month[date.getMonth()].toLocaleUpperCase() + ', ' + date.getFullYear();
                    var msg = result.Data[i].Message;
                    var note_item = '';
                    note_item += '<div class="user-note">';
                    var clickclose = 'deleteNote(\'' + username + '\', \'' + pwd + '\', \'' + result.Data[i].ID + '\', \'' + (indexInList + 1) + '\')';
                    note_item += '<span class="btn-close" onclick="' + clickclose + '">';
                    note_item += '<i class="fa fa-close fa-2x"></i>';
                    note_item += '</span>';
                    note_item += '<p class="note-text"><span class="note-time">' + date + '</span>' + msg + '</p>';
                    note_item += '</div>';
                    $('.notes-list > #noteofuser').append(note_item);
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function postNote(username, pwd, id, indexInList) {
    var message = $('#Note').val();
    //console.log(message);
    if (message != '' && message != null) {
        $.ajax({
            type: 'POST',
            url: POST_NOTE_FOR_ARTICLE_URL.replace("{0}", id),
            dataType: 'json',
            data: { "Username": username, "Password": pwd, "Message": message },
            crossDomain: true,
            success: function (result) {
                if (result.Error == 0) {
                    if (indexInList < 0) {
                        location.reload();
                        return;
                    }
                    clickArticle(indexInList);
                }
            },
            error: function () {
                //alert('error');
            }
        });
    }
}

function deleteNote(username, pwd, id, indexInList) {
    $.ajax({
        type: 'POST',
        url: DELETE_NOTE_URL.replace("{0}", id),
        dataType: 'json',
        data: { "Username": username, "Password": pwd },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                if (indexInList < 0) {
                    location.reload();
                    return;
                }
                clickArticle(indexInList);
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function getComments(username, pwd, id, indexInList) {
    $.ajax({
        type: 'POST',
        url: GET_COMMENTS_OF_ARTICLE_URL.replace("{0}", id),
        dataType: 'json',
        data: { "Username": username, "Password": pwd, "Page": 1, "ItemsPerPage": 99999 },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                $('.comments-list .media-list').html('');
                for (var i = 0; i < result.Data.length; i++) {
                    var idx = result.Data[i].ID;
                    var avatar = result.Data[i].Avatar;
                    if (avatar == null || avatar == '') { avatar = avatar_default; }
                    var fullname = result.Data[i].Fullname;
                    if (fullname == null || fullname == '') { fullname = fullname_default; }
                    var name = result.Data[i].Username;
                    if (name == null || name == '') { name = name_default; }
                    var msg = result.Data[i].Message;
                    var likes = result.Data[i].Likes;
                    var liked = result.Data[i].Liked;


                    var comment = '';
                    comment += '<li class="media" >';
                    comment += '<a href="#" class="pull-left col-xs-2">';
                    comment += '<img src="' + (avatar != null ? avatar : '') + '" class="img-responsive" alt="' + name.substr(0, name.indexOf('@')) + '" />';
                    comment += '</a>';
                    comment += '<div class="media-body col-xs-10">';
                    comment += '<h3 class="media-heading">' + fullname + '</h3>';
                    comment += '<p class="comment-text">' + msg + '</p>';
                    comment += '<span class="reply-comment">Reply</span>';
                    comment += '<span class="like-tool" id="' + idx + '">';
                    comment += '<span class="like-number">' + likes + '</span>';
                    var selected = '';
                    if (liked == true)
                        selected = 'selected';
                    comment += '<span class="btn-tool ' + selected + '">';
                    comment += '<i class="tool-icon tool-icon-like"></i></span>';
                    comment += '</span>';
                    comment += '</div>';
                    comment += '<div class="clear"></div>';

                    $('.comments-list .media-list').append(comment);

                    getReplies(username, pwd, idx);

                    if (liked == true)
                        $('#' + idx + ' > .btn-tool').attr('onclick', 'clickUnLike("' + username + '", "' + pwd + '", "' + idx + '")');
                    else
                        $('#' + idx + ' > .btn-tool').attr('onclick', 'clickLike("' + username + '", "' + pwd + '", "' + idx + '")');

                    $('#' + idx).parent().find('.reply-comment').attr('onclick', 'writeReply("' + username + '", "' + pwd + '", "' + idx + '", "' + (indexInList + 1) + '")');

                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function postComment(username, pwd, id, indexInList) {
    var message = $('#Comment').val();
    //console.log(message);
    if (message != '' && message != null) {
        $.ajax({
            type: 'POST',
            url: POST_COMMENT_URL.replace("{0}", id),
            dataType: 'json',
            data: { "Username": username, "Password": pwd, "Message": message },
            crossDomain: true,
            success: function (result) {
                if (result.Error == 0) {
                    if ($('.feature-item.' + id + ' > p').length) {
                        var match = $('.feature-item.' + id + ' > p').html().match(/(\d) comments/);
                        var comments = parseInt(match[1]) + 1 || match[1];
                        $('.feature-item.' + id + ' > p').html($('.feature-item.' + id + ' > p').html().replace(/\d comments/, comments + ' comments'));
                    }
                    if (indexInList < 0) {
                        location.reload();
                        return;
                    }
                    clickArticle(indexInList);
                }
            },
            error: function () {
                //alert('error');
            }
        });
    }
}

function writeReply(username, pwd, id, indexInList) {
    $('#Comment').focus();
    $('#send-comment').attr('onclick', 'replyComment("' + username + '", "' + pwd + '", "' + id + '", "' + indexInList + '")');
}

function replyComment(username, pwd, id, indexInList) {
    var message = $('#Comment').val();
    //console.log(message);
    $.ajax({
        type: 'POST',
        url: POST_REPLY_URL.replace("{0}", id),
        dataType: 'json',
        data: { "Username": username, "Password": pwd, "Message": message },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                if (indexInList < 0) {
                    location.reload();
                    return;
                }
                clickArticle(indexInList);
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function getReplies(username, pwd, id) {
    $.ajax({
        type: 'POST',
        url: GET_REPLIES_OF_COMMENT_URL.replace("{0}", id),
        dataType: 'json',
        data: { "Username": username, "Password": pwd, "Page": 1, "ItemsPerPage": 99999 },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                for (var i = 0; i < result.Data.length; i++) {
                    var idx = result.Data[i].ID;
                    var avatar = result.Data[i].Avatar;
                    if (avatar == null || avatar == '') { avatar = avatar_default; }
                    var fullname = result.Data[i].Fullname;
                    if (fullname == null || fullname == '') { fullname = fullname_default; }
                    var msg = result.Data[i].Message;
                    var likes = result.Data[i].Likes;
                    var liked = result.Data[i].Liked;
                    var selected = '';

                    var reply = '';
                    reply += '<div class="media" >';
                    reply += '<a href="#" class="pull-left col-xs-2 col-xs-offset-0">';
                    reply += '<img src="' + avatar + '" class="img-responsive" />';
                    reply += '</a>';
                    reply += '<div class="media-body col-xs-10">';
                    reply += '<h4 class="media-heading">' + fullname + '</h4>';
                    reply += '<p class="comment-text">' + msg + '</p>';
                    reply += '<span class="like-tool" id="' + idx + '">';
                    reply += '<span class="like-number">' + likes + '</span>';
                    if (liked == true)
                        selected = 'selected';
                    reply += '<span class="btn-tool ' + selected + '">';
                    reply += '<i class="tool-icon tool-icon-like"></i></span>';
                    reply += '</span>';
                    reply += '</div>';
                    reply += '<div class="clear"></div>';
                    reply += '</div>';

                    $('#' + id).parent().parent().append(reply);

                    getReplies(username, pwd, idx);

                    if (liked == true)
                        $('#' + idx + ' > .btn-tool').attr('onclick', 'clickUnLike("' + username + '", "' + pwd + '", "' + idx + '")');
                    else
                        $('#' + idx + ' > .btn-tool').attr('onclick', 'clickLike("' + username + '", "' + pwd + '", "' + idx + '")');

                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function shareArticle(username, pwd, id, social) {
    //alert('Shared on ' + social);
    //console.log(social);
    //$.ajax({
    //    type: 'POST',
    //    url: SHARE_POST_URL.replace("{0}", id),
    //    dataType: 'json',
    //    data: { "Username": username, "Password": pwd, "Message": message },
    //    crossDomain: true,
    //    success: function (result) {
    //        if (result.Error == 0) {
    //            alert('Shared on ' + social);
    //        }
    //    },
    //    error: function () {
    //        //alert('error');
    //    }
    //});
}

//Sponsor
function getSponsors(username, pwd) {
    var pages = 1;
    var itemsPerPage = 99;

    //console.log('BEGIN');
    $('.sponsor-view.list-view').hide();

    $.ajax({
        type: 'POST',
        url: GET_SPONSORS_URL,
        dataType: 'json',
        data: { "Username": username, "Password": pwd, "Page": pages, "ItemsPerPage": itemsPerPage },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 250 || result.Error == 251) {
                redirect404();
            }

            if (result.Error == 0) {
                $('.sponsor-view.list-view').html('');
                for (var i = 0; i < result.Data.length; i++) {
                    var id = result.Data[i].ID;
                    var logo = result.Data[i].Logo;
                    var name = result.Data[i].Name;
                    if (name == null || name == '') { name = name_default; }
                    var shortDescription = result.Data[i].ShortDescription;
                    if (shortDescription == null || shortDescription == '') { shortDescription = shortDescr_default; }
                    var web = result.Data[i].WebAddress;
                    if (web == null || web == '') { web = web_default; }

                    var slug = result.Data[i].Slug;

                    var link = '/sponsor/' + slug;

                    var view = '';
                    view += '<div class="view-item">';
                    view += '<div class="col-xs-4">';
                    view += '<img src="' + logo + '" class="img-responsive" />';
                    view += '</div>';
                    view += '<div class="col-xs-8">';
                    view += '<a href="' + link + '"><h2 class="title" >' + name + '</h2></a>';
                    view += '<p style="margin: 0; padding-top: 0;">' + shortDescription + '</p>';
                    view += '<h3 class="webaddr"><a href="' + web + '">' + web + '</a></h3>';
                    view += '</div>';
                    view += '<div class="clear"></div>';
                    view += '</div>';

                    $('.sponsor-view.list-view').append(view);
                }

                //console.log('END.');
                $('.sponsor-view.list-view').show();

            }
        },
        error: function () {
            $('.sponsor-view.list-view').html('');
        }
    });
}

function getSponsorDetail(username, pwd, slug) {

    //console.log('BEGIN');
    $('#sponsor-nav').hide();
    $('.sponsor-view.sponsor-detail').hide();

    $.ajax({
        type: 'POST',
        url: GET_SPONSOR_DETAIL_SLUG_URL.replace("{0}", slug),
        dataType: 'json',
        data: { "Username": username, "Password": pwd },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 250 || result.Error == 251) {
                redirect404();
            }

            if (result.Error == 0) {
                if (result.Data == null) {
                    redirect404();
                }
                else {
                    if (result.Data.length <= 0) {
                        redirect404();
                    }
                }

                $('.sponsor-view.list-view').hide();

                $('.sponsor-view.sponsor-detail').html('');
                var name = result.Data.Name;
                if (name == null || name == '') { name = name_default; }
                var web = result.Data.WebAddress;
                if (web == null || web == '') { web = web_default; }
                var body = result.Data.BodyContent;
                if (body == null || body == '') { body = body_default; }

                $('#sponsor-nav').find('.btn-address.active').text(name);
                $('#sponsor-nav').find('.sponsor-more-info > a').attr('href', web);

                $('.sponsor-view.sponsor-detail').html(body);
            }

            //console.log('END.');
            $('#sponsor-nav').show();
            $('.sponsor-view.sponsor-detail').show();

        },
        error: function () {
            $('.sponsor-view.sponsor-detail').html('');
        }
    });
}

//Profile
function getProfile(username, pwd) {

    //console.log('BEGIN');

    $.ajax({
        type: 'POST',
        url: GET_PROFILE_URL,
        dataType: 'json',
        data: { "Username": username, "Password": pwd },
        crossDomain: true,
        success: function (result) {

            if (result.Error == 0) {
                var avatar = result.Data.Avatar;
                if (avatar == null || avatar == '') { avatar = avatar_default; }
                var gender = result.Data.Gender;
                var dob = result.Data.BirthDay;
                var caption = result.Data.Caption;
                if (caption == null || caption == '') { caption = caption_default; }
                var fullname = result.Data.Fullname;
                if (fullname == null || fullname == '') { fullname = fullname_default; }

                $('img.profile-avatar').attr('src', avatar);
                $('.profile-name').text(fullname);
                $('.profile-addr').text(caption);
                $('.profile-name-input').attr('value', fullname);
                $('.profile-addr-input').attr('value', caption);

                var date = new Date(dob);
                //$('#Birthday').attr('value',);
                date = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
                $('#dob').text(date);

                var sex = new Array();
                sex[0] = "N/A";
                sex[1] = "Male";
                sex[2] = "Female";
                $('#gender').text(sex[gender].toLocaleUpperCase());

                //$('#upavatar').attr('onclick', 'updateAvatar("' + username + '", "' + pwd + '")');

                $('#logout').attr('onclick', 'signOutProfile("' + username + '", "' + pwd + '")');

                $('#log-in, #sign-up').hide();
                var hght = Math.max($('#avatar_profile').outerHeight(true), $('#ajaxUpdate > .profile-avatar').outerHeight(true), $('#name_addr').outerHeight(true));
                var padtop = hght - $('#name_addr').outerHeight(true);
                $('#name_addr').height(hght).css('padding-top', padtop);

                //console.log('END.');
                $('.profile-view').show();

            }
            else {
                $('.profile-view').hide();
                //console.log('END.');
                $('#log-in').show();
            }
        },
        error: function () {
            //alert('error');
        }
    });

}

function updateProfile(username, pwd, email, fullname, caption, gender, birthday) {
    //console.log({ "Username": username, "Password": pwd, "Email": email, "FullName": fullname, "Caption": caption, "Gender": gender, "Birthday": birthday });
    //$.ajax({
    //    type: 'POST',
    //    url: UPDATE_PROFILE_URL,
    //    dataType: 'json',
    //    data: { "Username": username, "Password": pwd, "Email": email, "FullName": fullname, "Caption": caption, "Gender": gender, "Birthday": birthday },
    //    crossDomain: true,
    //    success: function (result) {
    //        if (result.Error == 0) {
    //            getProfile(username, pwd)
    //        }
    //    },
    //    error: function () {
    //        alert('error');
    //    }
    //});
}

function updateAvatar(username, pwd, data, filename, filetype) {
    var timestmp = Date.now;
    if (!timestmp) {
        timestmp = function () { return new Date().getTime(); };
    }
    var name = '';
    name = username.substr(0, username.indexOf('@')) + '_' + timestmp();
    filename = name + filename.substr(filename.lastIndexOf('.'), filename.length);
    data = data.replace(/^data:image\/(png|jpg);base64,/, '')
}

function signUpProfile() {
    var fullname = $('#name-signup').val();
    var pwd = $('#pwd-signup').val();
    var username = $('#email-signup').val();
    var birthday = $('#date-signup').val();
    var gender = 0;
    var type = "Normal";
    var id = createGuid();
    //console.log({ "Username": username, "Password": pwd, "ConfirmPassword": pwd, "FullName": fullname, "Birthday": birthday, "Gender": gender, "AccountType": type, "AccountID": id });
    $.ajax({
        type: 'POST',
        url: SIGN_UP_URL,
        dataType: 'json',
        data: { "Username": username, "Password": pwd, "ConfirmPassword": pwd, "FullName": fullname, "Birthday": birthday, "Gender": gender, "AccountType": type, "AccountID": id },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                $('#sign-up').hide();
                $('#name-signup').val('');
                $('#pwd-signup').val('');
                $('#email-signup').val('');
                $('#date-signup').val('');

                logInProfile(username, pwd);
            }
            else {
                $('#sign-up > .error_msg').text('* ' + result.ErrorMessage);
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function logInProfile(usr, password) {
    var pwd = $('#pwd-login').val() || password || '';
    var username = $('#email-login').val() || usr;
    //console.log({ "Username": username, "Password": pwd });
    $.ajax({
        type: 'POST',
        url: SIGN_IN_URL,
        dataType: 'json',
        data: { "Username": username, "Password": $.md5(pwd) },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                $('#pwd-login').val('');
                $('#email-login').val('');
                $('#log-in').hide();

                $.cookie('username', username, { expires: 7, path: '/' });
                $.cookie('password', $.md5(pwd), { expires: 7, path: '/' });

                //getFeatured(username, $.md5(pwd));
                //getProfile(username, $.md5(pwd));
                //$('.profile-view').show();

                var url = $('#Logo_Bento').attr('href');
                location.replace(url);
            }
            else {
                $('#log-in > .error_msg').text('* ' + result.ErrorMessage);
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function showPopupPwd(user_name, user_email, user_gender, social) {
    $.ajax({
        type: 'POST',
        url: SIGN_IN_URL,
        dataType: 'json',
        data: { "Username": user_email, "Password": $.md5('***') },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
            }
            else {
                if (result.ErrorMessage == "Username not exist") {
                    //register
                    $('.popup-pwd .content').children('input#fullname').val(user_name);
                    $('.popup-pwd .content').children('input#username').val(user_email);
                    $('.popup-pwd .content').append('<input id="social" name="social" class="text-box" value="' + social + '" type="hidden" />');
                    $('.popup-pwd .content').append('<input id="gender" name="gender" class="text-box" value="' + user_gender + '" type="hidden" />');
                    $('.trans-background').show();
                    $('.popup-pwd').fadeIn();
                }
                else {
                    $('#log-in #email-login').val(user_email);
                }
            }
        },
        error: function () {
        }
    });
}

function signOutProfile(usr, password) {
    var pwd = password;
    var username = usr;
    //console.log({ "Username": username, "Password": pwd });
    $.ajax({
        type: 'POST',
        url: SIGN_OUT_URL,
        dataType: 'json',
        data: { "Username": username, "Password": pwd },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                $('#pwd-login').val('');
                $('#email-login').val('');
                $('#log-in').show();
                $('.profile-view').hide();

                $.removeCookie('username', { path: '/' });
                $.removeCookie('password', { path: '/' });
                $('#facebooklogout').click();
                googlelogout();
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function update_password() {
    var fullname = $('.popup-pwd .content').children('input#fullname').val();
    var username = $('.popup-pwd .content').children('input#username').val();
    var pwd = $('.popup-pwd .content').children('input#password').val();
    var confirmpwd = $('.popup-pwd .content').children('input#confirmpassword').val();
    var birthday = '01-01-2000';
    var gender = $('.popup-pwd .content').children('input#gender').val();
    var type = $('.popup-pwd .content').children('input#social').val();
    var id = createGuid();
    //console.log({ "Username": username, "Password": pwd, "ConfirmPassword": pwd, "FullName": fullname, "Birthday": birthday, "Gender": gender, "AccountType": type, "AccountID": id });
    $.ajax({
        type: 'POST',
        url: SIGN_UP_URL,
        dataType: 'json',
        data: { "Username": username, "Password": pwd, "ConfirmPassword": confirmpwd, "FullName": fullname, "Birthday": birthday, "Gender": gender, "AccountType": type, "AccountID": id },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                $('.popup-pwd').fadeOut();
                $('.trans-background').hide();
                logInProfile(username, pwd);
            }
            else {
                $('.popup-pwd .content').children('span.dummy-error').remove();
                $('.popup-pwd .content').append('<span class="dummy-error" style="color:red;">' + '* ' + result.ErrorMessage + '</span>');
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

//Manage
function getFavoriteInterestStructure(username, pwd) {
    $.ajax({
        type: 'POST',
        url: GET_STRUCTURE_FAVORITE_INTERESTS_URL,
        dataType: 'json',
        data: { "Username": username, "Password": pwd },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 250 || result.Error == 251) {
                redirect404();
            }

            if (result.Error == 0) {
                $('.article-view').hide();
                $('.level.multi-level').html('');
                //console.log(result.Data.length);
                for (var i = 0; i < result.Data.length; i++) {
                    //console.log(i + '------' + result.Data[i].Level);
                    var id = result.Data[i].Id;
                    var total = result.Data[i].Total;
                    var name = result.Data[i].Name;

                    var categSlug = result.Data[i].Slug;

                    var link = '/favorite/' + categSlug;

                    var itemLvl = '';
                    var itemSubLvl = '';
                    var cont = { "Items": itemSubLvl, "Index": i };

                    //console.log(i);
                    if (result.Data[i].Level == 1 && result.Data[i].ParentId == null) {
                        var next = Math.max(i + 1, result.Data.length - 1);
                        itemLvl += '<li class="" id="' + result.Data[i].Id + '" >';

                        //console.log(i);
                        if (next > i && next < result.Data.length) {
                            cont = buildSubManageInterest(result, i, page);
                            //console.log(cont);
                        }

                        if (cont.Items != null && cont.Items != '') {
                            itemLvl += '<a href="'
                                + link + '">' + name;
                        }
                        else {
                            itemLvl += '<a href="'
                                + link + '">' + name;
                        }
                        itemLvl += '</a>';
                        itemLvl += '<span class="count-number out"'
                                + ' onclick="openLevel(\'' + id + '\')"'
                                + '>' + total + '&nbsp<i class="fa fa-angle-right"></i></span>';
                        itemLvl += cont.Items;

                        itemLvl += '</li>';

                        $('.level.multi-level').append(itemLvl);
                    }

                    i = cont.Index;
                }

            }
        },
        error: function () {
            //alert('error');
            $('.level1.multi-level').html('');
        }
    });

}

function buildSubManageInterest(result, curr, page) {
    var idxnextlvl = curr + 1;
    var itemLvl = '';
    var index = curr;
    if (idxnextlvl > curr && idxnextlvl < result.Data.length && result.Data[idxnextlvl].Level > result.Data[curr].Level) {
        //console.log('curr = ' + curr + '--- next = ' + idxnextlvl);
        var lvl = result.Data[idxnextlvl].Level;
        itemLvl += '<ul class="sub-level" >';
        var itemSubLvl = '';

        for (var i = idxnextlvl; i < result.Data.length; i++) {
            //console.log(i + '------' + result.Data[i].Level);
            var cont = { "Items": itemSubLvl, "Index": i };
            var id = result.Data[i].Id;
            var total = result.Data[i].Total;
            var name = result.Data[i].Name;

            var categSlug = result.Data[i].Slug;

            var link = '/favorite/' + categSlug;

            if (result.Data[i].Level == result.Data[curr].Level + 1
                && result.Data[i].ParentId == result.Data[curr].Id) {
                var next = Math.max(i + 1, result.Data.length - 1);

                itemSubLvl += '<li class="" id="' + result.Data[i].Id + '" >';
                //console.log(i);
                if (next > i) {// && next < result.Data.length) {
                    cont = buildSubManageInterest(result, i, page);
                    //console.log(cont);
                }

                if (cont.Items != null && cont.Items != '') {
                    itemSubLvl += '<a href="'
                                + link + '">' + name;
                }
                else {
                    itemSubLvl += '<a href="'
                                + link + '">' + name;
                }

                itemSubLvl += '</a>';
                itemSubLvl += '<span class="count-number out"'
                        + ' onclick="openLevel(\'' + id + '\')"'
                        + '>' + total + '&nbsp<i class="fa fa-angle-right"></i></span>';
                itemLvl += cont.Items;


                itemSubLvl += '</li>';
            }

            i = cont.Index;
        }

        itemLvl += itemSubLvl;
        itemLvl += '</ul>';
    }

    return { "Items": itemLvl, "Index": index };
}

function getFavListArticlesOfInterest(username, pwd, manageSlug, pages, itemsPerPage, name) {
    $.ajax({
        type: 'POST',
        url: GET_USER_FAVORITES_OF_INTEREST_SLUG_URL.replace("{0}", manageSlug),
        dataType: 'json',
        data: { "Username": username, "Password": pwd, "Page": pages, "ItemsPerPage": itemsPerPage },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 250 || result.Error == 251) {
                redirect404();
            }

            if (result.Error == 0) {
                $('.manage-view.list-view').show();
                $('.manage-view > .multi-level').parent().hide();
                $('#main-manage').removeClass('active');

                var categName = name;
                $.cookie("categName", categName, { path: '/' });
                //$('.manage-view > .multi-level a').each(function (index) {
                //    if ($(this).attr('href') == location.pathname) {
                //        categName = $(this).text();
                //    }
                //});
                //$('#manage-breadcrumb')
                //    .append('<li class="btn-address active" title="' + categName + '">' + categName.substr(0, 17) + '...</li>');

                $('.list-view').html('');
                for (var i = 0; i < result.Data.length; i++) {
                    var id = result.Data[i].ID;
                    var categ_str = html_text(result.Data[i].Categories);
                    var categories = categ_str.split(";")[0];

                    var thumbnail = result.Data[i].Thumbnail;
                    var title = result.Data[i].Title;
                    var shortDescription = result.Data[i].ShortDescription;
                    if (shortDescription == null || shortDescription == '') { shortDescription = shortDescr_default; }

                    var categSlug = categ_str.split(";")[1];
                    var articleSlug = result.Data[i].Slug;

                    var link = '/favorite/' + categSlug + '/' + articleSlug;

                    var likes = result.Data[i].Likes;
                    var author = result.Data[i].Author;
                    if (author == null || author == '') { author = author_default; }
                    var liked = result.Data[i].Liked;
                    var selected = '';

                    var view = '';
                    view += '<div class="view-item" id="' + (i + 1) + '">';
                    view += '<div class="col-xs-4">';
                    view += '<img src="' + thumbnail + '" class="img-responsive zoom_popup" />';
                    view += '</div>';
                    view += '<div class="col-xs-8">';
                    view += '<a href="' + link + '"><h2 class="title">' + title + '</h2></a>';
                    view += '<p style="margin: 0; padding-top: 0;">' + shortDescription + '</p>';
                    view += '<div class="author">';
                    view += '<h3>' + author + '</h3>';
                    view += '<span class="like-tool">';
                    view += '<span class="like-number">' + likes + '</span>';
                    if (liked == true)
                        selected = 'selected';
                    view += '<span class="btn-tool ' + selected + '">';
                    view += '<i class="tool-icon tool-icon-like"></i></span>';
                    view += '</span>';
                    view += '</div>';
                    view += '</div>';
                    view += '<div class="clear"></div>';
                    view += '</div>';
                    $('.list-view').append(view);
                }
            }
        },
        error: function () {
            $('.list-view').html('');
        }
    });
}

function getMoreFavListArticlesOfInterest(username, pwd, manageSlug, pages, itemsPerPage, name) {
    $.ajax({
        type: 'POST',
        url: GET_USER_FAVORITES_OF_INTEREST_SLUG_URL.replace("{0}", manageSlug),
        dataType: 'json',
        data: { "Username": username, "Password": pwd, "Page": pages, "ItemsPerPage": itemsPerPage },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                for (var i = 0; i < result.Data.length; i++) {
                    var id = result.Data[i].ID;
                    var categ_str = html_text(result.Data[i].Categories);
                    var categories = categ_str.split(";")[0];

                    var thumbnail = result.Data[i].Thumbnail;
                    var title = result.Data[i].Title;
                    var shortDescription = result.Data[i].ShortDescription;
                    if (shortDescription == null || shortDescription == '') { shortDescription = shortDescr_default; }

                    var categSlug = categ_str.split(";")[1];
                    var articleSlug = result.Data[i].Slug;

                    var link = '/favorite/' + categSlug + '/' + articleSlug;

                    var likes = result.Data[i].Likes;
                    var author = result.Data[i].Author;
                    if (author == null || author == '') { author = author_default; }
                    var liked = result.Data[i].Liked;
                    var selected = '';

                    var view = '';
                    view += '<div class="view-item" id="' + (i + 1) + '">';
                    view += '<div class="col-xs-4">';
                    view += '<img src="' + thumbnail + '" class="img-responsive zoom_popup" />';
                    view += '</div>';
                    view += '<div class="col-xs-8">';
                    view += '<a href="' + link + '"><h2 class="title">' + title + '</h2></a>';
                    view += '<p style="margin: 0; padding-top: 0;">' + shortDescription + '</p>';
                    view += '<div class="author">';
                    view += '<h3>' + author + '</h3>';
                    view += '<span class="like-tool">';
                    view += '<span class="like-number">' + likes + '</span>';
                    if (liked == true)
                        selected = 'selected';
                    view += '<span class="btn-tool ' + selected + '">';
                    view += '<i class="tool-icon tool-icon-like"></i></span>';
                    view += '</span>';
                    view += '</div>';
                    view += '</div>';
                    view += '<div class="clear"></div>';
                    view += '</div>';
                    $('.list-view').append(view);
                }
            }
        },
        error: function () {
        }
    });
}

// Search
function searchMyArticle(username, pwd, string) {
    itemsPerPage = $.cookie("sumTotal") || 999;

    $.ajax({
        type: 'POST',
        url: GET_MYSEARCH_ARTICLES_SLUG_URL,
        dataType: 'json',
        data: { "Username": username, "Password": pwd, "SearchTerm": string, "Page": page, "ItemsPerPage": itemsPerPage },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {

                $('.manage-view > .multi-level').parent().hide();
                $('#main-manage').removeClass('active');
                $('#Search').hide("slow").val('');
                $('#main-manage').removeClass('active');
                $('#manage-breadcrumb > li').each(function (index) {
                    if (index > 1) { $(this).remove(); }
                });

                $('#manage-breadcrumb').append('<li class="btn-address active"><a href="javascript:void(0)">Search \'' + string + '\'</a></li>');
                $('.list-view').html('<p>' + result.Data.length + ' Results Found</p><hr class="hr_search" /><div class="clear"></div>');
                for (var i = 0; i < result.Data.length; i++) {
                    var id = result.Data[i].ID;
                    var categ_str = html_text(result.Data[i].Categories);
                    var categories = categ_str.split(";")[0];

                    var thumbnail = result.Data[i].Thumbnail;
                    var title = result.Data[i].Title;
                    var shortDescription = result.Data[i].ShortDescription;
                    if (shortDescription == null || shortDescription == '') { shortDescription = shortDescr_default; }

                    var categSlug = categ_str.split(";")[1];
                    var articleSlug = result.Data[i].Slug;

                    var link = '/' + categSlug + '/' + articleSlug;

                    var likes = result.Data[i].Likes;
                    var author = result.Data[i].Author;
                    if (author == null || author == '') { author = author_default; }
                    var liked = result.Data[i].Liked;
                    var selected = '';

                    var view = '';
                    view += '<div class="view-item" id="' + (i + 1) + '">';
                    view += '<div class="col-xs-4">';
                    view += '<img src="' + thumbnail + '" class="img-responsive zoom_popup" />';
                    view += '</div>';
                    view += '<div class="col-xs-8">';
                    view += '<a href="' + link + '"><h2 class="title">' + title + '</h2></a>';
                    view += '<p style="margin: 0; padding-top: 0;">' + shortDescription + '</p>';
                    view += '<div class="author">';
                    view += '<h3>' + author + '</h3>';
                    view += '<span class="like-tool">';
                    view += '<span class="like-number">' + likes + '</span>';
                    if (liked == true)
                        selected = 'selected';
                    view += '<span class="btn-tool ' + selected + '">';
                    view += '<i class="tool-icon tool-icon-like"></i></span>';
                    view += '</span>';
                    view += '</div>';
                    view += '</div>';
                    view += '<div class="clear"></div>';
                    view += '</div>';
                    $('.list-view').append(view);
                }

                //console.log('END.');
                $('.manage-view.list-view').show();

            }
        },
        error: function () {
            $('.list-view').html('');
        }
    });
}

function searchArticle(username, pwd, search_string) {
    itemsPerPage = $.cookie("sumTotal") || 999;

    $('#choose-interest').removeClass('in');
    $('#collapse_choose_interest').addClass('collapsed');
    $.ajax({
        type: 'POST',
        url: GET_SEARCH_ARTICLES_SLUG_URL,
        dataType: 'json',
        data: { "Username": username, "Password": pwd, "SearchTerm": search_string, "Page": page, "ItemsPerPage": itemsPerPage },
        crossDomain: true,
        success: function (result) {
            if (result.Error == 0) {
                $('.nav.setting > .dropdown').hide();
                $('.nav.setting > .collapse-menu').hide();
                $('.nav.setting > .change-view').hide();
                $('.nav.setting > #breadcrumb_link').hide();

                $('#search-breadcrumb.breadcrumb').append('<li class="btn-address active">Search \'' + search_string + '\'</li>');
                $('#search-breadcrumb').show();

                $('.list-view').html('');
                $('#result_search').append('<p class="result">' + result.Data.length + ' Results Found</p><div class="clear"></div>');

                for (var i = 0; i < result.Data.length; i++) {
                    var id = result.Data[i].ID;
                    var categ_str = html_text(result.Data[i].Categories);
                    var categories = categ_str.split(";")[0];

                    var thumbnail = result.Data[i].Thumbnail;
                    var title = result.Data[i].Title;
                    if (title == null || title == '') { title = title_default; }
                    var shortDescription = result.Data[i].ShortDescription;
                    if (shortDescription == null || shortDescription == '') { shortDescription = shortDescr_default; }

                    var categSlug = categ_str.split(";")[1];
                    var articleSlug = result.Data[i].Slug;

                    var link = '/' + categSlug + '/' + articleSlug;

                    var likes = result.Data[i].Likes;
                    var author = result.Data[i].Author;
                    if (author == null || author == '') { author = author_default; }

                    var view = '';
                    view += '<div class="view-item" id="' + (i + 1) + '">';
                    if ($(window).width() < 768)
                        view += '<div class="col-xs-2">';
                    else
                        view += '<div class="col-xs-4">';
                    view += '<img src="' + thumbnail + '" data-original="' + thumbnail + '" class="img-responsive zoom_popup" />';
                    view += '</div>';
                    if ($(window).width() < 768)
                        view += '<div class="col-xs-10">';
                    else
                        view += '<div class="col-xs-8">';
                    view += '<h3 class="interest-name">' + categories + '</h3>';
                    view += '<a href="' + link + '"><h1 class="title">' + title + '</h1></a>';
                    view += '<a href="' + link + '"><h2 class="title">' + title + '</h2></a>';
                    view += '<cite>' + author + '</cite>';
                    view += '<p style="margin: 0; padding-top: 20px;">' + shortDescription + '<a class="more" href="' + link + '">more&nbsp<i class="fa fa-angle-right"></i></a></p>';
                    view += '</div>';
                    view += '<div class="clear"></div>';
                    view += '</div>';

                    $('.list-view').append(view);
                }

                //console.log('END.');
                $('.list-view').show();

            }
        },
        error: function () {
            $('.list-view').html('');
        }
    });
}

function html_text(text) {
    //var text = '&lt;p&gt;name&lt;/p&gt;&lt;p&gt;&lt;span style="font-size:xx-small;"&gt;ajde&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;em&gt;da&lt;/em&gt;&lt;/p&gt;';
    var decoded = $('#hiddendiv').html(text).text();
    $('#hiddendiv').html('');
    return decoded;
}