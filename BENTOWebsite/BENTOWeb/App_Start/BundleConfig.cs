﻿using System.Web;
using System.Web.Optimization;

namespace BENTOWeb
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/script").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/bootstrap-datepicker.js",
                        "~/Scripts/jquery.cookie.js",
                        "~/Scripts/jquery.md5.js",
                        "~/Scripts/jquery.transit.js",
                        "~/Scripts/modelScript.js",
                        "~/Scripts/Site.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/datepicker.css",
                      "~/Content/font-awesome.css",
                      "~/Content/Site.css"));

        }
    }
}