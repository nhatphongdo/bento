﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BENTOWeb
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Error404",
                url: "error404",
                defaults: new { controller = "Home", action = "Error404" }
            );

            routes.MapRoute(
                name: "Profile",
                url: "profile",
                defaults: new { controller = "Home", action = "Profile" }
            );

            routes.MapRoute(
                name: "Sponsor",
                url: "sponsor/{slug}",
                defaults: new { controller = "Home", action = "Sponsor", slug = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Manage",
                url: "favorite/{manageslug}/{articleslug}",
                defaults: new { controller = "Home", action = "Manage", manageslug = UrlParameter.Optional, articleslug = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Article",
                url: "{manageslug}/{articleslug}",
                defaults: new { controller = "Home", action = "Index", manageslug = UrlParameter.Optional, articleslug = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}