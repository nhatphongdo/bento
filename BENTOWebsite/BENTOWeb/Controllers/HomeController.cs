﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Cryptography;
using System.Text;

namespace BENTOWeb.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        private static string USR = String.Empty;
        private static string PWD = String.Empty;

        public ActionResult Index(string manageslug = "", string articleslug = "")
        {
            HttpCookie mycookie_usr = Request.Cookies["username"];
            if (mycookie_usr != null)
            {
                USR = Server.UrlDecode(mycookie_usr.Value);
            }
            HttpCookie mycookie_pwd = Request.Cookies["password"];
            if (mycookie_pwd != null)
            {
                PWD = Server.UrlDecode(mycookie_pwd.Value);
            }

            if (mycookie_usr == null || mycookie_pwd == null
                || mycookie_usr.Value == String.Empty || mycookie_pwd.Value == String.Empty)
            {
                ViewBag.username = String.Empty;
                ViewBag.md5pwd = String.Empty;
                return RedirectToAction("Profile");
            }

            ViewBag.username = USR;
            ViewBag.md5pwd = PWD;
            //using (MD5 md5Hash = MD5.Create())
            //{
            //    string hash = GetMd5Hash(md5Hash, PWD);
            //    ViewBag.md5pwd = hash;
            //}
            ViewBag.manageSlug = manageslug;
            ViewBag.articleSlug = articleslug;

            return View();
        }

        public ActionResult Manage(string manageslug = "", string articleslug = "")
        {
            HttpCookie mycookie_usr = Request.Cookies["username"];
            if (mycookie_usr != null)
            {
                USR = Server.UrlDecode(mycookie_usr.Value);
            }
            HttpCookie mycookie_pwd = Request.Cookies["password"];
            if (mycookie_pwd != null)
            {
                PWD = Server.UrlDecode(mycookie_pwd.Value);
            }

            if (mycookie_usr == null || mycookie_pwd == null
                || mycookie_usr.Value == String.Empty || mycookie_pwd.Value == String.Empty)
            {
                ViewBag.username = String.Empty;
                ViewBag.md5pwd = String.Empty;
                return RedirectToAction("Profile");
            }

            ViewBag.username = USR;
            ViewBag.md5pwd = PWD;
            //using (MD5 md5Hash = MD5.Create())
            //{
            //    string hash = GetMd5Hash(md5Hash, PWD);
            //    ViewBag.md5pwd = hash;
            //}

            ViewBag.manageSlug = manageslug;
            ViewBag.articleSlug = articleslug;
            return View();
        }

        public ActionResult Profile()
        {
            HttpCookie mycookie_usr = Request.Cookies["username"];
            if (mycookie_usr != null)
            {
                USR = Server.UrlDecode(mycookie_usr.Value);
            }
            HttpCookie mycookie_pwd = Request.Cookies["password"];
            if (mycookie_pwd != null)
            {
                PWD = Server.UrlDecode(mycookie_pwd.Value);
            }

            if (mycookie_usr == null || mycookie_pwd == null
                || mycookie_usr.Value == String.Empty || mycookie_pwd.Value == String.Empty)
            {
                ViewBag.username = String.Empty;
                ViewBag.md5pwd = String.Empty;
                return View();
            }

            ViewBag.username = USR;
            ViewBag.md5pwd = PWD;
            //using (MD5 md5Hash = MD5.Create())
            //{
            //    string hash = GetMd5Hash(md5Hash, PWD);
            //    ViewBag.md5pwd = hash;
            //}

            return View();
        }

        public ActionResult Sponsor(string slug = "")
        {
            HttpCookie mycookie_usr = Request.Cookies["username"];
            if (mycookie_usr != null)
            {
                USR = Server.UrlDecode(mycookie_usr.Value);
            }
            HttpCookie mycookie_pwd = Request.Cookies["password"];
            if (mycookie_pwd != null)
            {
                PWD = Server.UrlDecode(mycookie_pwd.Value);
            }

            if (mycookie_usr == null || mycookie_pwd == null
                || mycookie_usr.Value == String.Empty || mycookie_pwd.Value == String.Empty)
            {
                ViewBag.username = String.Empty;
                ViewBag.md5pwd = String.Empty;
                return RedirectToAction("Profile");
            }

            ViewBag.username = USR;
            ViewBag.md5pwd = PWD;
            //using (MD5 md5Hash = MD5.Create())
            //{
            //    string hash = GetMd5Hash(md5Hash, PWD);
            //    ViewBag.md5pwd = hash;
            //}
            ViewBag.sponsorSlug = slug;

            return View();
        }

        public ActionResult Error404()
        {
            return View();
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash. 
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes 
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data  
            // and format each one as a hexadecimal string. 
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }
    }
}
