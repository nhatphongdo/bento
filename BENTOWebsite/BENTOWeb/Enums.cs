﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BENTOWeb
{
    public static class Enums
    {
        public static string rootUrl = "http://web.vietdev.vn/bento/api/";

        public static string SIGN_IN_URL                            = rootUrl + "accounts/signin";
        public static string SIGN_OUT_URL                           = rootUrl + "accounts/signout";
        public static string SIGN_UP_URL                            = rootUrl + "accounts/signup";
        public static string FORGOT_PASSWORD_URL                    = rootUrl + "accounts/forgot";
        public static string GET_PROFILE_URL                        = rootUrl + "accounts/profile";
        public static string UPDATE_PROFILE_URL                     = rootUrl + "accounts/update";
        public static string UPLOAD_AVATAR_URL                      = rootUrl + "accounts/update/avatar";
        public static string GET_USER_INTERESTS_URL                 = rootUrl + "accounts/interests";
        public static string SET_USER_INTERESTS_URL                 = rootUrl + "accounts/interests/update";

        public static string GET_INTERESTS_URL                      = rootUrl + "interests";
        public static string GET_STRUCTURE_INTERESTS_URL            = rootUrl + "interests/structure";
        public static string GET_STRUCTURE_USER_INTERESTS_URL       = rootUrl + "interests/structure/user";
        public static string GET_STRUCTURE_FAVORITE_INTERESTS_URL   = rootUrl + "interests/structure/favorite";
        public static string GET_ARTICLES_OF_INTEREST_URL           = rootUrl + "interests/{0}/articles";
        public static string GET_USER_ARTICLES_OF_INTEREST_URL      = rootUrl + "interests/{0}/myarticles";
        public static string GET_USER_FAVORITES_OF_INTEREST_URL      = rootUrl + "interests/{id}/myfavorites";

        public static string GET_LATEST_ARTICLES_URL                = rootUrl + "articles/latest";
        public static string GET_SEARCH_ARTICLES_URL                = rootUrl + "articles/search";
        public static string GET_MYSEARCH_ARTICLES_URL              = rootUrl + "articles/mysearch";
        public static string GET_DETAIL_ARTICLE_URL                 = rootUrl + "articles/{0}";
        public static string GET_COMMENTS_OF_ARTICLE_URL            = rootUrl + "articles/{0}/comments";
        public static string POST_COMMENT_URL                       = rootUrl + "articles/{0}/comments/post";
        public static string GET_REPLIES_OF_COMMENT_URL             = rootUrl + "comments/{0}/replies";
        public static string POST_REPLY_URL                         = rootUrl + "comments/{0}/reply";
        public static string LIKE_POST_URL                          = rootUrl + "action/{0}/like";
        public static string UNLIKE_POST_URL                        = rootUrl + "action/{0}/unlike";
        public static string SHARE_POST_URL                         = rootUrl + "action/{0}/share";
        public static string GET_NOTES_OF_ARTICLE_URL               = rootUrl + "articles/{0}/notes";
        public static string POST_NOTE_FOR_ARTICLE_URL              = rootUrl + "articles/{0}/notes/post";
        public static string DELETE_NOTE_URL                        = rootUrl + "notes/{0}/delete";
        public static string GET_SHARES_OF_ARTICLE_URL              = rootUrl + "articles/{0}/shares";
        public static string GET_LIKES_OF_ARTICLE_URL               = rootUrl + "articles/{0}/likes";
        public static string POST_REPORTS_OF_ARTICLE_URL            = rootUrl + "articles/{id}/reports/post";
        public static string ADD_ARTICLE_TO_FAVORITE_LIST_URL       = rootUrl + "action/{id}/favorite";
        public static string REMOVE_ARTICLE_FROM_FAVORITE_LIST_URL  = rootUrl + "action/{id}/unfavorite";

        public static string GET_SPONSORS_URL                       = rootUrl + "sponsors";
        public static string GET_SPONSOR_DETAIL_URL                 = rootUrl + "sponsors/{0}";

    }
}