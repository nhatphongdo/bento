﻿$(document).ready(function () {
    $('.setting-icon-card').hide();

    $('.change-view').click(function () {
        $('.setting-icon-card').toggle();
        $('.setting-icon-title').toggle();
        $('.list-view').toggleClass("title-view").toggleClass("card-view");
        $('.view-item div:nth-child(1)').toggleClass("col-lg-4").toggleClass("col-lg-2");
        $('.view-item div:nth-child(2)').toggleClass("col-lg-8").toggleClass("col-lg-10");
    });

    $('#choose-interest a').click(function () {
        $(this).find('.check-interest').toggleClass("checked");
        $(this).parent().find('input[type="checkbox"]').click();
    });

    $('.article-tools > .btn-tool').click(function () {
        $('.article-tools > .btn-tool:nth-child(' + ($(this).index() + 1) + ')').toggleClass("selected");
        switch ($(this).index() + 1) {
            case 1:
                $('.comments-list').slideToggle("slow");
                $('.notes-list, .share-tools').slideUp("fast");
                $('.article-tools > .btn-tool:nth-child(2).selected, .article-tools > .btn-tool:nth-child(3).selected').toggleClass("selected");
                break;
            case 2:
                $('.notes-list').slideToggle("slow");
                $('.comments-list, .share-tools').slideUp("fast");
                $('.article-tools > .btn-tool:nth-child(1).selected, .article-tools > .btn-tool:nth-child(3).selected').toggleClass("selected");
                break;
            case 3:
                $('.share-tools').slideToggle("slow");
                $('.notes-list, .comments-list').slideUp("fast");
                $('.article-tools > .btn-tool:nth-child(1).selected, .article-tools > .btn-tool:nth-child(2).selected').toggleClass("selected");
                break;
        }
    });

    $('.article-tools .like-tool > .btn-tool').click(function () {
        var number = parseInt($(this).prev().text())
        if ($(this).hasClass("selected")) {
            number--;
        }
        else {
            number++;
        }
        $('.article-tools .like-tool > .btn-tool').toggleClass("selected");
        $('.article-tools .like-tool > .btn-tool').parent().find(".like-number").text(number);
    });

    $('.media .like-tool > .btn-tool, .view-item .like-tool > .btn-tool').click(function () {
        var number = parseInt($(this).prev().text())
        if ($(this).hasClass("selected")) {
            number--;
        }
        else {
            number++;
        }
        $(this).toggleClass("selected");
        $(this).parent().find(".like-number").text(number);
    });

    $('.reply-comment').click(function () {
        $("#Comment").focus();
    });

    $('.level1 > li > a, .level2 > li > a').click(function () {
        $(this).parent().toggleClass("open");
        $(this).find(".fa").toggleClass("fa-angle-down").toggleClass("fa-angle-right");
    });

    $('img[data-toggle="modal"]').click(function () {
        $('#zoom_img').find("img").attr("src", $(this).attr("src"));
    });

    $('.zoom-in').click(function () {
        var width = $('#zoom_img').find(".modal-dialog").width() + 100;
        $('#zoom_img').find(".modal-dialog").width(width >= screen.availWidth ? screen.width : width);
    });

    $('.zoom-out').click(function () {
        var width = $('#zoom_img').find(".modal-dialog").width() - 100;

        $('#zoom_img').find(".modal-dialog").width(width < 600 ? 600 : width);
    });

});