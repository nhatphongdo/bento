﻿using BentoAdm.Helper;
using BentoAdm.Models;
using BentoAdm.Models.Api;
using System;
using System.Linq;
using System.Web.Http;

namespace BentoAdm.Controllers.Api
{
    [RoutePrefix("api/articles")]
    public class ArticlesController : ApiController
    {
        /// <summary>
        /// Get the latests articles
        /// </summary>
        /// <param name="model">The Load request model</param>
        /// <returns>JSON object</returns>
        [Route("latest")]
        [HttpPost]
        public IHttpActionResult Latest(LoadRequestModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Page <= 0)
                {
                    model.Page = 1;
                }

                if (model.ItemsPerPage <= 0)
                {
                    model.ItemsPerPage = 10;
                }

                // Get latest articles
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var articles = entities.GetLatestArticles(user.ID, model.Page, model.ItemsPerPage).ToList();
                            return Json(new ResultDataModel()
                            {
                                Error = LoadRequestErrors.Success,
                                ErrorMessage = "Get the latest articles successfully",
                                Data = articles
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = LoadRequestErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Get the latests articles includeslug
        /// </summary>
        /// <param name="model">The Load request model</param>
        /// <returns>JSON object</returns>
        [Route("latestincludeslug")]
        [HttpPost]
        public IHttpActionResult LatestIncludeSlug(LoadRequestModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Page <= 0)
                {
                    model.Page = 1;
                }

                if (model.ItemsPerPage <= 0)
                {
                    model.ItemsPerPage = 10;
                }

                // Get latest articles
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var articles = entities.GetLatestArticlesIncludeSlug(user.ID, model.Page, model.ItemsPerPage).ToList();
                            return Json(new ResultDataModel()
                            {
                                Error = LoadRequestErrors.Success,
                                ErrorMessage = "Get the latest articles successfully",
                                Data = articles
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = LoadRequestErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Search articles
        /// </summary>
        /// <param name="model">The Search request model</param>
        /// <returns>JSON object</returns>
        [Route("search")]
        [HttpPost]
        public IHttpActionResult Search(SearchRequestModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Page <= 0)
                {
                    model.Page = 1;
                }

                if (model.ItemsPerPage <= 0)
                {
                    model.ItemsPerPage = 10;
                }

                // Get latest articles
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var articles = entities.SearchArticles(user.ID, model.SearchTerm, model.Page, model.ItemsPerPage).ToList();
                            return Json(new ResultDataModel()
                            {
                                Error = LoadRequestErrors.Success,
                                ErrorMessage = "Search articles successfully",
                                Data = articles
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = LoadRequestErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Search articles Inlcude Slug
        /// </summary>
        /// <param name="model">The Search request model</param>
        /// <returns>JSON object</returns>
        [Route("searchincludeslug")]
        [HttpPost]
        public IHttpActionResult SearchIncludeSlug(SearchRequestModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Page <= 0)
                {
                    model.Page = 1;
                }

                if (model.ItemsPerPage <= 0)
                {
                    model.ItemsPerPage = 10;
                }

                // Get latest articles
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var articles = entities.SearchArticlesIncludeSlug(user.ID, model.SearchTerm, model.Page, model.ItemsPerPage).ToList();
                            return Json(new ResultDataModel()
                            {
                                Error = LoadRequestErrors.Success,
                                ErrorMessage = "Search articles successfully",
                                Data = articles
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = LoadRequestErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Search articles in user's favorite list Inlcude Slug
        /// </summary>
        /// <param name="model">The Search request model</param>
        /// <returns>JSON object</returns>
        [Route("mysearchincludeslug")]
        [HttpPost]
        public IHttpActionResult MySearchIncludeSlug(SearchRequestModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Page <= 0)
                {
                    model.Page = 1;
                }

                if (model.ItemsPerPage <= 0)
                {
                    model.ItemsPerPage = 10;
                }

                // Get latest articles
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var articles = entities.SearchUserArticlesIncludeSlug(user.ID, model.SearchTerm, model.Page, model.ItemsPerPage).ToList();
                            return Json(new ResultDataModel()
                            {
                                Error = LoadRequestErrors.Success,
                                ErrorMessage = "Search user's articles successfully",
                                Data = articles
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = LoadRequestErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Gets the specified article.
        /// </summary>
        /// <param name="id">The article's identifier.</param>
        /// <param name="model">The Sign in model includes user name and password.</param>
        /// <returns>JSON object</returns>
        [Route("{id}")]
        [HttpPost]
        public IHttpActionResult Get([FromUri] Guid id, [FromBody] SignInModel model)
        {
            if (ModelState.IsValid)
            {
                // Get detail of article
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = SignInErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var article = entities.Articles.FirstOrDefault(a => a.ID == id);
                            return Json(new ResultDataModel()
                            {
                                Error = SignInErrors.Success,
                                ErrorMessage = "Get article successfully",
                                Data = new
                                {
                                    article.ID,
                                    article.Title,
                                    article.Author,
                                    article.Body,
                                    article.Thumbnail,
                                    article.DateCreated,
                                    article.PublishedDate,
                                    Favorited = entities.Favorites.Any(f => f.ObjectID == article.ID && f.ProfileID == user.ID)
                                }
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = SignInErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Gets the specified article.
        /// </summary>
        /// <param name="slug">The article's identifier.</param>
        /// <param name="model">The Sign in model includes user name and password.</param>
        /// <returns>JSON object</returns>
        [Route("{slug}/slug")]
        [HttpPost]
        public IHttpActionResult GetSlug([FromUri] String slug, [FromBody] SignInModel model)
        {
            if (ModelState.IsValid)
            {
                // Get detail of article
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = SignInErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var article = entities.Articles.FirstOrDefault(a => a.Slug == slug);
                            return Json(new ResultDataModel()
                            {
                                Error = SignInErrors.Success,
                                ErrorMessage = "Get article successfully",
                                Data = new
                                {
                                    article.ID,
                                    article.Slug,
                                    article.Title,
                                    article.Author,
                                    article.Body,
                                    article.Thumbnail,
                                    article.DateCreated,
                                    article.PublishedDate,
                                    Favorited = entities.Favorites.Any(f => f.ObjectID == article.ID && f.ProfileID == user.ID)
                                }
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = SignInErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Get the comments of specified article.
        /// </summary>
        /// <param name="id">The article's identifier.</param>
        /// <param name="model">The Load request model.</param>
        /// <returns>JSON object</returns>
        [Route("{id}/comments")]
        [HttpPost]
        public IHttpActionResult Comments([FromUri] Guid id, [FromBody] LoadRequestModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Page <= 0)
                {
                    model.Page = 1;
                }

                if (model.ItemsPerPage <= 0)
                {
                    model.ItemsPerPage = 10;
                }

                // Get comments of each article
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var comments = entities.GetCommentsOfArticle(user.ID, id, model.Page, model.ItemsPerPage).ToList();
                            return Json(new ResultDataModel()
                            {
                                Error = LoadRequestErrors.Success,
                                ErrorMessage = "Get comments of article successfully",
                                Data = comments
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = LoadRequestErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Posts the comment.
        /// </summary>
        /// <param name="id">The article's identifier.</param>
        /// <param name="model"></param>
        /// <returns>JSON object</returns>
        [Route("{id}/comments/post")]
        [HttpPost]
        public IHttpActionResult PostComment([FromUri] Guid id, [FromBody] PostCommentModel model)
        {
            if (ModelState.IsValid)
            {
                // Post comment for article
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = PostCommentErrors.UserNotExistence,
                            ErrorMessage = "User does not exist"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = PostCommentErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var comment = new Comment()
                            {
                                ID = Guid.NewGuid(),
                                DateCreated = DateTime.Now,
                                IP = Request.GetIpAddress(),
                                Message = model.Message,
                                ObjectID = id,
                                ProfileID = user.ID
                            };
                            entities.Comments.Add(comment);

                            var objectIndex = new ObjectIndex()
                            {
                                ID = comment.ID,
                                DateCreated = DateTime.Now,
                                ObjectType = "Comment"
                            };
                            entities.ObjectIndexes.Add(objectIndex);
                            entities.SaveChanges();

                            return Json(new ResultModel()
                            {
                                Error = PostCommentErrors.Success,
                                ErrorMessage = "Post comment successfully"
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = PostCommentErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = PostCommentErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Get replies of specific comment
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="model">The model.</param>
        /// <returns>IHttpActionResult.</returns>
        [Route("~/api/comments/{id}/replies")]
        [HttpPost]
        public IHttpActionResult Replies([FromUri] Guid id, [FromBody] LoadRequestModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Page <= 0)
                {
                    model.Page = 1;
                }

                if (model.ItemsPerPage <= 0)
                {
                    model.ItemsPerPage = 10;
                }

                // Get comments of each article
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var comments = entities.GetRepliesOfComment(user.ID, id, model.Page, model.ItemsPerPage).ToList();
                            return Json(new ResultDataModel()
                            {
                                Error = LoadRequestErrors.Success,
                                ErrorMessage = "Get comments of article successfully",
                                Data = comments
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = LoadRequestErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Replies the comment.
        /// </summary>
        /// <param name="id">The comment's identifier.</param>
        /// <param name="model"></param>
        /// <returns>JSON object</returns>
        [Route("~/api/comments/{id}/reply")]
        [HttpPost]
        public IHttpActionResult ReplyComment([FromUri] Guid id, [FromBody] PostCommentModel model)
        {
            if (ModelState.IsValid)
            {
                // Reply for comment
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = PostCommentErrors.UserNotExistence,
                            ErrorMessage = "User does not exist"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = PostCommentErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var comment = new Comment()
                            {
                                ID = Guid.NewGuid(),
                                DateCreated = DateTime.Now,
                                IP = Request.GetIpAddress(),
                                Message = model.Message,
                                ObjectID = id,
                                ProfileID = user.ID
                            };
                            entities.Comments.Add(comment);

                            var objectIndex = new ObjectIndex()
                            {
                                ID = comment.ID,
                                DateCreated = DateTime.Now,
                                ObjectType = "Comment"
                            };
                            entities.ObjectIndexes.Add(objectIndex);
                            entities.SaveChanges();

                            return Json(new ResultModel()
                            {
                                Error = PostCommentErrors.Success,
                                ErrorMessage = "Post reply successfully"
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = PostCommentErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = PostCommentErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Likes the article / comment.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="model"></param>
        /// <returns>JSON object</returns>
        [Route("~/api/action/{id}/like")]
        [HttpPost]
        public IHttpActionResult Like([FromUri] Guid id, [FromBody] SignInModel model)
        {
            if (ModelState.IsValid)
            {
                // Like a comment
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = ActionErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = ActionErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var like = entities.Likes.FirstOrDefault(l => l.ProfileID == user.ID && l.ObjectID == id);
                            if (like != null && like.Action == 1)
                            {
                                return Json(new ResultModel()
                                {
                                    Error = ActionErrors.AlreadyDone,
                                    ErrorMessage = "Already liked"
                                });
                            }

                            if (like == null)
                            {
                                like = new Like()
                                {
                                    ID = Guid.NewGuid(),
                                    DateCreated = DateTime.Now,
                                    IP = Request.GetIpAddress(),
                                    ObjectID = id,
                                    ProfileID = user.ID,
                                    Action = 1
                                };
                                entities.Likes.Add(like);

                                var objectIndex = new ObjectIndex()
                                {
                                    ID = like.ID,
                                    DateCreated = DateTime.Now,
                                    ObjectType = "Like"
                                };
                                entities.ObjectIndexes.Add(objectIndex);
                            }
                            else
                            {
                                like.Action = 1;
                                like.DateCreated = DateTime.Now;
                            }

                            entities.SaveChanges();

                            return Json(new ResultDataModel()
                            {
                                Error = ActionErrors.Success,
                                ErrorMessage = "Like article / comment successfully",
                                Data = new
                                {
                                    Count = entities.CountLikes(id).FirstOrDefault()
                                }
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = ActionErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = ActionErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Unlikes the article / comment.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="model"></param>
        /// <returns>JSON object</returns>
        [Route("~/api/action/{id}/unlike")]
        [HttpPost]
        public IHttpActionResult UnLike([FromUri] Guid id, [FromBody] SignInModel model)
        {
            if (ModelState.IsValid)
            {
                // Like a comment
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = ActionErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = ActionErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var like = entities.Likes.FirstOrDefault(l => l.ProfileID == user.ID && l.ObjectID == id);
                            if (like != null && like.Action == 2)
                            {
                                return Json(new ResultModel()
                                {
                                    Error = ActionErrors.AlreadyDone,
                                    ErrorMessage = "Already unliked"
                                });
                            }

                            if (like == null)
                            {
                                like = new Like()
                                {
                                    ID = Guid.NewGuid(),
                                    DateCreated = DateTime.Now,
                                    IP = Request.GetIpAddress(),
                                    ObjectID = id,
                                    ProfileID = user.ID,
                                    Action = 2
                                };
                                entities.Likes.Add(like);

                                var objectIndex = new ObjectIndex()
                                {
                                    ID = like.ID,
                                    DateCreated = DateTime.Now,
                                    ObjectType = "Like"
                                };
                                entities.ObjectIndexes.Add(objectIndex);
                            }
                            else
                            {
                                like.Action = 2;
                                like.DateCreated = DateTime.Now;
                            }

                            entities.SaveChanges();

                            return Json(new ResultDataModel()
                            {
                                Error = ActionErrors.Success,
                                ErrorMessage = "Unlike article successfully",
                                Data = new
                                {
                                    Count = entities.CountLikes(id).FirstOrDefault()
                                }
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = ActionErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = ActionErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Shares the article / comment.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="model">The model.</param>
        /// <returns>IHttpActionResult.</returns>
        [Route("~/api/action/{id}/share")]
        [HttpPost]
        public IHttpActionResult Share([FromUri] Guid id, [FromBody] ShareModel model)
        {
            if (ModelState.IsValid)
            {
                // Like a comment
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = ShareErrors.UserNotExistence,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = ShareErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var share = new Share()
                            {
                                ID = Guid.NewGuid(),
                                DateCreated = DateTime.Now,
                                IP = Request.GetIpAddress(),
                                ObjectID = id,
                                ProfileID = user.ID,
                                SocialName = model.SocialName
                            };
                            entities.Shares.Add(share);

                            var objectIndex = new ObjectIndex()
                            {
                                ID = share.ID,
                                DateCreated = DateTime.Now,
                                ObjectType = "Share"
                            };
                            entities.ObjectIndexes.Add(objectIndex);
                            entities.SaveChanges();

                            return Json(new ResultModel()
                            {
                                Error = ShareErrors.Success,
                                ErrorMessage = "Like article successfully"
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = ShareErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = ShareErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Get the notes of article.
        /// </summary>
        /// <param name="id">The article's identifier.</param>
        /// <param name="model">The Load request model.</param>
        /// <returns>JSON object</returns>
        [Route("{id}/notes")]
        [HttpPost]
        public IHttpActionResult Notes([FromUri] Guid id, [FromBody] LoadRequestModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Page <= 0)
                {
                    model.Page = 1;
                }

                if (model.ItemsPerPage <= 0)
                {
                    model.ItemsPerPage = 10;
                }

                // Get notes of each article
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var notes = entities.GetNotesOfArticle(id, user.ID, model.Page, model.ItemsPerPage).ToList();
                            return Json(new ResultDataModel()
                            {
                                Error = LoadRequestErrors.Success,
                                ErrorMessage = "Get notes of article successfully",
                                Data = notes
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = LoadRequestErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Post note
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="model">The model.</param>
        /// <returns>IHttpActionResult.</returns>
        [Route("{id}/notes/post")]
        [HttpPost]
        public IHttpActionResult PostNote([FromUri] Guid id, [FromBody] PostCommentModel model)
        {
            if (ModelState.IsValid)
            {
                // Post comment for article
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = PostCommentErrors.UserNotExistence,
                            ErrorMessage = "User does not exist"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = PostCommentErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var note = new Note()
                            {
                                ID = Guid.NewGuid(),
                                DateCreated = DateTime.Now,
                                IP = Request.GetIpAddress(),
                                Message = model.Message,
                                ObjectID = id,
                                ProfileID = user.ID
                            };
                            entities.Notes.Add(note);

                            var objectIndex = new ObjectIndex()
                            {
                                ID = note.ID,
                                DateCreated = DateTime.Now,
                                ObjectType = "Note"
                            };
                            entities.ObjectIndexes.Add(objectIndex);
                            entities.SaveChanges();

                            return Json(new ResultModel()
                            {
                                Error = PostCommentErrors.Success,
                                ErrorMessage = "Post note successfully"
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = PostCommentErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = PostCommentErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Post note
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="model">The model.</param>
        /// <returns>IHttpActionResult.</returns>
        [Route("~/api/notes/{id}/delete")]
        [HttpPost]
        public IHttpActionResult DeleteNote([FromUri] Guid id, [FromBody] SignInModel model)
        {
            if (ModelState.IsValid)
            {
                // Post comment for article
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = SignInErrors.AuthenticationFailed,
                            ErrorMessage = "User does not exist"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var note = entities.Notes.FirstOrDefault(n => n.ID == id);
                            if (note != null)
                            {
                                if (note.ProfileID != user.ID)
                                {
                                    // This note does not belong to this user
                                    return Json(new ResultModel()
                                    {
                                        Error = SignInErrors.DeniesPermission,
                                        ErrorMessage = "You don't have permission to delete this note"
                                    });

                                }
                                entities.Notes.Remove(note);
                            }

                            var objectIndex = entities.ObjectIndexes.FirstOrDefault(o => o.ID == id);
                            if (objectIndex != null)
                            {
                                entities.ObjectIndexes.Remove(objectIndex);
                            }

                            entities.SaveChanges();

                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.Success,
                                ErrorMessage = "Delete note successfully"
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = SignInErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Get the shares of article.
        /// </summary>
        /// <param name="id">The article's identifier.</param>
        /// <param name="model">The Load request model.</param>
        /// <returns>JSON object</returns>
        [Route("{id}/shares")]
        [HttpPost]
        public IHttpActionResult Shares([FromUri] Guid id, [FromBody] LoadRequestModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Page <= 0)
                {
                    model.Page = 1;
                }

                if (model.ItemsPerPage <= 0)
                {
                    model.ItemsPerPage = 10;
                }

                // Get shares of each article
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var shares = entities.GetSharesOfArticle(id, model.Page, model.ItemsPerPage).ToList();
                            return Json(new ResultDataModel()
                            {
                                Error = LoadRequestErrors.Success,
                                ErrorMessage = "Get shares of article successfully",
                                Data = shares
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = LoadRequestErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Get the likes of article.
        /// </summary>
        /// <param name="id">The article's identifier.</param>
        /// <param name="model">The Load request model.</param>
        /// <returns>JSON object</returns>
        [Route("{id}/likes")]
        [HttpPost]
        public IHttpActionResult Likes([FromUri] Guid id, [FromBody] LoadRequestModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Page <= 0)
                {
                    model.Page = 1;
                }

                if (model.ItemsPerPage <= 0)
                {
                    model.ItemsPerPage = 10;
                }

                // Get shares of each article
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var likes = entities.GetLikesOfArticle(id, model.Page, model.ItemsPerPage).ToList();
                            return Json(new ResultDataModel()
                            {
                                Error = LoadRequestErrors.Success,
                                ErrorMessage = "Get likes of article successfully",
                                Data = likes
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = LoadRequestErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Post report
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="model">The model.</param>
        /// <returns>IHttpActionResult.</returns>
        [Route("{id}/reports/post")]
        [HttpPost]
        public IHttpActionResult PostReport([FromUri] Guid id, [FromBody] PostReportModel model)
        {
            if (ModelState.IsValid)
            {
                // Post comment for article
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = PostReportErrors.UserNotExistence,
                            ErrorMessage = "User does not exist"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = PostReportErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var report = new ContentReport()
                            {
                                ID = Guid.NewGuid(),
                                DateCreated = DateTime.Now,
                                IP = Request.GetIpAddress(),
                                ReportContent = model.Message,
                                Type = model.Type,
                                ObjectID = id,
                                ProfileID = user.ID
                            };
                            entities.ContentReports.Add(report);

                            var objectIndex = new ObjectIndex()
                            {
                                ID = report.ID,
                                DateCreated = DateTime.Now,
                                ObjectType = "ContentReport"
                            };
                            entities.ObjectIndexes.Add(objectIndex);
                            entities.SaveChanges();

                            return Json(new ResultModel()
                            {
                                Error = PostReportErrors.Success,
                                ErrorMessage = "Post report successfully"
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = PostReportErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = PostReportErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Add the article to favorite list.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="model"></param>
        /// <returns>JSON object</returns>
        [Route("~/api/action/{id}/favorite")]
        [HttpPost]
        public IHttpActionResult Favorite([FromUri] Guid id, [FromBody] SignInModel model)
        {
            if (ModelState.IsValid)
            {
                // Like a comment
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = ActionErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = ActionErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var favorite = entities.Favorites.FirstOrDefault(l => l.ProfileID == user.ID && l.ObjectID == id);
                            if (favorite != null)
                            {
                                return Json(new ResultModel()
                                {
                                    Error = ActionErrors.AlreadyDone,
                                    ErrorMessage = "Already added to favorite"
                                });
                            }

                            favorite = new Favorite()
                            {
                                ID = Guid.NewGuid(),
                                DateCreated = DateTime.Now,
                                IP = Request.GetIpAddress(),
                                ObjectID = id,
                                ProfileID = user.ID,
                            };
                            entities.Favorites.Add(favorite);

                            var objectIndex = new ObjectIndex()
                            {
                                ID = favorite.ID,
                                DateCreated = DateTime.Now,
                                ObjectType = "Favorite"
                            };
                            entities.ObjectIndexes.Add(objectIndex);

                            entities.SaveChanges();

                            return Json(new ResultDataModel()
                            {
                                Error = ActionErrors.Success,
                                ErrorMessage = "Add article to favorite successfully"
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = ActionErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = ActionErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Remove the article from favorite list
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="model"></param>
        /// <returns>JSON object</returns>
        [Route("~/api/action/{id}/unfavorite")]
        [HttpPost]
        public IHttpActionResult UnFavorite([FromUri] Guid id, [FromBody] SignInModel model)
        {
            if (ModelState.IsValid)
            {
                // Like a comment
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = ActionErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = ActionErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var favorite = entities.Favorites.FirstOrDefault(l => l.ProfileID == user.ID && l.ObjectID == id);
                            if (favorite != null)
                            {
                                entities.Favorites.Remove(favorite);

                                var objectIndex = entities.ObjectIndexes.FirstOrDefault(o => o.ID == favorite.ID);
                                if (objectIndex != null)
                                {
                                    entities.ObjectIndexes.Remove(objectIndex);
                                }
                                entities.SaveChanges();
                            }

                            return Json(new ResultDataModel()
                            {
                                Error = ActionErrors.Success,
                                ErrorMessage = "Unfavorite article successfully"
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = ActionErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = ActionErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

    }
}