﻿using BentoAdm.Models;
using BentoAdm.Models.Api;
using System;
using System.Linq;
using System.Web.Http;

namespace BentoAdm.Controllers.Api
{
    [RoutePrefix("api/sponsors")]
    public class SponsorController : ApiController
    {
        /// <summary>
        /// Gets the list of interests.
        /// </summary>
        /// <param name="model">The Sign in model includes user name and password.</param>
        /// <returns>JSON object</returns>
        [Route("")]
        [HttpPost]
        public IHttpActionResult Get(SignInModel model)
        {
            if (ModelState.IsValid)
            {
                // Get list of interests
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }
                        try
                        {
                            var sponsors = entities.Sponsors
                                .Select(s => new
                                {
                                    s.ID,
                                    s.Slug,
                                    s.Address,
                                    s.Email,
                                    s.Fax,
                                    s.Logo,
                                    s.Name,
                                    s.Phone,
                                    s.ShortDescription,
                                    s.WebAddress
                                })
                                .ToList();
                            return Json(new ResultDataModel()
                            {
                                Error = SignInErrors.Success,
                                ErrorMessage = "Get sponsors successfully",
                                Data = sponsors
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = SignInErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Gets Sponsor by ID.
        /// </summary>
        /// <param name="model">The Sign in model includes user name and password.</param>
        /// <returns>JSON object</returns>
        [Route("{id}")]
        [HttpPost]
        public IHttpActionResult Get([FromUri] Guid id, [FromBody] SignInModel model)
        {
            if (ModelState.IsValid)
            {
                // Get list of interests
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }
                        try
                        {
                            var sponsor = entities.Sponsors
                                .Where(s => s.ID == id)
                                .Select(s => new
                                {
                                    s.ID,
                                    s.Address,
                                    s.Email,
                                    s.Fax,
                                    s.Logo,
                                    s.Name,
                                    s.Phone,
                                    s.ShortDescription,
                                    s.WebAddress,
                                    s.BodyContent
                                })
                                .FirstOrDefault();

                            return Json(new ResultDataModel()
                            {
                                Error = SignInErrors.Success,
                                ErrorMessage = "Get sponsor's detail successfully",
                                Data = sponsor
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = SignInErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Gets Sponsor by Slug.
        /// </summary>
        /// <param name="model">The Sign in model includes user name and password.</param>
        /// <returns>JSON object</returns>
        [Route("{slug}/slug")]
        [HttpPost]
        public IHttpActionResult GetSlug([FromUri] string slug, [FromBody] SignInModel model)
        {
            if (ModelState.IsValid)
            {
                // Get list of interests
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }
                        try
                        {
                            var sponsor = entities.Sponsors
                                .Where(s => s.Slug == slug)
                                .Select(s => new
                                {
                                    s.ID,
                                    s.Address,
                                    s.Email,
                                    s.Fax,
                                    s.Logo,
                                    s.Name,
                                    s.Phone,
                                    s.ShortDescription,
                                    s.WebAddress,
                                    s.BodyContent
                                })
                                .FirstOrDefault();

                            return Json(new ResultDataModel()
                            {
                                Error = SignInErrors.Success,
                                ErrorMessage = "Get sponsor's detail successfully",
                                Data = sponsor
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = SignInErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }
    }
}
