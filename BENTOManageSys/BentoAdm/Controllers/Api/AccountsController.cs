﻿using BentoAdm.Helper;
using BentoAdm.Models;
using BentoAdm.Models.Api;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Routing;

namespace BentoAdm.Controllers.Api
{
    [RoutePrefix("api/accounts")]
    public class AccountsController : ApiController
    {
        /// <summary>
        /// Sign in to system
        /// </summary>
        /// <param name="model">The Sign in model includes user name and password.</param>
        /// <returns>JSON object</returns>
        [Route("signin")]
        [HttpPost]
        public IHttpActionResult SignIn(SignInModel model)
        {
            if (ModelState.IsValid)
            {
                // Sign in
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower());
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = SignInErrors.AuthenticationFailed,
                            ErrorMessage = "Username not exist"
                        });
                    }
                    else
                    {
                        var userpwd = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                        if (userpwd == null)
                        {
                            // Not found
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.AuthenticationFailed,
                                ErrorMessage = "Wrong username or password"
                            });
                        }
                        else
                        {
                            try
                            {
                                userpwd.LastSignedIn = DateTime.Now;
                                userpwd.IsSigningIn = true;
                                entities.SaveChanges();

                                return Json(new ResultModel()
                                {
                                    Error = SignInErrors.Success,
                                    ErrorMessage = "Sign in successfully"
                                });
                            }
                            catch (Exception exc)
                            {
                                return Json(new ResultModel()
                                {
                                    Error = SignInErrors.DatabaseFailed,
                                    ErrorMessage = "Database connection failed"
                                });
                            }
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new ResultModel()
                {
                    Error = SignInErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Sign out user
        /// </summary>
        /// <param name="model">The Sign in model includes user name and password.</param>
        /// <returns>JSON object</returns>
        [Route("signout")]
        [HttpPost]
        public IHttpActionResult SignOut(SignInModel model)
        {
            if (ModelState.IsValid)
            {
                // Sign out
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = SignInErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        try
                        {
                            user.IsSigningIn = false;
                            entities.SaveChanges();

                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.Success,
                                ErrorMessage = "Sign out successfully"
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new ResultModel()
                {
                    Error = SignInErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Signs up new account.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>IHttpActionResult.</returns>
        [Route("signup")]
        [HttpPost]
        public IHttpActionResult SignUp(SignUpModel model)
        {
            if (ModelState.IsValid)
            {
                // Sign up
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower());
                    if (user != null)
                    {
                        // Existence
                        return Json(new ResultModel()
                        {
                            Error = SignUpErrors.UserExistence,
                            ErrorMessage = "This username is used"
                        });
                    }
                    else
                    {
                        try
                        {
                            user = new Profile()
                            {
                                ID = Guid.NewGuid(),
                                Username = model.Username,
                                Email = model.Username,
                                Fullname = model.FullName,
                                BirthDay = model.Birthday,
                                Gender = model.Gender,
                                AccountType = model.AccountType,
                                AccountID = model.AccountID,
                                Password = Cryptographer.EncryptMd5(model.Password),
                                IP = Request.GetIpAddress(),
                                Banned = false,
                                DateCreated = DateTime.Now,
                                LastSignedIn = DateTime.Now,
                                IsSigningIn = true
                            };
                            entities.Profiles.Add(user);
                            entities.SaveChanges();

                            return Json(new ResultModel()
                            {
                                Error = SignUpErrors.Success,
                                ErrorMessage = "Sign up successfully"
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignUpErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new ResultModel()
                {
                    Error = SignUpErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Request recovery password
        /// </summary>
        /// <param name="model">The Forgot password model includes user name and email.</param>
        /// <returns>JSON object</returns>
        [Route("forgot")]
        [HttpPost]
        public IHttpActionResult Forgot(ForgotPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                // Forgot password
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Email == model.Email);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = ForgotPasswordErrors.WrongEmail,
                            ErrorMessage = "Wrong email"
                        });
                    }
                    else
                    {
                        try
                        {
                            var token = new ForgotPasswordToken()
                            {
                                DateCreated = DateTime.Now,
                                ID = Guid.NewGuid(),
                                Email = user.Email,
                                Username = user.Username,
                                IsValid = true,
                                Token = Guid.NewGuid().ToString()
                            };
                            entities.ForgotPasswordTokens.Add(token);
                            entities.SaveChanges();

                            return Json(new ResultModel()
                            {
                                Error = ForgotPasswordErrors.Success,
                                ErrorMessage = "Check mail box for reset pass email"
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = ForgotPasswordErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new ResultModel()
                {
                    Error = ForgotPasswordErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Get user's profile
        /// </summary>
        /// <param name="model">The Sign in model includes user name and password.</param>
        /// <returns>IHttpActionResult.</returns>
        [Route("profile")]
        [HttpPost]
        public IHttpActionResult Profile(SignInModel model)
        {
            if (ModelState.IsValid)
            {
                // Get account's profile
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = SignInErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            return Json(new ResultDataModel()
                            {
                                Error = SignInErrors.Success,
                                ErrorMessage = "Get profile successfully",
                                Data = new
                                {
                                    user.Username,
                                    user.Gender,
                                    user.AccountType,
                                    user.Avatar,
                                    user.BirthDay,
                                    user.Caption,
                                    user.Email,
                                    user.Fullname
                                }
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new ResultModel()
                {
                    Error = SignInErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Updates user's profile.
        /// </summary>
        /// <param name="model">The Profile model includes user name and password.</param>
        /// <returns>JSON object</returns>
        [Route("update")]
        [HttpPost]
        public IHttpActionResult Update(ProfileModel model)
        {
            if (ModelState.IsValid)
            {
                // Update account's profile
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = ProfileErrors.UserNotExistence,
                            ErrorMessage = "User does not exist"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = ProfileErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            user.Fullname = model.FullName;
                            user.Gender = model.Gender;
                            user.BirthDay = model.Birthday;
                            user.Caption = model.Caption;
                            user.Email = model.Email;
                            entities.SaveChanges();

                            return Json(new ResultModel()
                            {
                                Error = ProfileErrors.Success,
                                ErrorMessage = "Update profile successfully"
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = ProfileErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new ResultModel()
                {
                    Error = ProfileErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Updates user's avatar.
        /// </summary>
        /// <param name="model">The Sign in model includes user name and password.</param>
        /// <returns>JSON object</returns>
        [Route("update/avatar")]
        [HttpPost]
        public async Task<IHttpActionResult> UpdateAvatar()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            // Update account's profile
            using (var entities = new BentoEntities())
            {
                try
                {
                    var uploadFolderPath = HostingEnvironment.MapPath("~/Upload/Avatar/");

                    var streamProvider = new NamedMultipartFormDataStreamProvider(uploadFolderPath, Guid.NewGuid().ToString());
                    var result = await Request.Content.ReadAsMultipartAsync(streamProvider);

                    if (result.FileData.Count < 1)
                    {
                        return Json(new ResultModel()
                        {
                            Error = ProfileErrors.OtherException,
                            ErrorMessage = "Cannot upload file"
                        });
                    }

                    var username = result.FormData["username"].ToLower();
                    var password = result.FormData["password"];
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == username && u.Password == password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = ProfileErrors.UserNotExistence,
                            ErrorMessage = "User does not exist"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = ProfileErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        var path = result.FileData.Select(entry => entry.LocalFileName).FirstOrDefault();

                        user.Avatar = Url.Content("~/Upload/Avatar/") + Path.GetFileName(path);
                        entities.SaveChanges();

                        return Json(new ResultModel()
                        {
                            Error = ProfileErrors.Success,
                            ErrorMessage = "Update profile successfully"
                        });
                    }
                }
                catch (Exception exc)
                {
                    return Json(new ResultModel()
                    {
                        Error = ProfileErrors.DatabaseFailed,
                        ErrorMessage = exc.ToString()
                    });
                }
            }
        }

        /// <summary>
        /// Get interests of user
        /// </summary>
        /// <param name="model">The Sign in model includes user name and password.</param>
        /// <returns>JSON object</returns>
        [Route("interests")]
        [HttpPost]
        public IHttpActionResult Interests(SignInModel model)
        {
            if (ModelState.IsValid)
            {
                // Get account's interests
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = SignInErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var interests = entities.GetUserInterests(user.ID).ToList();
                            return Json(new ResultDataModel()
                            {
                                Error = SignInErrors.Success,
                                ErrorMessage = "Get interests of user successfully",
                                Data = interests
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = SignInErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Updates the interests.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>IHttpActionResult.</returns>
        [Route("interests/update")]
        [HttpPost]
        public IHttpActionResult UpdateInterests(UpdateInterestModel model)
        {
            if (ModelState.IsValid)
            {
                // Get account's interests
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = SignInErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var result = entities.SetUserInterests(user.ID, model.Interests);
                            return Json(new ResultDataModel()
                            {
                                Error = SignInErrors.Success,
                                ErrorMessage = "Update interests of user successfully"
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = SignInErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        public class NamedMultipartFormDataStreamProvider : MultipartFormDataStreamProvider
        {
            private string fileName;

            public NamedMultipartFormDataStreamProvider(string rootPath, string fileName)
                : base(rootPath)
            {
                this.fileName = fileName;
            }

            public override string GetLocalFileName(System.Net.Http.Headers.HttpContentHeaders headers)
            {
                var filename = headers.ContentDisposition.FileName.Trim(new char[] { '"' });
                var extension = !string.IsNullOrWhiteSpace(filename) ? Path.GetExtension(filename) : "";
                return this.fileName + extension;
            }
        }
    }
}