﻿using BentoAdm.Models;
using BentoAdm.Models.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;

namespace BentoAdm.Controllers.Api
{
    [System.Web.Http.RoutePrefix("api/interests")]
    public class InterestsController : ApiController
    {
        /// <summary>
        /// Gets the list of interests.
        /// </summary>
        /// <param name="model">The Sign in model includes user name and password.</param>
        /// <returns>JSON object</returns>
        [System.Web.Http.Route("")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult Get(SignInModel model)
        {
            if (ModelState.IsValid)
            {
                // Get list of interests
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }
                        try
                        {
                            var interests = entities.Interests
                                .Where(i => i.ParentId.HasValue == false)
                                .Select(i => new
                                {
                                    i.ID,
                                    i.Name,
                                    i.Slug,
                                    i.Thumbnail
                                })
                                .ToList();
                            return Json(new ResultDataModel()
                            {
                                Error = SignInErrors.Success,
                                ErrorMessage = "Get interests successfully",
                                Data = interests
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = SignInErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Gets the list of interests.
        /// </summary>
        /// <param name="model">The Sign in model includes user name and password.</param>
        /// <returns>JSON object</returns>
        [System.Web.Http.Route("structure")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult GetStructure(SignInModel model)
        {
            if (ModelState.IsValid)
            {
                // Get list of interests
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }
                        try
                        {
                            var interests = entities.Interests
                                .Select(i => new InterestModel
                                {
                                    Id = i.ID,
                                    Name = i.Name,
                                    Slug = i.Slug,
                                    Thumbnail = i.Thumbnail,
                                    ParentId = i.ParentId,
                                    Total = entities.ArticleOfCategories.Count(a => entities.InterestOfCategories.Where(ioc => ioc.InterestID == i.ID).Select(ioc => ioc.CategoryID).Contains(a.CategoryID))
                                })
                                .ToList();

                            var structureInterests = StructurizeInterest(interests, null, 1);

                            return Json(new ResultDataModel()
                            {
                                Error = SignInErrors.Success,
                                ErrorMessage = "Get interests successfully",
                                Data = structureInterests
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = SignInErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Gets the list of interests by parent id.
        /// </summary>
        /// <param name="model">The Sign in model includes user name and password.</param>
        /// <returns>JSON object</returns>
        [System.Web.Http.Route("{id}/children")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult GetTopicByParentId([FromUri] Guid id, [FromBody] LoadRequestModel model)
        {
            if (ModelState.IsValid)
            {
                // Get list of interests
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }
                        try
                        {
                            var interests = entities.Interests.Where(p => p.ParentId == id)
                                .Select(i => new InterestModel
                                {
                                    Id = i.ID,
                                    Name = i.Name,
                                    Thumbnail = i.Thumbnail,
                                    ParentId = i.ParentId,
                                    Total = entities.ArticleOfCategories.Count(a => entities.InterestOfCategories.Where(ioc => ioc.InterestID == i.ID).Select(ioc => ioc.CategoryID).Contains(a.CategoryID))
                                }).ToList();
                                

                            return Json(new ResultDataModel()
                            {
                                Error = SignInErrors.Success,
                                ErrorMessage = "Get interests successfully",
                                Data = interests
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = SignInErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Gets the list of interests.
        /// </summary>
        /// <param name="model">The Sign in model includes user name and password.</param>
        /// <returns>JSON object</returns>
        [System.Web.Http.Route("structure/user")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult GetUserStructure(SignInModel model)
        {
            if (ModelState.IsValid)
            {
                // Get list of interests
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }
                        try
                        {
                            var interests = entities.Interests
                                .Select(i => new InterestModel
                                {
                                    Id = i.ID,
                                    Name = i.Name,
                                    Slug = i.Slug,
                                    Thumbnail = i.Thumbnail,
                                    ParentId = i.ParentId,
                                    Total = entities.ArticleOfCategories.Count(a => entities.InterestOfCategories.Where(ioc => ioc.InterestID == i.ID).Select(ioc => ioc.CategoryID).Contains(a.CategoryID))
                                })
                                .ToList();

                            var structureInterests = StructurizeInterest(interests, null, 1);

                            var userInterests = entities.InterestOfUsers.Where(iou => iou.ProfileID == user.ID)
                                    .Select(iou => iou.InterestID);

                            for (var i = structureInterests.Count - 1; i >= 0; i--)
                            {
                                if (!userInterests.Contains(structureInterests[i].Id) && (structureInterests[i].ParentId.HasValue == false || (structureInterests[i].ParentId.HasValue && !userInterests.Contains(structureInterests[i].ParentId.Value))))
                                {
                                    structureInterests.RemoveAt(i);
                                }
                            }

                            return Json(new ResultDataModel()
                            {
                                Error = SignInErrors.Success,
                                ErrorMessage = "Get user's interests successfully",
                                Data = structureInterests
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = SignInErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Gets the list of interests.
        /// </summary>
        /// <param name="model">The Sign in model includes user name and password.</param>
        /// <returns>JSON object</returns>
        [System.Web.Http.Route("structure/favorite")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult GetUserFavoriteStructure(SignInModel model)
        {
            if (ModelState.IsValid)
            {
                // Get list of interests
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }
                        try
                        {
                            var interests = entities.ArticleOfCategories
                                .Where(aoc => entities.Favorites.Any(l => l.ObjectID == aoc.ArticleID && l.ProfileID == user.ID))
                                .GroupBy(aoc => aoc.CategoryID)
                                .Join(entities.InterestOfCategories, g => g.Key, ioc => ioc.CategoryID,
                                    (g, ioc) => new
                                    {
                                        CategoryID = g.Key,
                                        InterestID = ioc.InterestID,
                                        Total = g.Count()
                                    })
                                .GroupBy(t => t.InterestID)
                                .Join(entities.Interests, g => g.Key, i => i.ID,
                                    (g, i) => new InterestModel()
                                    {
                                        Id = i.ID,
                                        Slug = i.Slug,
                                        Name = i.Name,
                                        Thumbnail = i.Thumbnail,
                                        ParentId = i.ParentId,
                                        Total = g.Sum(it => it.Total)
                                    })
                                .ToList();

                            // Add parents
                            var parents = new List<InterestModel>();
                            foreach (var i in interests)
                            {
                                if (i.ParentId != null)
                                {
                                    if (!parents.Any(p => p.Id == i.ParentId))
                                    {
                                        // New parent
                                        parents.Add(entities.Interests
                                            .Where(itr => itr.ID == i.ParentId)
                                            .Select(itr => new InterestModel()
                                            {
                                                Id = itr.ID,
                                                Slug = itr.Slug,
                                                Name = itr.Name,
                                                Thumbnail = itr.Thumbnail,
                                                ParentId = itr.ParentId,
                                                Total = 0
                                            }).FirstOrDefault());
                                    }
                                }
                            }

                            interests.AddRange(parents);
                            var structureInterests = StructurizeInterest(interests, null, 1);

                            var userInterests = entities.InterestOfUsers.Where(iou => iou.ProfileID == user.ID)
                                    .Select(iou => iou.InterestID);

                            for (var i = structureInterests.Count - 1; i >= 0; i--)
                            {
                                if (!userInterests.Contains(structureInterests[i].Id) && (structureInterests[i].ParentId.HasValue == false || (structureInterests[i].ParentId.HasValue && !userInterests.Contains(structureInterests[i].ParentId.Value))))
                                {
                                    structureInterests.RemoveAt(i);
                                }
                            }

                            return Json(new ResultDataModel()
                            {
                                Error = SignInErrors.Success,
                                ErrorMessage = "Get user's interests successfully",
                                Data = structureInterests
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = SignInErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = SignInErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        private static List<InterestModel> StructurizeInterest(IEnumerable<InterestModel> list, Guid? parentId, int level)
        {
            var result = new List<InterestModel>();
            foreach (var node in list.Where(i => i.ParentId == parentId))
            {
                var childNodes = StructurizeInterest(list, node.Id, level + 1);
                var newNode = new InterestModel()
                {
                    Id = node.Id,
                    Name = node.Name,
                    Slug = node.Slug,
                    Thumbnail = node.Thumbnail,
                    ParentId = node.ParentId,
                    Total = node.Total + childNodes.Sum(n => n.Total),
                    Level = level
                };
                result.Add(newNode);
                result.AddRange(childNodes);
            }

            return result;
        }

        /// <summary>
        /// Get article list of the specified interest
        /// </summary>
        /// <param name="id">The interest's identifier.</param>
        /// <param name="model">The Load request model.</param>
        /// <returns>JSON object</returns>
        [System.Web.Http.Route("{id}/articles")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult Articles([FromUri] Guid id, [FromBody] LoadRequestModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Page <= 0)
                {
                    model.Page = 1;
                }

                if (model.ItemsPerPage <= 0)
                {
                    model.ItemsPerPage = 10;
                }

                // Get articles of interest
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var articles = entities.GetArticlesOfInterest(user.ID, id, model.Page, model.ItemsPerPage).ToList();
                            return Json(new ResultDataModel()
                            {
                                Error = LoadRequestErrors.Success,
                                ErrorMessage = "Get articles of interest successfully",
                                Data = articles
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = LoadRequestErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Get article list of the specified interest
        /// </summary>
        /// <param name="id">The interest's identifier.</param>
        /// <param name="model">The Load request model.</param>
        /// <returns>JSON object</returns>
        [System.Web.Http.Route("{id}/articlesubtopics")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult ArticlesSubTopic([FromUri] Guid id, [FromBody] LoadRequestModel model)
        {
            if (ModelState.IsValid)
            {
                // Get articles of interest
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var topics = entities.Interests.Where(p => p.ParentId == id).ToList();
                            List<object> articles = new List<object>();
                            foreach (Interest item in topics)
                            {
                                var twoarticle = (from p in entities.Articles
                                                  join c in entities.ArticleOfCategories on p.ID equals c.ArticleID
                                                  join d in entities.InterestOfCategories on c.CategoryID equals d.CategoryID
                                                  where d.InterestID == item.ID && p.IsPublished == true
                                                  orderby p.DateCreated descending
                                                  orderby p.PublishedDate descending
                                                  select new
                                                  {
                                                      ID = p.ID,
                                                      Title = p.Title,
                                                      Thumbnail = p.Thumbnail,
                                                      ShortDescription = p.ShortDescription,
                                                      Author = p.Author,
                                                      Likes = entities.Likes.Join(entities.ObjectIndexes, l => l.ObjectID, o => o.ID, (l, o) => new { Like = l, ObjectIndex = o }).Where(rs => rs.Like.ObjectID == p.ID && rs.ObjectIndex.ObjectType.ToLower() == "article" && rs.Like.Action == 1).Count(),
                                                      Liked = entities.Likes.Join(entities.ObjectIndexes, l => l.ObjectID, o => o.ID, (l, o) => new { Like = l, ObjectIndex = o }).Where(zs => zs.Like.ObjectID == p.ID && zs.Like.Action == 1 && zs.ObjectIndex.ObjectType.ToLower() == "article" && zs.Like.ProfileID == user.ID).Count() > 0 ? 1 : 0,
                                                      Categories = item.ID,
                                                      DateCreated = p.DateCreated
                                                  }).Skip(0).Take(2).ToList();
                                if (twoarticle.Count > 0)
                                {
                                    foreach (var ob in twoarticle)
                                    {
                                        articles.Add(ob);
                                    }
                                }
                            }
                            return Json(new ResultDataModel()
                            {
                                Error = LoadRequestErrors.Success,
                                ErrorMessage = "Get ArticlesSubTopic successfully",
                                Data = articles
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = LoadRequestErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Get user's article list of the specified interest
        /// </summary>
        /// <param name="id">The interest's identifier.</param>
        /// <param name="model">The Load request model.</param>
        /// <returns>JSON object</returns>
        [System.Web.Http.Route("{id}/myarticlesubtopics")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult MyArticlesSubTopic([FromUri] Guid id, [FromBody] LoadRequestModel model)
        {
            if (ModelState.IsValid)
            {
                // Get articles of interest
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var interests = entities.ArticleOfCategories
                                .Where(aoc => entities.Favorites.Any(l => l.ObjectID == aoc.ArticleID && l.ProfileID == user.ID))
                                .GroupBy(aoc => aoc.CategoryID)
                                .Join(entities.InterestOfCategories, g => g.Key, ioc => ioc.CategoryID,
                                    (g, ioc) => new
                                    {
                                        CategoryID = g.Key,
                                        InterestID = ioc.InterestID,
                                        Total = g.Count()
                                    })
                                .GroupBy(t => t.InterestID)
                                .Join(entities.Interests, g => g.Key, i => i.ID,
                                    (g, i) => new InterestModel()
                                    {
                                        Id = i.ID,
                                        Slug = i.Slug,
                                        Name = i.Name,
                                        Thumbnail = i.Thumbnail,
                                        ParentId = i.ParentId,
                                        Total = g.Sum(it => it.Total)
                                    }).Where(e=>e.ParentId == id)
                                .ToList();

                            List<object> articles = new List<object>();
                            foreach (InterestModel item in interests)
                            {
                                var twoarticle = (from p in entities.Articles
                                                  join c in entities.ArticleOfCategories on p.ID equals c.ArticleID
                                                  join d in entities.InterestOfCategories on c.CategoryID equals d.CategoryID
                                                  join e in entities.Favorites on c.ArticleID equals e.ObjectID
                                                  join f in entities.ObjectIndexes on e.ObjectID equals f.ID
                                                  where d.InterestID == item.Id && p.IsPublished == true && f.ObjectType.ToLower() == "article" && e.ProfileID == user.ID
                                                  orderby p.DateCreated descending
                                                  orderby p.PublishedDate descending
                                                  select new
                                                  {
                                                      ID = p.ID,
                                                      Title = p.Title,
                                                      Thumbnail = p.Thumbnail,
                                                      ShortDescription = p.ShortDescription,
                                                      Author = p.Author,
                                                      Likes = entities.Likes.Join(entities.ObjectIndexes, l => l.ObjectID, o => o.ID, (l, o) => new { Like = l, ObjectIndex = o }).Where(rs => rs.Like.ObjectID == p.ID && rs.ObjectIndex.ObjectType.ToLower() == "article" && rs.Like.Action == 1).Count(),
                                                      Liked = entities.Likes.Join(entities.ObjectIndexes, l => l.ObjectID, o => o.ID, (l, o) => new { Like = l, ObjectIndex = o }).Where(zs => zs.Like.ObjectID == p.ID && zs.Like.Action == 1 && zs.ObjectIndex.ObjectType.ToLower() == "article" && zs.Like.ProfileID == user.ID).Count() > 0 ? 1 : 0,
                                                      Categories = item.Id,
                                                      DateCreated = p.DateCreated
                                                  }).Skip(0).Take(2).ToList();
                                if (twoarticle.Count > 0)
                                {
                                    foreach (var ob in twoarticle)
                                    {
                                        articles.Add(ob);
                                    }
                                }
                            }
                            return Json(new ResultDataModel()
                            {
                                Error = LoadRequestErrors.Success,
                                ErrorMessage = "Get ArticlesSubTopic successfully",
                                Data = articles
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = LoadRequestErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Get article list of the specified interest of specified user
        /// </summary>
        /// <param name="id">The interest's identifier.</param>
        /// <param name="model">The Load request model.</param>
        /// <returns>JSON object</returns>
        [System.Web.Http.Route("{id}/myarticles")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult MyArticles([FromUri] Guid id, [FromBody] LoadRequestModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Page <= 0)
                {
                    model.Page = 1;
                }

                if (model.ItemsPerPage <= 0)
                {
                    model.ItemsPerPage = 10;
                }

                // Get favorite articles of interest
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var articles = entities.GetUserArticlesOfInterest(user.ID, id, model.Page, model.ItemsPerPage).ToList();
                            return Json(new ResultDataModel()
                            {
                                Error = LoadRequestErrors.Success,
                                ErrorMessage = "Get articles of interest successfully",
                                Data = articles
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = LoadRequestErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        [System.Web.Http.Route("{id}/myfavorites")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult MyFavorites([FromUri] Guid id, [FromBody] LoadRequestModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Page <= 0)
                {
                    model.Page = 1;
                }

                if (model.ItemsPerPage <= 0)
                {
                    model.ItemsPerPage = 10;
                }

                // Get favorite articles of interest
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var articles = entities.GetUserFavoriteArticlesOfInterest(user.ID, id, model.Page, model.ItemsPerPage).ToList();
                            return Json(new ResultDataModel()
                            {
                                Error = LoadRequestErrors.Success,
                                ErrorMessage = "Get favorite articles of interest successfully",
                                Data = articles
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = LoadRequestErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Get article list of the specified interest by Slug
        /// </summary>
        /// <param name="slug">The interest's slug.</param>
        /// <param name="model">The Load request model.</param>
        /// <returns>JSON object</returns>
        [System.Web.Http.Route("{slug}/slugarticles")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult Articles([FromUri] string slug, [FromBody] LoadRequestModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Page <= 0)
                {
                    model.Page = 1;
                }

                if (model.ItemsPerPage <= 0)
                {
                    model.ItemsPerPage = 10;
                }

                // Get articles of interest
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var articles = entities.GetArticlesOfInterestBySlug(user.ID, slug, model.Page, model.ItemsPerPage).ToList();
                            return Json(new ResultDataModel()
                            {
                                Error = LoadRequestErrors.Success,
                                ErrorMessage = "Get articles of interest successfully",
                                Data = articles
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = LoadRequestErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Get article list of the specified interest of specified user by slug
        /// </summary>
        /// <param name="slug">The interest's identifier.</param>
        /// <param name="model">The Load request model.</param>
        /// <returns>JSON object</returns>
        [System.Web.Http.Route("{slug}/slugmyarticles")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult MyArticles([FromUri] string slug, [FromBody] LoadRequestModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Page <= 0)
                {
                    model.Page = 1;
                }

                if (model.ItemsPerPage <= 0)
                {
                    model.ItemsPerPage = 10;
                }

                // Get favorite articles of interest
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var articles = entities.GetUserArticlesOfInterestBySlug(user.ID, slug, model.Page, model.ItemsPerPage).ToList();
                            return Json(new ResultDataModel()
                            {
                                Error = LoadRequestErrors.Success,
                                ErrorMessage = "Get articles of interest successfully",
                                Data = articles
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = LoadRequestErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }

        /// <summary>
        /// Get article list of the favorite interest of specified user by Slug
        /// </summary>
        /// <param name="slug">The interest's identifier.</param>
        /// <param name="model">The Load request model.</param>
        /// <returns>JSON object</returns>
        [System.Web.Http.Route("{slug}/slugmyfavorites")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult MyFavorites([FromUri] string slug, [FromBody] LoadRequestModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Page <= 0)
                {
                    model.Page = 1;
                }

                if (model.ItemsPerPage <= 0)
                {
                    model.ItemsPerPage = 10;
                }

                // Get favorite articles of interest
                using (var entities = new BentoEntities())
                {
                    var user = entities.Profiles.FirstOrDefault(u => u.Username.ToLower() == model.Username.ToLower() && u.Password == model.Password);
                    if (user == null)
                    {
                        // Not found
                        return Json(new ResultModel()
                        {
                            Error = LoadRequestErrors.AuthenticationFailed,
                            ErrorMessage = "Wrong username or password"
                        });
                    }
                    else
                    {
                        if (user.IsSigningIn == false)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.NotAuthenticated,
                                ErrorMessage = "Not sign in"
                            });
                        }

                        try
                        {
                            var articles = entities.GetUserFavoriteArticlesOfInterestBySlug(user.ID, slug, model.Page, model.ItemsPerPage).ToList();
                            return Json(new ResultDataModel()
                            {
                                Error = LoadRequestErrors.Success,
                                ErrorMessage = "Get favorite articles of interest successfully",
                                Data = articles
                            });
                        }
                        catch (Exception exc)
                        {
                            return Json(new ResultModel()
                            {
                                Error = LoadRequestErrors.DatabaseFailed,
                                ErrorMessage = "Database connection failed"
                            });
                        }
                    }
                }
            }
            else
            {
                // Return first error of ModelState
                return Json(new
                {
                    Error = LoadRequestErrors.OtherException,
                    ErrorMessage = ModelState.Values.SelectMany(s => s.Errors).Select(e => e.ErrorMessage).FirstOrDefault()
                });
            }
        }
    }
}
