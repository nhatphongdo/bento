﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;

namespace BentoAdm.Controllers
{
    public class AdminController : Controller
    {
        public static List<Models.InterestList> BuildInterestList(List<Models.InterestList> list, Guid? parentID, int level)
        {
            var result = new List<Models.InterestList>();
            foreach (var interest in list.Where(w => w.ParentID == parentID))
            {
                interest.Level = level;
                result.Add(interest);
                result.AddRange(BuildInterestList(list, interest.ID, level + 1));
            }

            return result;
        }

        public static string GetIPAddress()
        {
            var context = System.Web.HttpContext.Current;
            var sIPAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(sIPAddress))
            {
                return context.Request.ServerVariables["REMOTE_ADDR"];
            }
            else
            {
                var ipArray = sIPAddress.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                return ipArray[0];
            }
        }

        public bool IsLogIn()
        {
            if (Request.Cookies["bentologin"] != null)
            {
                if (Request.Cookies["bentologin"]["Time"] != null && Request.Cookies["bentologin"]["IsLog"] != null)
                {
                    if (Request.Cookies["bentologin"]["IsLog"] == "true")
                    { return true; }
                }
            }
            return (Session["LoggedIn"] != null && (bool)Session["LoggedIn"] == true);
        }

        public ActionResult Login()
        {
            if (IsLogIn())
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Login(FormCollection fOrm)
        {
            if (fOrm["Password"] == "BENTO123!@#")
            {
                HttpCookie myCookie = new HttpCookie("bentologin");
                myCookie["Time"] = DateTime.Now.ToShortDateString();
                myCookie["IsLog"] = "true";
                myCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(myCookie);

                Session.Add("LoggedIn", true);
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.ErrMsg = "Wrong Password!!!";
            }
            return View();
        }

        public ActionResult Logout()
        {
            if (Request.Cookies["bentologin"] != null)
            {
                HttpCookie myCookie = new HttpCookie("bentologin");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
                return RedirectToAction("Login");
            }

            Session.Remove("LoggedIn");
            Session.Remove("ErrMsg");
            return RedirectToAction("Login");
        }

        //
        // GET: /Admin/

        public ActionResult Index()
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            ViewBag.TotalLike = BentoDB.Likes.Count();

            ViewBag.TotalShare = BentoDB.Shares.Count();

            ViewBag.TotalRating = BentoDB.Ratings.Count();

            ViewBag.TotalComment = BentoDB.Comments.Count();

            ViewBag.TotalNote = BentoDB.Notes.Count();

            ViewBag.TotalProfile = BentoDB.Profiles.Count();
            ViewBag.TotalBanned = BentoDB.Profiles.Where(w => w.Banned == true).Count();
            ViewBag.TotalSigningIn = BentoDB.Profiles.Where(w => w.IsSigningIn == true).Count();

            ViewBag.TotalArticle = BentoDB.Articles.Count();
            ViewBag.TotalArticlePublished = BentoDB.Articles.Where(w => w.IsPublished == true).Count();
            ViewBag.TotalArticleUnPublished = BentoDB.Articles.Where(w => w.IsPublished == false).Count();

            ViewBag.TotalMessage = BentoDB.Contacts.Count();

            ViewBag.TotalSubscribe = BentoDB.Subscribes.Count();

            List<Models.Profile> plist = BentoDB.Profiles.Where(w => w.Banned == false).OrderByDescending(o => o.LastSignedIn).Take(10).ToList();
            List<Models.Profile> banlist = BentoDB.Profiles.Where(w => w.Banned).OrderByDescending(o => o.LastSignedIn).Take(10).ToList();
            List<Models.Article> alist = BentoDB.Articles.Where(w => w.IsPublished).OrderByDescending(o => o.DateCreated).Take(5).ToList();

            Tuple<List<Models.Profile>, List<Models.Profile>, List<Models.Article>> tuple
                = new Tuple<List<Models.Profile>, List<Models.Profile>, List<Models.Article>>(plist, banlist, alist);

            return View(tuple);
        }

        BentoAdm.Models.BentoEntities BentoDB = new Models.BentoEntities();

        public static List<SelectListItem> AccountTypeItems = new List<SelectListItem>(new[] {
                new SelectListItem() { Text="Normal", Value="Normal"},
                new SelectListItem() { Text="Facebook", Value="Facebook"},
                new SelectListItem() { Text="Google+", Value="Google+"},
                //new SelectListItem() { Text="Administrator", Value="Administrator"},
            });

        public static List<SelectListItem> GenderItems = new List<SelectListItem>(new[] {
                new SelectListItem() { Text="None", Value="0"},
                new SelectListItem() { Text="Male", Value="1"},
                new SelectListItem() { Text="Female", Value="2"},
            });

        public static List<SelectListItem> StatusItems = new List<SelectListItem>(new[] {
                new SelectListItem() { Text="Default", Value="0"},
                new SelectListItem() { Text="Status 1", Value="1"},
                new SelectListItem() { Text="Status 2", Value="2"},
                new SelectListItem() { Text="Status 3", Value="3"},
                new SelectListItem() { Text="Status 4", Value="4"}
            });

        public static List<SelectListItem> IsPublishedItems = new List<SelectListItem>(new[] {
                new SelectListItem() { Text="False", Value="false"},
                new SelectListItem() { Text="True", Value="true"},
            });

        #region Create New
        public ActionResult NewUserProfile()
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            ViewData.Add("AccountTypeItems", AccountTypeItems);

            ViewData.Add("GenderItems", GenderItems);

            ViewData.Add("Banned", IsPublishedItems);

            return View();
        }

        [HttpPost]
        public ActionResult NewUserProfile(Models.Profile profile, HttpPostedFileBase avatar)
        {
            if (ModelState.IsValid
                //&& profile.Username != null
                && ((profile.Email != null && profile.Password != null)
                    || (profile.AccountID != null && profile.AccountType != null))
                )
            {
                try
                {
                    //default
                    profile.ID = Guid.NewGuid();
                    profile.DateCreated = DateTime.Now;
                    profile.LastSignedIn = DateTime.Now;
                    profile.IsSigningIn = false;
                    profile.IP = GetIPAddress();
                    //manual
                    profile.Username = profile.Email;
                    using (MD5 md5Hash = MD5.Create())
                    {
                        string hash = GetMd5Hash(md5Hash, profile.Password);
                        profile.Password = hash;
                    }

                    var image = WebImage.GetImageFromRequest();
                    if (image != null)
                    {
                        var min = image.Width > image.Height ? image.Height : image.Width;
                        if (min > 800)
                        {
                            image.Resize(800, 800);
                        }
                        var path = string.Empty;
                        if (avatar != null)
                        {
                            string filename = profile.ID + Path.GetExtension(avatar.FileName);
                            string root = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~/");
                            path = HttpContext.Server.MapPath("~/Upload/Avatar/") + filename;
                            //avatar.SaveAs(HttpContext.Server.MapPath("~/Upload/Avatar/") + filename);
                            profile.Avatar = root + "Upload/Avatar/" + filename;
                        }
                        image.Save(path);
                    }
                    BentoDB.Profiles.Add(profile);

                    BentoAdm.Models.ObjectIndex obj = new Models.ObjectIndex();
                    obj.ID = profile.ID;
                    obj.DateCreated = DateTime.Now;
                    obj.ObjectType = "Profile";
                    BentoDB.ObjectIndexes.Add(obj);

                    BentoDB.SaveChanges();

                    return this.RedirectToAction("UserProfiles");
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Username cannot be empty; (Email and Password) or (Account Type and ID) cannot be empty";

            ViewData.Add("AccountTypeItems", AccountTypeItems);

            ViewData.Add("GenderItems", GenderItems);

            ViewData.Add("Banned", IsPublishedItems);

            return View(profile);
        }

        public ActionResult NewCategory()
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }
            return RedirectToAction("NewInterest");

            ViewData.Add("StatusItems", StatusItems);

            return View();
        }

        [HttpPost]
        public ActionResult NewCategory(Models.Category category)
        {
            return RedirectToAction("Interest");

            if (ModelState.IsValid)
            {
                try
                {
                    category.ID = Guid.NewGuid();
                    category.DateCreated = DateTime.Now;
                    BentoDB.Categories.Add(category);

                    BentoAdm.Models.ObjectIndex objc = new Models.ObjectIndex();
                    objc.ID = category.ID;
                    objc.DateCreated = DateTime.Now;
                    objc.ObjectType = "Category";
                    BentoDB.ObjectIndexes.Add(objc);

                    BentoDB.SaveChanges();

                    return this.RedirectToAction("Category");
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            ViewData.Add("StatusItems", StatusItems);

            return View(category);
        }

        public ActionResult NewInterest()
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            ViewData.Add("StatusItems", StatusItems);

            #region select list Interest
            //var resuft = this.BentoDB.Interests.OrderBy(o => o.ParentId).ToList();
            //List<Models.InterestList> interestList = new List<Models.InterestList>();

            //foreach (var row in resuft)
            //{
            //    var category = this.BentoDB.Categories.Find(
            //        this.BentoDB.InterestOfCategories.Where(w => w.InterestID == row.ID).Select(s => s.CategoryID).First());

            //    var parent = this.BentoDB.Interests.Find(row.ParentId);
            //    var parentID = Guid.Empty;
            //    var parentName = string.Empty;

            //    if (parent != null)
            //    {
            //        parentName = parent.Name;
            //        parentID = parent.ID;
            //    }

            //    interestList.Add(new Models.InterestList
            //    {
            //        ID = row.ID,
            //        DateCreated = row.DateCreated,
            //        Name = row.Name,
            //        Thumbnail = row.Thumbnail,
            //        Selected = false,
            //        CategoryID = category.ID,
            //        CategoryName = category.Name,
            //        ParentID = parentID,
            //        ParentName = parentName
            //    });

            //}

            ////ViewData["Interests"] = BentoDB.Interests.OrderBy(o => o.Name).Select(x => new SelectListItem
            ////{
            ////    Text = x.Name,
            ////    Value = x.ID.ToString()
            ////}).ToList();

            //ViewData["Interests"] = interestList.OrderBy(o => o.ParentName).ToList();
            #endregion

            var resuft = this.BentoDB.Interests
                                .OrderBy(o => o.Name)
                                .Select(s => new BentoAdm.Models.InterestList()
                                {
                                    ID = s.ID,
                                    Name = s.Name,
                                    ParentID = s.ParentId,
                                }).ToList();

            var structuredInterest = BuildInterestList(resuft, null, 1);

            var listInterest = new List<SelectListItem>();
            listInterest.Add(new SelectListItem()
            {
                Value = "",
                Text = "Không có đơn vị cha"
            });
            foreach (var intrst in structuredInterest)
            {
                listInterest.Add(new SelectListItem()
                {
                    Value = intrst.ID.ToString(),
                    Text = new string('-', 10 * (intrst.Level - 1)) + " " + intrst.Name
                });
            }
            ViewBag.Interests = listInterest;

            return View();
        }

        [HttpPost]
        public ActionResult NewInterest(Models.Interest interest, HttpPostedFileBase thumbnail)
        {
            if (ModelState.IsValid)
            {
                Models.Interest slugOfInterest = this.BentoDB.Interests.Where(s => s.Slug == interest.Slug).First();
                if (slugOfInterest == null)
                {

                    try
                    {
                        interest.ID = Guid.NewGuid();
                        interest.DateCreated = DateTime.Now;

                        var image = WebImage.GetImageFromRequest();
                        if (image != null)
                        {
                            var min = image.Width > image.Height ? image.Height : image.Width;
                            if (min > 800)
                            {
                                image.Resize(800, 800);
                            }
                            var path = string.Empty;
                            if (thumbnail != null)
                            {
                                string filename = interest.ID + Path.GetExtension(thumbnail.FileName);
                                string root = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~/");
                                path = HttpContext.Server.MapPath("~/Upload/Interest/") + filename;
                                //thumbnail.SaveAs(HttpContext.Server.MapPath("~/Upload/Interest/") + filename);
                                interest.Thumbnail = root + "Upload/Interest/" + filename;
                            }
                            image.Save(path);
                        }

                        if (Request.Form["parentid"] != null)
                        {
                            string selection = Request.Form["parentid"];
                            Guid? pid = null;
                            if (selection != "")
                            {
                                pid = Guid.Parse(selection);
                            }
                            interest.ParentId = pid;
                        }
                        BentoDB.Interests.Add(interest);

                        //if (Request.Form["categories"] != null)
                        //{
                        //    foreach (var selection in Request.Form["categories"].Split(','))
                        //    {
                        //        Models.InterestOfCategory ioc = new Models.InterestOfCategory();
                        //        ioc.ID = Guid.NewGuid();
                        //        ioc.DateCreated = DateTime.Now;
                        //        ioc.InterestID = interest.ID;
                        //        ioc.CategoryID = Guid.Parse(selection);
                        //        BentoDB.InterestOfCategories.Add(ioc);
                        //    }
                        //}

                        BentoAdm.Models.ObjectIndex obji = new Models.ObjectIndex();
                        obji.ID = interest.ID;
                        obji.DateCreated = DateTime.Now;
                        obji.ObjectType = "Interest";
                        BentoDB.ObjectIndexes.Add(obji);

                        //--//
                        BentoAdm.Models.Category category = new Models.Category();
                        category.ID = Guid.NewGuid();
                        category.DateCreated = DateTime.Now;
                        category.Name = interest.Name;
                        category.Status = interest.Status;
                        BentoDB.Categories.Add(category);

                        BentoAdm.Models.ObjectIndex objc = new Models.ObjectIndex();
                        objc.ID = category.ID;
                        objc.DateCreated = DateTime.Now;
                        objc.ObjectType = "Category";
                        BentoDB.ObjectIndexes.Add(objc);

                        BentoAdm.Models.InterestOfCategory ioc = new Models.InterestOfCategory();
                        ioc.ID = Guid.NewGuid();
                        ioc.DateCreated = DateTime.Now;
                        ioc.InterestID = interest.ID;
                        ioc.CategoryID = category.ID;
                        BentoDB.InterestOfCategories.Add(ioc);

                        BentoDB.SaveChanges();

                        return this.RedirectToAction("Interest");
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                {
                    ViewData["EditError"] = "Sorry, slug already exists.";
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            ViewData.Add("StatusItems", StatusItems);

            #region select list Interest
            //var resuft = this.BentoDB.Interests.OrderBy(o => o.ParentId).ToList();
            //List<Models.InterestList> interestList = new List<Models.InterestList>();

            //foreach (var row in resuft)
            //{
            //    var category = this.BentoDB.Categories.Find(
            //        this.BentoDB.InterestOfCategories.Where(w => w.InterestID == row.ID).Select(s => s.CategoryID).First());

            //    var parent = this.BentoDB.Interests.Find(row.ParentId);
            //    var parentID = Guid.Empty;
            //    var parentName = string.Empty;

            //    if (parent != null)
            //    {
            //        parentName = parent.Name;
            //        parentID = parent.ID;
            //    }

            //    interestList.Add(new Models.InterestList
            //    {
            //        ID = row.ID,
            //        DateCreated = row.DateCreated,
            //        Name = row.Name,
            //        Thumbnail = row.Thumbnail,
            //        Selected = false,
            //        CategoryID = category.ID,
            //        CategoryName = category.Name,
            //        ParentID = parentID,
            //        ParentName = parentName
            //    });

            //}

            ////ViewData["Interests"] = BentoDB.Interests.OrderBy(o => o.Name).Select(x => new SelectListItem
            ////{
            ////    Text = x.Name,
            ////    Value = x.ID.ToString()
            ////}).ToList();

            //ViewData["Interests"] = interestList.OrderBy(o => o.ParentName).ToList();
            #endregion

            var resuft = this.BentoDB.Interests
                                .OrderBy(o => o.Name)
                                .Select(s => new BentoAdm.Models.InterestList()
                                {
                                    ID = s.ID,
                                    Name = s.Name,
                                    ParentID = s.ParentId,
                                }).ToList();

            var structuredInterest = BuildInterestList(resuft, null, 1);

            var listInterest = new List<SelectListItem>();
            listInterest.Add(new SelectListItem()
            {
                Value = "",
                Text = "Không có đơn vị cha"
            });
            foreach (var intrst in structuredInterest)
            {
                listInterest.Add(new SelectListItem()
                {
                    Value = intrst.ID.ToString(),
                    Text = new string('-', 10 * (intrst.Level - 1)) + " " + intrst.Name
                });
            }
            ViewBag.Interests = listInterest;

            return View(interest);
        }

        public ActionResult NewArticle()
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            ViewData.Add("IsPublishedItems", IsPublishedItems);

            List<Models.InterestList> interestList = new List<Models.InterestList>();

            interestList = (from interest in BentoDB.Interests
                            join parent in BentoDB.Interests
                            on interest.ParentId equals parent.ID
                            into ParentAndChild
                            join ioc in BentoDB.InterestOfCategories
                            on interest.ID equals ioc.InterestID
                            into InterestOfCategories
                            from item in InterestOfCategories.DefaultIfEmpty()
                            join category in BentoDB.Categories
                            on item.CategoryID equals category.ID
                            into Category
                            from category in Category.DefaultIfEmpty()
                            from parent in ParentAndChild.DefaultIfEmpty()
                            select new Models.InterestList
                 {
                     ID = interest.ID,
                     DateCreated = interest.DateCreated,
                     Name = interest.Name,
                     Thumbnail = interest.Thumbnail,
                     Selected = false,
                     CategoryID = category.ID,
                     CategoryName = category.Name,
                     ParentID = parent.ID,
                     ParentName = parent.Name
                 }).ToList();

            #region old query
            //var resuft = this.BentoDB.Interests.OrderBy(o => o.ParentId).ToList();

            //foreach (var row in resuft)
            //{
            //    var category = this.BentoDB.Categories.Find(
            //        this.BentoDB.InterestOfCategories.Where(w => w.InterestID == row.ID).Select(s => s.CategoryID).First());

            //    var parent = this.BentoDB.Interests.Find(row.ParentId);
            //    var parentID = Guid.Empty;
            //    var parentName = string.Empty;

            //    if (parent != null)
            //    {
            //        parentName = parent.Name;
            //        parentID = parent.ID;
            //    }

            //    interestList.Add(new Models.InterestList
            //    {
            //        ID = row.ID,
            //        DateCreated = row.DateCreated,
            //        Name = row.Name,
            //        Thumbnail = row.Thumbnail,
            //        Selected = false,
            //        CategoryID = category.ID,
            //        CategoryName = category.Name,
            //        ParentID = parentID,
            //        ParentName = parentName
            //    });

            //}
            #endregion

            //ViewData["Categories"] = BentoDB.Categories.OrderBy(o => o.Name).Select(x => new SelectListItem
            //{
            //    Text = x.Name,
            //    Value = x.ID.ToString()
            //}).ToList();

            List<SelectListItem> SponsorWatermark = BentoDB.Sponsors.Select(s => new SelectListItem() { Value = s.ID.ToString(), Text = s.Name }).ToList();
            ViewData.Add("SponsorWatermark", SponsorWatermark);

            ViewData["Categories"] = interestList.OrderBy(o => o.ParentName).ThenBy(t => t.Name).ToList();

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult NewArticle(Models.Article article, HttpPostedFileBase thumbnail)
        {
            if (ModelState.IsValid)
            {
                Models.Article slugOfArticle = this.BentoDB.Articles.Where(s => s.Slug == article.Slug).First();
                if (slugOfArticle == null)
                {
                    try
                    {
                        article.ID = Guid.NewGuid();
                        article.DateCreated = DateTime.Now;

                        var image = WebImage.GetImageFromRequest();
                        if (image != null)
                        {
                            var min = image.Width > image.Height ? image.Height : image.Width;
                            if (min > 800)
                            {
                                image.Resize(800, 800);
                            }
                            var path = string.Empty;
                            if (thumbnail != null)
                            {
                                string filename = article.ID + Path.GetExtension(thumbnail.FileName);
                                string root = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~/");
                                path = HttpContext.Server.MapPath("~/Upload/Categories/") + filename;
                                //thumbnail.SaveAs(HttpContext.Server.MapPath("~/Upload/Categories/") + filename);
                                article.Thumbnail = root + "Upload/Categories/" + filename;
                            }

                            if (Request.Form["SponsorWatermark"] != null)
                            {
                                var watermarkID = Request.Form["SponsorWatermark"].ToString();
                                var pathwatermark = BentoDB.Sponsors.Find(Guid.Parse(watermarkID)).ImageWatermark;
                                image.AddImageWatermark(Server.MapPath("~/Upload/Sponsors/" + watermarkID + Path.GetExtension(pathwatermark)));
                            }
                            image.Save(path);
                        }
                        BentoDB.Articles.Add(article);

                        if (Request.Form["categories"] != null)
                        {
                            foreach (var selection in Request.Form["categories"].Split(','))
                            {
                                Models.ArticleOfCategory aoc = new Models.ArticleOfCategory();
                                aoc.ID = Guid.NewGuid();
                                aoc.CreateDate = DateTime.Now;
                                aoc.ArticleID = article.ID;
                                aoc.CategoryID = Guid.Parse(selection);
                                BentoDB.ArticleOfCategories.Add(aoc);
                            }
                        }

                        BentoAdm.Models.ObjectIndex obj = new Models.ObjectIndex();
                        obj.ID = article.ID;
                        obj.DateCreated = DateTime.Now;
                        obj.ObjectType = "Article";
                        BentoDB.ObjectIndexes.Add(obj);

                        BentoDB.SaveChanges();

                        return this.RedirectToAction("Articles");
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                {
                    ViewData["EditError"] = "Sorry, slug already exists.";
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            ViewData.Add("IsPublishedItems", IsPublishedItems);

            List<Models.InterestList> interestList = new List<Models.InterestList>();

            interestList = (from interest in BentoDB.Interests
                            join parent in BentoDB.Interests
                            on interest.ParentId equals parent.ID
                            into ParentAndChild
                            join ioc in BentoDB.InterestOfCategories
                            on interest.ID equals ioc.InterestID
                            into InterestOfCategories
                            from item in InterestOfCategories.DefaultIfEmpty()
                            join category in BentoDB.Categories
                            on item.CategoryID equals category.ID
                            into Category
                            from category in Category.DefaultIfEmpty()
                            from parent in ParentAndChild.DefaultIfEmpty()
                            select new Models.InterestList
                            {
                                ID = interest.ID,
                                DateCreated = interest.DateCreated,
                                Name = interest.Name,
                                Thumbnail = interest.Thumbnail,
                                Selected = false,
                                CategoryID = category.ID,
                                CategoryName = category.Name,
                                ParentID = parent.ID,
                                ParentName = parent.Name
                            }).ToList();

            #region old query
            //var resuft = this.BentoDB.Interests.OrderBy(o => o.ParentId).ToList();

            //foreach (var row in resuft)
            //{
            //    var category = this.BentoDB.Categories.Find(
            //        this.BentoDB.InterestOfCategories.Where(w => w.InterestID == row.ID).Select(s => s.CategoryID).First());

            //    var parent = this.BentoDB.Interests.Find(row.ParentId);
            //    var parentID = Guid.Empty;
            //    var parentName = string.Empty;

            //    if (parent != null)
            //    {
            //        parentName = parent.Name;
            //        parentID = parent.ID;
            //    }

            //    interestList.Add(new Models.InterestList
            //    {
            //        ID = row.ID,
            //        DateCreated = row.DateCreated,
            //        Name = row.Name,
            //        Thumbnail = row.Thumbnail,
            //        Selected = false,
            //        CategoryID = category.ID,
            //        CategoryName = category.Name,
            //        ParentID = parentID,
            //        ParentName = parentName
            //    });

            //}
            #endregion

            //ViewData["Categories"] = BentoDB.Categories.OrderBy(o => o.Name).Select(x => new SelectListItem
            //{
            //    Text = x.Name,
            //    Value = x.ID.ToString()
            //}).ToList();

            List<SelectListItem> SponsorWatermark = BentoDB.Sponsors.Select(s => new SelectListItem() { Value = s.ID.ToString(), Text = s.Name }).ToList();
            ViewData.Add("SponsorWatermark", SponsorWatermark);

            ViewData["Categories"] = interestList.OrderBy(o => o.ParentName).ThenBy(t => t.Name).ToList();

            return View(article);
        }

        public ActionResult NewSponsor()
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult NewSponsor(Models.Sponsor sponsor, HttpPostedFileBase logo, HttpPostedFileBase imagewatermark)
        {
            if (ModelState.IsValid)
            {
                Models.Sponsor slugOfSponsor = this.BentoDB.Sponsors.Where(s => s.Slug == sponsor.Slug).First();
                if (slugOfSponsor == null)
                {
                    try
                    {
                        sponsor.ID = Guid.NewGuid();
                        sponsor.DateCreated = DateTime.Now;

                        var image = WebImage.GetImageFromRequest("Logo");
                        if (image != null)
                        {
                            var min = image.Width > image.Height ? image.Height : image.Width;
                            if (min > 800)
                            {
                                image.Resize(800, 800);
                            }
                            var path = string.Empty;
                            if (logo != null)
                            {
                                string filename = sponsor.ID + Path.GetExtension(logo.FileName);
                                string root = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~/");
                                path = HttpContext.Server.MapPath("~/Upload/Sponsors/") + filename;
                                //logo.SaveAs(HttpContext.Server.MapPath("~/Upload/Sponsors/") + filename);
                                sponsor.Logo = root + "Upload/Sponsors/" + filename;
                            }
                            image.Save(path);
                        }

                        image = WebImage.GetImageFromRequest("ImageWatermark");
                        if (image != null)
                        {
                            var min = image.Width > image.Height ? image.Height : image.Width;
                            if (min > 200)
                            {
                                image.Resize(200, 200);
                            }
                            var path = string.Empty;

                            if (imagewatermark != null)
                            {
                                string filename = sponsor.ID + "_watermark" + Path.GetExtension(imagewatermark.FileName);
                                string root = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~/");
                                path = HttpContext.Server.MapPath("~/Upload/Sponsors/") + filename;
                                //logo.SaveAs(HttpContext.Server.MapPath("~/Upload/Sponsors/") + filename);
                                sponsor.ImageWatermark = root + "Upload/Sponsors/" + filename;
                            }
                            image.Save(path);
                        }
                        BentoDB.Sponsors.Add(sponsor);
                        BentoDB.SaveChanges();

                        return this.RedirectToAction("Sponsors");
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                {
                    ViewData["EditError"] = "Sorry, slug already exists.";
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            return View(sponsor);
        }
        #endregion

        #region  Show list
        public ActionResult UserProfiles()
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            return View(BentoDB.Profiles.OrderByDescending(o => o.DateCreated).ToList());
        }

        public ActionResult Category()
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }
            return RedirectToAction("Interest");

            return View(BentoDB.Categories.OrderByDescending(o => o.DateCreated).ToList());
        }

        public ActionResult Interest()
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            List<BentoAdm.Models.InterestView> interestviews = new List<Models.InterestView>();

            interestviews = (from child in BentoDB.Interests
                             join parent in BentoDB.Interests
                             on child.ParentId equals parent.ID into family
                             from item in family.DefaultIfEmpty()
                             select new Models.InterestView()
                        {
                            ID = child.ID,
                            Name = child.Name,
                            DateCreated = child.DateCreated,
                            Description = child.Description,
                            Slug = child.Slug,
                            Status = child.Status,
                            Thumbnail = child.Thumbnail,
                            ParentName = item.Name,
                        }).OrderByDescending(o => o.DateCreated).ToList();

            return View(interestviews);

            //return View(BentoDB.Interests.OrderByDescending(o => o.DateCreated).ToList());
        }

        public ActionResult Articles()
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            List<BentoAdm.Models.ArticleView> articleviews = new List<Models.ArticleView>();

            articleviews = (from article in BentoDB.Articles
                            join aoc in BentoDB.ArticleOfCategories
                            on article.ID equals aoc.ArticleID
                            into ArticleOfCategories
                            from item in ArticleOfCategories.DefaultIfEmpty()
                            join category in BentoDB.Categories
                            on item.CategoryID equals category.ID
                            into Category
                            from row in Category.DefaultIfEmpty()
                            select new Models.ArticleView()
                             {
                                 ID = article.ID,
                                 DateCreated = article.DateCreated,
                                 Author = article.Author,
                                 CreatedBy = article.CreatedBy,
                                 IsPublished = article.IsPublished,
                                 PublishedBy = article.PublishedBy,
                                 PublishedDate = article.PublishedDate,
                                 ShortDescription = article.ShortDescription,
                                 Slug = article.Slug,
                                 Thumbnail = article.Thumbnail,
                                 Title = article.Title,
                                 CategoryID = row.ID,
                                 CategoryName = row.Name,
                             }).OrderByDescending(o => o.DateCreated).ToList();

            #region query inner join
            //articleviews = BentoDB.Articles
            //    .Join(BentoDB.ArticleOfCategories.DefaultIfEmpty()
            //    .Join(BentoDB.Categories.DefaultIfEmpty(), aoc => aoc.CategoryID, category => category.ID
            //    , (aoc, category) => new
            //    {
            //        aoc.ID,
            //        aoc.ArticleID,
            //        aoc.CategoryID,
            //        category.Name
            //    }), article => article.ID, aoc => aoc.ArticleID, (article, aoc) => new
            //    {
            //        article.ID,
            //        article.DateCreated,
            //        article.Author,
            //        article.CreatedBy,
            //        article.IsPublished,
            //        article.PublishedBy,
            //        article.PublishedDate,
            //        article.ShortDescription,
            //        article.Slug,
            //        article.Thumbnail,
            //        article.Title,
            //        aoc.CategoryID,
            //        aoc.Name
            //    })
            //    .OrderByDescending(o => o.DateCreated)
            //    .Select(s => new Models.ArticleView()
            //    {
            //        ID = s.ID,
            //        DateCreated = s.DateCreated,
            //        Author = s.Author,
            //        CreatedBy = s.CreatedBy,
            //        IsPublished = s.IsPublished,
            //        PublishedBy = s.PublishedBy,
            //        PublishedDate = s.PublishedDate,
            //        ShortDescription = s.ShortDescription,
            //        Slug = s.Slug,
            //        Thumbnail = s.Thumbnail,
            //        Title = s.Title,
            //        CategoryID = s.CategoryID,
            //        CategoryName = s.Name,
            //    }).ToList();
            #endregion

            ViewBag.countAllArticles = this.BentoDB.Articles.Count();

            return View(articleviews);

            //return View(BentoDB.Articles.OrderByDescending(o => o.DateCreated).ToList());
        }

        public ActionResult Comments()
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            List<BentoAdm.Models.CommentView> commentviews = new List<Models.CommentView>();

            commentviews = (from comment in BentoDB.Comments
                            from objectindex in BentoDB.ObjectIndexes
                            from profile in BentoDB.Profiles
                            where comment.ObjectID == objectindex.ID
                            && comment.ProfileID == profile.ID
                            select new Models.CommentView()
                            {
                                ID = comment.ID,
                                IP = comment.IP,
                                DateCreated = comment.DateCreated,
                                Message = comment.Message,
                                ObjectType = objectindex.ObjectType,
                                Username = profile.Username
                            }).OrderByDescending(o => o.DateCreated).ToList();

            return View(commentviews);

            //return View(BentoDB.Comments.OrderByDescending(o => o.DateCreated).ToList());
        }

        public ActionResult Notes()
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            List<BentoAdm.Models.NoteView> noteviews = new List<Models.NoteView>();

            noteviews = (from note in BentoDB.Notes
                         from objectindex in BentoDB.ObjectIndexes
                         from profile in BentoDB.Profiles
                         where note.ObjectID == objectindex.ID
                         && note.ProfileID == profile.ID
                         select new Models.NoteView()
                         {
                             ID = note.ID,
                             IP = note.IP,
                             DateCreated = note.DateCreated,
                             Message = note.Message,
                             ObjectType = objectindex.ObjectType,
                             Username = profile.Username
                         }).OrderByDescending(o => o.DateCreated).ToList();

            return View(noteviews);

            //return View(BentoDB.Notes.OrderByDescending(o => o.DateCreated).ToList());
        }

        public ActionResult Messages()
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            return View(BentoDB.Contacts.OrderByDescending(o => o.DateCreated).ToList());
        }

        public ActionResult Subscribe()
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            return View(BentoDB.Subscribes.OrderByDescending(o => o.DateCreated).ToList());
        }

        public ActionResult Sponsors()
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            return View(BentoDB.Sponsors.OrderByDescending(o => o.DateCreated).ToList());
        }
        #endregion

        #region DELETE
        public ActionResult DeleteUserProfile(Guid id)
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            Models.Profile profile = this.BentoDB.Profiles.Find(id);

            if (profile == null)
            {
                try
                {
                    return Redirect(Request.UrlReferrer.ToString());
                }
                catch (Exception e)
                { }

                return this.RedirectToAction("UserProfiles");
            }

            return this.View(profile);
        }

        [HttpPost]
        public ActionResult DeleteUserProfile(Guid id, FormCollection form)
        {
            Models.Profile profile = this.BentoDB.Profiles.Find(id);

            this.BentoDB.Profiles.Remove(profile);
            this.BentoDB.SaveChanges();

            return this.RedirectToAction("UserProfiles");
        }

        public ActionResult DeleteCategory(Guid id)
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }
            return RedirectToAction("Interest");

            Models.Category category = this.BentoDB.Categories.Find(id);

            if (category == null)
            {
                try
                {
                    return Redirect(Request.UrlReferrer.ToString());
                }
                catch (Exception e)
                { }

                return this.RedirectToAction("Category");
            }

            return this.View(category);
        }

        [HttpPost]
        public ActionResult DeleteCategory(Guid id, FormCollection form)
        {
            return RedirectToAction("Interest");

            Models.Category category = this.BentoDB.Categories.Find(id);

            this.BentoDB.Categories.Remove(category);
            this.BentoDB.SaveChanges();

            return this.RedirectToAction("Category");
        }

        public ActionResult DeleteInterest(Guid id)
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            Models.Interest interest = this.BentoDB.Interests.Find(id);

            if (interest == null)
            {
                try
                {
                    return Redirect(Request.UrlReferrer.ToString());
                }
                catch (Exception e)
                { }

                return this.RedirectToAction("Interest");
            }

            ViewBag.countArticle = BentoDB.ArticleOfCategories.Where(w => w.CategoryID == (BentoDB.InterestOfCategories.Where(h => h.InterestID == id).Select(s => s.CategoryID).FirstOrDefault())).Count();

            return this.View(interest);
        }

        [HttpPost]
        public ActionResult DeleteInterest(Guid id, FormCollection form)
        {
            Models.Interest interest = this.BentoDB.Interests.Find(id);

            Models.Category category = this.BentoDB.Categories.Where(w => w.Name == interest.Name).FirstOrDefault();

            this.BentoDB.Interests.Remove(interest);

            if (category != null)
            {
                Models.InterestOfCategory ioc = this.BentoDB.InterestOfCategories.Where(w => w.InterestID == interest.ID).Where(w => w.CategoryID == category.ID).FirstOrDefault();
                if (ioc != null)
                {
                    this.BentoDB.InterestOfCategories.Remove(ioc);
                    this.BentoDB.Categories.Remove(category);
                }
            }

            this.BentoDB.SaveChanges();
            return this.RedirectToAction("Interest");
        }

        public ActionResult DeleteArticle(Guid id)
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            Models.Article article = this.BentoDB.Articles.Find(id);

            if (article == null)
            {
                try
                {
                    return Redirect(Request.UrlReferrer.ToString());
                }
                catch (Exception e)
                { }

                return this.RedirectToAction("Articles");
            }

            return this.View(article);
        }

        [HttpPost]
        public ActionResult DeleteArticle(Guid id, FormCollection form)
        {
            Models.Article article = this.BentoDB.Articles.Find(id);

            List<Models.ArticleOfCategory> list_aoc = this.BentoDB.ArticleOfCategories.Where(w => w.ArticleID == id).ToList();

            this.BentoDB.Articles.Remove(article);

            if (list_aoc != null)
            {
                foreach (var aoc_item in list_aoc)
                {
                    this.BentoDB.ArticleOfCategories.Remove(aoc_item);
                }
            }

            this.BentoDB.SaveChanges();

            return this.RedirectToAction("Articles");
        }

        public ActionResult PublishArticle(Guid id, FormCollection form)
        {
            Models.Article article = this.BentoDB.Articles.Find(id);

            article.IsPublished = !article.IsPublished;
            article.PublishedDate = DateTime.Now;

            this.BentoDB.SaveChanges();

            return this.RedirectToAction("Articles");
        }

        public ActionResult DeleteComment(Guid id)
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            Models.Comment comment = this.BentoDB.Comments.Find(id);

            if (comment == null)
            {
                try
                {
                    return Redirect(Request.UrlReferrer.ToString());
                }
                catch (Exception e)
                { }

                return this.RedirectToAction("Comments");
            }

            return this.View(comment);
        }

        [HttpPost]
        public ActionResult DeleteComment(Guid id, FormCollection form)
        {
            Models.Comment comment = this.BentoDB.Comments.Find(id);

            this.BentoDB.Comments.Remove(comment);
            this.BentoDB.SaveChanges();

            return this.RedirectToAction("Comments");
        }

        public ActionResult DeleteMessage(Guid id)
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            Models.Contact message = this.BentoDB.Contacts.Find(id);

            if (message == null)
            {
                try
                {
                    return Redirect(Request.UrlReferrer.ToString());
                }
                catch (Exception e)
                { }

                return this.RedirectToAction("Messages");
            }

            return this.View(message);
        }

        [HttpPost]
        public ActionResult DeleteMessage(Guid id, FormCollection form)
        {
            Models.Contact message = this.BentoDB.Contacts.Find(id);

            this.BentoDB.Contacts.Remove(message);
            this.BentoDB.SaveChanges();

            return this.RedirectToAction("Messages");
        }

        public ActionResult DeleteSubscribe(Guid id)
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            Models.Subscribe subscribe = this.BentoDB.Subscribes.Find(id);

            if (subscribe == null)
            {
                try
                {
                    return Redirect(Request.UrlReferrer.ToString());
                }
                catch (Exception e)
                { }

                return this.RedirectToAction("Subscribe");
            }

            return this.View(subscribe);
        }

        [HttpPost]
        public ActionResult DeleteSubscribe(Guid id, FormCollection form)
        {
            Models.Subscribe subscribe = this.BentoDB.Subscribes.Find(id);

            this.BentoDB.Subscribes.Remove(subscribe);
            this.BentoDB.SaveChanges();

            return this.RedirectToAction("Subscribe");
        }

        public ActionResult DeleteSponsor(Guid id)
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            Models.Sponsor sponsor = this.BentoDB.Sponsors.Find(id);

            if (sponsor == null)
            {
                try
                {
                    return Redirect(Request.UrlReferrer.ToString());
                }
                catch (Exception e)
                { }

                return this.RedirectToAction("Sponsors");
            }

            return this.View(sponsor);
        }

        [HttpPost]
        public ActionResult DeleteSponsor(Guid id, FormCollection form)
        {
            Models.Sponsor sponsor = this.BentoDB.Sponsors.Find(id);

            this.BentoDB.Sponsors.Remove(sponsor);
            this.BentoDB.SaveChanges();

            return this.RedirectToAction("Sponsors");
        }
        #endregion

        #region EDIT
        public ActionResult EditUserProfile(Guid id)
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            Models.Profile profile = this.BentoDB.Profiles.Find(id);

            if (profile == null)
            {
                try
                {
                    return Redirect(Request.UrlReferrer.ToString());
                }
                catch (Exception e)
                { }

                return this.RedirectToAction("UserProfiles");
            }

            ViewData.Add("AccountTypeItems", AccountTypeItems);

            ViewData.Add("GenderItems", GenderItems);

            ViewData.Add("Banned", IsPublishedItems);

            return this.View(profile);
        }

        [HttpPost]
        public ActionResult EditUserProfile(Models.Profile profile, HttpPostedFileBase avatar)
        {
            Models.Profile userprofile = this.BentoDB.Profiles.Find(profile.ID);

            if (ModelState.IsValid)
            {
                try
                {
                    if (profile.Email != null && profile.Email != userprofile.Email)
                    { userprofile.Username = profile.Email; userprofile.Email = profile.Email; }
                    if (profile.Password != null && profile.Password != userprofile.Password)
                    {
                        userprofile.Password = profile.Password;
                        using (MD5 md5Hash = MD5.Create())
                        {
                            string hash = GetMd5Hash(md5Hash, profile.Password);
                            userprofile.Password = hash;
                        }
                    }
                    if (profile.AccountType != null && profile.AccountType != userprofile.AccountType)
                    { userprofile.AccountType = profile.AccountType; }
                    if (profile.AccountID != null && profile.AccountID != userprofile.AccountID)
                    { userprofile.AccountID = profile.AccountID; }
                    if (profile.Fullname != null && profile.Fullname != userprofile.Fullname)
                    { userprofile.Fullname = profile.Fullname; }
                    if (profile.Address != null && profile.Address != userprofile.Address)
                    { userprofile.Address = profile.Address; }
                    if (profile.Gender != null && profile.Gender != userprofile.Gender)
                    { userprofile.Gender = profile.Gender; }

                    var image = WebImage.GetImageFromRequest();
                    if (image != null)
                    {
                        var min = image.Width > image.Height ? image.Height : image.Width;
                        if (min > 800)
                        {
                            image.Resize(800, 800);
                        }
                        var path = string.Empty;
                        if (avatar != null)
                        {
                            string filename = userprofile.ID + Path.GetExtension(avatar.FileName);
                            string root = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~/");
                            path = HttpContext.Server.MapPath("~/Upload/Avatar/") + filename;
                            //avatar.SaveAs(HttpContext.Server.MapPath("~/Upload/Avatar/") + filename);
                            userprofile.Avatar = root + "Upload/Avatar/" + filename;
                        }
                        image.Save(path);
                    }
                    if (profile.Caption != null && profile.Caption != userprofile.Caption)
                    { userprofile.Caption = profile.Caption; }
                    if (profile.BirthDay != null && profile.BirthDay != userprofile.BirthDay)
                    { userprofile.BirthDay = profile.BirthDay; }
                    if (profile.IP != null && profile.IP != userprofile.IP)
                    { userprofile.IP = profile.IP; }
                    if (!(profile.Banned && userprofile.Banned))
                    { userprofile.Banned = profile.Banned; }
                    BentoDB.SaveChanges();

                    return this.RedirectToAction("UserProfiles");
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            ViewData.Add("AccountTypeItems", AccountTypeItems);

            ViewData.Add("GenderItems", GenderItems);

            ViewData.Add("Banned", IsPublishedItems);

            return View(profile);
        }

        public ActionResult EditCategory(Guid id)
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }
            return RedirectToAction("Interest");

            Models.Category category = this.BentoDB.Categories.Find(id);

            if (category == null)
            {
                try
                {
                    return Redirect(Request.UrlReferrer.ToString());
                }
                catch (Exception e)
                { }

                return this.RedirectToAction("Category");
            }

            ViewData.Add("StatusItems", StatusItems);

            return View(category);
        }

        [HttpPost]
        public ActionResult EditCategory(Models.Category category)
        {
            return RedirectToAction("Interest");
            Models.Category oldcategory = this.BentoDB.Categories.Find(category.ID);

            if (ModelState.IsValid)
            {
                try
                {
                    if (category.Name != null && category.Name != oldcategory.Name)
                    { oldcategory.Name = category.Name; }
                    if (category.Description != null && category.Description != oldcategory.Description)
                    { oldcategory.Description = category.Description; }
                    if (category.Status >= 0 && category.Status != oldcategory.Status)
                    { oldcategory.Status = category.Status; }
                    if (category.Slug != null && category.Slug != oldcategory.Slug)
                    { oldcategory.Slug = category.Slug; }
                    BentoDB.SaveChanges();

                    return this.RedirectToAction("Category");
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            ViewData.Add("StatusItems", StatusItems);

            return View(category);
        }

        public ActionResult EditInterest(Guid id)
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            Models.Interest interest = this.BentoDB.Interests.Find(id);

            if (interest == null)
            {
                try
                {
                    return Redirect(Request.UrlReferrer.ToString());
                }
                catch (Exception e)
                { }

                return this.RedirectToAction("Interest");
            }

            ViewData.Add("StatusItems", StatusItems);

            #region select list Interest
            //var resuft = this.BentoDB.Interests.OrderBy(o => o.ParentId).ToList();
            //List<Models.InterestList> interestList = new List<Models.InterestList>();

            //foreach (var row in resuft)
            //{
            //    var category = this.BentoDB.Categories.Find(
            //        this.BentoDB.InterestOfCategories.Where(w => w.InterestID == row.ID).Select(s => s.CategoryID).First());

            //    var parent = this.BentoDB.Interests.Find(row.ParentId);
            //    var parentID = Guid.Empty;
            //    var parentName = string.Empty;

            //    if (parent != null)
            //    {
            //        parentName = parent.Name;
            //        parentID = parent.ID;
            //    }

            //    interestList.Add(new Models.InterestList
            //    {
            //        ID = row.ID,
            //        DateCreated = row.DateCreated,
            //        Name = row.Name,
            //        Thumbnail = row.Thumbnail,
            //        Selected = (row.ID == interest.ParentId),
            //        CategoryID = category.ID,
            //        CategoryName = category.Name,
            //        ParentID = parentID,
            //        ParentName = parentName
            //    });

            //}

            ////ViewData["Interests"] = BentoDB.Interests.OrderBy(o => o.Name).Select(x => new SelectListItem
            ////{
            ////    Text = x.Name,
            ////    Value = x.ID.ToString(),
            ////    Selected = (x.ID == interest.ParentId),
            ////}).ToList();

            //ViewData["Interests"] = interestList.OrderBy(o => o.ParentName).ToList();
            #endregion

            var resuft = this.BentoDB.Interests
                                .OrderBy(o => o.Name)
                                .Select(s => new BentoAdm.Models.InterestList()
                                {
                                    ID = s.ID,
                                    Name = s.Name,
                                    ParentID = s.ParentId,
                                }).ToList();

            var structuredInterest = BuildInterestList(resuft, null, 1);

            var listInterest = new List<SelectListItem>();
            listInterest.Add(new SelectListItem()
            {
                Value = "",
                Text = "Không có đơn vị cha"
            });
            foreach (var intrst in structuredInterest)
            {
                listInterest.Add(new SelectListItem()
                {
                    Value = intrst.ID.ToString(),
                    Text = new string('-', 10 * (intrst.Level - 1)) + " " + intrst.Name
                });
            }
            ViewBag.Interests = listInterest;

            #region ViewData["Categories"]
            //ViewData["Categories"] = BentoDB.Categories.OrderBy(o => o.Name).Select(x => new SelectListItem
            //{
            //    Text = x.Name,
            //    Value = x.ID.ToString(),
            //    Selected = (this.BentoDB.InterestOfCategories.Where(w => w.InterestID == interest.ID).Where(w => w.CategoryID == x.ID).FirstOrDefault() != null),
            //}).ToList();
            #endregion

            return View(interest);
        }

        [HttpPost]
        public ActionResult EditInterest(Models.Interest interest, HttpPostedFileBase thumbnail)
        {
            Models.Interest oldinterest = this.BentoDB.Interests.Find(interest.ID);

            Models.Category oldcategory = this.BentoDB.Categories.Where(w => w.Name == oldinterest.Name).FirstOrDefault();

            if (ModelState.IsValid)
            {
                List<Models.Interest> slugOfInterest = this.BentoDB.Interests
                    .Where(s => s.Slug == interest.Slug)
                    .Where(i => i.ID != oldinterest.ID)
                    .ToList();
                if (slugOfInterest.Count == 0)
                {

                    try
                    {
                        if (interest.Name != null && interest.Name != oldinterest.Name)
                        { oldinterest.Name = interest.Name; }
                        if (interest.Description != null && interest.Description != oldinterest.Description)
                        { oldinterest.Description = interest.Description; }
                        if (interest.Status >= 0 && interest.Status != oldinterest.Status)
                        { oldinterest.Status = interest.Status; }
                        if (interest.Slug != null && interest.Slug != oldinterest.Slug)
                        { oldinterest.Slug = interest.Slug; }

                        var image = WebImage.GetImageFromRequest();
                        if (image != null)
                        {
                            var min = image.Width > image.Height ? image.Height : image.Width;
                            if (min > 800)
                            {
                                image.Resize(800, 800);
                            }
                            var path = string.Empty;
                            if (thumbnail != null)
                            {
                                string filename = oldinterest.ID + Path.GetExtension(thumbnail.FileName);
                                string root = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~/");
                                path = HttpContext.Server.MapPath("~/Upload/Interest/") + filename;
                                //thumbnail.SaveAs(HttpContext.Server.MapPath("~/Upload/Interest/") + filename);
                                oldinterest.Thumbnail = root + "Upload/Interest/" + filename;
                            }
                            image.Save(path);
                        }
                        if (Request.Form["parentid"] != null)
                        {
                            string selection = Request.Form["parentid"];
                            Guid? pid = null;
                            if (selection != "")
                            {
                                pid = Guid.Parse(selection);
                            }
                            if (pid != oldinterest.ParentId)
                            { oldinterest.ParentId = pid; }
                        }

                        if (oldcategory != null)
                        {
                            Models.InterestOfCategory ioc = this.BentoDB.InterestOfCategories.Where(w => w.InterestID == interest.ID).Where(w => w.CategoryID == oldcategory.ID).FirstOrDefault();
                            if (ioc != null)
                            {
                                oldcategory.Name = interest.Name;
                                oldcategory.Description = interest.Description;
                                oldcategory.Status = interest.Status;
                                oldcategory.Slug = interest.Slug;
                            }

                        }
                        #region Request.Form["categories"]
                        //if (Request.Form["categories"] != null)
                        //{
                        //    foreach (var selection in Request.Form["categories"].Split(','))
                        //    {
                        //        Guid curr = Guid.Parse(selection);
                        //        Models.InterestOfCategory find = this.BentoDB.InterestOfCategories.Where(w => w.InterestID == interest.ID).Where(w => w.CategoryID == curr).FirstOrDefault();
                        //        if (find == null)
                        //        {
                        //            Models.InterestOfCategory ioc = new Models.InterestOfCategory();
                        //            ioc.ID = Guid.NewGuid();
                        //            ioc.DateCreated = DateTime.Now;
                        //            ioc.InterestID = interest.ID;
                        //            ioc.CategoryID = Guid.Parse(selection);
                        //            BentoDB.InterestOfCategories.Add(ioc);
                        //        }
                        //    }
                        //}
                        #endregion

                        BentoDB.SaveChanges();

                        return this.RedirectToAction("Interest");
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                {
                    ViewData["EditError"] = "Sorry, slug already exists.";
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            ViewData.Add("StatusItems", StatusItems);

            #region select list interest
            //var resuft = this.BentoDB.Interests.OrderBy(o => o.ParentId).ToList();
            //List<Models.InterestList> interestList = new List<Models.InterestList>();

            //foreach (var row in resuft)
            //{
            //    var category = this.BentoDB.Categories.Find(
            //        this.BentoDB.InterestOfCategories.Where(w => w.InterestID == row.ID).Select(s => s.CategoryID).First());

            //    var parent = this.BentoDB.Interests.Find(row.ParentId);
            //    var parentID = Guid.Empty;
            //    var parentName = string.Empty;

            //    if (parent != null)
            //    {
            //        parentName = parent.Name;
            //        parentID = parent.ID;
            //    }

            //    interestList.Add(new Models.InterestList
            //    {
            //        ID = row.ID,
            //        DateCreated = row.DateCreated,
            //        Name = row.Name,
            //        Thumbnail = row.Thumbnail,
            //        Selected = (row.ID == interest.ParentId),
            //        CategoryID = category.ID,
            //        CategoryName = category.Name,
            //        ParentID = parentID,
            //        ParentName = parentName
            //    });

            //}

            ////ViewData["Interests"] = BentoDB.Interests.OrderBy(o => o.Name).Select(x => new SelectListItem
            ////{
            ////    Text = x.Name,
            ////    Value = x.ID.ToString(),
            ////    Selected = (x.ID == interest.ParentId),
            ////}).ToList();

            //ViewData["Interests"] = interestList.OrderBy(o => o.ParentName).ToList();
            #endregion

            var resuft = this.BentoDB.Interests
                                .OrderBy(o => o.Name)
                                .Select(s => new BentoAdm.Models.InterestList()
                                {
                                    ID = s.ID,
                                    Name = s.Name,
                                    ParentID = s.ParentId,
                                }).ToList();

            var structuredInterest = BuildInterestList(resuft, null, 1);

            var listInterest = new List<SelectListItem>();
            listInterest.Add(new SelectListItem()
            {
                Value = "",
                Text = "Không có đơn vị cha"
            });
            foreach (var intrst in structuredInterest)
            {
                listInterest.Add(new SelectListItem()
                {
                    Value = intrst.ID.ToString(),
                    Text = new string('-', 10 * (intrst.Level - 1)) + " " + intrst.Name
                });
            }
            ViewBag.Interests = listInterest;

            #region ViewData["Categories"]
            //ViewData["Categories"] = BentoDB.Categories.OrderBy(o => o.Name).Select(x => new SelectListItem
            //{
            //    Text = x.Name,
            //    Value = x.ID.ToString(),
            //    Selected = (this.BentoDB.InterestOfCategories.Where(w => w.InterestID == interest.ID).Where(w => w.CategoryID == x.ID).FirstOrDefault() != null),
            //}).ToList();
            #endregion

            return View(interest);
        }

        public ActionResult EditArticle(Guid id)
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            Models.Article article = this.BentoDB.Articles.Find(id);

            if (article == null)
            {
                try
                {
                    return Redirect(Request.UrlReferrer.ToString());
                }
                catch (Exception e)
                { }

                return this.RedirectToAction("Articles");
            }

            ViewData.Add("IsPublishedItems", IsPublishedItems);

            #region select list Category
            List<Models.InterestList> interestList = new List<Models.InterestList>();

            interestList = (from interest in BentoDB.Interests
                            join parent in BentoDB.Interests
                            on interest.ParentId equals parent.ID
                            into ParentAndChild
                            join ioc in BentoDB.InterestOfCategories
                            on interest.ID equals ioc.InterestID
                            into InterestOfCategories
                            from item in InterestOfCategories.DefaultIfEmpty()
                            join category in BentoDB.Categories
                            on item.CategoryID equals category.ID
                            into Category
                            from category in Category.DefaultIfEmpty()
                            from parent in ParentAndChild.DefaultIfEmpty()
                            select new Models.InterestList
                            {
                                ID = interest.ID,
                                DateCreated = interest.DateCreated,
                                Name = interest.Name,
                                Thumbnail = interest.Thumbnail,
                                Selected = (this.BentoDB.ArticleOfCategories.Where(w => w.ArticleID == article.ID).Where(w => w.CategoryID == category.ID).FirstOrDefault() != null),
                                CategoryID = category.ID,
                                CategoryName = category.Name,
                                ParentID = parent.ID,
                                ParentName = parent.Name
                            }).ToList();

            #region old query
            //var resuft = this.BentoDB.Interests.OrderBy(o => o.ParentId).ToList();

            //foreach (var row in resuft)
            //{
            //    var category = this.BentoDB.Categories.Find(
            //        this.BentoDB.InterestOfCategories.Where(w => w.InterestID == row.ID).Select(s => s.CategoryID).First());

            //    var parent = this.BentoDB.Interests.Find(row.ParentId);
            //    var parentID = Guid.Empty;
            //    var parentName = string.Empty;

            //    if (parent != null)
            //    {
            //        parentName = parent.Name;
            //        parentID = parent.ID;
            //    }

            //    interestList.Add(new Models.InterestList
            //    {
            //        ID = row.ID,
            //        DateCreated = row.DateCreated,
            //        Name = row.Name,
            //        Thumbnail = row.Thumbnail,
            //        Selected = (this.BentoDB.ArticleOfCategories.Where(w => w.ArticleID == article.ID).Where(w => w.CategoryID == category.ID).FirstOrDefault() != null),
            //        CategoryID = category.ID,
            //        CategoryName = category.Name,
            //        ParentID = parentID,
            //        ParentName = parentName
            //    });

            //}
            #endregion

            //ViewData["Categories"] = BentoDB.Categories.OrderBy(o => o.Name).Select(x => new SelectListItem
            //{
            //    Text = x.Name,
            //    Value = x.ID.ToString(),
            //    Selected = (this.BentoDB.ArticleOfCategories.Where(w => w.ArticleID == article.ID).Where(w => w.CategoryID == x.ID).FirstOrDefault() != null),
            //}).ToList();

            ViewData["Categories"] = interestList.OrderBy(o => o.ParentName).ThenBy(t => t.Name).ToList();
            #endregion

            List<SelectListItem> SponsorWatermark = BentoDB.Sponsors.Select(s => new SelectListItem() { Value = s.ID.ToString(), Text = s.Name }).ToList();
            ViewData.Add("SponsorWatermark", SponsorWatermark);

            return this.View(article);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditArticle(Models.Article article, HttpPostedFileBase thumbnail)
        {
            Models.Article oldarticle = this.BentoDB.Articles.Find(article.ID);

            if (ModelState.IsValid)
            {
                List<Models.Article> slugOfArticle = this.BentoDB.Articles
                    .Where(s => s.Slug == article.Slug)
                    .Where(i => i.ID != oldarticle.ID)
                    .ToList();
                if (slugOfArticle.Count == 0)
                {
                    try
                    {
                        var image = WebImage.GetImageFromRequest();
                        if (image != null)
                        {
                            var min = image.Width > image.Height ? image.Height : image.Width;
                            if (min > 800)
                            {
                                image.Resize(800, 800);
                            }
                            var path = string.Empty;
                            if (thumbnail != null)
                            {
                                string filename = oldarticle.ID + Path.GetExtension(thumbnail.FileName);
                                string root = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~/");
                                path = HttpContext.Server.MapPath("~/Upload/Categories/") + filename;
                                //thumbnail.SaveAs(HttpContext.Server.MapPath("~/Upload/Categories/") + filename);
                                oldarticle.Thumbnail = root + "Upload/Categories/" + filename;
                            }

                            if (Request.Form["SponsorWatermark"] != null)
                            {
                                var watermarkID = Request.Form["SponsorWatermark"].ToString();
                                var pathwatermark = BentoDB.Sponsors.Find(Guid.Parse(watermarkID)).ImageWatermark;
                                image.AddImageWatermark(Server.MapPath("~/Upload/Sponsors/" + watermarkID + "_watermark" + Path.GetExtension(pathwatermark)));
                            }
                            image.Save(path);
                        }
                        if (article.Slug != null && article.Slug != oldarticle.Slug)
                        { oldarticle.Slug = article.Slug; }
                        if (article.Title != null && article.Title != oldarticle.Title)
                        { oldarticle.Title = article.Title; }
                        if (article.ShortDescription != null && article.ShortDescription != oldarticle.ShortDescription)
                        { oldarticle.ShortDescription = article.ShortDescription; }
                        if (article.Body != null && article.Body != oldarticle.Body)
                        { oldarticle.Body = article.Body; }
                        if (article.Author != null && article.Author != oldarticle.Author)
                        { oldarticle.Author = article.Author; }
                        if (article.CreatedBy != null && article.CreatedBy != oldarticle.CreatedBy)
                        { oldarticle.CreatedBy = article.CreatedBy; }
                        if (article.PublishedBy != null && article.PublishedBy != oldarticle.PublishedBy)
                        { oldarticle.PublishedBy = article.PublishedBy; }
                        if (article.PublishedDate != null && article.PublishedDate != oldarticle.PublishedDate)
                        { oldarticle.PublishedDate = article.PublishedDate; }
                        if (!(article.IsPublished && oldarticle.IsPublished))
                        { oldarticle.IsPublished = article.IsPublished; }

                        List<Models.ArticleOfCategory> oldlistAOC
                            = this.BentoDB.ArticleOfCategories.Where(w => w.ArticleID == article.ID).ToList();

                        if (Request.Form["categories"] != null)
                        {
                            foreach (var selection in Request.Form["categories"].Split(','))
                            {
                                Guid curr = Guid.Parse(selection);
                                Models.ArticleOfCategory find = this.BentoDB.ArticleOfCategories.Where(w => w.ArticleID == article.ID).Where(w => w.CategoryID == curr).FirstOrDefault();
                                if (find == null)
                                {
                                    Models.ArticleOfCategory aoc = new Models.ArticleOfCategory();
                                    aoc.ID = Guid.NewGuid();
                                    aoc.CreateDate = DateTime.Now;
                                    aoc.ArticleID = article.ID;
                                    aoc.CategoryID = Guid.Parse(selection);
                                    BentoDB.ArticleOfCategories.Add(aoc);
                                }
                                else
                                {
                                    oldlistAOC.Remove(find);
                                }
                            }
                        }
                        BentoDB.ArticleOfCategories.RemoveRange(oldlistAOC);
                        BentoDB.SaveChanges();

                        return this.RedirectToAction("Articles");
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                {
                    ViewData["EditError"] = "Sorry, slug already exists.";
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            ViewData.Add("IsPublishedItems", IsPublishedItems);

            #region select list Category
            List<Models.InterestList> interestList = new List<Models.InterestList>();

            interestList = (from interest in BentoDB.Interests
                            join parent in BentoDB.Interests
                            on interest.ParentId equals parent.ID
                            into ParentAndChild
                            join ioc in BentoDB.InterestOfCategories
                            on interest.ID equals ioc.InterestID
                            into InterestOfCategories
                            from item in InterestOfCategories.DefaultIfEmpty()
                            join category in BentoDB.Categories
                            on item.CategoryID equals category.ID
                            into Category
                            from category in Category.DefaultIfEmpty()
                            from parent in ParentAndChild.DefaultIfEmpty()
                            select new Models.InterestList
                            {
                                ID = interest.ID,
                                DateCreated = interest.DateCreated,
                                Name = interest.Name,
                                Thumbnail = interest.Thumbnail,
                                Selected = (this.BentoDB.ArticleOfCategories.Where(w => w.ArticleID == article.ID).Where(w => w.CategoryID == category.ID).FirstOrDefault() != null),
                                CategoryID = category.ID,
                                CategoryName = category.Name,
                                ParentID = parent.ID,
                                ParentName = parent.Name
                            }).ToList();

            #region old query
            //var resuft = this.BentoDB.Interests.OrderBy(o => o.ParentId).ToList();

            //foreach (var row in resuft)
            //{
            //    var category = this.BentoDB.Categories.Find(
            //        this.BentoDB.InterestOfCategories.Where(w => w.InterestID == row.ID).Select(s => s.CategoryID).First());

            //    var parent = this.BentoDB.Interests.Find(row.ParentId);
            //    var parentID = Guid.Empty;
            //    var parentName = string.Empty;

            //    if (parent != null)
            //    {
            //        parentName = parent.Name;
            //        parentID = parent.ID;
            //    }

            //    interestList.Add(new Models.InterestList
            //    {
            //        ID = row.ID,
            //        DateCreated = row.DateCreated,
            //        Name = row.Name,
            //        Thumbnail = row.Thumbnail,
            //        Selected = (this.BentoDB.ArticleOfCategories.Where(w => w.ArticleID == article.ID).Where(w => w.CategoryID == category.ID).FirstOrDefault() != null),
            //        CategoryID = category.ID,
            //        CategoryName = category.Name,
            //        ParentID = parentID,
            //        ParentName = parentName
            //    });

            //}
            #endregion

            //ViewData["Categories"] = BentoDB.Categories.OrderBy(o => o.Name).Select(x => new SelectListItem
            //{
            //    Text = x.Name,
            //    Value = x.ID.ToString(),
            //    Selected = (this.BentoDB.ArticleOfCategories.Where(w => w.ArticleID == article.ID).Where(w => w.CategoryID == x.ID).FirstOrDefault() != null),
            //}).ToList();

            ViewData["Categories"] = interestList.OrderBy(o => o.ParentName).ThenBy(t => t.Name).ToList();
            #endregion

            List<SelectListItem> SponsorWatermark = BentoDB.Sponsors.Select(s => new SelectListItem() { Value = s.ID.ToString(), Text = s.Name }).ToList();
            ViewData.Add("SponsorWatermark", SponsorWatermark);

            return View(article);
        }

        public ActionResult EditComment(Guid id)
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            Models.Comment comment = this.BentoDB.Comments.Find(id);

            if (comment == null)
            {
                try
                {
                    return Redirect(Request.UrlReferrer.ToString());
                }
                catch (Exception e)
                { }

                return this.RedirectToAction("Comments");
            }

            return this.View(comment);
        }

        [HttpPost]
        public ActionResult EditComment(Models.Comment comment)
        {
            Models.Comment usercomment = this.BentoDB.Comments.Find(comment.ID);

            if (ModelState.IsValid)
            {
                try
                {
                    if (comment.Message != null && comment.Message != usercomment.Message)
                    { usercomment.Message = comment.Message; }
                    if (comment.IP != null && comment.IP != usercomment.IP)
                    { usercomment.IP = comment.IP; }
                    BentoDB.SaveChanges();

                    return this.RedirectToAction("Comments");
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            return View(comment);
        }

        public ActionResult EditSubscribe(Guid id)
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            Models.Subscribe subscribe = this.BentoDB.Subscribes.Find(id);

            if (subscribe == null)
            {
                try
                {
                    return Redirect(Request.UrlReferrer.ToString());
                }
                catch (Exception e)
                { }

                return this.RedirectToAction("Subscribe");
            }

            ViewData.Add("IsSubscribe", IsPublishedItems);

            return this.View(subscribe);
        }

        [HttpPost]
        public ActionResult EditSubscribe(Models.Subscribe subscribe)
        {
            Models.Subscribe emailsubscribe = this.BentoDB.Subscribes.Find(subscribe.ID);

            if (ModelState.IsValid)
            {
                try
                {
                    if (subscribe.Email != null && subscribe.Email != emailsubscribe.Email)
                    { emailsubscribe.Email = subscribe.Email; }
                    if (!((bool)subscribe.IsSubscribe && (bool)emailsubscribe.IsSubscribe))
                    { emailsubscribe.IsSubscribe = subscribe.IsSubscribe; }
                    if (subscribe.IP != null && subscribe.IP != emailsubscribe.IP)
                    { emailsubscribe.IP = subscribe.IP; }
                    BentoDB.SaveChanges();

                    return this.RedirectToAction("Subscribe");
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            ViewData.Add("IsSubscribe", IsPublishedItems);

            return View(subscribe);
        }

        public ActionResult EditMessage(Guid id)
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            Models.Contact message = this.BentoDB.Contacts.Find(id);

            if (message == null)
            {
                try
                {
                    return Redirect(Request.UrlReferrer.ToString());
                }
                catch (Exception e)
                { }

                return this.RedirectToAction("Messages");
            }

            return this.View(message);
        }

        [HttpPost]
        public ActionResult EditMessage(Models.Contact contact)
        {
            Models.Contact message = this.BentoDB.Contacts.Find(contact.ID);

            if (ModelState.IsValid)
            {
                try
                {
                    if (contact.Email != null && contact.Email != message.Email)
                    { message.Email = contact.Email; }
                    if (contact.Name != null && contact.Name != message.Name)
                    { message.Name = contact.Name; }
                    if (contact.Subject != null && contact.Subject != message.Subject)
                    { message.Subject = contact.Subject; }
                    if (contact.Message != null && contact.Message != message.Message)
                    { message.Message = contact.Message; }
                    if (contact.IP != null && contact.IP != message.IP)
                    { message.IP = contact.IP; }
                    BentoDB.SaveChanges();

                    return this.RedirectToAction("Messages");
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            return View(contact);
        }

        public ActionResult EditSponsor(Guid id)
        {
            if (!IsLogIn())
            {
                return RedirectToAction("Login");
            }

            Models.Sponsor sponsor = this.BentoDB.Sponsors.Find(id);

            if (sponsor == null)
            {
                try
                {
                    return Redirect(Request.UrlReferrer.ToString());
                }
                catch (Exception e)
                { }

                return this.RedirectToAction("Sponsors");
            }

            return this.View(sponsor);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditSponsor(Models.Sponsor sponsor, HttpPostedFileBase logo, HttpPostedFileBase imagewatermark)
        {
            Models.Sponsor oldsponsor = this.BentoDB.Sponsors.Find(sponsor.ID);

            if (ModelState.IsValid)
            {
                List<Models.Sponsor> slugOfSponsor = this.BentoDB.Sponsors
                    .Where(s => s.Slug == sponsor.Slug)
                    .Where(i => i.ID != oldsponsor.ID)
                    .ToList();
                if (slugOfSponsor.Count == 0)
                {
                    try
                    {
                        var image = WebImage.GetImageFromRequest("Logo");
                        if (image != null)
                        {
                            var min = image.Width > image.Height ? image.Height : image.Width;
                            if (min > 800)
                            {
                                image.Resize(800, 800);
                            }
                            var path = string.Empty;
                            if (logo != null)
                            {
                                string filename = oldsponsor.ID + Path.GetExtension(logo.FileName);
                                string root = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~/");
                                path = HttpContext.Server.MapPath("~/Upload/Sponsors/") + filename;
                                //logo.SaveAs(HttpContext.Server.MapPath("~/Upload/Sponsors/") + filename);
                                oldsponsor.Logo = root + "Upload/Sponsors/" + filename;
                            }
                            image.Save(path);
                        }

                        image = WebImage.GetImageFromRequest("ImageWatermark");
                        if (image != null)
                        {
                            var min = image.Width > image.Height ? image.Height : image.Width;
                            if (min > 200)
                            {
                                image.Resize(200, 200);
                            }
                            var path = string.Empty;
                            if (imagewatermark != null)
                            {
                                string filename = oldsponsor.ID + "_watermark" + Path.GetExtension(imagewatermark.FileName);
                                string root = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~/");
                                path = HttpContext.Server.MapPath("~/Upload/Sponsors/") + filename;
                                //logo.SaveAs(HttpContext.Server.MapPath("~/Upload/Sponsors/") + filename);
                                oldsponsor.ImageWatermark = root + "Upload/Sponsors/" + filename;
                            }
                            image.Save(path);
                        }
                        if (sponsor.Name != null && sponsor.Name != oldsponsor.Name)
                        { oldsponsor.Name = sponsor.Name; }
                        if (sponsor.ShortDescription != null && sponsor.ShortDescription != oldsponsor.ShortDescription)
                        { oldsponsor.ShortDescription = sponsor.ShortDescription; }
                        if (sponsor.WebAddress != null && sponsor.WebAddress != oldsponsor.WebAddress)
                        { oldsponsor.WebAddress = sponsor.WebAddress; }
                        if (sponsor.BodyContent != null && sponsor.BodyContent != oldsponsor.BodyContent)
                        { oldsponsor.BodyContent = sponsor.BodyContent; }
                        if (sponsor.Address != null && sponsor.Address != oldsponsor.Address)
                        { oldsponsor.Address = sponsor.Address; }
                        if (sponsor.Email != null && sponsor.Email != oldsponsor.Email)
                        { oldsponsor.Email = sponsor.Email; }
                        if (sponsor.Phone != null && sponsor.Phone != oldsponsor.Phone)
                        { oldsponsor.Phone = sponsor.Phone; }
                        if (sponsor.Fax != null && sponsor.Fax != oldsponsor.Fax)
                        { oldsponsor.Fax = sponsor.Fax; }

                        BentoDB.SaveChanges();

                        return this.RedirectToAction("Sponsors");
                    }
                    catch (Exception e)
                    {
                        ViewData["EditError"] = e.Message;
                    }
                }
                else
                {
                    ViewData["EditError"] = "Sorry, slug already exists.";
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";

            return View(sponsor);
        }
        #endregion

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash. 
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes 
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data  
            // and format each one as a hexadecimal string. 
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }
    }
}
