﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BentoAdm.Models
{
    public class ArticleView
    {
        public System.Guid ID { get; set; }
        public System.DateTime DateCreated { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string Thumbnail { get; set; }
        public string Author { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> PublishedDate { get; set; }
        public string PublishedBy { get; set; }
        public bool IsPublished { get; set; }
        public Nullable<Guid> CategoryID { get; set; }
        public string CategoryName { get; set; }
    }
}