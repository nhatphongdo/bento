//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BentoAdm.Models
{
    using System;
    
    public partial class GetLatestArticlesIncludeSlug_Result
    {
        public System.Guid ID { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
        public string Thumbnail { get; set; }
        public string ShortDescription { get; set; }
        public string Author { get; set; }
        public Nullable<int> Likes { get; set; }
        public Nullable<bool> Liked { get; set; }
        public string Categories { get; set; }
    }
}
