﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BentoAdm.Models.Api
{
    /// <summary>
    /// Enum ProfileErrors
    /// </summary>
    public enum ProfileErrors
    {
        Success = 0,
        UserNotExistence = 1,
        NotAuthenticated = 2,
        DatabaseFailed = 250,
        OtherException = 251,
    }
}