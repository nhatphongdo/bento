﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BentoAdm.Models.Api
{
    /// <summary>
    /// Class SignUpModel.
    /// </summary>
    public class SignUpModel
    {
        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>The username.</value>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Username cannot be empty")]
        public string Username
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password.</value>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Password cannot be empty")]
        [DataType(DataType.Password)]
        public string Password
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the confirm password.
        /// </summary>
        /// <value>The confirm password.</value>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Confirm password cannot be empty")]
        [Compare("Password", ErrorMessage = "Password and confirm password must be the same")]
        [DataType(DataType.Password)]
        public string ConfirmPassword
        {
            get;
            set;
        }

        public string FullName
        {
            get;
            set;
        }

        public DateTime Birthday
        {
            get;
            set;
        }

        public int Gender
        {
            get;
            set;
        }

        public string AccountType
        {
            get;
            set;
        }

        public string AccountID
        {
            get;
            set;
        }
    }
}