﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BentoAdm.Models.Api
{
    /// <summary>
    /// Enum ForgotPasswordErrors
    /// </summary>
    public enum ForgotPasswordErrors
    {
        Success = 0,
        WrongEmail = 1,
        DatabaseFailed = 250,
        OtherException = 251,
    }
}