﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BentoAdm.Models.Api
{
    /// <summary>
    /// Enum SignUpErrors
    /// </summary>
    public enum SignUpErrors
    {
        Success = 0,
        UserExistence = 1,
        DatabaseFailed = 250,
        OtherException = 251,
    }
}