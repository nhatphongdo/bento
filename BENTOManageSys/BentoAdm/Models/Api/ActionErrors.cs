﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BentoAdm.Models.Api
{
    public enum ActionErrors
    {
        Success = 0,
        AuthenticationFailed = 1,
        NotAuthenticated = 2,
        AlreadyDone = 3,
        DatabaseFailed = 250,
        OtherException = 251,
    }
}