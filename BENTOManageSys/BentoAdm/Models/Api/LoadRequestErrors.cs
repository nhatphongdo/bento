﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BentoAdm.Models.Api
{
    /// <summary>
    /// Enum LoadRequestErrors
    /// </summary>
    public enum LoadRequestErrors
    {
        Success = 0,
        AuthenticationFailed = 1,
        NotAuthenticated = 2,
        DatabaseFailed = 250,
        OtherException = 251,
    }
}