﻿
namespace BentoAdm.Models.Api
{
    /// <summary>
    /// Enum SignInErrors
    /// </summary>
    public enum SignInErrors
    {
        Success = 0,
        AuthenticationFailed = 1,
        NotAuthenticated = 2,
        DeniesPermission = 3,
        DatabaseFailed = 250,
        OtherException = 251,
    }
}