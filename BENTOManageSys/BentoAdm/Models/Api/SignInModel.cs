﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BentoAdm.Models.Api
{
    /// <summary>
    /// Class SignInModel.
    /// </summary>
    public class SignInModel
    {
        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        /// <value>The user name.</value>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Username cannot be empty")]
        public string Username
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the encrypted password (MD5).
        /// </summary>
        /// <value>The encrypted password (MD5).</value>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Password cannot be empty")]
        [DataType(DataType.Password)]
        public string Password
        {
            get;
            set;
        }
    }
}