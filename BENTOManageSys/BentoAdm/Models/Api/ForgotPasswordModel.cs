﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BentoAdm.Models.Api
{
    /// <summary>
    /// Class ForgotPasswordModel.
    /// </summary>
    public class ForgotPasswordModel
    {
        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>The username.</value>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Username cannot be empty")]
        public string Username
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email cannot be empty")]
        [DataType(DataType.EmailAddress)]
        public string Email
        {
            get;
            set;
        }
    }
}