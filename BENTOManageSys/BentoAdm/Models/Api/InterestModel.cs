﻿using System;

namespace BentoAdm.Models.Api
{
    public class InterestModel
    {
        public Guid Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Slug
        {
            get;
            set;
        }

        public string Thumbnail
        {
            get;
            set;
        }

        public Guid? ParentId
        {
            get;
            set;
        }

        public int Level
        {
            get;
            set;
        }

        public int Total
        {
            get;
            set;
        }
    }
}