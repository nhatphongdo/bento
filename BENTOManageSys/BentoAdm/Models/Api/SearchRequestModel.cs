﻿using System.ComponentModel.DataAnnotations;

namespace BentoAdm.Models.Api
{
    /// <summary>
    /// Class SearchRequestModel.
    /// </summary>
    public class SearchRequestModel
    {
        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>The username.</value>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Username cannot be empty")]
        public string Username
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password.</value>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Password cannot be empty")]
        public string Password
        {
            get;
            set;
        }

        public string SearchTerm
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the page.
        /// </summary>
        /// <value>The page.</value>
        public int Page
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the items per page.
        /// </summary>
        /// <value>The items per page.</value>
        public int ItemsPerPage
        {
            get;
            set;
        }
    }
}