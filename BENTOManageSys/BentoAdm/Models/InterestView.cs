﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BentoAdm.Models
{
    public class InterestView
    {
        public System.Guid ID { get; set; }
        public System.DateTime DateCreated { get; set; }
        public string Slug { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Thumbnail { get; set; }
        public int Status { get; set; }
        public string ParentName { get; set; }
    }
}