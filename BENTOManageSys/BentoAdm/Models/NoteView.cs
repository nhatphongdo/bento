﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BentoAdm.Models
{
    public class NoteView
    {
        public System.Guid ID { get; set; }
        public System.DateTime DateCreated { get; set; }
        public string ObjectType { get; set; }
        public string Username { get; set; }
        public string Message { get; set; }
        public string IP { get; set; }
    }
}