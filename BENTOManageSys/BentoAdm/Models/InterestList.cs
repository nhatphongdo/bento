﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BentoAdm.Models
{
    public class InterestList
    {
        public System.Guid ID { get; set; }
        public System.DateTime DateCreated { get; set; }
        public string Slug { get; set; }
        public string Name { get; set; }
        public string Thumbnail { get; set; }
        public System.Guid CategoryID { get; set; }
        public string CategoryName { get; set; }
        public Nullable<Guid> ParentID { get; set; }
        public string ParentName { get; set; }
        public bool Selected { get; set; }
        public int Level { get; set; }
    }
}