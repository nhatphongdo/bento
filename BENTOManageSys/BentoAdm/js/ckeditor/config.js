/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';

    // config.removePlugins = 'resize';

    config.filebrowserBrowseUrl = '/portal/ckfinder/ckfinder.html';
    config.filebrowserImageBrowseUrl = '/portal/ckfinder/ckfinder.html?type=Images';
    config.filebrowserFlashBrowseUrl = '/portal/ckfinder/ckfinder.html?type=Flash';
    config.filebrowserUploadUrl = '/portal/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl = '/portal/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Images';
    config.filebrowserFlashUploadUrl = '/portal/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Flash';
    config.filebrowserWindowWidth = 1000;
    config.filebrowserWindowHeight = 700;

    config.extraPlugins = 'fontawesome,glyphicons,mathjax';
    config.contentsCss = ['/portal/css/font-awesome.css', '/portal/css/bootstrap.css', '/portal/css/Site.css'];
    config.allowedContent = true;

    config.font_names += ';Helvetica Neue/Helvetica Neue Regular';

    config.enterMode = CKEDITOR.ENTER_BR;
    config.shiftEnterMode = CKEDITOR.ENTER_P;
};
