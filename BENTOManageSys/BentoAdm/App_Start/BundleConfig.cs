﻿using System.Web;
using System.Web.Optimization;

namespace BentoAdm
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/script").Include(
                        "~/js/jquery-{version}.js",
                        "~/js/bootstrap.js",
                        "~/js/jquery.dataTables.min.js",
                        "~/js/dataTables.bootstrap.js",
                        "~/js/site.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                      "~/css/bootstrap.css",
                      "~/css/dataTables.bootstrap.css",
                      "~/css/font-awesome.css",
                      "~/css/sb-admin.css",
                      "~/css/site.css"));
        }
    }
}
