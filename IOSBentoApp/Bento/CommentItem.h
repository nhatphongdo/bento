//
//  CommentItem.h
//  Bento
//
//  Created by Do Nhat Phong on 9/29/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIPlaceholderTextView.h"
#import "CommentViewController.h"
#import "CommentModel.h"


@interface CommentItem : UIViewController {
    NSArray *replies;
}

@property (strong, nonatomic) CommentViewController *containerViewController;

@property (strong, nonatomic) NSString *commentID;
@property (nonatomic) BOOL liked;

@property (strong, nonatomic) IBOutlet UIImageView *commentAvatarImage;
@property (strong, nonatomic) IBOutlet UILabel *commentNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *commentBodyLabel;
@property (strong, nonatomic) IBOutlet UIButton *replyButton;
@property (strong, nonatomic) IBOutlet UIButton *likeButton;
@property (strong, nonatomic) IBOutlet UIScrollView *repliesScrollView;
@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UIPlaceholderTextView *commentTextView;
@property (strong, nonatomic) IBOutlet UIView *commentPostView;
@property (strong, nonatomic) IBOutlet UIView *commentBodyView;

- (IBAction)replyButtonTouched:(id)sender;
- (IBAction)likeButtonTouched:(id)sender;
- (IBAction)postCommentTouched:(id)sender;

- (void)setComment:(CommentModel *)comment;

@end
