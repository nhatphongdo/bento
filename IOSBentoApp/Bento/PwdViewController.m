//
//  PwdViewController.m
//  Bento
//
//  Created by VO HONG HAI on 10/27/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "PwdViewController.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import "Constants.h"

@interface PwdViewController ()

@end

@implementation PwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.emailLabel.text = self.email;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)regisNewUser:(NSString *)username
                pwd:(NSString *)pwd
         confirmpwd:(NSString *)confirmpwd
           fullname:(NSString *)fullname
           birthday:(NSString *)birthday
        accountType:(NSString *)accountType
          accountID:(NSString *)accountID
{
    [ServiceClient signUp:self.view username:username password:pwd confirmpassword:confirmpwd fullname:fullname birthday:birthday accountType:accountType accountID:accountID successBlock:^(id responseObject) {
        // Sign up succeeded
        NSString *md5Password = [ClientUtils encryptMd5:pwd];
        [ClientUtils saveSetting:USERNAME_KEY value:username];
        [ClientUtils saveSetting:MD5_PASSWORD_KEY value:md5Password];
        if([self.social isEqualToString:TEMPFB_PASSWORD_KEY])
        {
            [ClientUtils saveSetting:TEMPFB_EMAIL_KEY value:username];
            [ClientUtils saveSetting:TEMPFB_PASSWORD_KEY value:md5Password];
        }
        else if([self.social isEqualToString:TEMPGG_PASSWORD_KEY])
        {
            [ClientUtils saveSetting:TEMPGG_EMAIL_KEY value:username];
            [ClientUtils saveSetting:TEMPGG_PASSWORD_KEY value:md5Password];
        }
//        NSString *timestamp = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970] * 1000];
//        NSString *imgExtension = [self contentTypeForImageData:self.profilePic];
//        NSString *imageName = [NSString stringWithFormat:@"%@_%@",[username componentsSeparatedByString:@"@"][0], [timestamp componentsSeparatedByString:@"."][0]];
//        NSString *filename = [NSString stringWithFormat:@"%@.%@",imgExtension.lowercaseString];
//        [ServiceClient saveProfilePicture:self.view username:username md5Password:md5Password data:self.profilePic name:imageName filename:filename filetype:imgExtension successBlock:^(id responseObject) {
//
//        } failureBlock:^(NSError *error) {
//        }];
        [self dismissViewControllerAnimated:YES completion:nil];
    } failureBlock:^(NSError *error) {
    }];
}

- (IBAction)confirmButton:(id)sender
{
    if([self.pwdTextField.text isEqualToString:@""] || self.pwdTextField.text == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Password cannot be empty"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
    }
    if([self.confirmpwdTextField.text isEqualToString:@""] || self.confirmpwdTextField.text == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APPLICATION_TITLE message:@"Confirm password cannot be empty" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        alert = nil;
    }
    if(![self.confirmpwdTextField.text isEqualToString:self.confirmpwdTextField.text])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Password not equal to confirm password"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
    }
    
    [self regisNewUser:self.email pwd:self.pwdTextField.text confirmpwd:self.confirmpwdTextField.text fullname:self.fullname birthday:self.birthday accountType:self.accountType accountID:self.accountID];
}

- (IBAction)closeTouched:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSString *)contentTypeForImageData:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
        case 0x89:
            return @"image/png";
        case 0x47:
            return @"image/gif";
        case 0x49:
            break;
        case 0x42:
            return @"image/bmp";
        case 0x4D:
            return @"image/tiff";
    }
    return nil;
}

@end
