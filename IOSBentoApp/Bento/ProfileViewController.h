//
//  ProfileViewController.h
//  Bento
//
//  Created by VO HONG HAI on 9/18/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ProfileViewController : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate,MFMailComposeViewControllerDelegate>

@property (nonatomic,strong) IBOutlet UIButton *btShare;
@property (nonatomic,strong) IBOutlet UIButton *btRate;
@property (nonatomic,strong) IBOutlet UIButton *btSuggest;
@property (nonatomic,strong) IBOutlet UIButton *btReport;
@property (nonatomic,strong) IBOutlet UIButton *btLogout;
@property (nonatomic,strong) IBOutlet UILabel  *lblFullname;
@property (nonatomic,strong) IBOutlet UILabel  *lblAddress;
@property (nonatomic,strong) IBOutlet UILabel  *lblBirthday;
@property (nonatomic,strong) IBOutlet UILabel  *lblGender;
@property (nonatomic,strong) IBOutlet UIImageView  *ThumbImage;
@property (nonatomic,strong) IBOutlet UIButton *btUploadThumbnail;

@property (nonatomic, retain) UIImagePickerController *imgPicker;

- (IBAction)logOutTouched:(id)sender;
- (IBAction)uploadPhoto:(id)sender;
- (IBAction)shareThisAppTouched:(id)sender;
- (IBAction)rateThisAppTouched:(id)sender;
- (IBAction)reportThisAppTouched:(id)sender;
- (IBAction)suggestThisAppTouched:(id)sender;

@end
