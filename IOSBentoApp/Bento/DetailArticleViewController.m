//
//  DetailArticleViewController.m
//  Bento
//
//  Created by Do Nhat Phong on 10/12/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "DetailArticleViewController.h"
#import "ClientUtils.h"


@implementation DetailArticleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.contentWebview.delegate = self;
    self.previewView.minimumZoomScale = 0.2;
    self.previewView.maximumZoomScale = 10;
    self.previewView.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)hidePreviewTouched:(id)sender {
    [self.previewView setHidden:YES];
    [self.hidePreviewButton setHidden:YES];
}

- (void)setTitle:(NSString *)text {
    self.titleLabel.frame = CGRectMake(8, self.titleLabel.frame.origin.y, self.view.frame.size.width - 2 * 8, 20);
    self.titleLabel.text = [text uppercaseString];
    [self.titleLabel sizeToFit];
    CGSize titleSize = self.titleLabel.frame.size;
    
    CGRect frameAuthor = self.authorLabel.frame;
    frameAuthor.origin.y = self.titleLabel.frame.origin.y + titleSize.height + 5;
    if ([self.authorLabel.text isEqualToString:@""] || self.authorLabel.text == nil) {
        frameAuthor.size.height = 0;
    }
    self.authorLabel.frame = frameAuthor;
    
    CGRect frameContent = self.contentWebview.frame;
    frameContent.origin.y = self.authorLabel.frame.origin.y + self.authorLabel.frame.size.height + 5;
    frameContent.size.height = self.view.frame.size.height - frameContent.origin.y;
    self.contentWebview.frame = frameContent;
}

- (void)setBody:(NSString *)body {
    if (body == nil) {
        [self.contentWebview loadHTMLString:@"" baseURL:nil];
        return;
    }
    
    if ([body rangeOfString:@"<html>" options:NSCaseInsensitiveSearch].location == NSNotFound) {
        body = [NSString stringWithFormat:@"\
                <html>\
                <head>\
                <style>\
                a {\
                    cursor: pointer;\
                }\
                img {\
                    max-width: 100%%;\
                    height: auto;\
                }\
                </style>\
                </head>\
                <body style='color: #333; font-family: Helvetica; font-size: 12px;'>%@</body>\
                </html>", body];
    }
    
    [self.contentWebview loadHTMLString:body baseURL:nil];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSString *jsContent = @"\
    var images = document.getElementsByTagName('img');\
    for (var i = images.length - 1; i >= 0; i--) {\
        images[i].style.display = 'none';\
        var anchor = document.createElement('a');\
        anchor.href = 'nolink://#' + images[i].src;\
        var img = document.createElement('img');\
        img.src = images[i].src;\
        anchor.appendChild(img);\
        var parent = images[i].parentNode;\
        parent.insertBefore(anchor, images[i]);\
    }\
    ";
    [webView stringByEvaluatingJavaScriptFromString:jsContent];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if(navigationType == UIWebViewNavigationTypeLinkClicked || navigationType == UIWebViewNavigationTypeFormSubmitted) {
        NSURL *url = [request URL];
        if ([[url scheme] isEqualToString:@"nolink"]) {
            NSString *urlstr = [url fragment];

            [self.previewImageView setUrl:urlstr];
            [self.previewView setHidden:NO];
            [self.hidePreviewButton setHidden:NO];
            [self.previewView setZoomScale:1.0 animated:YES];
            
            return NO;
        }
    }
    return YES;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.previewImageView;
}


@end
