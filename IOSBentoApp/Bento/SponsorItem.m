//
//  SponsorItem.m
//  Bento
//
//  Created by VO HONG HAI on 10/13/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "SponsorItem.h"
#import "Constants.h"
#import "ClientUtils.h"
#import "ServiceClient.h"

@implementation SponsorItem

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
