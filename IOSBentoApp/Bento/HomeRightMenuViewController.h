//
//  HomeRightMenuViewController.h
//  Bento
//
//  Created by Do Nhat Phong on 10/11/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeRightMenuViewController : UIViewController<UIGestureRecognizerDelegate> {
    NSArray *interests;
    BOOL hasChanges;
}

@property (strong, nonatomic) NSMutableArray *selectedInterests;

@property (strong, nonatomic) IBOutlet UITableView *interestsTableView;

- (IBAction)closeTouched:(id)sender;

@end
