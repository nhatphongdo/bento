//
//  SponsorDetailViewController.m
//  Bento
//
//  Created by VO HONG HAI on 10/15/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "SponsorDetailViewController.h"
#import "QuartzCore/QuartzCore.h"
#import "Constants.h"
#import "ServiceClient.h"   
#import "ClientUtils.h"
#import "SponsorModel.h"

@interface SponsorDetailViewController ()
{
    SponsorModel *sponsor;
}
@end

@implementation SponsorDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.wrapButton.layer.borderWidth = 1.0f;
    self.wrapButton.layer.borderColor = [[UIColor colorWithRed:58.0f/255.0f green:194.0f/255.0f blue:207.0f/255.0f alpha:1.0f]CGColor];
    
    [self loadSponsorDetail];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)loadSponsorDetail {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    [ServiceClient getDetailSponsor:self.view username:username md5Password:password sponsorID:self.SponsorID successBlock:^(id responseObject) {
        sponsor = responseObject;
        self.navigationItem.title = sponsor.Name;

        NSString *body = sponsor.BodyContent;
        if ([body rangeOfString:@"<html>" options:NSCaseInsensitiveSearch].location == NSNotFound) {
            body = [NSString stringWithFormat:@"<html><body style='color: #ffffff; font-family: Helvetica; font-size: 12px;  padding:0; margin:0;'>%@</body></html>", body];
        }
        [self.bodyWebView loadHTMLString:body baseURL:nil];
        
    } failureBlock:^(NSError *error) {
    }];
}

- (IBAction)linkTouch:(id)sender {
    NSURL *URL = [NSURL URLWithString:sponsor.WebAddress];
    [[UIApplication sharedApplication] openURL:URL];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
