//
//  HomeLeftMenuViewController.h
//  Bento
//
//  Created by Do Nhat Phong on 10/11/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeLeftMenuViewController : UIViewController<UIGestureRecognizerDelegate> {
    NSArray *interests;
    NSMutableArray *appearInterests;
}

@property (strong, nonatomic) NSString *selectedInterest;

@property (strong, nonatomic) IBOutlet UITableView *interestsTableView;

- (void)loadInterests;
- (IBAction)closeTouched:(id)sender;

@end
