//
//  FeedItem.m
//  Bento
//
//  Created by VO HONG HAI on 9/8/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "FeedItem.h"
#import "Constants.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import "ArticleModel.h"


@implementation FeedItem

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (IBAction)seemoreBtnClick:(id)sender {
//    self.rearImageView.image = self.image.image;
//    self.rearTitleLabel.text = self.lblTitle.text;
//    self.rearAuthorLabel.text = self.lblAuthor.text;
    
//    [CATransaction flush];
//    [UIView transitionFromView:self.frontView toView:self.rearView duration:0.4f options:UIViewAnimationOptionTransitionFlipFromLeft completion:^(BOOL finished) {
//        // Load body
//        NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
//        NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
//        
//        [ServiceClient getDetailArticle:self.rearBodyView username:username md5Password:password articleID:self.articleID successBlock:^(id responseObject) {
//            // Load articles succeeded
//            ArticleModel *item = (ArticleModel *)responseObject;
//            [self setBody:item.Body];
//        } failureBlock:^(NSError *error) {
//        }];
//    }];
}

- (IBAction)hideDetailButtonTouched:(id)sender {
//    [CATransaction flush];
//    [UIView transitionFromView:self.rearView toView:self.frontView duration:0.4f options:UIViewAnimationOptionTransitionFlipFromLeft completion:NULL];
}

- (void)setTitle:(NSString *)text {
    self.lblTitle.text = [text uppercaseString];
    [self.lblTitle sizeToFit];
    CGRect frameTitle = self.lblTitle.frame;
    if (frameTitle.origin.y + frameTitle.size.height > self.frame.size.height) {
        frameTitle.size.height = self.frame.size.height - frameTitle.origin.y;
        self.lblTitle.frame = frameTitle;
    }
    
    CGSize titleSize = self.lblTitle.frame.size;

    CGRect frameContent = self.txtContent.frame;
    frameContent.origin.y = self.lblTitle.frame.origin.y + titleSize.height;
    frameContent.size.height = self.frame.size.height - frameContent.origin.y;
    self.txtContent.frame = frameContent;
}

- (void)setBody:(NSString *)body {
    if (body == nil) {
        [self.rearBodyView loadHTMLString:@"" baseURL:nil];
        return;
    }
    
    if ([body rangeOfString:@"<html>" options:NSCaseInsensitiveSearch].location == NSNotFound) {
        body = [NSString stringWithFormat:@"<html><body style='color: #ffffff; font-family: Helvetica; font-size: 12px'>%@</body></html>", body];
    }
    
    [self.rearBodyView loadHTMLString:body baseURL:nil];
}

@end
