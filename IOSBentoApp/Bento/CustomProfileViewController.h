//
//  CustomProfileViewController.h
//  Bento
//
//  Created by VO HONG HAI on 9/29/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileModel.h"
#import "NIDropDown.h"


@interface CustomProfileViewController : UIViewController<NIDropDownDelegate> {
    NSArray *interests;
    NIDropDown *dropDown;
}

@property (nonatomic,strong) IBOutlet UIImageView *ThumbnailImage;
@property (nonatomic,strong) IBOutlet UITextField *FullNameTextField;
@property (nonatomic,strong) IBOutlet UITextField *BirthDayTextField;
@property (nonatomic,strong) IBOutlet UIButton *GenderButton;
@property (nonatomic,strong) IBOutlet UITableView *interestsTableView;

-(IBAction)updateProfile:(id)sender;
-(IBAction)selectGender:(id)sender;

@end
