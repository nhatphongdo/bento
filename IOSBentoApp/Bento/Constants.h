//
//  Constants.h
//  Bento
//
//  Created by Do Nhat Phong on 9/25/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#ifndef Bento_Constants_h
#define Bento_Constants_h

#define APPLICATION_TITLE @"BENTO"
#define GOOGLE_PLUS_ID @"402410395378-t71h8ue4pjdmusmuogihgocpl7rm1jmu.apps.googleusercontent.com"

#define USERNAME_KEY @"UserNameKey"
#define MD5_PASSWORD_KEY @"Md5PasswordKey"
#define TEMPGG_EMAIL_KEY @"TempGGEmailKey"
#define TEMPGG_PASSWORD_KEY @"TempGGPasswordKey"
#define TEMPFB_EMAIL_KEY @"TempFBEmailKey"
#define TEMPFB_PASSWORD_KEY @"TempFBPasswordKey"
#define FULLNAME_KEY @"FullNameKey"
#define AVATAR_KEY @"AvatarKey"
#define GENDER_KEY @"GenderKey"
#define ACCOUNT_TYPE_KEY @"AccountTypeKey"
#define DATE_OF_BIRTH_KEY @"DateOfBirthKey"
#define EMAIL_KEY @"EmailKey"
#define CAPTION_KEY @"CaptionKey"

#define ROOT_API_URL @"http://bentoportal.com/portal/api/"
#define SIGN_IN_URL [ROOT_API_URL stringByAppendingString:@"accounts/signin"]
#define SIGN_OUT_URL [ROOT_API_URL stringByAppendingString:@"accounts/signout"]
#define SIGN_UP_URL [ROOT_API_URL stringByAppendingString:@"accounts/signup"]
#define FORGOT_PASSWORD_URL [ROOT_API_URL stringByAppendingString:@"accounts/forgot"]
#define GET_PROFILE_URL [ROOT_API_URL stringByAppendingString:@"accounts/profile"]
#define UPDATE_PROFILE_URL [ROOT_API_URL stringByAppendingString:@"accounts/update"]
#define GET_USER_INTERESTS_URL [ROOT_API_URL stringByAppendingString:@"accounts/interests"]
#define SET_USER_INTERESTS_URL [ROOT_API_URL stringByAppendingString:@"accounts/interests/update"]
#define UPLOAD_AVATAR_URL [ROOT_API_URL stringByAppendingString:@"accounts/update/avatar"] //add binary file to this

#define GET_INTERESTS_URL [ROOT_API_URL stringByAppendingString:@"interests"]
#define GET_STRUCTURE_INTERESTS_URL [ROOT_API_URL stringByAppendingString:@"interests/structure"]
#define GET_STRUCTURE_FAVORITE_URL [ROOT_API_URL stringByAppendingString:@"interests/structure/favorite"]
#define GET_STRUCTURE_USER_INTERESTS_URL [ROOT_API_URL stringByAppendingString:@"interests/structure/user"]
#define GET_ARTICLES_OF_INTEREST_URL [ROOT_API_URL stringByAppendingString:@"interests/%@/articles"]
#define GET_USER_ARTICLES_OF_INTEREST_URL [ROOT_API_URL stringByAppendingString:@"interests/%@/myarticles"]
#define GET_FAVORITE_ARTICLE_URL [ROOT_API_URL stringByAppendingString:@"interests/%@/myfavorites"]

#define GET_LATEST_ARTICLES_URL [ROOT_API_URL stringByAppendingString:@"articles/latest"]
#define GET_DETAIL_ARTICLE_URL [ROOT_API_URL stringByAppendingString:@"articles/%@"]
#define GET_COMMENTS_OF_ARTICLE_URL [ROOT_API_URL stringByAppendingString:@"articles/%@/comments"]
#define GET_REPLIES_OF_COMMENT_URL [ROOT_API_URL stringByAppendingString:@"comments/%@/replies"]
#define POST_COMMENT_URL [ROOT_API_URL stringByAppendingString:@"articles/%@/comments/post"]
#define POST_REPLY_URL [ROOT_API_URL stringByAppendingString:@"comments/%@/reply"]
#define GET_NOTES_OF_ARTICLE_URL [ROOT_API_URL stringByAppendingString:@"articles/%@/notes"]
#define POST_NOTE_FOR_ARTICLE_URL [ROOT_API_URL stringByAppendingString:@"articles/%@/notes/post"]
#define DELETE_NOTE_URL [ROOT_API_URL stringByAppendingString:@"notes/%@/delete"]
#define GET_SHARES_OF_ARTICLE_URL [ROOT_API_URL stringByAppendingString:@"articles/%@/shares"]
#define GET_LIKES_OF_ARTICLE_URL [ROOT_API_URL stringByAppendingString:@"articles/%@/likes"]
#define LIKE_POST_URL [ROOT_API_URL stringByAppendingString:@"action/%@/like"]
#define UNLIKE_POST_URL [ROOT_API_URL stringByAppendingString:@"action/%@/unlike"]
#define SHARE_POST_URL [ROOT_API_URL stringByAppendingString:@"action/%@/share"]
#define FAVORITE_POST_URL [ROOT_API_URL stringByAppendingString:@"action/%@/favorite"]
#define UNFAVORITE_POST_URL [ROOT_API_URL stringByAppendingString:@"action/%@/unfavorite"]
#define REPORT_POST_URL [ROOT_API_URL stringByAppendingString:@"articles/%@/reports/post"]

#define GET_SPONSORS_URL [ROOT_API_URL stringByAppendingString:@"sponsors"]
#define GET_SPONSOR_DETAIL_URL [ROOT_API_URL stringByAppendingString:@"sponsors/%@"]

#define SEARCH_ARTICLES_URL [ROOT_API_URL stringByAppendingString:@"articles/search"]
#define SEARCH_MY_ARTICLES_URL [ROOT_API_URL stringByAppendingString:@"articles/mysearch"]

#define IMAGE_ROUND_CORNER_RADIUS 5


#endif
