//
//  FavoriteViewController.m
//  Bento
//
//  Created by VO HONG HAI on 9/18/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "FavoriteViewController.h"
#import "CategoryItem.h"
#import "Constants.h"
#import "ServiceClient.h"
#import "ClientUtils.h"
#import "InterestModel.h"
#import "FeedViewController.h"
#import "InterestCell.h"
#import "SearchViewController.h"


@implementation FavoriteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    pullToRefreshControl = [[SRRefreshView alloc] init];
    pullToRefreshControl.delegate = self;
    pullToRefreshControl.upInset = 10;
    pullToRefreshControl.slimeMissWhenGoingBack = YES;
    pullToRefreshControl.slime.bodyColor = [UIColor blackColor];
    pullToRefreshControl.slime.skinColor = [UIColor whiteColor];
    pullToRefreshControl.slime.lineWith = 1;
    pullToRefreshControl.slime.shadowBlur = 4;
    pullToRefreshControl.slime.shadowColor = [UIColor blackColor];
    [self.tableView addSubview:pullToRefreshControl];
    
    [self loadInterests];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self loadInterests];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadInterests {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    [ServiceClient getStructureUserFavorites:self.view username:username md5Password:password successBlock:^(id responseObject) {
        interests = responseObject;
        
        appearInterests = [[NSMutableArray alloc] init];
        for (uint i = 0; i < [interests count]; i++) {
            InterestModel *item = [interests objectAtIndex:i];
            if ([item.ParentID isEqualToString:@""]) {
                InterestModel *newItem = [[InterestModel alloc] init];
                newItem.ID = item.ID;
                newItem.Name = item.Name;
                newItem.ParentID = item.ParentID;
                newItem.Thumbnail = item.Thumbnail;
                newItem.Level = item.Level;
                newItem.isCollapsed = YES;
                newItem.Total = item.Total;
                [appearInterests addObject:newItem];
            }
        }
        [self.tableView reloadData];
    } failureBlock:^(NSError *error) {
    }];
}

#pragma mark - slimeRefresh delegate

- (void)slimeRefreshStartRefresh:(SRRefreshView *)refreshView {
    
    [appearInterests removeAllObjects];
    [self loadInterests];
    [pullToRefreshControl performSelector:@selector(endRefresh)
                               withObject:nil
                               afterDelay:0
                                  inModes:[NSArray arrayWithObject:NSRunLoopCommonModes]];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Count
    return [appearInterests count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"interestCell";
    InterestCell *cell = (InterestCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[InterestCell alloc] init];
    }
    
    InterestModel *item = [appearInterests objectAtIndex:indexPath.row];
    if (item.isCollapsed == YES) {
        [cell.collapseButton setImage:[UIImage imageNamed:@"iconArrowLeft"] forState:UIControlStateNormal];
    }
    else {
        [cell.collapseButton setImage:[UIImage imageNamed:@"iconArrowDown"] forState:UIControlStateNormal];
    }
    cell.collapseButton.tag = indexPath.row;
    
    [cell.collapseButton addTarget:self action:@selector(openCollapse:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.nameLabel.text = item.Name;
    CGRect frame = cell.nameLabel.frame;
    frame.origin.x = cell.collapseButton.frame.origin.x + cell.collapseButton.frame.size.width + 10 * (item.Level - 1);
    frame.size.width = cell.frame.size.width - frame.origin.x - 10 - cell.countLabel.frame.size.width;
    cell.nameLabel.frame = frame;
    
    cell.countLabel.text = [NSString stringWithFormat:@"%d",item.Total];
    CGRect frameCount = cell.countLabel.frame;
    frameCount.origin.x = frame.origin.x + frame.size.width + 2;
    cell.countLabel.frame = frameCount;

    if (item.Level > 1) {
        [cell.collapseButton setHidden:YES];
    }
    else {
        [cell.collapseButton setHidden:NO];
    }
    
//    cell.tag = indexPath.row;
//    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
//                                          initWithTarget:self
//                                          action:@selector(handleViewTap:)];
//    tapGesture.delegate = self;
//    [cell addGestureRecognizer:tapGesture];
//    tapGesture = nil;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"managetofeed" sender:indexPath];
}

- (void)openCollapse:(id)sender {
    // Change collapse
    UIButton *button = (UIButton *)sender;
    InterestModel *item = [appearInterests objectAtIndex:button.tag];
    
    if (item.isCollapsed == YES) {
        uint idx = button.tag + 1;
        for (uint i = 0; i < [interests count]; i++) {
            InterestModel *childItem = [interests objectAtIndex:i];
            if ([childItem.ParentID isEqualToString:item.ID]) {
                InterestModel *newItem = [[InterestModel alloc] init];
                newItem.ID = childItem.ID;
                newItem.Name = childItem.Name;
                newItem.ParentID = childItem.ParentID;
                newItem.Thumbnail = childItem.Thumbnail;
                newItem.Level = childItem.Level;
                newItem.isCollapsed = YES;
                newItem.Total = childItem.Total;
                [appearInterests insertObject:newItem atIndex:idx];
                ++idx;
            }
        }
        item.isCollapsed = NO;
    }
    else {
        // Remove appeared items
        for (int i = [appearInterests count] - 1; i >= 0; i--) {
            InterestModel *childItem = [appearInterests objectAtIndex:i];
            if ([childItem.ParentID isEqualToString:item.ID]) {
                [appearInterests removeObjectAtIndex:i];
            }
        }
        item.isCollapsed = YES;
    }
    
    [self.tableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"managetofeed"]) {
        NSIndexPath *indexPath = (NSIndexPath *)sender;
        FeedViewController *feedView = (FeedViewController *)segue.destinationViewController;
        InterestModel *item = [appearInterests objectAtIndex:indexPath.row];
        feedView.loadForUserOnly = YES;
        feedView.interestID = item.ID;
        feedView.interestName = item.Name;
    }
}

- (IBAction)searchViewTouched:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Bento" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"searchfavoriteview"];
    //[self.navigationController pushViewController:vc animated:YES];
    [self presentViewController:vc animated:YES completion: nil];
}
@end
