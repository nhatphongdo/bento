//
//  SplashViewController.m
//  Bento
//
//  Created by Do Nhat Phong on 10/12/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "SplashViewController.h"
#import <pop/POP.h>


@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // Animate loading logo
    POPBasicAnimation *opacityAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    opacityAnimation.fromValue = @(0);
    opacityAnimation.toValue = @(1);
    opacityAnimation.duration = 0.5;
    opacityAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [opacityAnimation setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        [self performSegueWithIdentifier:@"LoginSegue" sender:nil];
    }];
    
    POPBasicAnimation *opacityAnimation1 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    opacityAnimation1.fromValue = @(0);
    opacityAnimation1.toValue = @(1);
    opacityAnimation1.duration = 0.5;
    opacityAnimation1.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [opacityAnimation1 setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        self.textBentImage.hidden = NO;
        self.textOImage.hidden = NO;
        self.textBusiness.hidden = YES;
        self.textExpert.hidden = YES;
        self.textNetworks.hidden = YES;
        self.textTraining.hidden = YES;
        self.textOperation.hidden = YES;
        [self.textOImage.layer pop_addAnimation:opacityAnimation forKey:@"opacityAnimation"];
        [self.textBentImage.layer pop_addAnimation:opacityAnimation forKey:@"opacityAnimation"];
    }];
    
    POPBasicAnimation *opacityAnimation2 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    opacityAnimation2.fromValue = @(0);
    opacityAnimation2.toValue = @(1);
    opacityAnimation2.duration = 0.5;
    opacityAnimation2.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [opacityAnimation2 setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        self.textOperation.hidden = NO;
        [self.textOperation.layer pop_addAnimation:opacityAnimation1 forKey:@"opacityAnimation"];
    }];
    
    POPBasicAnimation *opacityAnimation3 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    opacityAnimation3.fromValue = @(0);
    opacityAnimation3.toValue = @(1);
    opacityAnimation3.duration = 0.5;
    opacityAnimation3.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [opacityAnimation3 setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        self.textTraining.hidden = NO;
        [self.textTraining.layer pop_addAnimation:opacityAnimation2 forKey:@"opacityAnimation"];
    }];
    
    POPBasicAnimation *opacityAnimation4 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    opacityAnimation4.fromValue = @(0);
    opacityAnimation4.toValue = @(1);
    opacityAnimation4.duration = 0.5;
    opacityAnimation4.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [opacityAnimation4 setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        self.textNetworks.hidden = NO;
        [self.textNetworks.layer pop_addAnimation:opacityAnimation3 forKey:@"opacityAnimation"];
    }];
    
    POPBasicAnimation *opacityAnimation5 = [POPBasicAnimation animationWithPropertyNamed:kPOPLayerOpacity];
    opacityAnimation5.fromValue = @(0);
    opacityAnimation5.toValue = @(1);
    opacityAnimation5.duration = 0.5;
    opacityAnimation5.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [opacityAnimation5 setCompletionBlock:^(POPAnimation *anim, BOOL finished) {
        self.textExpert.hidden = NO;
        [self.textExpert.layer pop_addAnimation:opacityAnimation4 forKey:@"opacityAnimation"];
    }];
    [self.textBusiness.layer pop_addAnimation:opacityAnimation5 forKey:@"opacityAnimation"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
