//
//  NoteItemViewController.m
//  Bento
//
//  Created by Do Nhat Phong on 10/14/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "NoteItem.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import "Constants.h"


@implementation NoteItem

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)deleteTouched:(id)sender {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    [ServiceClient deleteNote:self.view username:username md5Password:password noteID:self.noteID successBlock:^(id responseObject) {
        // Post note succeeded
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Your note is deleted successfully"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        NoteModel *item = nil;
        for (uint i = 0; i < [self.containerViewController.notes count]; i++) {
            if ([((NoteModel *)[self.containerViewController.notes objectAtIndex:i]).ID isEqualToString:self.noteID]) {
                item = [self.containerViewController.notes objectAtIndex:i];
                [self.containerViewController.notes removeObject:item];
                [self.containerViewController reloadNotesView];
                break;
            }
        }
    } failureBlock:^(NSError *error) {
    }];
}

- (void)setNote:(NoteModel *)note {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd MMMM, yyyy";
    self.dateLabel.text = [dateFormatter stringFromDate:note.DateCreated];
    
    self.contentLabel.text = note.Message;
    [self.contentLabel sizeToFit];
    
    CGRect contentFrame = self.contentLabel.frame;
    contentFrame.origin.y = self.dateLabel.frame.origin.y + self.dateLabel.frame.size.height + 5;
    self.contentLabel.frame = contentFrame;
    
    CGRect frame = self.view.frame;
    frame.size.height = self.contentLabel.frame.origin.y + self.contentLabel.frame.size.height + 30;
    self.view.frame = frame;
    
    self.noteID = note.ID;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
