//
//  CommentItem.m
//  Bento
//
//  Created by Do Nhat Phong on 9/29/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "CommentItem.h"
#import "Constants.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <QuartzCore/QuartzCore.h>
#import "ReplyItem.h"


@interface CommentItem ()

@end

@implementation CommentItem

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.commentAvatarImage.layer.cornerRadius = IMAGE_ROUND_CORNER_RADIUS;
    self.commentAvatarImage.clipsToBounds = YES;

    self.avatarImageView.layer.cornerRadius = IMAGE_ROUND_CORNER_RADIUS;
    self.avatarImageView.clipsToBounds = YES;
    
    self.commentTextView.placeholder = @"Write your comment";
    
    [self.avatarImageView setUrl:[ClientUtils loadSetting:AVATAR_KEY]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)replyButtonTouched:(id)sender {
    if ([self.commentPostView isHidden] == YES)
    {
        // Show reply input
        [self.avatarImageView setHidden:NO];
        [self.commentPostView setHidden:NO];
        
        [self.replyButton setTitle:@"Cancel" forState:UIControlStateNormal];
        
        CGRect repliesFrame = self.repliesScrollView.frame;
        repliesFrame.origin.y += self.commentPostView.frame.size.height + 8;
        self.repliesScrollView.frame = repliesFrame;
        
        CGRect frame = self.view.frame;
        frame.size.height += self.commentPostView.frame.size.height + 8; // 8 is gap
        self.view.frame = frame;
    }
    else {
        [self.avatarImageView setHidden:YES];
        [self.commentPostView setHidden:YES];
        
        [self.replyButton setTitle:@"Reply" forState:UIControlStateNormal];
        
        CGRect repliesFrame = self.repliesScrollView.frame;
        repliesFrame.origin.y -= (self.commentPostView.frame.size.height + 8);
        self.repliesScrollView.frame = repliesFrame;
        
        CGRect frame = self.view.frame;
        frame.size.height -= (self.commentPostView.frame.size.height + 8); // 8 is gap
        self.view.frame = frame;
    }
    
    if (self.containerViewController) {
        [self.containerViewController refreshCommentsView];
    }
}

- (IBAction)likeButtonTouched:(id)sender {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    if (self.liked == YES) {
        // Unlike
        self.liked = NO;
        [ServiceClient unlikePost:self.view username:username md5Password:password postID:self.commentID successBlock:^(id responseObject) {
            // UnLike article succeeded
            [self.likeButton setTitle:[NSString stringWithFormat:@"%@", responseObject] forState:UIControlStateNormal];
            [self.likeButton setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
        } failureBlock:^(NSError *error) {
        }];
    }
    else {
        // Like
        self.liked = YES;
        [ServiceClient likePost:self.view username:username md5Password:password postID:self.commentID successBlock:^(id responseObject) {
            // Like article succeeded
            [self.likeButton setTitle:[NSString stringWithFormat:@"%@", responseObject] forState:UIControlStateNormal];
            [self.likeButton setImage:[UIImage imageNamed:@"like_hover"] forState:UIControlStateNormal];
        } failureBlock:^(NSError *error) {
        }];
    }
}

- (IBAction)postCommentTouched:(id)sender {
    if ([self.commentTextView.text isEqualToString:@""]) {
        return;
    }
    
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    [ServiceClient postReply:self.containerViewController.view username:username md5Password:password postID:self.commentID message:self.commentTextView.text successBlock:^(id responseObject) {
        // Post comment succeeded
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Your comment is saved successfully"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        self.commentTextView.text = @"";
        [self replyButtonTouched:self.replyButton];
        
        [self loadReplies];
    } failureBlock:^(NSError *error) {
    }];
}

- (void)setComment:(CommentModel *)comment {
    self.commentID = comment.ID;
    [self.commentAvatarImage setImageWithURL:[NSURL URLWithString:comment.Avatar] placeholderImage:[UIImage imageNamed:@"no_image"]];
    self.commentNameLabel.text = comment.Fullname;
    self.commentBodyLabel.text = comment.Message;
    [self.commentBodyLabel sizeToFit];
    CGRect commentFrame = self.commentBodyLabel.frame;
    commentFrame.size.width = self.commentBodyView.frame.size.width - 10;
    self.commentBodyLabel.frame = commentFrame;
    [self.likeButton setTitle:[NSString stringWithFormat:@"%d", comment.Likes] forState:UIControlStateNormal];
    self.liked = comment.Liked;
    if (comment.Liked == YES) {
        [self.likeButton setImage:[UIImage imageNamed:@"like_hover"] forState:UIControlStateNormal];
    }
    else {
        [self.likeButton setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
    }
    
    CGRect commentViewFrame = self.commentBodyView.frame;
    commentViewFrame.size.height = commentFrame.size.height + 10;
    self.commentBodyView.frame = commentViewFrame;
    
    float y = self.commentBodyView.frame.origin.y + self.commentBodyView.frame.size.height + 8; // 8 is gap
    CGRect replyButtonFrame = self.replyButton.frame;
    replyButtonFrame.origin.y = y;
    self.replyButton.frame = replyButtonFrame;
    CGRect likeButtonFrame = self.likeButton.frame;
    likeButtonFrame.origin.y = y;
    self.likeButton.frame = likeButtonFrame;
    
    y += self.replyButton.frame.size.height + 8;
    CGRect repliesFrame = self.repliesScrollView.frame;
    repliesFrame.origin.y = y;
    self.repliesScrollView.frame = repliesFrame;
    
    CGRect replyAvatarFrame = self.avatarImageView.frame;
    replyAvatarFrame.origin.y = y;
    self.avatarImageView.frame = replyAvatarFrame;
    CGRect replyCommentView = self.commentPostView.frame;
    replyCommentView.origin.y = y;
    self.commentPostView.frame = replyCommentView;
    
    y += self.repliesScrollView.frame.size.height + 8 + 2 + 5;
    CGRect frame = self.view.frame;
    frame.size.height = y;
    self.view.frame = frame;
    
    [self loadReplies];
}

- (void)loadReplies {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    [ServiceClient getRepliesOfComment:self.containerViewController.view username:username md5Password:password postID:self.commentID page:1 itemsPerPage:10 successBlock:^(id responseObject) {
        // Load articles succeeded
        replies = responseObject;
        
        [[self.repliesScrollView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        float y = 0;
        for (uint i = 0; i < [replies count]; i++) {
            CommentModel *item = [replies objectAtIndex:i];
            ReplyItem *commentView = [[ReplyItem alloc] initWithNibName:@"ReplyItem" bundle:nil];
            commentView.containerViewController = self.containerViewController;
            [self addChildViewController:commentView];
            [self.repliesScrollView addSubview:commentView.view];
            [commentView didMoveToParentViewController:self];
            [commentView setReply:item];
            commentView.view.frame = CGRectMake(0, y, self.repliesScrollView.frame.size.width, commentView.view.frame.size.height);
            y += commentView.view.frame.size.height;
        }
        
        CGRect repliesViewFrame = self.repliesScrollView.frame;
        repliesViewFrame.origin.x = self.replyButton.frame.origin.x;
        repliesViewFrame.origin.y = self.likeButton.frame.origin.y + self.likeButton.frame.size.height + 8;
        repliesViewFrame.size.height = y;
        repliesViewFrame.size.width = self.view.frame.size.width - repliesViewFrame.origin.x - 8;
        self.repliesScrollView.frame = repliesViewFrame;
        
        CGRect frame = self.view.frame;
        frame.size.height = self.repliesScrollView.frame.origin.y + self.repliesScrollView.frame.size.height + 8 + 2 + 5;
        self.view.frame = frame;

        if (self.containerViewController) {
            [self.containerViewController refreshCommentsView];
        }
    } failureBlock:^(NSError *error) {
    }];
}

@end
