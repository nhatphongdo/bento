//
//  ReportViewController.h
//  Bento
//
//  Created by VO HONG HAI on 11/17/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICheckbox.h"

@interface ReportViewController : UIViewController<UITextViewDelegate> {
    //type = 1: content
    //type = 2: technical
    //type = 3: other
    NSString *type;
}

@property (nonatomic,strong) IBOutlet UIButton *btnSendReport;
@property (nonatomic,strong) IBOutlet UITextView *message;
@property (nonatomic,strong) IBOutlet UICheckbox *cbContent;
@property (nonatomic,strong) IBOutlet UICheckbox *cbTechnical;
@property (nonatomic,strong) IBOutlet UICheckbox *cbOther;
@property (nonatomic,strong) NSString *articleID;

- (IBAction)selectcbContent:(id)sender;
- (IBAction)selectcbTechnical:(id)sender;
- (IBAction)selectcbOther:(id)sender;

- (IBAction)sendTouched:(id)sender;
- (IBAction)closeTouched:(id)sender;
@end
