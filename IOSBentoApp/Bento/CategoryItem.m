//
//  CategoryItem.m
//  Bento
//
//  Created by VO HONG HAI on 9/18/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "CategoryItem.h"

@implementation CategoryItem

- (void)awakeFromNib {
    // Initialization code
}

- (void) setInterestSelected:(BOOL)selected {
    if (selected == YES) {
        [self.selectorImage setImage:[UIImage imageNamed:@"bg_selector_selected"]];
    }
    else {
        [self.selectorImage setImage:[UIImage imageNamed:@"bg_selector"]];
    }
}

@end
