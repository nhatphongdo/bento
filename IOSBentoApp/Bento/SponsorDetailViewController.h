//
//  SponsorDetailViewController.h
//  Bento
//
//  Created by VO HONG HAI on 10/15/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SponsorDetailViewController : UIViewController

@property (nonatomic,strong) IBOutlet UIButton *linkButton;
@property (nonatomic,strong) IBOutlet UIWebView *bodyWebView;
@property (nonatomic,strong) IBOutlet UIView *wrapButton;

@property (nonatomic,strong) NSString *SponsorID;

-(IBAction)linkTouch:(id)sender;

@end
