//
//  SponsorModel.h
//  Bento
//
//  Created by VO HONG HAI on 10/15/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SponsorModel : NSObject

@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) NSString *Name;
@property (strong, nonatomic) NSString *WebAddress;
@property (strong, nonatomic) NSString *Logo;
@property (strong, nonatomic) NSString *ShortDescription;
@property (strong, nonatomic) NSString *Address;
@property (strong, nonatomic) NSString *Email;
@property (strong, nonatomic) NSString *Phone;
@property (strong, nonatomic) NSString *Fax;
@property (strong, nonatomic) NSString *BodyContent;
@property (strong, nonatomic) NSDate *DateCreated;

@end
