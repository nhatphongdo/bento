//
//  SearchViewController.h
//  Bento
//
//  Created by VO HONG HAI on 10/31/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddNoteViewController.h"
#import "CommentViewController.h"
#import "SharesViewController.h"
#import "DetailArticleViewController.h"
#import "SRRefreshView.h"


@interface SearchViewController :  UIViewController<SRRefreshDelegate> {
    AddNoteViewController *addNoteViewController;
    CommentViewController *commentsViewController;
    SharesViewController *sharesViewController;
    DetailArticleViewController *detailViewController;
    DetailArticleViewController *leftDetailViewController;
    DetailArticleViewController *rightDetailViewController;
    SRRefreshView *pullToRefreshControl;
    NSMutableArray *articles;
    BOOL isMovingNext;
    BOOL isRunning;
    NSTimer *timer;
}

@property (nonatomic) int currentPage;
@property (nonatomic) NSInteger currentItemIndex;

@property (nonatomic,strong) IBOutlet UITableView *tableView;
@property (nonatomic,strong) IBOutlet UIButton *closeButton;
@property (nonatomic,strong) IBOutlet UIButton *cancelButton;
@property (nonatomic,strong) IBOutlet UITextField *searchTextField;
@property (nonatomic,strong) IBOutlet UILabel *searchLabel;

- (IBAction)closeTouched:(id)sender;
- (IBAction)searchTouched:(id)sender;

@end
