//
//  ClientUtils.m
//  Bento
//
//  Created by Do Nhat Phong on 9/27/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "ClientUtils.h"
#import <CommonCrypto/CommonDigest.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <TFHpple.h>


@implementation ClientUtils

+ (void) saveSetting:(NSString *)name value:(id)value {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:name];
    [defaults synchronize];
}

+ (id) loadSetting:(NSString *)name {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:name];
}

+ (void) deleteSetting:(NSString *)name {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:name];
    [defaults synchronize];
}

+ (NSString *) encryptMd5:(NSString *)input {
    const char *cStr = [input UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, strlen(cStr), result);
    return [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1],
            result[2], result[3],
            result[4], result[5],
            result[6], result[7],
            result[8], result[9],
            result[10], result[11],
            result[12], result[13],
            result[14], result[15]
            ];
}

+ (BOOL)validateEmail:(NSString *)emailStr {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}

+ (NSString *)urlEncode:(NSString *)input {
    if (input == nil || [input isEqualToString:@""]) {
        return @"";
    }
    
    input = [input stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    NSURL *url = [NSURL URLWithString:input];
    
    NSMutableString *output = [[NSMutableString alloc] init];
    [output appendString:[url scheme]];
    [output appendString:@"://"];
    [output appendString:[url host]];
    for (int i = 0; i < [[url pathComponents] count]; i++) {
        if ([[[url pathComponents] objectAtIndex:i] isEqualToString:@"/"]) {
            continue;
        }
        
        [output appendString:@"/"];
        [output appendString:((NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                                    NULL,
                                                                                                    (CFStringRef)[[url pathComponents] objectAtIndex:i],
                                                                                                    NULL,
                                                                                                    (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                    kCFStringEncodingUTF8 )))];
    }
    
    return output;
}

+ (NSString *)getGender:(NSInteger *)index{
    NSArray *genderArr = [NSArray arrayWithObjects:@"Female",@"Male", nil];
    return [genderArr objectAtIndex:index];
}

+ (NSArray *)getImages:(NSString *)htmlContent baseUrl:(NSString *)baseUrl {
    NSData *htmlData = [htmlContent dataUsingEncoding:NSUTF8StringEncoding];
    TFHpple *document = [[TFHpple alloc] initWithHTMLData:htmlData];
    
    NSArray *imageElements = [document searchWithXPathQuery:@"//img"];
    NSMutableArray *images = [[NSMutableArray alloc] init];
    for (TFHppleElement *element in imageElements) {
        [images addObject:[element objectForKey:@"src"]];
    }
    
    return images;
}

@end


@implementation UIImageView (NetworkImage)

- (void) setUrl:(NSString *)url {
    if (url == nil || [url isEqualToString:@""]) {
        self.image = [UIImage imageNamed:@"no_image"];
        return;
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    [self setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"no_image"] success:nil failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        self.image = [UIImage imageNamed:@"no_image"];
    }];
}

@end

