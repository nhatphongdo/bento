//
//  FeedViewController.h
//  Bento
//
//  Created by VO HONG HAI on 9/9/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddNoteViewController.h"
#import "CommentViewController.h"
#import "SharesViewController.h"
#import "DetailArticleViewController.h"
#import "SRRefreshView.h"


@interface FeedViewController : UIViewController<SRRefreshDelegate> {
    AddNoteViewController *addNoteViewController;
    CommentViewController *commentsViewController;
    SharesViewController *sharesViewController;
    DetailArticleViewController *detailViewController;
    DetailArticleViewController *leftDetailViewController;
    DetailArticleViewController *rightDetailViewController;
    SRRefreshView *pullToRefreshControl;
    NSMutableArray *articles;
    BOOL isMovingNext;
    BOOL isRunning;
    NSTimer *timer;
}

@property (nonatomic) int currentPage;
@property (nonatomic) NSInteger currentItemIndex;
@property (nonatomic) BOOL loadForUserOnly;
@property (nonatomic,strong) NSString *interestID;
@property (nonatomic,strong) NSString *interestName;

@property (nonatomic,strong) IBOutlet UITableView *tableView;
@property (nonatomic,strong) IBOutlet UIImageView *logoImage;
@property (nonatomic,strong) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *viewTypeButton;

- (IBAction)categoryTouched:(id)sender;
- (IBAction)settingTouched:(id)sender;
- (IBAction)viewTypeTouched:(id)sender;

- (void)loadLatestArticles;
- (void)loadArticleOfInterest;
- (void)timeforLoadData:(NSTimer *)theTimer;

- (void)moveNext;
- (void)movePrevious;

@end
