//
//  ProfileViewController.m
//  Bento
//
//  Created by VO HONG HAI on 9/18/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "ProfileViewController.h"
#import "Constants.h"
#import "ClientUtils.h"
#import "ProfileModel.h"
#import "ServiceClient.h"
#import "CustomProfileViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <FacebookSDK/FacebookSDK.h>


@implementation ProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.imgPicker = [[UIImagePickerController alloc] init];
    self.imgPicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    self.imgPicker.allowsImageEditing = YES;
    self.imgPicker.delegate = self;
    
    self.lblFullname.text = [ClientUtils loadSetting:FULLNAME_KEY];
    [self.ThumbImage setImageWithURL:[NSURL URLWithString:[ClientUtils loadSetting:AVATAR_KEY]] placeholderImage:[UIImage imageNamed:@"no_image"]];
    
    NSInteger *gender = [[ClientUtils loadSetting:GENDER_KEY] intValue];
    if(gender == 0 || gender == 1)
    {
        self.lblGender.text = [ClientUtils getGender:gender];
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSDate *date =[ClientUtils loadSetting:DATE_OF_BIRTH_KEY];
    NSString *newDateString = [dateFormatter stringFromDate:date];
    self.lblBirthday.text = newDateString;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)uploadPhoto:(id)sender
{
    [self presentModalViewController:self.imgPicker animated:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)img editingInfo:(NSDictionary *)editInfo {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *md5Password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    UIImage *imageIWantToUpload = img;
    NSData* data = UIImageJPEGRepresentation(imageIWantToUpload, 1.0);
    NSString *timestamp = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970] * 1000];
    NSString *email = [ClientUtils loadSetting:EMAIL_KEY];
    NSString *imageName = [NSString stringWithFormat:@"%@_%@",[email componentsSeparatedByString:@"@"][0], [timestamp componentsSeparatedByString:@"."][0]];
    NSURL *assetUrl = [editInfo objectForKey:@"UIImagePickerControllerReferenceURL"];
    NSString *extension = [assetUrl pathExtension];
    NSString *filename = [NSString stringWithFormat:@"%@.%@",imageName,extension.lowercaseString];
    if ([extension.lowercaseString isEqualToString:@"jpg"] || [extension.lowercaseString isEqualToString:@"jpeg"]) {
        extension = @"image/jpeg";
    }
    else if([extension.lowercaseString isEqualToString:@"png"])
    {
        extension = @"image/png";
    }

    [ServiceClient saveProfilePicture:self.view username:username md5Password:md5Password data:data name:imageName filename:filename filetype:extension successBlock:^(id responseObject) {
        self.ThumbImage.image = img;
    } failureBlock:^(NSError *error) {
    }];
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)logOutTouched:(id)sender {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    [ServiceClient signOut:self.view username:username md5Password:password successBlock:^(id responseObject) {
        // Load Profile success
        [ClientUtils deleteSetting:USERNAME_KEY];
        [ClientUtils deleteSetting:MD5_PASSWORD_KEY];
        [ClientUtils deleteSetting:FULLNAME_KEY];
        [ClientUtils deleteSetting:AVATAR_KEY];
        [ClientUtils deleteSetting:GENDER_KEY];
        [ClientUtils deleteSetting:ACCOUNT_TYPE_KEY];
        [ClientUtils deleteSetting:DATE_OF_BIRTH_KEY];
        [ClientUtils deleteSetting:CAPTION_KEY];
        [ClientUtils deleteSetting:EMAIL_KEY];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    } failureBlock:^(NSError *error) {
    }];
    
    // logout facebook
    FBSession* session = [FBSession activeSession];
    [session closeAndClearTokenInformation];
    [session close];
    [FBSession setActiveSession:nil];
    
    NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray* facebookCookies = [cookies cookiesForURL:[NSURL URLWithString:@"https://facebook.com/"]];
    
    for (NSHTTPCookie* cookie in facebookCookies) {
        [cookies deleteCookie:cookie];
    }
    
}

- (IBAction)rateThisAppTouched:(id)sender {
    //    if(store.email == nil || [store.email length] == 0 ) {
    //        [MGUtilities showAlertTitle:LOCALIZED(@"EMAIL_ERROR")
    //                            message:LOCALIZED(@"EMAIL_ERROR_MSG")];
    //        return;
    //    }
    
    if ([MFMailComposeViewController canSendMail]) {
        
        // set the sendTo address
        NSMutableArray *recipients = [[NSMutableArray alloc] initWithCapacity:1];
        [recipients addObject:@"khuong.le@ksqagency.com"];
        
        MFMailComposeViewController* mailController = [[MFMailComposeViewController alloc] init];
        mailController.mailComposeDelegate = self;
        
        [mailController setToRecipients:recipients];
        
        [self presentViewController:mailController animated:YES completion:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        }];
    }
    else {

    }
}

- (IBAction)suggestThisAppTouched:(id)sender {
    if ([MFMailComposeViewController canSendMail]) {
        
        // set the sendTo address
        NSMutableArray *recipients = [[NSMutableArray alloc] initWithCapacity:1];
        [recipients addObject:@"khuong.le@ksqagency.com"];
        
        MFMailComposeViewController* mailController = [[MFMailComposeViewController alloc] init];
        mailController.mailComposeDelegate = self;
        
        [mailController setToRecipients:recipients];
        
        [self presentViewController:mailController animated:YES completion:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        }];
    }
    else {
        
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error {
    
    [self becomeFirstResponder];
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
