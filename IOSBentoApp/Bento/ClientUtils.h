//
//  ClientUtils.h
//  Bento
//
//  Created by Do Nhat Phong on 9/27/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClientUtils : NSObject

+ (void) saveSetting:(NSString *)name value:(id)value;
+ (id) loadSetting:(NSString *)name;
+ (void) deleteSetting:(NSString *)name;

+ (NSString *) encryptMd5:(NSString *)input;

+ (BOOL)validateEmail:(NSString *)emailStr;
+ (NSString *)urlEncode:(NSString *)input;
+ (NSString *)getGender:(NSInteger *)index;

+ (NSArray *)getImages:(NSString *)htmlContent baseUrl:(NSString *)baseUrl;

@end


@interface UIImageView (NetworkImage)

- (void) setUrl:(NSString *)url;

@end
