//
//  CommentViewController.m
//  Bento
//
//  Created by VO HONG HAI on 9/19/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "CommentViewController.h"
#import "Constants.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "CommentModel.h"
#import "CommentItem.h"


@implementation CommentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.avatarImageView.layer.cornerRadius = IMAGE_ROUND_CORNER_RADIUS;
    self.avatarImageView.clipsToBounds = YES;

    self.commentTextView.placeholder = @"Write your comment";
    
    [self.avatarImageView setUrl:[ClientUtils loadSetting:AVATAR_KEY]];

    _isVisibleKeyboard = NO;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self.view endEditing:YES];
}

- (void)hideKeyboard:(double)time height:(float)height {
    if (_isVisibleKeyboard == NO) {
        return;
    }

    NSTimeInterval animationDuration = time;
    CGRect frame = self.view.frame;
    frame.size.height += height;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];
    _isVisibleKeyboard = NO;
}

- (void)showKeyboard:(double)time height:(float)height {
    if (_isVisibleKeyboard == YES) {
        return;
    }
    
    NSTimeInterval animationDuration = time;
    CGRect frame = self.view.frame;
    frame.size.height -= height;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];
    _isVisibleKeyboard = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeButtonTouched:(id)sender {
    [self.view setHidden:YES];
    [self.view endEditing:YES];

    if (self.sender) {
        [self.sender setSelected:NO];
    }
}

- (IBAction)sendCommentTouched:(id)sender {
    if ([self.commentTextView.text isEqualToString:@""]) {
        return;
    }
    
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    [ServiceClient postComment:self.view username:username md5Password:password postID:self.postID message:self.commentTextView.text successBlock:^(id responseObject) {
        // Post comment succeeded
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Your comment is saved successfully"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        self.commentTextView.text = @"";
        [self loadComments];
    } failureBlock:^(NSError *error) {
    }];
}

- (void)loadComments {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    [ServiceClient getCommentsOfArticle:self.view username:username md5Password:password postID:self.postID page:1 itemsPerPage:10 successBlock:^(id responseObject) {
        // Load articles succeeded
        comments = responseObject;

        [self reloadCommentsView];
    } failureBlock:^(NSError *error) {
    }];
}

- (void)reloadCommentsView {
    [[self.commentsScrollView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    float y = 0;
    for (uint i = 0; i < [comments count]; i++) {
        CommentModel *item = [comments objectAtIndex:i];
        CommentItem *commentView = [[CommentItem alloc] initWithNibName:@"CommentItem" bundle:nil];
        commentView.containerViewController = self;
        [self addChildViewController:commentView];
        [self.commentsScrollView addSubview:commentView.view];
        [commentView didMoveToParentViewController:self];
        [commentView setComment:item];
        commentView.view.frame = CGRectMake(0, y, self.commentsScrollView.frame.size.width, commentView.view.frame.size.height);
        y += commentView.view.frame.size.height;
    }
    
    self.commentsScrollView.contentSize = CGSizeMake(self.commentsScrollView.frame.size.width, y);
}

- (void)refreshCommentsView {
    NSArray *subviews = [self.commentsScrollView subviews];
    float y = 0;
    for (uint i = 0; i < [subviews count]; i++) {
        UIView *view = [subviews objectAtIndex:i];
        view.frame = CGRectMake(view.frame.origin.x, y, view.frame.size.width, view.frame.size.height);
        y += view.frame.size.height;
    }
    
    self.commentsScrollView.contentSize = CGSizeMake(self.commentsScrollView.frame.size.width, y);
}

@end
