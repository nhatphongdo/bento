//
//  MainTabBarController.m
//  Bento
//
//  Created by Do Nhat Phong on 9/30/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "MainTabBarController.h"

@interface MainTabBarController ()

@end

@implementation MainTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    ((UITabBarItem *)[[self.tabBar items] objectAtIndex:0]).image = [[UIImage imageNamed:@"nav_home"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    ((UITabBarItem *)[[self.tabBar items] objectAtIndex:1]).image = [[UIImage imageNamed:@"nav_favorite"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    ((UITabBarItem *)[[self.tabBar items] objectAtIndex:2]).image = [[UIImage imageNamed:@"nav_user"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    ((UITabBarItem *)[[self.tabBar items] objectAtIndex:3]).image = [[UIImage imageNamed:@"nav_logo"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

@end
