//
//  ProfileModel.h
//  Bento
//
//  Created by VO HONG HAI on 9/29/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProfileModel : NSObject

@property (nonatomic, strong) NSString *Username;
@property (nonatomic) int Gender;
@property (nonatomic, strong) NSString *AccountType;
@property (nonatomic, strong) NSString *Avatar;
@property (nonatomic, strong) NSDate *BirthDay;
@property (nonatomic, strong) NSString *Caption;
@property (nonatomic, strong) NSString *Email;
@property (nonatomic, strong) NSString *Fullname;

@end
