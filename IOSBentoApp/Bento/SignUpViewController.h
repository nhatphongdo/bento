//
//  SignUpViewController.h
//  Bento
//
//  Created by VO HONG HAI on 10/14/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"

@interface SignUpViewController : UIViewController<NIDropDownDelegate>{
    NIDropDown *dropDown;
}

@property (nonatomic,strong) IBOutlet UIButton *GenderButton;


-(IBAction)backTouch:(id)sender;
-(IBAction)selectGender:(id)sender;
@end
