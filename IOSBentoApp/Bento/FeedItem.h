//
//  FeedItem.h
//  Bento
//
//  Created by VO HONG HAI on 9/8/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FeedItem : UITableViewCell

@property (nonatomic, strong) NSString *articleID;

@property (nonatomic, strong) IBOutlet UIImageView *image;
@property (nonatomic, strong) IBOutlet UIButton *btSeemore;
@property (nonatomic, strong) IBOutlet UILabel *lblTitle;
@property (nonatomic, strong) IBOutlet UILabel *lblAuthor;
@property (nonatomic, strong) IBOutlet UITextView *txtContent;

@property (strong, nonatomic) IBOutlet UIImageView *rearImageView;
@property (strong, nonatomic) IBOutlet UILabel *rearTitleLabel;
@property (strong, nonatomic) IBOutlet UIWebView *rearBodyView;
@property (strong, nonatomic) IBOutlet UILabel *rearAuthorLabel;

@property (nonatomic, strong) IBOutlet UIButton *btComment;
@property (nonatomic, strong) IBOutlet UIButton *btNote;
@property (nonatomic, strong) IBOutlet UIButton *btShare;
@property (nonatomic, strong) IBOutlet UIButton *btLike;

@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIView *frontView;
@property (strong, nonatomic) IBOutlet UIView *rearView;

- (IBAction)seemoreBtnClick:(id)sender;
- (IBAction)hideDetailButtonTouched:(id)sender;

- (void)setTitle:(NSString *)text;
- (void)setBody:(NSString *)body;

@end
