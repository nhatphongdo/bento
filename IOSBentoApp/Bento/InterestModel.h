//
//  InterestModel.h
//  Bento
//
//  Created by Do Nhat Phong on 9/28/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InterestModel : NSObject

@property (nonatomic) BOOL isCollapsed;

@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) NSString *Name;
@property (strong, nonatomic) NSString *Thumbnail;
@property (strong, nonatomic) NSString *ParentID;
@property (nonatomic) NSInteger Total;
@property (nonatomic) NSInteger Level;

@end
