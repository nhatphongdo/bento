//
//  SearchViewController.m
//  Bento
//
//  Created by VO HONG HAI on 10/31/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "SearchViewController.h"
#import "FeedItem.h"
#import "Constants.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import "ArticleModel.h"
#import <PKRevealController/PKRevealController.h>


@interface SearchViewController ()

@end

@implementation SearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    pullToRefreshControl = [[SRRefreshView alloc] init];
    pullToRefreshControl.delegate = self;
    pullToRefreshControl.upInset = 10;
    pullToRefreshControl.slimeMissWhenGoingBack = YES;
    pullToRefreshControl.slime.bodyColor = [UIColor blackColor];
    pullToRefreshControl.slime.skinColor = [UIColor whiteColor];
    pullToRefreshControl.slime.lineWith = 1;
    pullToRefreshControl.slime.shadowBlur = 4;
    pullToRefreshControl.slime.shadowColor = [UIColor blackColor];
    [self.tableView addSubview:pullToRefreshControl];
    
    // Add detail view
    leftDetailViewController = [[DetailArticleViewController alloc] initWithNibName:@"DetailArticleViewController" bundle:nil];
    leftDetailViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self addChildViewController:leftDetailViewController];
    [self.view addSubview:leftDetailViewController.view];
    [leftDetailViewController didMoveToParentViewController:self];
    [leftDetailViewController.view setHidden:YES];
    
    [leftDetailViewController.hideButton addTarget:self action:@selector(hideDetailTouched:) forControlEvents:UIControlEventTouchUpInside];
    [leftDetailViewController.commentButton addTarget:self action:@selector(showCommentTouched:) forControlEvents:UIControlEventTouchUpInside];
    [leftDetailViewController.noteButton addTarget:self action:@selector(showNoteTouched:) forControlEvents:UIControlEventTouchUpInside];
    [leftDetailViewController.shareButton addTarget:self action:@selector(showSharesTouched:) forControlEvents:UIControlEventTouchUpInside];
    [leftDetailViewController.likeButton addTarget:self action:@selector(likeArticleTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    rightDetailViewController = [[DetailArticleViewController alloc] initWithNibName:@"DetailArticleViewController" bundle:nil];
    rightDetailViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self addChildViewController:rightDetailViewController];
    [rightDetailViewController didMoveToParentViewController:self];
    
    [rightDetailViewController.hideButton addTarget:self action:@selector(hideDetailTouched:) forControlEvents:UIControlEventTouchUpInside];
    [rightDetailViewController.commentButton addTarget:self action:@selector(showCommentTouched:) forControlEvents:UIControlEventTouchUpInside];
    [rightDetailViewController.noteButton addTarget:self action:@selector(showNoteTouched:) forControlEvents:UIControlEventTouchUpInside];
    [rightDetailViewController.shareButton addTarget:self action:@selector(showSharesTouched:) forControlEvents:UIControlEventTouchUpInside];
    [rightDetailViewController.likeButton addTarget:self action:@selector(likeArticleTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    detailViewController = leftDetailViewController;
    
    isMovingNext = NO;
    
    // Load Article
    articles = [[NSMutableArray alloc] init];
    
    // init Swipe Gesture
    UISwipeGestureRecognizer *swipeleft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer *swiperight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];
    
    //init content position textfield search
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 0)];
    self.searchTextField.leftView = paddingView;
    self.searchTextField.leftViewMode = UITextFieldViewModeAlways;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
//    self.navigationController.navigationBar.hidden = NO;
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)keyboardWillHide:(NSNotification *)aNotification {
    // Get the size of the keyboard.
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    if (addNoteViewController) {
        [addNoteViewController hideKeyboard:[[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue] height:(kbSize.height - [self.tabBarController tabBar].bounds.size.height)];
    }
    
    if (commentsViewController) {
        [commentsViewController hideKeyboard:[[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue] height:(kbSize.height - [self.tabBarController tabBar].bounds.size.height)];
    }
}

- (void)keyboardWillShow:(NSNotification *)aNotification {
    // Get the size of the keyboard.
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    if (addNoteViewController && [addNoteViewController.view isHidden] == NO) {
        [addNoteViewController showKeyboard:[[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue] height:(kbSize.height - [self.tabBarController tabBar].bounds.size.height)];
    }
    
    if (commentsViewController && [commentsViewController.view isHidden] == NO) {
        [commentsViewController showKeyboard:[[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue] height:(kbSize.height - [self.tabBarController tabBar].bounds.size.height)];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - slimeRefresh delegate

- (void)slimeRefreshStartRefresh:(SRRefreshView *)refreshView {
    [articles removeAllObjects];
    isMovingNext = NO;
    [self loadArticleOfInterest];
    [pullToRefreshControl performSelector:@selector(endRefresh)
                               withObject:nil
                               afterDelay:0
                                  inModes:[NSArray arrayWithObject:NSRunLoopCommonModes]];
    
}

#pragma mark - scrollView delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [pullToRefreshControl scrollViewDidScroll];
    
    if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height && isRunning == NO) {
        // Next page
        isMovingNext = NO;
        isRunning = YES;
        ++self.currentPage;
        [self loadArticleOfInterest];
        
        timer = [NSTimer timerWithTimeInterval:2 target:self selector:@selector(timeforLoadData) userInfo:nil repeats:NO];
    }
}

- (void)timeforLoadData {
    isRunning = NO;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [pullToRefreshControl scrollViewDidEndDraging];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (articles && [articles count] > 0) {
        tableView.backgroundView = nil;
        return 1;
    }
    else {
        // Display a message when the table is empty
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        messageLabel.text = @"No data is currently available. Please pull down to refresh.";
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
        [messageLabel sizeToFit];
        
        tableView.backgroundView = messageLabel;
        
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [articles count];
}

- (void)loadArticleOfInterest {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    if(![self.searchTextField.text isEqualToString:@""] && self.searchTextField.text != nil)
    {
        [ServiceClient searchUserFavoriteArticles:self.view username:username md5Password:password searchTerm:self.searchTextField.text Page:self.currentPage ItemsPerPage:10 successBlock:^(id responseObject) {
            // Load articles succeeded
            if (self.currentPage == 1) {
                [articles removeAllObjects];
            }
            if (self.currentPage > 1 && [responseObject count] == 0) {
                --self.currentPage;
            }
            NSLog(@"object Data Count: %d", [responseObject count]);
            
            if([responseObject count] > 0)
            {
                [articles addObjectsFromArray:responseObject];
            }
            
            [self.tableView reloadData];
            [pullToRefreshControl performSelector:@selector(endRefresh)
                                       withObject:nil
                                       afterDelay:0
                                          inModes:[NSArray arrayWithObject:NSRunLoopCommonModes]];
            
            if (isMovingNext && [responseObject count] > 0) {
                ++self.currentItemIndex;
                [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentItemIndex inSection:0]];
            }

        } failureBlock:^(NSError *error) {
            
        }];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"BentoCell";
    FeedItem *cell = (FeedItem *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FeedItem" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    ArticleModel *item = [articles objectAtIndex:indexPath.row];
    
    cell.articleID = item.ID;
    [cell setTitle:item.Title];
    cell.txtContent.text = item.ShortDescription;
    cell.lblAuthor.text = item.Category;
    [cell.image setUrl:item.Thumbnail];
    [cell.btLike setTitle:[NSString stringWithFormat:@"%d", item.Likes] forState:UIControlStateNormal];
    if (item.Liked == YES) {
        [cell.btLike setImage:[UIImage imageNamed:@"like_hover"] forState:UIControlStateNormal];
    }
    else {
        [cell.btLike setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
    }
    
    // Set tag for the cell UIButton to the same as the indexPath of the cell
    cell.btNote.tag = indexPath.row;
    cell.btComment.tag = indexPath.row;
    cell.btLike.tag = indexPath.row;
    cell.btShare.tag = indexPath.row;
    
    [cell.btNote addTarget:self action:@selector(showNoteTouched:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btComment addTarget:self action:@selector(showCommentTouched:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btLike addTarget:self action:@selector(likeArticleTouched:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btShare addTarget:self action:@selector(showSharesTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 114.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Show detail view
    self.currentItemIndex = indexPath.row;
    [self loadContentForDetailArticle];
}

- (void)showNoteTouched:(id)sender {
    // Add note view
    if (addNoteViewController == nil) {
        addNoteViewController = [[AddNoteViewController alloc] initWithNibName:@"AddNoteViewController" bundle:nil];
        addNoteViewController.view.frame = CGRectMake(0, detailViewController.titleBarView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - detailViewController.titleBarView.frame.size.height);
        [self addChildViewController:addNoteViewController];
        [self.view addSubview:addNoteViewController.view];
        [addNoteViewController didMoveToParentViewController:self];
        [addNoteViewController.view setHidden:YES];
        [addNoteViewController.hideButton addTarget:self action:@selector(hideNoteTouched:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    // Create snapshot of this view
    UIGraphicsBeginImageContext(CGSizeMake(detailViewController.view.frame.size.width, detailViewController.view.frame.size.height - detailViewController.titleBarView.frame.size.height));
    [detailViewController.view drawViewHierarchyInRect:CGRectMake(0, -detailViewController.titleBarView.frame.size.height, detailViewController.view.frame.size.width, detailViewController.view.frame.size.height + detailViewController.titleBarView.frame.size.height) afterScreenUpdates:YES];
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [addNoteViewController setBackground:snapshotImage];
    [addNoteViewController.view setHidden:NO];
    
    UIButton *button = (UIButton *)sender;
    ArticleModel *item = [articles objectAtIndex:button.tag];
    
    addNoteViewController.articleID = item.ID;
    addNoteViewController.sender = button;
    [addNoteViewController loadSavedNotes];
    
    [button setSelected:YES];
}

- (void)hideNoteTouched:(id)sender {
    [addNoteViewController.view setHidden:YES];
    
    if (addNoteViewController.sender != nil) {
        [addNoteViewController.sender setSelected:NO];
    }
    
    [self.view endEditing:YES];
}

- (void)showCommentTouched:(id)sender {
    UIButton *button = (UIButton *)sender;
    [button setSelected:YES];
    
    ArticleModel *item = [articles objectAtIndex:button.tag];
    
    if (commentsViewController == nil) {
        // Comment view
        commentsViewController = [[CommentViewController alloc] initWithNibName:@"CommentViewController" bundle:nil];
        commentsViewController.view.frame = self.view.frame;
        [self addChildViewController:commentsViewController];
        [self.view addSubview:commentsViewController.view];
        [commentsViewController didMoveToParentViewController:self];
    }
    
    commentsViewController.sender = button;
    commentsViewController.postID = item.ID;
    [commentsViewController.view setHidden:NO];
    [commentsViewController loadComments];
}

- (void)showSharesTouched:(id)sender {
    [self hideNoteTouched:nil];
    
    UIButton *button = (UIButton *)sender;
    [button setSelected:YES];
    
    ArticleModel *item = [articles objectAtIndex:button.tag];
    
    if (sharesViewController == nil) {
        // Share view
        sharesViewController = [[SharesViewController alloc] initWithNibName:@"SharesViewController" bundle:nil];
        sharesViewController.view.frame = CGRectMake(0, detailViewController.titleBarView.frame.size.height, sharesViewController.view.frame.size.width, sharesViewController.view.frame.size.height);
        [self addChildViewController:sharesViewController];
        [self.view addSubview:sharesViewController.view];
        [sharesViewController didMoveToParentViewController:self];
    }
    
    sharesViewController.sender = button;
    sharesViewController.postID = item.ID;
    [sharesViewController.view setHidden:NO];
}

- (void)likeArticleTouched:(id)sender {
    UIButton *button = (UIButton *)sender;
    ArticleModel *item = [articles objectAtIndex:button.tag];
    
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    if (item.Liked == YES) {
        // Unlike
        item.Liked = NO;
        [ServiceClient unlikePost:self.view username:username md5Password:password postID:item.ID successBlock:^(id responseObject) {
            // UnLike article succeeded
            [button setTitle:[NSString stringWithFormat:@"%@", responseObject] forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@"iconWhiteLike"] forState:UIControlStateNormal];
        } failureBlock:^(NSError *error) {
        }];
    }
    else {
        // Like
        item.Liked = YES;
        [ServiceClient likePost:self.view username:username md5Password:password postID:item.ID successBlock:^(id responseObject) {
            // Like article succeeded
            [button setTitle:[NSString stringWithFormat:@"%@", responseObject] forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@"like_hover"] forState:UIControlStateNormal];
        } failureBlock:^(NSError *error) {
        }];
    }
}

- (void)hideDetailTouched:(id)sender {
    // Show navigation bar
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [detailViewController.view setHidden:YES];
    if (addNoteViewController != nil) {
        if (addNoteViewController.sender) {
            [addNoteViewController.sender setSelected:NO];
        }
        [addNoteViewController.view setHidden:YES];
    }
    [sharesViewController.view setHidden:YES];
    
    [self.view endEditing:YES];
}

- (void)moveNext {
    if (self.currentItemIndex == [articles count] - 1) {
        // Go to end, load more
        isMovingNext = YES;
        ++self.currentPage;
        [self loadArticleOfInterest];
    }
    else {
        ++self.currentItemIndex;
        [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentItemIndex inSection:0]];
    }
}

- (void)movePrevious {
    if (self.currentItemIndex > 0) {
        --self.currentItemIndex;
        [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentItemIndex inSection:0]];
    }
}

- (void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer {
    if (detailViewController.view.hidden == NO && gestureRecognizer.state == UIGestureRecognizerStateRecognized) {
        self.view.userInteractionEnabled = NO;
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.75];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(swipeLeftAnimationStop:finished:context:)];
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
        
        [leftDetailViewController.view removeFromSuperview];
        [self.view addSubview:rightDetailViewController.view];
        [UIView commitAnimations];
    }
}

- (void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer {
    if (detailViewController.view.hidden == NO && gestureRecognizer.state == UIGestureRecognizerStateRecognized) {
        self.view.userInteractionEnabled = NO;
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.75];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(swipeRightAnimationStop:finished:context:)];
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
        
        [leftDetailViewController.view removeFromSuperview];
        [self.view addSubview:rightDetailViewController.view];
        [UIView commitAnimations];
    }
}

- (void)swipeLeftAnimationStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    self.view.userInteractionEnabled = YES;
    
    detailViewController = rightDetailViewController;
    rightDetailViewController = leftDetailViewController;
    leftDetailViewController = detailViewController;
    [self clearContent:rightDetailViewController];
    
    [self moveNext];
    [self loadContentForDetailArticle];
}

- (void)swipeRightAnimationStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    self.view.userInteractionEnabled = YES;
    
    detailViewController = rightDetailViewController;
    rightDetailViewController = leftDetailViewController;
    leftDetailViewController = detailViewController;
    [self clearContent:rightDetailViewController];
    
    [self movePrevious];
    [self loadContentForDetailArticle];
}

- (void)loadContentForDetailArticle {
    ArticleModel *item = [articles objectAtIndex:self.currentItemIndex];
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    [ServiceClient getDetailArticle:self.view username:username md5Password:password articleID:item.ID successBlock:^(id responseObject) {
        // Load articles succeeded
        
        // Hide navigation bar
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        
        ArticleModel *article = (ArticleModel *)responseObject;
        
        detailViewController.authorLabel.text = article.Author;
        [detailViewController setTitle:article.Title];
        [detailViewController setBody:article.Body];
        [detailViewController.likeButton setTitle:[NSString stringWithFormat:@"%d", item.Likes] forState:UIControlStateNormal];
        if (item.Liked == YES) {
            [detailViewController.likeButton setImage:[UIImage imageNamed:@"like_hover"] forState:UIControlStateNormal];
        }
        else {
            [detailViewController.likeButton setImage:[UIImage imageNamed:@"iconWhiteLike"] forState:UIControlStateNormal];
        }
        
        detailViewController.commentButton.tag = self.currentItemIndex;
        detailViewController.noteButton.tag = self.currentItemIndex;
        detailViewController.shareButton.tag = self.currentItemIndex;
        detailViewController.likeButton.tag = self.currentItemIndex;
        
        [detailViewController.view setHidden:NO];
    } failureBlock:^(NSError *error) {
    }];
}

- (void)clearContent:(DetailArticleViewController *)viewController {
    [viewController setTitle:@""];
    [viewController setBody:@""];
    [viewController.likeButton setTitle:@"0" forState:UIControlStateNormal];
}

- (IBAction)searchTouched:(id)sender {
    self.searchLabel.text = self.searchTextField.text;
    [self loadArticleOfInterest];
}

- (IBAction)closeTouched:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
