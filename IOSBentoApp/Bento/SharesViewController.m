//
//  SharesViewController.m
//  Bento
//
//  Created by Do Nhat Phong on 10/1/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "SharesViewController.h"
#import <Social/Social.h>
#import <Twitter/Twitter.h>
#import <MessageUI/MessageUI.h>
#import <GooglePlus/GooglePlus.h>
#import <Pinterest/Pinterest.h>
#import <FacebookSDK/FacebookSDK.h>


@implementation SharesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)facebookTouched:(id)sender {
    // Check if the Facebook app is installed and we can present the share dialog
    FBLinkShareParams *params = [[FBLinkShareParams alloc] init];
    params.link = [NSURL URLWithString:@"https://developers.facebook.com/docs/ios/share/"];
    params.name = @"Bento";
    params.caption = @"Business experts network training operation";
    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithParams:params]) {
        // Present the share dialog
        [FBDialogs presentShareDialogWithLink:params.link
                                      handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                          if(error) {
                                              // An error occurred, we need to handle the error
                                              // See: https://developers.facebook.com/docs/ios/errors
                                              NSLog(@"Error publishing story: %@", error.description);
                                          } else {
                                              // Success
                                              NSLog(@"result %@", results);
                                          }
                                      }];
    } else {
        // Present the feed dialog
        // Put together the dialog parameters
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       @"Bento", @"name",
                                       @"Business experts network training operation", @"caption",
                                       @"To become essential mobile app for entrepreneur, dominating in the Vietnamese market, by providing training contents, and operation tools to support entrepreneur effectively organize and manage their business.", @"description",
                                       @"http://vietdev.vn", @"link",
                                       @"http://vietdev.vn/Content/images/logo_vietdev.png", @"picture",
                                       nil];
        
        // Show the feed dialog
        [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                               parameters:params
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // An error occurred, we need to handle the error
                                                          // See: https://developers.facebook.com/docs/ios/errors
                                                          NSLog(@"Error publishing story: %@", error.description);
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // User cancelled.
                                                              NSLog(@"User cancelled.");
                                                          } else {
                                                              // Handle the publish feed callback
                                                              NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                              
                                                              if (![urlParams valueForKey:@"post_id"]) {
                                                                  // User cancelled.
                                                                  NSLog(@"User cancelled.");
                                                                  
                                                              } else {
                                                                  // User clicked the Share button
                                                                  NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                                  NSLog(@"result %@", result);
                                                              }
                                                          }
                                                      }
                                                  }];
    }
}

- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}

- (IBAction)googlePlusTouched:(id)sender {
    id<GPPNativeShareBuilder> shareBuilder = [[GPPShare sharedInstance] shareDialog];
    
    // This line will fill out the title, description, and thumbnail from
    // the URL that you are sharing and includes a link to that URL.
    [shareBuilder setURLToShare:[NSURL URLWithString:@"http://vietdev.vn"]];
    
    // Optionally attach a deep link ID for your mobile app
    [shareBuilder setContentDeepLinkID:@"/restaurant/sf/1234567/"];
    
    [shareBuilder open];
}

- (IBAction)pinterestTouched:(id)sender {
    Pinterest *_pinterest = [[Pinterest alloc] initWithClientId:@"1440915"];
    [_pinterest createPinWithImageURL:[NSURL URLWithString:@"http://vietdev.vn/Content/images/logo_vietdev.png"]
                            sourceURL:[NSURL URLWithString:@"http://vietdev.vn"]
                          description:@"Pinning from Pin It Demo"];
}

- (IBAction)twitterTouched:(id)sender {
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                NSLog(@"Cancelled");
                
            } else
                
            {
                NSLog(@"Done");
            }
            
            [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        [controller setInitialText:@"Share this article to your friends !"];
        [controller addURL:[NSURL URLWithString:@"http://vietdev.vn"]]; //link to article on website for this url
        [controller addImage:[UIImage imageNamed:@"fb.png"]]; 
        [self presentViewController:controller animated:YES completion:Nil];
    }
    else
    {
        NSLog(@"UnAvailable");
    }
}

- (IBAction)tumblrTouched:(id)sender {
}

- (IBAction)smsTouched:(id)sender {
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    
    if([MFMessageComposeViewController canSendText]) {
        
//        NSString* formattedBody = [NSString stringWithFormat:@"%@", LOCALIZED(@"SMS_BODY")];
        
//        controller.body = @"lorem ispu isu";
        
//        NSString* trim = [MGUtilities removeDelimetersInPhoneNo:store.sms_no];
        
//        if(store.sms_no != nil || [store.sms_no length] == 0)
//            trim = [MGUtilities removeDelimetersInPhoneNo:store.sms_no];
        
//        controller.recipients = @[@"username@gmail.com"];
        controller.messageComposeDelegate = self;
//        controller.view.backgroundColor = BG_VIEW_COLOR;
        
        [self presentViewController:controller animated:YES completion:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        }];
    }
}

- (IBAction)emailTouched:(id)sender {
    // Email Subject
    NSString *emailTitle = @"Report for ";
    // Email Content
    NSString *messageBody = @"";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@"khuong.le@ksqagency.com"];
    
    if ( [MFMailComposeViewController canSendMail] )
    {
        [APP.globalMailComposer setToRecipients:
         [NSArray arrayWithObjects: @"khuong.le@ksqagency.com", nil] ];
        [APP.globalMailComposer setSubject:emailTitle];
        [APP.globalMailComposer setMessageBody:messageBody isHTML:NO];
        APP.globalMailComposer.mailComposeDelegate = self;
        [self presentViewController:APP.globalMailComposer
                           animated:YES completion:nil];
    }
    else
    {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Error"
                                                          message:@"Unable to mail. No email on this device?"
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        
        [message show];
        [APP cycleTheGlobalMailComposer];
    }
}


- (IBAction)copyTouched:(id)sender {
}

- (IBAction)downloadTouched:(id)sender {
}

- (IBAction)closeTouched:(id)sender {
    [self.view setHidden:YES];

    if (self.sender) {
        [self.sender setSelected:NO];
    }
}

//- (void)mailComposeController:(MFMailComposeViewController*)controller
//          didFinishWithResult:(MFMailComposeResult)result
//                        error:(NSError*)error {
//    
//    [self becomeFirstResponder];
//    [controller dismissViewControllerAnimated:YES completion:nil];
//}

-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result
                       error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:^
     { [APP cycleTheGlobalMailComposer]; }
     ];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
