//
//  SponsorItem.h
//  Bento
//
//  Created by VO HONG HAI on 10/13/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SponsorItem : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel *titleLabel;
@property (nonatomic,strong) IBOutlet UILabel *descriptionLabel;
@property (nonatomic,strong) IBOutlet UILabel *webaddressLabel;
@property (nonatomic,strong) IBOutlet UILabel *hiddenIDSponsor; //not have control in view
@property (nonatomic,strong) IBOutlet UIImageView *thumbnailImage;

@end
