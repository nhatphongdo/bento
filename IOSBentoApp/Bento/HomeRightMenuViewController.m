//
//  HomeRightMenuViewController.m
//  Bento
//
//  Created by Do Nhat Phong on 10/11/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "HomeRightMenuViewController.h"
#import <PKRevealController/PKRevealController.h>
#import "ClientUtils.h"
#import "ServiceClient.h"
#import "Constants.h"
#import "InterestModel.h"
#import "CategoryItem.h"
#import "HomeLeftMenuViewController.h"


@implementation HomeRightMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (self.selectedInterests == nil) {
        self.selectedInterests = [[NSMutableArray alloc] init];
    }

    hasChanges = NO;
    [self loadInterests];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) loadInterests {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    [ServiceClient getInterests:self.view username:username md5Password:password successBlock:^(id responseObject) {
        // Load interest succeeded
        interests = responseObject;
        
        [ServiceClient getUserInterests:self.view username:username md5Password:password successBlock:^(id responseObject) {
            // Load interest succeeded
            self.selectedInterests = responseObject;
            
            [self.interestsTableView reloadData];
        } failureBlock:^(NSError *error) {
            [self.interestsTableView reloadData];
        }];
    } failureBlock:^(NSError *error) {
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [interests count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"InterestCell";
    CategoryItem *cell = (CategoryItem *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CategoryItem" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    InterestModel *item = [interests objectAtIndex:indexPath.row];
    
    cell.titleLabel.text = item.Name;
    cell.tag = indexPath.row;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self
                                          action:@selector(handleViewTap:)];
    tapGesture.delegate = self;
    [cell addGestureRecognizer:tapGesture];
    tapGesture = nil;
    
    InterestModel *found = [self findItemById:item.ID];
    if (found != nil) {
        [cell setInterestSelected:YES];
    }
    return cell;
}

- (void)handleViewTap:(UITapGestureRecognizer *)recognizer {
    // Insert your own code to handle singletap
    hasChanges = YES;
    // Invert selection
    InterestModel *item = [interests objectAtIndex:recognizer.view.tag];
    InterestModel *found = [self findItemById:item.ID];
    if (found != nil) {
        // Selected
        [self.selectedInterests removeObject:found];
    }
    else {
        // Unselected
        found = [[InterestModel alloc] init];
        found.ID = item.ID;
        found.Name = item.Name;
        found.Thumbnail = item.Thumbnail;
        [self.selectedInterests addObject:found];
    }
    [self.interestsTableView reloadData];
}

- (id) findItemById:(NSString *)ID {
    for (uint i = 0; i < [self.selectedInterests count]; i++) {
        if ([((InterestModel *)[self.selectedInterests objectAtIndex:i]).ID isEqualToString:ID]) {
            return [self.selectedInterests objectAtIndex:i];
        }
    }
    return nil;
}

- (IBAction)closeTouched:(id)sender {
    if (hasChanges) {
        // Save selected interests
        NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
        NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
        [ServiceClient setUserInterests:self.view username:username md5Password:password interests:self.selectedInterests successBlock:^(id responseObject) {
            // Save interest list succeeded
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                           message:@"Save interests successfully"
                                                          delegate:self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"OK",nil];
            [alert show];
            alert = nil;
            
            [self.revealController showViewController:self.revealController.frontViewController];
            [((HomeLeftMenuViewController *)self.revealController.leftViewController) loadInterests];
        } failureBlock:^(NSError *error) {
        }];
    }
    else {
        [self.revealController showViewController:self.revealController.frontViewController];
    }
}


@end
