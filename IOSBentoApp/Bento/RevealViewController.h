//
//  RevealViewController.h
//  Bento
//
//  Created by Do Nhat Phong on 10/11/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PKRevealController/PKRevealController.h>


@interface RevealViewController : PKRevealController<PKRevealing>

@end
