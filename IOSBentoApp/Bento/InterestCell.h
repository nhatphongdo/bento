//
//  InterestCell.h
//  Bento
//
//  Created by Do Nhat Phong on 10/15/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InterestCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *collapseButton;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *countLabel;

@end
