//
//  RepwdViewController.m
//  Bento
//
//  Created by VO HONG HAI on 10/30/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "RepwdViewController.h"
#import "Constants.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import "ProfileModel.h"

@interface RepwdViewController ()

@end

@implementation RepwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if(![self.email isEqualToString:@""] && self.email != nil)
    {
        self.emailLabel.text = self.email;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginTouched:(id)sender {
    if([self.pwdTextField.text isEqualToString:@""] || self.pwdTextField.text == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APPLICATION_TITLE
                                                        message:@"Password cannot be empty"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK", nil];
        [alert show];
        alert = nil;
        
        [self.pwdTextField becomeFirstResponder];
        return;
    }
    
    if(![self.email isEqualToString:@""] && self.email != nil)
    {
        NSString *md5Password = [ClientUtils encryptMd5:self.pwdTextField.text];
        [self signIn:self.email md5Password:md5Password];
    }
}

- (void)signIn:(NSString *)username md5Password:(NSString *)md5Password {
    [ServiceClient signIn:self.view username:username md5Password:md5Password successBlock:^(id responseObject) {
        // Login succeeded
        [ClientUtils saveSetting:USERNAME_KEY value:username];
        [ClientUtils saveSetting:MD5_PASSWORD_KEY value:md5Password];
        [self dismissViewControllerAnimated:YES completion:nil];
    } failureBlock:^(NSError *error) {
    }];
}

- (IBAction)closeTouched:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
