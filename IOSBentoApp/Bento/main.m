//
//  main.m
//  Bento
//
//  Created by VO HONG HAI on 9/8/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
