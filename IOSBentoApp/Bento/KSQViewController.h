//
//  KSQViewController.h
//  Bento
//
//  Created by VO HONG HAI on 9/18/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KSQViewController : UIViewController
@property (nonatomic,strong) IBOutlet UITableView *tableView;
@property (nonatomic,strong) IBOutlet UIButton *btTextLink;
@property (nonatomic,strong) IBOutlet UIView *borderView;
@end
