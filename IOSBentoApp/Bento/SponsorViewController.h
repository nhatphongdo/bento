//
//  SponsorViewController.h
//  Bento
//
//  Created by VO HONG HAI on 10/15/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRRefreshView.h"

@interface SponsorViewController : UIViewController<SRRefreshDelegate>{
    SRRefreshView *pullToRefreshControl;
}

@property (nonatomic,strong) IBOutlet UITableView *tableView;

@end
