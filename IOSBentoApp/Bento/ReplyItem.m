//
//  ReplyItem.m
//  Bento
//
//  Created by Do Nhat Phong on 9/29/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "ReplyItem.h"
#import "Constants.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import <AFNetworking/UIImageView+AFNetworking.h>


@interface ReplyItem ()

@end

@implementation ReplyItem

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.avatarImageView.layer.cornerRadius = IMAGE_ROUND_CORNER_RADIUS;
    self.avatarImageView.clipsToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)likeButtonTouched:(id)sender {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    if (self.liked == YES) {
        // Unlike
        self.liked = NO;
        [ServiceClient unlikePost:self.containerViewController.view username:username md5Password:password postID:self.replyID successBlock:^(id responseObject) {
            // UnLike article succeeded
            [self.likeButton setTitle:[NSString stringWithFormat:@"%@", responseObject] forState:UIControlStateNormal];
            [self.likeButton setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
        } failureBlock:^(NSError *error) {
        }];
    }
    else {
        // Like
        self.liked = YES;
        [ServiceClient likePost:self.containerViewController.view username:username md5Password:password postID:self.replyID successBlock:^(id responseObject) {
            // Like article succeeded
            [self.likeButton setTitle:[NSString stringWithFormat:@"%@", responseObject] forState:UIControlStateNormal];
            [self.likeButton setImage:[UIImage imageNamed:@"like_hover"] forState:UIControlStateNormal];
        } failureBlock:^(NSError *error) {
        }];
    }
}

- (void)setReply:(CommentModel *)reply {
    self.replyID = reply.ID;
    [self.avatarImageView setUrl:reply.Avatar];
    self.nameLabel.text = reply.Fullname;
    self.contentLabel.text = reply.Message;
    [self.contentLabel sizeToFit];
    CGRect commentFrame = self.contentLabel.frame;
    commentFrame.size.width = self.contentView.frame.size.width - 10;
    self.contentLabel.frame = commentFrame;
    [self.likeButton setTitle:[NSString stringWithFormat:@"%d", reply.Likes] forState:UIControlStateNormal];
    self.liked = reply.Liked;
    if (reply.Liked == YES) {
        [self.likeButton setImage:[UIImage imageNamed:@"like_hover"] forState:UIControlStateNormal];
    }
    else {
        [self.likeButton setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
    }

    CGRect commentViewFrame = self.contentView.frame;
    commentViewFrame.size.height = commentFrame.size.height + 10;
    self.contentView.frame = commentViewFrame;
    
    float y = self.contentView.frame.origin.y + self.contentView.frame.size.height + 8; // 8 is gap
    CGRect likeButtonFrame = self.likeButton.frame;
    likeButtonFrame.origin.y = y;
    self.likeButton.frame = likeButtonFrame;
    
    y += likeButtonFrame.size.height + 8;
    CGRect frame = self.view.frame;
    frame.size.height = y;
    self.view.frame = frame;
}

@end
