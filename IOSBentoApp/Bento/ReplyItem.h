//
//  ReplyItem.h
//  Bento
//
//  Created by Do Nhat Phong on 9/29/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentModel.h"
#import "CommentViewController.h"


@interface ReplyItem : UIViewController

@property (strong, nonatomic) CommentViewController *containerViewController;

@property (strong, nonatomic) NSString *replyID;
@property (nonatomic) BOOL liked;

@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *contentLabel;
@property (strong, nonatomic) IBOutlet UIButton *likeButton;
@property (strong, nonatomic) IBOutlet UIView *contentView;

- (IBAction)likeButtonTouched:(id)sender;
- (void)setReply:(CommentModel *)reply;

@end
