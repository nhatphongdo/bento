//
//  NoteModel.h
//  Bento
//
//  Created by Do Nhat Phong on 10/14/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NoteModel : NSObject

@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) NSString *Message;
@property (strong, nonatomic) NSString *ProfileID;
@property (strong, nonatomic) NSString *Username;
@property (strong, nonatomic) NSString *Fullname;
@property (strong, nonatomic) NSString *Avatar;
@property (strong, nonatomic) NSDate *DateCreated;

@end
