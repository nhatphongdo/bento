//
//  SponsorViewController.m
//  Bento
//
//  Created by VO HONG HAI on 10/15/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "SponsorViewController.h"
#import "ClientUtils.h"
#import "ServiceClient.h"
#import "SponsorItem.h"
#import "SponsorModel.h"
#import "Constants.h"
#import "SponsorDetailViewController.h"

@interface SponsorViewController ()
{
    NSMutableArray *sponsors;
}

@end

@implementation SponsorViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    [self loadSponsors];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)loadSponsors
{
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];

    [ServiceClient getSponsors:self.view username:username md5Password:password successBlock:^(id responseObject) {
        sponsors = responseObject;
        [self.tableView reloadData];
    } failureBlock:^(NSError *error) {
        
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [sponsors count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"SponsorCell";
    SponsorItem *cell = (SponsorItem *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SponsorItem" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    SponsorModel *item = [sponsors objectAtIndex:indexPath.row];

    cell.hiddenIDSponsor.text = item.ID;
    cell.titleLabel.text = item.Name;
    cell.descriptionLabel.text = item.ShortDescription;
    cell.webaddressLabel.text = item.WebAddress;
    [cell.thumbnailImage setUrl:item.Logo];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"detailsponsorsegue" sender:indexPath];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"detailsponsorsegue"])
    {
        NSIndexPath *indexPath = (NSIndexPath *)sender;
        SponsorDetailViewController *sponsorView = (SponsorDetailViewController *)segue.destinationViewController;
        SponsorModel *item = [sponsors objectAtIndex:indexPath.row];
        sponsorView.SponsorID = item.ID;
    }
}

@end
