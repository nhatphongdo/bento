//
//  FavoriteViewController.h
//  Bento
//
//  Created by VO HONG HAI on 9/18/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRRefreshView.h"

@interface FavoriteViewController : UIViewController<UIGestureRecognizerDelegate,SRRefreshDelegate> {
    NSArray *interests;
    NSMutableArray *appearInterests;
    SRRefreshView *pullToRefreshControl;
}

@property (nonatomic,strong) IBOutlet UITableView *tableView;
@property (nonatomic,strong) IBOutlet UIButton *searchButton;

- (IBAction)searchViewTouched:(id)sender;

@end
