//
//  CustomProfileViewController.m
//  Bento
//
//  Created by VO HONG HAI on 9/29/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "CustomProfileViewController.h"
#import "Constants.h"
#import "ClientUtils.h"
#import "ProfileModel.h"
#import "ServiceClient.h"
#import "InterestModel.h"
#import <AFNetworking/UIImageView+AFNetworking.h>


@implementation CustomProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 0)];
    self.FullNameTextField.leftView = paddingView;
    self.FullNameTextField.leftViewMode = UITextFieldViewModeAlways;

    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 0)];
    self.BirthDayTextField.leftView = paddingView;
    self.BirthDayTextField.leftViewMode = UITextFieldViewModeAlways;

    self.FullNameTextField.text = [ClientUtils loadSetting:FULLNAME_KEY];
    [self.ThumbnailImage setImageWithURL:[NSURL URLWithString:[ClientUtils loadSetting:AVATAR_KEY]] placeholderImage:[UIImage imageNamed:@"no_image"]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MM/dd/yyyy";
    self.BirthDayTextField.text = [dateFormatter stringFromDate:(NSDate *)[ClientUtils loadSetting:DATE_OF_BIRTH_KEY]];
    NSLog(@"Day Store On Device: %@",[ClientUtils loadSetting:DATE_OF_BIRTH_KEY]);
    //NSLog(@"Visible Date: %@",[dateFormatter stringFromDate:(NSDate *)[ClientUtils loadSetting:DATE_OF_BIRTH_KEY]]);
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    [datePicker setDate:[NSDate date]];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    [self.BirthDayTextField setInputView:datePicker];
    
    if([(NSNumber *)[ClientUtils loadSetting:GENDER_KEY] intValue] == 2)
    {
        [self.GenderButton setTitle:@"Female" forState:UIControlStateNormal];
    }
    else
    {
        [self.GenderButton setTitle:@"Male" forState:UIControlStateNormal];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // Reload table
    [self loadUserInterests];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadUserInterests {
    
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    [ServiceClient getUserInterests:self.view username:username md5Password:password successBlock:^(id responseObject) {
        // Load interest succeeded
        interests = responseObject;
        [self.interestsTableView reloadData];
    } failureBlock:^(NSError *error) {
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [interests count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"InterestCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    InterestModel *item = [interests objectAtIndex:indexPath.row];
    cell.textLabel.text = item.Name;
    cell.tag = indexPath.row;
    
    return cell;
}

- (IBAction)updateProfile:(id)sender{
    
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];

    if ([self.FullNameTextField.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Name cannot be empty"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        [self.FullNameTextField becomeFirstResponder];
        
        return;
    }
    
    if ([self.BirthDayTextField.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Day Of Birth cannot be empty"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        [self.BirthDayTextField becomeFirstResponder];
        
        return;
    }
    
    if ([self.GenderButton.titleLabel.text.lowercaseString isEqualToString:@"select gender"]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Gender cannot be empty"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        [self.GenderButton becomeFirstResponder];
    
        return;
    }
    
    [self.view endEditing:YES];
    NSString *fullname = self.FullNameTextField.text;
    NSString *birthday   = self.BirthDayTextField.text;
    NSString *gender = [[NSString alloc] init];
    if ([[self.GenderButton.titleLabel.text lowercaseString] isEqualToString:@"male"])
    {
        gender = @"1";
    }
    else
    {
        gender = @"2";
    }
    NSString *email = [ClientUtils loadSetting:EMAIL_KEY];
        
    [ServiceClient updateProfile:self.view username:username md5Password:password fullname:fullname gender:gender birthday:birthday email:email successBlock:^(id responseObject) {
        [ClientUtils saveSetting:GENDER_KEY value:gender];
        [ClientUtils saveSetting:DATE_OF_BIRTH_KEY value:birthday];
        [ClientUtils saveSetting:FULLNAME_KEY value:fullname];
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Update profile information successfully"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
    } failureBlock:^(NSError *error) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Update profile information fail"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
    }];

}

- (IBAction)selectGender:(id)sender{
    NSArray *arr = [NSArray arrayWithObjects:@"Female", @"Male", nil];
    if (dropDown == nil) {
        CGFloat f = 80;
        dropDown = [[NIDropDown alloc] showDropDown:sender :&f :arr :nil :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        dropDown = nil;
    }
}

- (void)niDropDownDelegateMethod:(NIDropDown *)sender {
    dropDown = nil;
}

-(void)updateTextField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker *)self.BirthDayTextField.inputView;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSString *dateString = [dateFormat stringFromDate:picker.date];
    self.BirthDayTextField.text = [NSString stringWithFormat:@"%@",dateString];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

@end
