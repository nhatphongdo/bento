//
//  ReportViewController.m
//  Bento
//
//  Created by VO HONG HAI on 11/17/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "ReportViewController.h"
#import "Constants.h"
#import "ClientUtils.h" 
#import "ServiceClient.h"

@interface ReportViewController ()

@end

@implementation ReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.cbContent.text = @"Content";
    self.cbTechnical.text = @"Technical";
    self.cbOther.text = @"Other";
    [self.message.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [self.message.layer setBorderWidth:1.0f];
    self.message.layer.cornerRadius = 5;
    self.message.clipsToBounds = YES;
    self.cbContent.checked = YES;
    type = @"1";
    self.message.editable = YES;
    self.message.delegate = self;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sendTouched:(id)sender {
    
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *md5Password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    if([self.message.text isEqualToString:@""] || self.message.text == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please input text" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        alert = nil;
        [self.message  becomeFirstResponder];
        return;
    }
    
    [ServiceClient reportsArticle:self.view username:username md5Password:md5Password postID:self.articleID message:self.message.text type:type successBlock:^(id responseObject) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APPLICATION_TITLE message:@"Your report has been send." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        alert = nil;
        //[self dismissViewControllerAnimated:YES completion:nil];
        
    } failureBlock:^(NSError *error) {
    }];
}

- (IBAction)selectcbContent:(id)sender {
    UICheckbox *cb = (UICheckbox *)sender;
    if(cb.checked == YES)
    {
        type=@"1";
        self.cbTechnical.checked = NO;
        self.cbOther.checked = NO;
    }
}

- (IBAction)selectcbTechnical:(id)sender {
    UICheckbox *cb = (UICheckbox *)sender;
    if(cb.checked == YES)
    {
        type=@"2";
        self.cbContent.checked = NO;
        self.cbOther.checked = NO;
    }
}

- (IBAction)selectcbOther:(id)sender {
    UICheckbox *cb = (UICheckbox *)sender;
    if(cb.checked == YES)
    {
        type=@"3";
        self.cbTechnical.checked = NO;
        self.cbContent.checked = NO;
    }
}

- (IBAction)closeTouched:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text
{
    
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        
        return FALSE;
    }
    return TRUE;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
