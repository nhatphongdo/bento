//
//  LoginViewController.h
//  Bento
//
//  Created by Do Nhat Phong on 9/25/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GooglePlus/GooglePlus.h>
#import <FacebookSDK/FacebookSDK.h>

@class GPPSignInButton;

@interface LoginViewController : UIViewController<UIGestureRecognizerDelegate, GPPSignInDelegate, FBLoginViewDelegate>
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) IBOutlet UIButton *signUpButton;
@property (strong, nonatomic) IBOutlet UIButton *loginFBButton;

@property (weak, nonatomic) IBOutlet GPPSignInButton *loginGGButton;

- (IBAction)loginButtonTouched:(id)sender;

@end
