//
//  AddNoteViewController.h
//  Bento
//
//  Created by Do Nhat Phong on 9/24/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIPlaceholderTextView.h"


@interface AddNoteViewController : UIViewController {
    BOOL _isVisibleKeyboard;
}

@property (strong, nonatomic) NSString *articleID;
@property (strong, nonatomic) UIButton *sender;

@property (strong, nonatomic) NSMutableArray *notes;

@property (strong, nonatomic) IBOutlet UIPlaceholderTextView *noteTextView;
@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong, nonatomic) IBOutlet UIView *noteView;
@property (strong, nonatomic) IBOutlet UIButton *hideButton;
@property (strong, nonatomic) IBOutlet UIScrollView *notesScrollView;

- (IBAction)saveButtonTouched:(id)sender;

- (void)setBackground:(UIImage *)image;
- (void)showKeyboard:(double)time height:(float)height;
- (void)hideKeyboard:(double)time height:(float)height;

- (void)loadSavedNotes;
- (void)reloadNotesView;

@end
