//
//  HomeLeftMenuViewController.m
//  Bento
//
//  Created by Do Nhat Phong on 10/11/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "HomeLeftMenuViewController.h"
#import <PKRevealController/PKRevealController.h>
#import "ClientUtils.h"
#import "ServiceClient.h"
#import "Constants.h"
#import "InterestCell.h"
#import "InterestModel.h"
#import "FeedViewController.h"


@implementation HomeLeftMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Remove white space before seperator
    if ([self.interestsTableView respondsToSelector:@selector(setSeparatorInset:)]) {  // Safety check for below iOS 7
        [self.interestsTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    [self loadInterests];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)loadInterests {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    [ServiceClient getStructureUserInterests:self.view username:username md5Password:password successBlock:^(id responseObject) {
        interests = responseObject;
        
        appearInterests = [[NSMutableArray alloc] init];
        for (uint i = 0; i < [interests count]; i++) {
            InterestModel *item = [interests objectAtIndex:i];
            if ([item.ParentID isEqualToString:@""]) {
                InterestModel *newItem = [[InterestModel alloc] init];
                newItem.ID = item.ID;
                newItem.Name = item.Name;
                newItem.ParentID = item.ParentID;
                newItem.Thumbnail = item.Thumbnail;
                newItem.Level = item.Level;
                newItem.Total = item.Total;
                newItem.isCollapsed = YES;
                [appearInterests addObject:newItem];
            }
        }
        
        [self.interestsTableView reloadData];
    } failureBlock:^(NSError *error) {
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Count
    return [appearInterests count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"interestCell";
    InterestCell *cell = (InterestCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];

    // Configure the cell...
    if (cell == nil) {
        cell = [[InterestCell alloc] init];
    }

    InterestModel *item = [appearInterests objectAtIndex:indexPath.row];
    if (item.isCollapsed == YES) {
        [cell.collapseButton setImage:[UIImage imageNamed:@"iconArrowLeft"] forState:UIControlStateNormal];
    }
    else {
        [cell.collapseButton setImage:[UIImage imageNamed:@"iconArrowDown"] forState:UIControlStateNormal];
    }
    cell.collapseButton.tag = indexPath.row;
    
    [cell.collapseButton addTarget:self action:@selector(openCollapse:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.nameLabel.text = item.Name;
    CGRect frame = cell.nameLabel.frame;
    frame.origin.x = cell.collapseButton.frame.origin.x + cell.collapseButton.frame.size.width + 10 * (item.Level - 1);
    frame.size.width = cell.frame.size.width - frame.origin.x - 10 - cell.countLabel.frame.size.width;
    cell.nameLabel.frame = frame;
    
    cell.countLabel.text = [NSString stringWithFormat:@"%ld", item.Total];
    CGRect frameCount = cell.countLabel.frame;
    frameCount.origin.x = frame.origin.x + frame.size.width + 2;
    cell.countLabel.frame = frameCount; 
    
    if (item.Level > 1) {
        [cell.collapseButton setHidden:YES];
    }
    else {
        [cell.collapseButton setHidden:NO];
    }
    
    FeedViewController *feedViewController = (FeedViewController *)self.revealController.frontViewController;
    if ([item.ID isEqualToString:feedViewController.interestID]) {
        cell.contentView.backgroundColor = [UIColor colorWithRed:58/255.0 green:194/255.0 blue:207/255.0 alpha:1.0];
    }
    else {
        cell.contentView.backgroundColor = [UIColor clearColor];
    }
    
    cell.tag = indexPath.row;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self
                                          action:@selector(handleViewTap:)];
    tapGesture.delegate = self;
    [cell addGestureRecognizer:tapGesture];
    tapGesture = nil;

    return cell;
}

- (void)handleViewTap:(UITapGestureRecognizer *)recognizer {
    // Load articles of interesst
    InterestModel *interest = [appearInterests objectAtIndex:recognizer.view.tag];
    FeedViewController *feedViewController = (FeedViewController *)self.revealController.frontViewController;
    feedViewController.currentPage = 1;
    feedViewController.interestID = interest.ID;
    feedViewController.loadForUserOnly = NO;
    feedViewController.interestName = interest.Name;

    [feedViewController loadArticleOfInterest];
    [self.revealController showViewController:self.revealController.frontViewController];
    
    [self.interestsTableView reloadData];
}

- (void)openCollapse:(id)sender {
    // Change collapse
    UIButton *button = (UIButton *)sender;
    InterestModel *item = [appearInterests objectAtIndex:button.tag];
    
    if (item.isCollapsed == YES) {
        uint idx = button.tag + 1;
        for (uint i = 0; i < [interests count]; i++) {
            InterestModel *childItem = [interests objectAtIndex:i];
            if ([childItem.ParentID isEqualToString:item.ID]) {
                InterestModel *newItem = [[InterestModel alloc] init];
                newItem.ID = childItem.ID;
                newItem.Name = childItem.Name;
                newItem.ParentID = childItem.ParentID;
                newItem.Thumbnail = childItem.Thumbnail;
                newItem.Level = childItem.Level;
                newItem.Total = childItem.Total;
                newItem.isCollapsed = YES;
                [appearInterests insertObject:newItem atIndex:idx];
                ++idx;
            }
        }
        item.isCollapsed = NO;
    }
    else {
        // Remove appeared items
        for (int i = [appearInterests count] - 1; i >= 0; i--) {
            InterestModel *childItem = [appearInterests objectAtIndex:i];
            if ([childItem.ParentID isEqualToString:item.ID]) {
                [appearInterests removeObjectAtIndex:i];
            }
        }
        item.isCollapsed = YES;
    }
    
    [self.interestsTableView reloadData];
}

- (IBAction)closeTouched:(id)sender {
    [self.revealController showViewController:self.revealController.frontViewController];
}

@end
