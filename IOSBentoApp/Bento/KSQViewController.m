//
//  KSQViewController.m
//  Bento
//
//  Created by VO HONG HAI on 9/18/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "KSQViewController.h"


@interface KSQViewController ()

@end

@implementation KSQViewController
{
    NSArray *menu;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    menu = [NSArray arrayWithObjects:@"About Us", @"Our Services", @"Our Portfolio", @"Contact Us", nil];
   
    [self.tableView setSeparatorColor:[UIColor clearColor]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
   
    //config border of button text on bottom
    self.borderView.layer.borderWidth = 1.0f;
    self.borderView.layer.borderColor = [UIColor colorWithRed:51.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0f].CGColor;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
