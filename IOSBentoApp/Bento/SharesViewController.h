//
//  SharesViewController.h
//  Bento
//
//  Created by Do Nhat Phong on 10/1/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "AppDelegate.h"
#define APP ((AppDelegate *)[[UIApplication sharedApplication] delegate])


@interface SharesViewController : UIViewController<MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>

@property (strong, nonatomic) UIButton *sender;
@property (strong, nonatomic) NSString *postID;

- (IBAction)facebookTouched:(id)sender;
- (IBAction)googlePlusTouched:(id)sender;
- (IBAction)pinterestTouched:(id)sender;
- (IBAction)twitterTouched:(id)sender;
- (IBAction)tumblrTouched:(id)sender;
- (IBAction)smsTouched:(id)sender;
- (IBAction)emailTouched:(id)sender;
- (IBAction)copyTouched:(id)sender;
- (IBAction)downloadTouched:(id)sender;
- (IBAction)closeTouched:(id)sender;

@end
