//
//  DetailArticleViewController.h
//  Bento
//
//  Created by Do Nhat Phong on 10/12/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DetailArticleViewController : UIViewController<UIWebViewDelegate, UIScrollViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *titleBarView;
@property (strong, nonatomic) IBOutlet UIButton *hideButton;
@property (strong, nonatomic) IBOutlet UIButton *commentButton;
@property (strong, nonatomic) IBOutlet UIButton *noteButton;
@property (strong, nonatomic) IBOutlet UIButton *shareButton;
@property (strong, nonatomic) IBOutlet UIButton *likeButton;
@property (strong, nonatomic) IBOutlet UIButton *exclamationButton;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *authorLabel;
@property (strong, nonatomic) IBOutlet UIWebView *contentWebview;
@property (strong, nonatomic) IBOutlet UIScrollView *previewView;
@property (strong, nonatomic) IBOutlet UIImageView *previewImageView;
@property (strong, nonatomic) IBOutlet UIButton *hidePreviewButton;
@property (nonatomic) BOOL isFavorite;

- (IBAction)hidePreviewTouched:(id)sender;

- (void)setTitle:(NSString *)text;
- (void)setBody:(NSString *)body;

@end
