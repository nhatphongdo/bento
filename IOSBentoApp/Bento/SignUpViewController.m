//
//  SignUpViewController.m
//  Bento
//
//  Created by VO HONG HAI on 10/14/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "SignUpViewController.h"
#import "Constants.h"
#import "ServiceClient.h"
#import "ClientUtils.h"
#import "LoginViewController.h"

@interface SignUpViewController ()

@property (nonatomic,strong) IBOutlet UITextField *name;
@property (nonatomic,strong) IBOutlet UITextField *email;
@property (nonatomic,strong) IBOutlet UITextField *birthday;
@property (nonatomic,strong) IBOutlet UITextField *pwd;
@property (nonatomic,strong) IBOutlet UITextField *confirmpwd;
@property (nonatomic,strong) IBOutlet UIButton    *btSignUp;

-(IBAction)SignUpTouch:(id)sender;

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker setDate:[NSDate date]];
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    [self.birthday setInputView:datePicker];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateTextField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)self.birthday.inputView;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSString *newDateString = [dateFormatter stringFromDate:picker.date];
    self.birthday.text = [NSString stringWithFormat:@"%@",newDateString];
}

- (IBAction)SignUpTouch:(id)sender
{
    if ([self.name.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Name cannot be empty"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        [self.name becomeFirstResponder];
        
        return;
    }
    
    if ([self.birthday.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Day of Birth cannot be empty"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        [self.birthday becomeFirstResponder];
        
        return;
    }
    
    if ([self.email.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Email address cannot be empty"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        [self.email becomeFirstResponder];
        
        return;
    }
    
    if ([self.pwd.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Password cannot be empty"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        [self.pwd becomeFirstResponder];
        
        return;
    }
    
    if ([self.confirmpwd.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Confirm Password cannot be empty"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        [self.confirmpwd becomeFirstResponder];
        
        return;
    }
    
    if (![self.confirmpwd.text isEqualToString:self.pwd.text]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Confirm Password not equal password"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        [self.confirmpwd becomeFirstResponder];
        
        return;
    }
    
    [self.view endEditing:YES];
    [self.btSignUp setEnabled:NO];
    
    [ServiceClient signUp:self.view username:self.email.text password:self.pwd.text confirmpassword:self.confirmpwd.text fullname:self.name.text birthday:self.birthday.text accountType:@"Normal" accountID:@"" successBlock:^(id responseObject) {
        // Sign up succeeded
        NSString *md5Password = [ClientUtils encryptMd5:self.pwd.text];
        [ClientUtils saveSetting:USERNAME_KEY value:self.email.text];
        [ClientUtils saveSetting:MD5_PASSWORD_KEY value:md5Password];
        [self.btSignUp setEnabled:YES];
        LoginViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
        [self presentModalViewController:vc animated:YES];
        
    } failureBlock:^(NSError *error) {
        [self.btSignUp setEnabled:YES];
    }];

}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)keyboardWillHide:(NSNotification *)aNotification
{
    // Get the size of the keyboard.
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];
}

- (void)keyboardWillShow:(NSNotification *)aNotification
{
    // Get the size of the keyboard.
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect frame = self.view.frame;
    if (self.view.frame.size.height - kbSize.height < 370) {
        frame.origin.y = -150;
    }
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];
}


-(IBAction)backTouch:(id)sender{
    LoginViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
    [self presentModalViewController:vc animated:YES];
}


- (IBAction)selectGender:(id)sender{
    NSArray *arr = [NSArray arrayWithObjects:@"Female", @"Male", nil];
    if (dropDown == nil) {
        CGFloat f = 80;
        dropDown = [[NIDropDown alloc] showDropDown:sender :&f :arr :nil :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        dropDown = nil;
    }
}

- (void)niDropDownDelegateMethod:(NIDropDown *)sender {
    dropDown = nil;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
