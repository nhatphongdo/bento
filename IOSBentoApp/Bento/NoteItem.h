//
//  NoteItemViewController.h
//  Bento
//
//  Created by Do Nhat Phong on 10/14/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NoteModel.h"
#import "AddNoteViewController.h"


@interface NoteItem : UIViewController

@property (strong, nonatomic) AddNoteViewController *containerViewController;
@property (strong, nonatomic) NSString *noteID;

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *contentLabel;

- (IBAction)deleteTouched:(id)sender;

- (void)setNote:(NoteModel *)note;

@end
