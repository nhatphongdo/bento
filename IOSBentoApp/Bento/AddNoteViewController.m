//
//  AddNoteViewController.m
//  Bento
//
//  Created by Do Nhat Phong on 9/24/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "AddNoteViewController.h"
#import "UIImage+StackBlur.h"
#import "Constants.h"
#import "ServiceClient.h"
#import "ClientUtils.h"
#import "NoteItem.h"
#import "NoteModel.h"


@implementation AddNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.noteTextView.placeholder = @"Write your note";
    
    self.avatarImageView.layer.cornerRadius = IMAGE_ROUND_CORNER_RADIUS;
    self.avatarImageView.clipsToBounds = YES;
    
    [self.avatarImageView setUrl:[ClientUtils loadSetting:AVATAR_KEY]];
    
    _isVisibleKeyboard = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void)loadSavedNotes {
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    [ServiceClient getNotesOfArticle:self.view username:username md5Password:password postID:self.articleID page:1 itemsPerPage:10 successBlock:^(id responseObject) {
        // Get notes succeeded
        self.notes = responseObject;

        [self reloadNotesView];
    } failureBlock:^(NSError *error) {
    }];
}

- (void)reloadNotesView {
    [[self.notesScrollView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    float y = 0;
    for (uint i = 0; i < [self.notes count]; i++) {
        NoteModel *item = [self.notes objectAtIndex:i];
        NoteItem *noteView = [[NoteItem alloc] initWithNibName:@"NoteItem" bundle:nil];
        noteView.containerViewController = self;
        [self addChildViewController:noteView];
        [self.notesScrollView addSubview:noteView.view];
        [noteView didMoveToParentViewController:self];
        [noteView setNote:item];
        noteView.view.frame = CGRectMake(0, y, self.notesScrollView.frame.size.width, noteView.view.frame.size.height);
        y += noteView.view.frame.size.height;
    }
    
    self.notesScrollView.contentSize = CGSizeMake(self.notesScrollView.frame.size.width, y);
}

- (IBAction)saveButtonTouched:(id)sender {
    if ([self.noteTextView.text isEqualToString:@""]) {
        return;
    }
    
    NSString *username = [ClientUtils loadSetting:USERNAME_KEY];
    NSString *password = [ClientUtils loadSetting:MD5_PASSWORD_KEY];
    
    [ServiceClient postNoteForArticle:self.view username:username md5Password:password postID:self.articleID message:self.noteTextView.text successBlock:^(id responseObject) {
        // Post note succeeded
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:APPLICATION_TITLE
                                                       message:@"Your note is saved successfully"
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
        alert = nil;
        
        [self.view setHidden:YES];
        [self.view endEditing:YES];
        
        if (self.sender != nil) {
            [self.sender setSelected:NO];
        }
    } failureBlock:^(NSError *error) {
    }];
}

- (void)setBackground:(UIImage *)image {
    [self.backgroundImageView setImage:[image stackBlur:5]];
}

- (void)showKeyboard:(double)time height:(float)height {
    if (_isVisibleKeyboard == YES) {
        return;
    }
    
    NSTimeInterval animationDuration = time;
    CGRect frame = self.noteView.frame;
    frame.origin.y -= height;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.noteView.frame = frame;
    [UIView commitAnimations];
    _isVisibleKeyboard = YES;
}

- (void)hideKeyboard:(double)time height:(float)height {
    if (_isVisibleKeyboard == NO) {
        return;
    }
    
    NSTimeInterval animationDuration = time;
    CGRect frame = self.noteView.frame;
    frame.origin.y += height;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.noteView.frame = frame;
    [UIView commitAnimations];
    _isVisibleKeyboard = NO;
}

@end
