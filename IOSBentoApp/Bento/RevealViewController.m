//
//  RevealViewController.m
//  Bento
//
//  Created by Do Nhat Phong on 10/11/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import "RevealViewController.h"

@implementation RevealViewController

const float size = 200.0;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString *storyboardName = @"Bento";
    
    UIViewController *leftViewController = [[UIStoryboard storyboardWithName:storyboardName bundle:nil] instantiateViewControllerWithIdentifier:@"leftMenuView"];
    UIViewController *rightViewController = [[UIStoryboard storyboardWithName:storyboardName bundle:nil] instantiateViewControllerWithIdentifier:@"rightMenuView"];
    UIViewController *frontViewController = [[UIStoryboard storyboardWithName:storyboardName bundle:nil] instantiateViewControllerWithIdentifier:@"feedView"];
    
    [self setFrontViewController:frontViewController];
    [self setLeftViewController:leftViewController];
    [self setRightViewController:rightViewController];
    
    self.recognizesPanningOnFrontView = NO;
    self.recognizesResetTapOnFrontView = NO;

    self.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)revealController:(PKRevealController *)revealController didChangeToState:(PKRevealControllerState)state {
    [revealController setMinimumWidth:size maximumWidth:size forViewController:revealController.focusedController];
    if (revealController.focusedController == revealController.leftViewController) {
        [revealController.focusedController.view setFrame:CGRectMake(0, 0, size, self.view.frame.size.height)];
    }
    else if (revealController.focusedController == revealController.rightViewController) {
        [revealController.focusedController.view setFrame:CGRectMake(self.view.frame.size.width - size, 0, size, self.view.frame.size.height)];
    }
}

@end
