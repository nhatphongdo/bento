//
//  CommentViewController.h
//  Bento
//
//  Created by VO HONG HAI on 9/19/14.
//  Copyright (c) 2014 HAIVO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIPlaceholderTextView.h"


@interface CommentViewController : UIViewController {
    NSArray *comments;
    BOOL _isVisibleKeyboard;
}

@property (strong, nonatomic) UIButton *sender;
@property (strong, nonatomic) NSString *postID;

@property (strong, nonatomic) IBOutlet UIScrollView *commentsScrollView;
@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UIPlaceholderTextView *commentTextView;

- (IBAction)closeButtonTouched:(id)sender;
- (IBAction)sendCommentTouched:(id)sender;

- (void)loadComments;
- (void)refreshCommentsView;

- (void)showKeyboard:(double)time height:(float)height;
- (void)hideKeyboard:(double)time height:(float)height;

@end
