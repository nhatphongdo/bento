
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AFNetworking
#define COCOAPODS_POD_AVAILABLE_AFNetworking
#define COCOAPODS_VERSION_MAJOR_AFNetworking 2
#define COCOAPODS_VERSION_MINOR_AFNetworking 2
#define COCOAPODS_VERSION_PATCH_AFNetworking 4

// AFNetworking/NSURLConnection
#define COCOAPODS_POD_AVAILABLE_AFNetworking_NSURLConnection
#define COCOAPODS_VERSION_MAJOR_AFNetworking_NSURLConnection 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_NSURLConnection 2
#define COCOAPODS_VERSION_PATCH_AFNetworking_NSURLConnection 4

// AFNetworking/NSURLSession
#define COCOAPODS_POD_AVAILABLE_AFNetworking_NSURLSession
#define COCOAPODS_VERSION_MAJOR_AFNetworking_NSURLSession 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_NSURLSession 2
#define COCOAPODS_VERSION_PATCH_AFNetworking_NSURLSession 4

// AFNetworking/Reachability
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Reachability
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Reachability 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Reachability 2
#define COCOAPODS_VERSION_PATCH_AFNetworking_Reachability 4

// AFNetworking/Security
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Security
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Security 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Security 2
#define COCOAPODS_VERSION_PATCH_AFNetworking_Security 4

// AFNetworking/Serialization
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Serialization
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Serialization 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Serialization 2
#define COCOAPODS_VERSION_PATCH_AFNetworking_Serialization 4

// AFNetworking/UIKit
#define COCOAPODS_POD_AVAILABLE_AFNetworking_UIKit
#define COCOAPODS_VERSION_MAJOR_AFNetworking_UIKit 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_UIKit 2
#define COCOAPODS_VERSION_PATCH_AFNetworking_UIKit 4

// AGAssert
#define COCOAPODS_POD_AVAILABLE_AGAssert
#define COCOAPODS_VERSION_MAJOR_AGAssert 0
#define COCOAPODS_VERSION_MINOR_AGAssert 2
#define COCOAPODS_VERSION_PATCH_AGAssert 0

// AGGeometryKit
#define COCOAPODS_POD_AVAILABLE_AGGeometryKit
#define COCOAPODS_VERSION_MAJOR_AGGeometryKit 1
#define COCOAPODS_VERSION_MINOR_AGGeometryKit 0
#define COCOAPODS_VERSION_PATCH_AGGeometryKit 4

// AGGeometryKit/Core
#define COCOAPODS_POD_AVAILABLE_AGGeometryKit_Core
#define COCOAPODS_VERSION_MAJOR_AGGeometryKit_Core 1
#define COCOAPODS_VERSION_MINOR_AGGeometryKit_Core 0
#define COCOAPODS_VERSION_PATCH_AGGeometryKit_Core 4

// AGGeometryKit/Default
#define COCOAPODS_POD_AVAILABLE_AGGeometryKit_Default
#define COCOAPODS_VERSION_MAJOR_AGGeometryKit_Default 1
#define COCOAPODS_VERSION_MINOR_AGGeometryKit_Default 0
#define COCOAPODS_VERSION_PATCH_AGGeometryKit_Default 4

// AGGeometryKit/Dependencies
#define COCOAPODS_POD_AVAILABLE_AGGeometryKit_Dependencies
#define COCOAPODS_VERSION_MAJOR_AGGeometryKit_Dependencies 1
#define COCOAPODS_VERSION_MINOR_AGGeometryKit_Dependencies 0
#define COCOAPODS_VERSION_PATCH_AGGeometryKit_Dependencies 4

// AGGeometryKit+POP
#define COCOAPODS_POD_AVAILABLE_AGGeometryKit_POP
#define COCOAPODS_VERSION_MAJOR_AGGeometryKit_POP 0
#define COCOAPODS_VERSION_MINOR_AGGeometryKit_POP 1
#define COCOAPODS_VERSION_PATCH_AGGeometryKit_POP 0

// AGGeometryKit+POP/Default
#define COCOAPODS_POD_AVAILABLE_AGGeometryKit_POP_Default
#define COCOAPODS_VERSION_MAJOR_AGGeometryKit_POP_Default 0
#define COCOAPODS_VERSION_MINOR_AGGeometryKit_POP_Default 1
#define COCOAPODS_VERSION_PATCH_AGGeometryKit_POP_Default 0

// JRSwizzle
#define COCOAPODS_POD_AVAILABLE_JRSwizzle
#define COCOAPODS_VERSION_MAJOR_JRSwizzle 1
#define COCOAPODS_VERSION_MINOR_JRSwizzle 0
#define COCOAPODS_VERSION_PATCH_JRSwizzle 0

// JSONKit
#define COCOAPODS_POD_AVAILABLE_JSONKit
// This library does not follow semantic-versioning,
// so we were not able to define version macros.
// Please contact the author.
// Version: 1.5pre.

// MBProgressHUD
#define COCOAPODS_POD_AVAILABLE_MBProgressHUD
#define COCOAPODS_VERSION_MAJOR_MBProgressHUD 0
#define COCOAPODS_VERSION_MINOR_MBProgressHUD 9
#define COCOAPODS_VERSION_PATCH_MBProgressHUD 0

// PKRevealController
#define COCOAPODS_POD_AVAILABLE_PKRevealController
#define COCOAPODS_VERSION_MAJOR_PKRevealController 2
#define COCOAPODS_VERSION_MINOR_PKRevealController 0
#define COCOAPODS_VERSION_PATCH_PKRevealController 6

// Reachability
#define COCOAPODS_POD_AVAILABLE_Reachability
#define COCOAPODS_VERSION_MAJOR_Reachability 3
#define COCOAPODS_VERSION_MINOR_Reachability 1
#define COCOAPODS_VERSION_PATCH_Reachability 1

// pop
#define COCOAPODS_POD_AVAILABLE_pop
#define COCOAPODS_VERSION_MAJOR_pop 1
#define COCOAPODS_VERSION_MINOR_pop 0
#define COCOAPODS_VERSION_PATCH_pop 7

